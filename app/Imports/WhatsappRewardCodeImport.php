<?php

  

namespace App\Imports;

use App\WhatsappModels\RewardCode;
use Maatwebsite\Excel\Concerns\WithStartRow;

use Maatwebsite\Excel\Concerns\ToModel;

  

class WhatsappRewardCodeImport implements ToModel,WithStartRow

{
    protected $id;
    
    
    
    
    function __construct($id) {

            $this->id = $id;
            
           
    }
    public function startRow(): int
    {
        return 2;
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {
        
        return new RewardCode([
            'campaign_id' =>$this->id,
            
            'reward_code'     => $row[0],
            'offer_type' =>$row[1],
            'offer_type_slug' =>$row[2],

        ]);

    }

}