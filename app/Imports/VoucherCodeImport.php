<?php

  

namespace App\Imports;

use App\SmsModels\Voucher;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
  

class VoucherCodeImport implements ToModel,WithStartRow,WithValidation,WithHeadingRow, WithChunkReading

{
    protected $campaign_id;
    protected $batch_name;
    protected $sku_name;
    protected $offer_id;
    protected $start_date;
    protected $end_date;
    protected $voucher_activation_date_over_sms;
    protected $cashback_camp_id;
    
    
    function __construct($campaign_id,$batch_name,$sku_name,$offer_id,$start_date,$end_date,$voucher_activation_date_over_sms,$cashback_camp_id) {

            $this->campaign_id = $campaign_id;
            $this->batch_name = $batch_name;
            $this->sku_name = $sku_name;
            $this->offer_id = $offer_id;
            $this->start_date = $start_date;
            $this->end_date = $end_date;
            $this->voucher_activation_date_over_sms = $voucher_activation_date_over_sms;
            $this->cashback_camp_id = $cashback_camp_id;
           
    }
    public function startRow(): int
    {
        return 2;
    }
    
    public function rules(): array
    {
        return [
            '1' => 'unique:sms_tbl_vouchers,voucher_code'
        ];
    
    }

    public function customValidationMessages()
    {
        return [
            '1.unique' => 'Correo ya esta en uso.',
        ];
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {
        
        
        ini_set('max_execution_time', 0);
        return new Voucher([
            'campaign_id' =>$this->campaign_id,
            'batch_name' =>$this->batch_name,
            'sku_name' =>$this->sku_name,
            'offer_type_id' =>$this->offer_id,
            'start_date' =>$this->start_date,
            'end_date' =>$this->end_date,
            'voucher_activation_date_over_sms' =>$this->voucher_activation_date_over_sms,
            'cashback_camp_id' =>$this->cashback_camp_id,
            'voucher_code'     => $row['voucher_code'],

        ]);

    }

    public function chunkSize(): int
    {
        return 5000;
    }

}