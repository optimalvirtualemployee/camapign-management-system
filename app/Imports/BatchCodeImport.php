<?php

  

namespace App\Imports;

use App\SmsModels\BatchCode;
use Maatwebsite\Excel\Concerns\WithStartRow;

use Maatwebsite\Excel\Concerns\ToModel;

  

class BatchCodeImport implements ToModel,WithStartRow

{
    protected $id;
    
    
    
    
    function __construct($id) {

            $this->id = $id;
            
           
    }
    public function startRow(): int
    {
        return 2;
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {
        
        return new BatchCode([
            'campaign_id' =>$this->id,
            //'offer_type' =>$row[1],
            'batch_code'     => $row[0],
            //'offer_type_slug' =>$row[2],

        ]);

    }

}