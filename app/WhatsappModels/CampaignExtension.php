<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class CampaignExtension extends Model
{
    protected $table = 'sms_tbl_campaignextensions';
    protected $fillable = ['campaign_type_id'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign_type()
    {
        return $this->belongsTo('App\SmsModels\CampaignType','campaign_type_id');
    }

    //Campaign Type  function
    public function longCode()
    {
        return $this->hasMany('App\SmsModels\LongCode','parts','campaign_keyword');
    }
}
