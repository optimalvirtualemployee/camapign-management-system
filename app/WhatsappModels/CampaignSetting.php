<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;

class CampaignSetting extends Model
{
    protected $table = 'whatsapp_tbl_campaign_settings';
    protected $fillable = ['campaign_id','title','contact_no','menu_bar_color'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\WhatsappModels\Campaigns','campaign_id');
    }
}
