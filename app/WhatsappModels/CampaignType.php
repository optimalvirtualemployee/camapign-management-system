<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CampaignType extends Model
{
    
    use UserStampsTrait;
    
    protected $table = 'whatsapp_tbl_campaign_types';
    protected $fillable = ['name','template_id','created_at','updated_at','sms','stats','rules'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    
    //Template  function
    public function template()
    {
        return $this->belongsTo('App\WhatsappModels\Template','template_id','id');
    }
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
    
}
