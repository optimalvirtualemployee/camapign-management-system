<?php

namespace App\WhatsappModels;
use Illuminate\Database\Eloquent\Model;

class cashbackType extends Model
{
    
    protected $table = 'whatsapp_tbl_cashbacktypes';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

}
