<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;

class WhatsAppData extends Model
{
	
    protected $table = 'whatsapp_tbl_whatsapp_data';
    protected $fillable = ['from','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
