<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;


class FieldType extends Model
{
	
    protected $table = 'whatsapp_tbl_field_types';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
