<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    
    protected $table = 'whatsapp_tbl_offers';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];


    //User  function
    public function cashback()
    {
        return $this->belongsTo('App\WhatsappModels\CashbackType','cashback_type_id','id');
    }

}
