<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Rule extends Model
{
    use UserStampsTrait;
    protected $table = 'whatsapp_tbl_rules';
    protected $fillable = ['basic_rule_type_id','rule_type_id','rule_name','basic_rule_name','rule_sequence','basic_rule_sequence','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Rule  function
    public function rule_type()
    {
    	return $this->belongsTo('App\WhatsappModels\RuleType','rule_id','id');
    }

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
