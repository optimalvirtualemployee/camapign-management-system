<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;


class CampaignOfferTemplate extends Model
{
    
    protected $table = 'whatsapp_tbl_campaign_offer_templates';
    protected $fillable = ['campaign_id','field_value','template_keyword_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
