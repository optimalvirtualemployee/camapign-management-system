<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;


class CampaignTemplate extends Model
{
    
    protected $table = 'missedcall_tbl_campaign_templates';
    protected $fillable = ['field_id','field_value','template_keyword_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
