<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;

class SmsTemplate extends Model
{
    protected $table = 'missedcall_tbl_sms_templates';
    protected $fillable = ['id','camp_id','sms_text', 'smsentityid', 'smstemplateid', 'cstatus', 'created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    //User  function
    public function campaign_type()
    {
        return $this->belongsTo('App\MissedCallModels\CampaignType','camp_id','id');
    }

    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }

    public function field_template()
    {
        return $this->belongsToMany('App\MissedCallModels\Field')->withPivot('field_id','id','keyword');
    }
}
