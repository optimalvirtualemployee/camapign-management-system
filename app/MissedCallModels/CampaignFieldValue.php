<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CampaignFieldValue extends Model
{
    use UserStampsTrait;
    protected $table = 'missedcall_tbl_campaign_field_values';
    protected $fillable = ['campaign_field_id','field_value','campaign_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
}
