<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CampaignField extends Model
{
    use UserStampsTrait;
    protected $table = 'missedcall_tbl_campaign_fields';
    protected $fillable = ['field_name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function field()
    {
        return $this->belongsTo('App\MissedCallModels\Field','field_id');
    }
    
    
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
    
}
