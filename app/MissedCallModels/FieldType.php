<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;


class FieldType extends Model
{
	
    protected $table = 'missedcall_tbl_field_types';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
