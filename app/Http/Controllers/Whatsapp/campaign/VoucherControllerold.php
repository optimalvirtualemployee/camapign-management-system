<?php

namespace App\Http\Controllers\Whatsapp\campaign;


use App\Http\Controllers\Controller;
use App\WhatsappModels\Voucher;
use App\WhatsappModels\Offer;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\CampaignFieldValue;
use App\WhatsappModels\CampaignOfferTemplate;
;
use Illuminate\Http\Request;
use App\Imports\WhatsappVoucherCodeImport;
use Excel;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers = Voucher::with('campaign')->get();

        return view('whatsapp.voucher.index')->with([
        'vouchers'  => $vouchers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('whatsapp.voucher.create',compact('campaigns','offers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'voucher_code'   => 'required|unique:sms_tbl_vouchers,voucher_code',
            'campaign_id'    => 'required|not_in:0',
            'offer_type_id'       => 'required|not_in:0',
            'start_date'     => 'required',
            'end_date'       => 'required',
            'voucher_activation_date_over_sms' => ""
            
            
        ]);
    
        Voucher::create($validatedData);

        return redirect('admin/whatsapp/whatsappvouchers')->withSuccess('Voucher has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$voucher->campaign_id)->first();
        return view('whatsapp.voucher.show',compact('voucher','campaignFieldValue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher,$id)
    {
        $voucher = Voucher::find($id);
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('whatsapp.voucher.edit', compact('voucher','campaigns','offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher,$id)
    {
        $request->validate([
            'voucher_code'   => 'required|unique:sms_tbl_vouchers,voucher_code',
            
            
        ]);        
        $voucher = Voucher::find($id);
        $voucher->voucher_code =  $request->get('voucher_code');
        $voucher->campaign_id  =  $request->get('campaign_id');
        $voucher->offer_type_id     =  $request->get('offer_id');
        $voucher->voucher_activation_date_over_sms = $request->get('voucher_activation_date_over_sms');
        $voucher->touch();        
        return redirect('/admin/whatsapp/whatsappvouchers')->with('success', 'Voucher updated!');
    }
    
    public function uploadVoucherCodeCreate()
    {
        
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('whatsapp.voucher.upload',compact('campaigns','offers'));
    }
    
    //Upload mulitple city through excel file
    public function uploadVoucherCodeStore(Request $request)
    {   
        $request->validate([
            
            'campaign_id' => 'required|not_in:0',
            /*'offer_id'    => 'required|not_in:0',*/
            'start_date'  => 'required',
            'end_date'    => 'required',
            
            
        ]);
        $campaign_id = $request->campaign_id;
        $batch_name = $request->batch_name;
        $sku_name = $request->sku_name;
        $offer_id = $request->offer_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $voucher_activation_date_over_sms = $request->voucher_activation_date_over_sms;
        $cashback_camp_id = $request->cashback_camp_id;


        try {
            
            ini_set('max_execution_time', 0);
           Excel::import(new WhatsappVoucherCodeImport($campaign_id,$batch_name,$sku_name,$offer_id,$start_date,$end_date,$voucher_activation_date_over_sms,$cashback_camp_id),$request->voucher_code_file);
           

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        
        return redirect('/admin/whatsapp/whatsappvouchers')->withSuccess('You have successfully created a voucher Code!');
    }



    public function getOfferList(Request $request)
    {

        $offerData = CampaignOfferTemplate::where('campaign_id',$request->campaign_id)->first();
        $offer = json_decode($offerData->field_value);
        
        /*$arrayNew = [];
        foreach ($jsonnewdata as $key => $value) {

                            
                            
            $offerName = Offer::where('id',$value->offer_type_id)->first();

            $data = array(
                    'offer_name'     =>$offerName->name,
                    'offer_type_id'=>$value->offer_type_id,
                    );

            $offer = array_merge($data,$arrayNew);
        }*/
        
        /*$offer = Voucher::where('campaign_id',$request->campaign_id)->where('status','ACTIVE')->with('offer')->groupBy('offer_type_id','campaign_id')->get();*/
        //dd($offer);
        return response()->json($offer);
    }


    public function getOfferCashback(Request $request)
    {

        $offerData = Offer::where('id',$request->offer_id)->first();
       
        //$offer = json_decode($offerData->field_value);
        
        /*$arrayNew = [];
        foreach ($jsonnewdata as $key => $value) {

                            
                            
            $offerName = Offer::where('id',$value->offer_type_id)->first();

            $data = array(
                    'offer_name'     =>$offerName->name,
                    'offer_type_id'=>$value->offer_type_id,
                    );

            $offer = array_merge($data,$arrayNew);
        }*/
        
        /*$offer = Voucher::where('campaign_id',$request->campaign_id)->where('status','ACTIVE')->with('offer')->groupBy('offer_type_id','campaign_id')->get();*/
        //dd($offer);
        return response()->json(['cashback_price' => $offerData->cashback_price]);
    }

    function fetch_data(Request $request)
    {
      
        $bookingAllData = Voucher::select('id')->with('campaign','offer')->get();

        if($request->ajax()){
            
            if($request->from_date != '' && $request->to_date != '' && $request->campaign_id != ''){
                
                $vouchers = Voucher::with('campaign','offer')
                               ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->whereIn('campaign_id', explode(',',$request->campaign_id))
                               ->paginate(10);
                
            }elseif($request->from_date != '' && $request->to_date){
                
                $vouchers = Voucher::with('campaign','offer')
                               ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->paginate(10);
                
            }elseif($request->campaign_id != ''){
               
                $vouchers = Voucher::with('campaign','offer')->whereIn('campaign_id', explode(',',$request->campaign_id))->paginate(10);
                
            }elseif($request->global_search != ''){
                
                $vouchers = Voucher::with('campaign','offer')
                                ->where(function($query) use ($request) {
                                    $query->where('voucher_code', 'like', "%" . $request->global_search. "%");
                                    /*$query->orWhere('customer_mob_no', 'like', "%" . $request->global_search. "%");
                                    $query->orWhere('cname', 'like', "%" . $request->global_search. "%");*/
                                  })
                                ->paginate(10);

            }

            else{
              
                $vouchers = Voucher::with('campaign','offer')
                                /*->where(function($query) use ($request) {
                                    $query->where('status', '4');})*/
                                ->paginate(10);
            }
            return view('sms.voucher.pagination_data', compact('vouchers','bookingAllData'))->render();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function allVoucherCodeEdit()
    {
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $vouchers = Voucher::groupBy('batch_name')->get();
        
        return view('sms.voucher.edit-all', compact('campaigns','vouchers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function allVoucherCodeUpdate(Request $request, Voucher $voucher)
    {
        $request->validate([
            'voucher_code'   => 'required|unique:sms_tbl_vouchers,voucher_code',
            'campaign_id'    => 'required|not_in:0',
            /*'offer_id'       => 'required|not_in:0',*/
            'start_date'     => 'required',
            'end_date'       => 'required',
            
        ]);        
        
        $voucher->voucher_code =  $request->get('voucher_code');
        $voucher->campaign_id  =  $request->get('campaign_id');
        $voucher->offer_type_id     =  $request->get('offer_id');
        $voucher->voucher_activation_date_over_sms = $request->get('voucher_activation_date_over_sms');
        $voucher->touch();        
        return redirect('/admin/sms/vouchers')->with('success', 'Voucher updated!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function allVoucherCodeDeleteView()
    {
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $vouchers = Voucher::groupBy('batch_name')->get();
        
        return view('sms.voucher.edit-all', compact('campaigns','vouchers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function allVoucherCodeDeleteUpdate(Request $request, Voucher $voucher)
    {
        $request->validate([
            'voucher_code'   => 'required|unique:sms_tbl_vouchers,voucher_code',
            'campaign_id'    => 'required|not_in:0',
            /*'offer_id'       => 'required|not_in:0',*/
            'start_date'     => 'required',
            'end_date'       => 'required',
            
        ]);        
        
        $voucher->voucher_code =  $request->get('voucher_code');
        $voucher->campaign_id  =  $request->get('campaign_id');
        $voucher->offer_type_id     =  $request->get('offer_id');
        $voucher->voucher_activation_date_over_sms = $request->get('voucher_activation_date_over_sms');
        $voucher->touch();        
        return redirect('/admin/sms/vouchers')->with('success', 'Voucher updated!');
    }
}
