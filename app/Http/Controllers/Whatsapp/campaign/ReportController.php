<?php

namespace App\Http\Controllers\Whatsapp\campaign;



use App\WhatsappModels\Outgoing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use DB;
use App\WhatsappModels\WhatsAppData;
use App\Exports\AllWhatsappOutgoingExport;
use App\Exports\AllWhatsappExport;
use Excel;

class ReportController extends Controller
{
    

    
    public function incomingReportList(Request $request)
    {
        
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = LongCode::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();

        return view('whatsapp.report.incoming');
        /*$incomingData = LongCode::get();
        
        return view('admin.report.incoming',compact('incomingData'));*/
    }

    
    public function whatsappoutgoingReportList(Request $request)
    {
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = Outgoing::with('incoming_msg')->whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = Outgoing::with('incoming_msg')->get();
               
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        
        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();

        return view('whatsapp.report.outgoing');
        
        
    }

    public function whatsappReportList(Request $request)
    {
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = WhatsAppData::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = WhatsAppData::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return ($request->created_at !== null) ? $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A') : null;

                    //return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })

                ->editColumn('status', function ($request) {
                    return $status = (isset($request->image_view_status)) ? $request->image_view_status : 'NA'; // human readable 
                })

                ->addColumn('media', function ($request) {
                    return '<a href="' . $request->media_url . '" target="_blank">'. $request->media_url .'</a>';
                })->escapeColumns([])
                
                ->addColumn('media_url', function ($request) {
                    return '<a href="http://demo10.bigcityexperience.com/admin/whatsapp/report/list/' . $request->id . '"><img src=" '. $request->media_url .' " width="100"></a>';
                })->escapeColumns([])
               
            ->make(true);

        }

        
        
        return view('whatsapp.report.whatsapp-data-list');
        
        
    }
    
    public function allOutgoingDownload(Request $request)
    {
        
        
        $outgoing_data = Outgoing::with('incoming_msg')->get()->toArray();
      
        if(!empty($outgoing_data)){
             
            foreach($outgoing_data as $index=>$outgoing)
            {

                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'ID'           => $outgoing['id'],
                   'From'         => $outgoing['incoming_msg']['from'],
                   'Message'         => $outgoing['incoming_msg']['message'],
                   'Outgoing Message'         => $outgoing['outgoing_msg'],
                   
                    'Status'                => $outgoing['incoming_msg']['status'],

                   'Created Date & Time'   => date("Y-m-d", strtotime($outgoing['created_at'])).'&'.date("H:i", strtotime($outgoing['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($outgoing['updated_at'])).'&'.date("H:i", strtotime($outgoing['updated_at'])),
                   
                );
            }
        }else{


              $data[] = array(
                      
                     'SN'           => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'      => '',
                     'Outgoing Message'         => '',
                     
                      'Status'                => '',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );
              

            }
            ob_end_clean(); // this
            ob_start(); // and this
        return Excel::download(new AllWhatsappOutgoingExport($data), 'whatsapp-outgoing.xlsx');
    }


    public function allIncomingDownload(Request $request)
    {
        

        //$campaigns = Campaign::where('id',$request->campaign_id)->first();

        $incoming_data_camp = LongCode::where('parts',$request->campaign_name)->get()->toArray();

        $incoming_data_date = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))->get()->toArray();

        $incoming_data = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))->where('parts',$request->campaign_name)->get()->toArray();
            

        
            if($request->campaign_name != '' && $request->from_date != '' && $request->from_date != ''){

                if(!empty($incoming_data)){

                  foreach($incoming_data as $index=>$incoming){

                    $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                    $data[] = array(
                        
                       'SN'           => ++$index,
                       'ID'           => $incoming['id'],
                       'From'         => $incoming['from'],
                       'Message'      => $incoming['message'],
                       'ts'           => $incoming['ts'],
                       'circle'       => $incoming['circle'],
                       'operator'     => $incoming['operator'],
                       'msgid'        => $incoming['msgid'],
                       'parts'        => $incoming['parts'],
                       'Message Delivered Status'  => $message_delivered_status,
                        'Status'      =>     $incoming['status'],

                       'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                       'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                       
                    );


                    }

                }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );


              }
              
                
            }elseif($request->campaign_name != '' ){

                if(!empty($incoming_data_camp) ){

                  foreach($incoming_data_camp as $index=>$incoming){

                    $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                    $data[] = array(
                        
                       'SN'           => ++$index,
                       'ID'           => $incoming['id'],
                       'From'         => $incoming['from'],
                       'Message'      => $incoming['message'],
                       'ts'           => $incoming['ts'],
                       'circle'       => $incoming['circle'],
                       'operator'     => $incoming['operator'],
                       'msgid'        => $incoming['msgid'],
                       'parts'        => $incoming['parts'],
                       'Message Delivered Status'  => $message_delivered_status,
                        'Status'      =>     $incoming['status'],

                       'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                       'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                       
                    );
                  }



                }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }
              
                
            }elseif($request->from_date != '' && $request->from_date != ''){

              if(!empty($incoming_data_date)){

                foreach($incoming_data_date as $index=>$incoming){

                  $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                  $data[] = array(
                      
                     'SN'           => ++$index,
                     'ID'           => $incoming['id'],
                     'From'         => $incoming['from'],
                     'Message'      => $incoming['message'],
                     'ts'           => $incoming['ts'],
                     'circle'       => $incoming['circle'],
                     'operator'     => $incoming['operator'],
                     'msgid'        => $incoming['msgid'],
                     'parts'        => $incoming['parts'],
                     'Message Delivered Status'  => $message_delivered_status,
                      'Status'      =>     $incoming['status'],

                     'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                     'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                     
                  );

                }
              }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }
              
                
            }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }


              ob_end_clean(); // this
              ob_start(); // and this
              


            return Excel::download(new AllIncomingExport($data), 'cms_sms_incoming.xlsx');
            
            
    }

    public function issuePanelList(Request $request)
    {
        
        
        $data = Outgoing::with('incoming_msg')->paginate(10);

        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();
        
        return view('whatsapp.issue.list',compact('campaigns','data'));
        
        
    }

    
    public function whatsappImageView($id)
    {
        
        $data = WhatsAppData::where('id',$id)->first();
        
        return view('whatsapp.report.whatsapp-image',compact('data'));
        
        
    }

    public function whatsappImageViewStatus(Request $request,$id,$status)
    {
        if($status == 'APPROVED'){

            $data = WhatsAppData::where('id',$id)->first();
            $data->image_view_status = $status;
            $data->save();
            return redirect()->back()->withSuccess('Image has been approved successfully!');
        }else{

            $data = WhatsAppData::where('id',$id)->first();
            $data->image_view_status = $status;
            $data->save();
            return redirect()->back()->withSuccess('Image has been rejected successfully!');

        }
        
        
        
    }
    


    
}
