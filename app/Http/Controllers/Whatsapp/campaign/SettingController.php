<?php

namespace App\Http\Controllers\Whatsapp\campaign;


use App\WhatsappModels\CampaignSetting;
use App\WhatsappModels\CampaignRedirect;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\LongCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class SettingController extends Controller
{
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $campaign_setting = CampaignSetting::where('status','ACTIVE')->first();
        
        $campaigns = Campaign::where('status','ACTIVE')->get();
        return view('whatsapp.campaign.setting',compact('campaign_setting','campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id="")
    {
        $validatedData = $this->validate($request, [
            'campaign_id'        => 'required|not_in:0',
            'title'              => 'required',
            'contact_no'  => 'required' ,
            'menu_bar_color'     => 'required' ,
            
            
        ]);
        
        if($id == ""){
           
            CampaignSetting::create($validatedData);
            
            return redirect('admin/whatsapp/campaign/setting/create')->withSuccess('Campaign setting has been updated successfully!');
            
        }else{
            
            
            
            $campaign_setting = CampaignSetting::find($id);
            $campaign_setting->title = $request->title;
            $campaign_setting->contact_no = $request->contact_no;
            $campaign_setting->menu_bar_color = $request->menu_bar_color;
            $campaign_setting->campaign_id = $request->campaign_id;
            $campaign_setting->touch();
            
    
            return redirect('admin/whatsapp/campaign/setting/create')->withSuccess('Campaign setting has been updated successfully!');
            
        }
        
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createRidrect()
    {
        
        $campaign_setting = CampaignRedirect::where('status','ACTIVE')->first();
        
        $campaigns = Campaign::where('status','ACTIVE')->get();
        return view('whatsapp.campaign.redirect',compact('campaign_setting','campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRidrect(Request $request,$id="")
    {
        $validatedData = $this->validate($request, [
            'campaign_id'        => 'required|not_in:0',
            'redirect_url'              => 'required',
            
            
            
        ]);
        
        if($id == ""){
           
            CampaignRedirect::create($validatedData);
            
            return redirect('admin/whatsapp/campaign/redirect')->withSuccess('Campaign redirect has been updated successfully!');
            
        }else{
            
            
            
            $campaign_setting = CampaignRedirect::find($id);
            $campaign_setting->redirect_url = $request->redirect_url;
            $campaign_setting->campaign_id = $request->campaign_id;
            $campaign_setting->touch();
            
    
            return redirect('admin/whatsapp/campaign/redirect')->withSuccess('Campaign setting has been updated successfully!');
            
        }
        
        
    }

    
    
    


    
}
