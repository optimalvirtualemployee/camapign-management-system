<?php

namespace App\Http\Controllers\SMS\campaign;

use App\SmsModels\CampaignExtension;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SmsModels\Template;

class CampaignExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $campaignTypes = CampaignExtension::get();

        return view('sms.campaign_type.index')->with([
        'campaignTypes'  => $campaignTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sms.campaign_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:sms_tbl_campaign_types',
            
            
        ]);

        CampaignType::create($validatedData);

        return redirect('admin/sms/campaigntypes')->withSuccess('Campaign Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show(CampaignType $campaignType,$id)
    {
        $campaignType = CampaignExtension::find($id);
        return view('sms.campaign_type.show',compact('campaignType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit(CampaignType $campaignType,$id)
    {
        
        $campaignType = CampaignExtension::find($id);
        $templates= Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->get();
        return view('sms.campaign_type.edit', compact('campaignType','templates'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampaignType $campaignType,$id)
    {
        $request->validate([
            'name'=>'required',
            'sms_content' => 'required',
            'sms'       => 'required|in:YES,NO' ,
            'stats'     => 'required|in:YES,NO' ,
            'rules'     => 'required|in:YES,NO' 
            
        ]);        
        $CampaignType = CampaignExtension::find($id);
        $CampaignType->name =  $request->get('name');
        $CampaignType->sms =  $request->get('sms');
        $CampaignType->template_id =  $request->get('template_id');
        $CampaignType->stats =  $request->get('stats');
        $CampaignType->rules =  $request->get('rules');
        $CampaignType->sms_content = $request->get('sms_content');
        $CampaignType->touch();        
        return redirect('/admin/sms/campaigntypes')->with('success', 'Campaign Types updated!');
    }

   
}
