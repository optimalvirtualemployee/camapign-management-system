<?php

namespace App\Http\Controllers\Whatsapp\campaign;

use App\WhatsappModels\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\WhatsappModels\Template;

class CampaignTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $campaignTypes = CampaignType::get();

        return view('whatsapp.campaign_type.index')->with([
        'campaignTypes'  => $campaignTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whatsapp.campaign_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:whatsapp_tbl_campaign_types',
            'sms'       => 'required|in:YES,NO' ,
            'sms_content'      => 'required',
            'stats'     => 'required|in:YES,NO' ,
            'rules'     => 'required|in:YES,NO' ,
            'template_id'     => 'required|not_in:0' 
            
        ]);

        CampaignType::create($validatedData);

        return redirect('admin/whatsapp/whatsappcampaigntypes')->withSuccess('Campaign Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show(CampaignType $campaignType,$id)
    {
        $campaignType = CampaignType::find($id);
        return view('whatsapp.campaign_type.show',compact('campaignType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit(CampaignType $campaignType,$id)
    {
        
        $campaignType = CampaignType::find($id);
        $templates= Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->get();
        return view('whatsapp.campaign_type.edit', compact('campaignType','templates'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampaignType $campaignType,$id)
    {
        $request->validate([
            'name'=>'required',
            'sms_content'      => 'required',
            'sms'       => 'required|in:YES,NO' ,
            'stats'     => 'required|in:YES,NO' ,
            'rules'     => 'required|in:YES,NO' 
            
        ]);        
        $CampaignType = CampaignType::find($id);
        $CampaignType->name =  $request->get('name');
        $CampaignType->sms =  $request->get('sms');
        $CampaignType->template_id =  $request->get('template_id');
        $CampaignType->stats =  $request->get('stats');
        $CampaignType->rules =  $request->get('rules');
        $CampaignType->sms_content = $request->get('sms_content');
        $CampaignType->touch();        
        return redirect('/admin/whatsapp/whatsappcampaigntypes')->with('success', 'Campaign Types updated!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTemplateList()
    {
        $templates = Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->pluck("template_name","id");

    
        return response()->json($templates);
    }
}
