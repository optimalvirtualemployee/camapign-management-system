<?php

namespace App\Http\Controllers\MissedCall\campaign;

use App\MissedCallModels\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\MissedCallModels\Template;

class CampaignTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaignTypes = CampaignType::orderBy('id', 'DESC')->get();
        //  dd($campaignTypes);
        return view('missedcall.campaign_type.index')->with([
        'campaignTypes'  => $campaignTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('missedcall.campaign_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:missedcall_tbl_campaign_types',
            /*'sms'       => 'required|in:YES,NO' ,
            'stats'     => 'required|in:YES,NO' ,
            'rules'     => 'required|in:YES,NO' ,*/
            'types'     => 'required|not_in:0',
            'missedcallnumber' => 'required|unique:missedcall_tbl_campaign_types',
            'status'        => 'required'
        ]);

        CampaignType::create($validatedData);

        return redirect('admin/missedcall/missedcallcampaigntypes')->withSuccess('Campaign Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show(CampaignType $campaignType,$id)
    {
        $campaignType = CampaignType::find($id);
        return view('missedcall.campaign_type.show',compact('campaignType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit(CampaignType $campaignType,$id)
    {
        $campaignType = CampaignType::find($id);
        $templates= Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->get();
        return view('missedcall.campaign_type.edit', compact('campaignType','templates'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CampaignType $campaignType,$id)
    {
        $request->validate([
            'name'=>'required',
            /*'sms'       => 'required|in:YES,NO' ,
            'stats'     => 'required|in:YES,NO' ,
            'rules'     => 'required|in:YES,NO' */
            'missedcallnumber' => 'required',
            'status'            => 'required',
        ]);        
        $CampaignType = CampaignType::find($id);
        $CampaignType->name =  $request->get('name');
        $CampaignType->types =  $request->get('types');
        $CampaignType->missedcallnumber = $request->get('missedcallnumber');
        $CampaignType->cstatus = $request->get('status');
        /*$CampaignType->sms =  $request->get('sms');
        $CampaignType->template_id =  $request->get('template_id');
        $CampaignType->stats =  $request->get('stats');
        $CampaignType->rules =  $request->get('rules');*/
        $CampaignType->touch();        
        return redirect('/admin/missedcall/missedcallcampaigntypes')->with('success', 'Campaign Types updated!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTemplateList()
    {
        $templates = Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->pluck("template_name","id");
        return response()->json($templates);
    }
}
