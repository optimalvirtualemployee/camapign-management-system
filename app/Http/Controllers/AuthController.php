<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\SmsModels\RewardCode;
use Helper;
use App\SmsModels\CampaignFieldValue;
use App\SmsModels\Campaign;
use App\SmsModels\LongCode;
use App\SmsModels\Voucher;
use App\SmsModels\CampaignTemplate;
use App\SmsModels\Outgoing;
use App\SmsModels\Offer;
use DB;
use App\Chart;
use Carbon\Carbon;
use DateTime;
use Carbon\CarbonPeriod;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }  
 
    public function register()
    {
        return view('register');
    }
     
    public function postLogin(Request $request)
    {

        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);
 
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            // Authentication passed...
            return redirect()->intended('admin/dashboard');
        }
        
        return Redirect::to("/")->withSuccess('Oppes! You have entered invalid credentials');
    }
 
    public function postRegister(Request $request)
    {  
        request()->validate([
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        ]);
         
        $data = $request->all();
 
        $check = $this->create($data);
       
        return Redirect::to("admin/dashboard")->withSuccess('Great! You have Successfully loggedin');
    }
     
    
    public function dashboard(Request $request)
    {
        
        
        $method = $request->method();

        $totalEntries = 0;
        $totalValidEntries = 0;
        $totalInvalidEntries = 0;
        $totalExcessTriesEntries = 0;
        $totalUniqueEntries= 0;
        $campaigns= Campaign::orderBy('id','DESC')->get();
        $chart = new Chart;
        $chart->labels = 0;
        $chart->dataset = 0;
        $chart->colours = 0;
        $chartWeek = new Chart;
        $chartWeek->labels = 0;
        $chartWeek->dataset = 0;
        $chartWeek->colours = 0;
        $chartTime = new Chart;
        $chartTime->labels = 0;
        
        $chartTime->dataset = 0;
        $chartTime->colours = 0;
        if(Auth::check()){


            
            $circleWiseEntries = [];
            $SKUWiseEntries = [];
            $batchWiseEntries = [];
            $circleNewData = [];
            $OfferTypeWiseEntries  = [];
            $keywordWiseEntries  = [];
            $totalSmsSentReport  = [];
            $batchNewData = [];
            $SKUNewData = [];
            $OfferTypeNewData = [];
            $keywordWiseNewData = [];
            $totalSmsSentReportNewData = [];

            return view('sms.dashboard',compact('totalEntries','totalValidEntries','totalInvalidEntries','totalExcessTriesEntries','totalUniqueEntries','circleWiseEntries','campaigns','chart','chartWeek','chartTime','batchWiseEntries','OfferTypeWiseEntries','SKUWiseEntries','keywordWiseEntries','totalSmsSentReport','circleNewData','batchNewData','OfferTypeNewData','keywordWiseNewData','SKUNewData','totalSmsSentReportNewData'));
        }

        return Redirect::to("/")->withSuccess('Opps! You do not have access');

    }
    public function fetch_data(Request $request)
    {

        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if($request->ajax()){

            if($request->from_date != '' && $request->to_date && $request->campaign_id != ''){
                
                $totalEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                               ->where('parts',$request->campaign_id)
                               ->count();

                $totalValidEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->where('parts',$request->campaign_id)
                               ->Where(['status'=>'valid'])->count();
                $totalInvalidEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->where('parts',$request->campaign_id)
                               ->WhereIn('status',['INVALID','DUPLICATE','EXPIRED','REWARDOVER'])->count();
                $totalExcessTriesEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->where('parts',$request->campaign_id)
                               ->Where(['status'=>'EXCESS'])->count();
                $totalUniqueEntries= LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                               ->where('parts',$request->campaign_id)
                               ->Where(['status'=>'valid'])->distinct('from')->count();

                $campaignID = Campaign::where('campaign_keyword',$request->campaign_id)->first();

                $circleWiseEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('circle')->get();

                $circleArray = Array();

                foreach ($circleWiseEntries as $key => $entries) {
                    
                    $TotalCount1 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('circle',$entries['circle'])->where('parts',$entries['parts'])->count();

                    $BalanceCount1 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('circle',$entries['circle'])->where('parts',$entries['parts'])->where('status','VALID')->count();

                    $array1[] = array('parts'=>$entries->parts,
                        'circle'=>$entries->circle,
                        'TotalCount'=>$TotalCount1,
                        'BalanceCount'=>$BalanceCount1);

                    $circleNewData = array_merge($circleArray,$array1);

                }

               
                $SKUWiseEntries = LongCode::whereNotNull('sku_name')->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('sku_name')->get();
                
                $SKUArray = Array();

                if(count($SKUWiseEntries) != 0){

                    foreach ($SKUWiseEntries as $key => $entries) {
                       
                        $TotalCount6 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->where('sku_name',$entries->sku_name)
                                                ->count();

                        $TotalValidCount6 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('sku_name',$entries->sku_name)
                                            ->where('parts',$entries->parts)->where('status','VALID')
                                            ->count();

                        
                        $TotalVoucherCount6 = LongCode::whereHas('voucher', function($q) use($entries){
                                                $q->where('sku_name', $entries->sku_name);})
                                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->count();
                        

                        $array6[] = array('campaign_keyword'=>$entries->parts,
                            'sku_name'=>$entries->sku_name,
                            'TotalCount'=>$TotalCount6,
                            'TotalVoucherCount2' => $TotalVoucherCount6,
                            'TotalValidCount'=>$TotalValidCount6);

                        $SKUNewData = array_merge($SKUArray,$array6);

                    }
                }else{

                    $SKUNewData = [];

                }

                $batchWiseEntries = LongCode::whereNotNull('batch_name')->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('batch_name')->get();
                
                $batchArray = Array();

                if(count($batchWiseEntries) != 0){

                    foreach ($batchWiseEntries as $key => $entries) {
                       
                        $TotalCount2 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->where('batch_name',$entries->batch_name)
                                                ->count();

                        $TotalValidCount2 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('batch_name',$entries->batch_name)
                                            ->where('parts',$entries->parts)->where('status','VALID')
                                            ->count();

                        
                        $TotalVoucherCount2 = LongCode::whereHas('voucher', function($q) use($entries){
                                                $q->where('batch_name', $entries->batch_name);})
                                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->count();
                        

                        $array2[] = array('campaign_keyword'=>$entries->parts,
                            'batch_name'=>$entries->batch_name,
                            'TotalCount'=>$TotalCount2,
                            'TotalVoucherCount' => $TotalVoucherCount2,
                            'TotalValidCount'=>$TotalValidCount2);

                        $batchNewData = array_merge($batchArray,$array2);

                    }
                }else{

                    $batchNewData = [];

                }

                $OfferTypeWiseEntries  = LongCode::whereNotNull('offer_type_name')->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('offer_type_name')->get();

                $OfferTypeArray = Array();

                if(count($OfferTypeWiseEntries) != 0){

                    foreach ($OfferTypeWiseEntries as $key => $entries) {
                       
                        $TotalCount3 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->where('offer_type_name',$entries->parts3)
                                                ->count();

                        $TotalValidCount3 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('offer_type_name',$entries->parts3)
                                            ->where('parts',$entries->parts)->where('status','VALID')
                                            ->count();

                        $offerTypeId = Offer::where('name',$entries->offer_type_name)->first();

                        if($offerTypeId != null){

                            $TotalVoucherCount3 = LongCode::whereHas('voucher', function($q) use($offerTypeId){
                                                $q->where('offer_type_id', $offerTypeId->id);})
                                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                                ->where('parts',$entries->parts)
                                                ->count();

                        }else{

                            $TotalVoucherCount3 = 0;
                        }

                        

                        $array3[] = array('campaign_keyword'=>$entries->parts,
                            'parts3'=>$entries->offer_type_name,
                            'TotalCount'=>$TotalCount3,
                            'TotalVoucherCount'=> $TotalVoucherCount3,
                            'TotalValidCount'=>$TotalValidCount3);

                        $OfferTypeNewData = array_merge($OfferTypeArray,$array3);

                    }
                }else{

                    $OfferTypeNewData = [];
                }

                $keywordWiseEntries  = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('parts')->get();

                $keywordWiseArray = Array();

                foreach ($keywordWiseEntries as $key => $entries) {
                   
                    $TotalCount4 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('parts',$entries->parts)
                                            
                                            ->count();

                    $TotalValidCount4 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                        
                                        ->where('parts',$entries->parts)
                                        ->where('status','VALID')
                                        ->count();

                    

                    

                    $array4[] = array('campaign_keyword'=>$entries->parts,
                       
                        'TotalCount'=>$TotalCount4,
                        'TotalValidCount'=>$TotalValidCount4);

                    $keywordWiseNewData = array_merge($keywordWiseArray,$array4);

                }

                $totalSmsSentReport  = LongCode::whereNotNull('field_slug_value')->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->where('parts',$request->campaign_id)->groupBy('field_slug_value')->get();

                $totalSmsSentReportArray = Array();

                foreach ($totalSmsSentReport as $key => $entries) {
                   
                   

                   $TotalCount5 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('parts',$entries->parts)
                                            ->count();

                    $TotalSMSDelivered5 = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                            ->where('parts',$entries->parts)->where('field_slug_value',$entries->field_slug_value)->count();

                    

                    

                    

                    $array5[] = array('campaign_keyword'=>$entries->parts,
                        'field_slug_value'=>$entries->field_slug_value,
                        'TotalCount'=>$TotalCount5,
                        'TotalSMSDelivered'=>$TotalSMSDelivered5);

                    $totalSmsSentReportNewData = array_merge($totalSmsSentReportArray,$array5);

                }

                $groups = DB::table('sms_tbl_longcodes')
                    ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                    ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                    ->where(['parts'=>$request->campaign_id])
                    ->groupBy('day')
                    ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                    ->pluck('day_entries','day')->all();

                $groupsValidEntries = DB::table('sms_tbl_longcodes')
                                        ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                                        ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                        ->where(['parts'=>$request->campaign_id,'status'=>'VALID'])
                                        ->groupBy('day')
                                        ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                                        ->pluck('day_entries','day')->all();



                // Prepare the data for returning with the view
                $chart = new Chart;
                $chart->labels = (array_keys($groups));
                $chart->total = (array_values($groups));
                $chart->validTotal = (array_values($groupsValidEntries));

                $result = CarbonPeriod::create($request->from_date, '1 month', $request->to_date);
                $months = array();
                foreach ($result as $dt) {

                    $monthsArray[] =  array('month'=>$dt->format("n"),
                                        'year'=>$dt->format("y"));

                    $monthsFinalData = array_merge($months,$monthsArray);
                    
                   
                }
                
                

                /*foreach ($monthsFinalData as $key => $value) {

                    $month = date("n", strtotime($value['month']));
                
                    $year = date("Y", strtotime($value['month']));      

                    $week = date("W", strtotime($year . "-" . $month ."-01"));

                    $str='';
                    $str .= date("d M Y", strtotime($year . "-" . $month ."-01")) ."-";

                    $unix = strtotime($year."W".$week ."+1 week");

                    while(date("m", $unix) == $month){
                     $str .= date("d M Y", $unix-86400) . "|";
                     $str .= date("d M Y", $unix) ."-"; 
                     $unix = $unix + (86400*7);
                    }
                    $str .= date("d M Y", strtotime("last day of ".$year . "-" . $month));

                    $weeks_ar = explode('|',$str);


                    
                }*/

               $month = date("n", strtotime($request->from_date));
                
                $year = date("Y", strtotime($request->from_date));      

                $week = date("W", strtotime($year . "-" . $month ."-01"));

                $str='';
                $str .= date("d M Y", strtotime($year . "-" . $month ."-01")) ."-";

                $unix = strtotime($year."W".$week ."+1 week");

                while(date("m", $unix) == $month){
                 $str .= date("d M Y", $unix-86400) . "|";
                 $str .= date("d M Y", $unix) ."-"; 
                 $unix = $unix + (86400*7);
                }
                $str .= date("d M Y", strtotime("last day of ".$year . "-" . $month));

                $weeks_ar = explode('|',$str); 
                

                $groupsWeek = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('WEEK(created_at) as week'), DB::raw('count(*) as entries'))
                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->groupBy('week')
                                ->pluck('entries','week')->all();

                $groupsWeekValidEntries = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('WEEK(created_at) as week'), DB::raw('count(*) as entries'))
                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id,'status'=>'VALID'])
                                ->groupBy('week')
                                ->pluck('entries','week')->all();





                // Prepare the data for returning with the view
                $chartWeek = new Chart;
                $chartWeek->labels = $weeks_ar;

                $chartWeek->total = (array_values($groupsWeek));
                $chartWeek->validTotal = array_values($groupsWeekValidEntries);


                $a = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"00:00 AM - 03:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','00:00:00')
                                ->whereTime('created_at','<=','03:00:00')
                                ->get();

                $b = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"03:00 AM - 06:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','03:00:00')
                                ->whereTime('created_at','<=','06:00:00')
                                ->get();
                $c = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"06:00 AM - 09:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','06:00:00')
                                ->whereTime('created_at','<=','09:00:00')
                                ->get();
                $d = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"09:00 AM - 12:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','09:00:00')
                                ->whereTime('created_at','<=','12:00:00')
                                ->get();

                $e= array($a[0]); 
                $f= array($b[0]);
                $g= array($c[0]);
                $h= array($d[0]);

                $time_chart = array_merge($e,$f,$g,$h);
               
                // Generate random colours for the groups
                for ($i=0; $i<=count($time_chart); $i++) {

                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartTime = new Chart;
                $chartTime->labels = (array_keys($time_chart));
                
                $chartTime->dataset = (array_values($time_chart));
                $chartTime->colours = $colours;

                $circleData = DB::table('sms_tbl_longcodes')->select(\DB::raw("COUNT(*) as count"), \DB::raw("circle as day_name"), \DB::raw("circle as day"))
                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                ->where(['parts'=>$request->campaign_id])
                                ->where('created_at', '>', Carbon::today()->subDay(6))
                                ->groupBy('day_name')
                                ->orderBy('day')
                                ->get();
  
                $finalData = [];
         
                foreach($circleData as $row) {

                    $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    $finalData['label'][] = $row->day_name;
                    $finalData['data'][] = (int) $row->count;
                }
         
                $finalData['chart_data'] = json_encode($finalData);
                $finalData['colours'] = $colours;
                $one_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=1) tbl_lgcd');
                $one_time = $one_time[0]->cnt;
                $two_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=2) tbl_lgcd');
                $two_time = $two_time[0]->cnt;
                $three_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=3) tbl_lgcd');
                $three_time = $three_time[0]->cnt;
                $four_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=4) tbl_lgcd');
                $four_time = $four_time[0]->cnt;

                
                $circle_chart = json_encode(DB::select('SELECT `circle` as name , count(*) as circle FROM `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'"  group by circle '));
                

            }elseif($request->from_date != '' && $request->to_date){


                $totalEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->count();
                $totalValidEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->Where(['status'=>'valid'])->count();
                $totalInvalidEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->WhereIn('status',['INVALID','DUPLICATE','EXPIRED','REWARDOVER'])->count();
                $totalExcessTriesEntries = LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->Where(['status'=>'EXCESS'])->count();
                $totalUniqueEntries= LongCode::whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))->Where(['status'=>'valid'])->distinct('from')->count();

                $groups = DB::table('sms_tbl_longcodes')
                  ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                  ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                    

                  ->groupBy('day')
                  ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                  ->pluck('day_entries','day')->all();

                // Generate random colours for the groups
                for ($i=0; $i<=count($groups); $i++) {
                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chart = new Chart;
                        $chart->labels = (array_keys($groups));
                        $chart->dataset = (array_values($groups));
                        $chart->colours = $colours;



                $groupsWeek = DB::table('sms_tbl_longcodes')
                      ->select(DB::raw('WEEK(created_at) as week'), DB::raw('count(*) as entries'))
                      ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                        

                      ->groupBy('week')
                      /*->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))*/
                      ->pluck('entries','week')->all();

                // Generate random colours for the groups
                for ($i=0; $i<=count($groupsWeek); $i++) {
                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartWeek = new Chart;
                $chartWeek->labels = (array_keys($groupsWeek));
                $chartWeek->dataset = (array_values($groupsWeek));
                $chartWeek->colours = $colours;


                $a = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"00:00 AM - 03:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                                ->whereTime('created_at','>=','00:00:00')
                                ->whereTime('created_at','<=','03:00:00')
                                ->get();

                $b = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"03:00 AM - 06:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                                ->whereTime('created_at','>=','03:00:00')
                                ->whereTime('created_at','<=','06:00:00')
                                ->get();
                $c = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"06:00 AM - 09:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                                ->whereTime('created_at','>=','06:00:00')
                                ->whereTime('created_at','<=','09:00:00')
                                ->get();
                $d = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"09:00 AM - 12:00 AM" as time'), DB::raw('count(*) as entries'))->
                                whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                                ->whereTime('created_at','>=','09:00:00')
                                ->whereTime('created_at','<=','12:00:00')
                                ->get();

                $e= array($a[0]); 
                $f= array($b[0]);
                $g= array($c[0]);
                $h= array($d[0]);

                $time_chart = array_merge($e,$f,$g,$h);
               
                // Generate random colours for the groups
                for ($i=0; $i<=count($time_chart); $i++) {

                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartTime = new Chart;
                $chartTime->labels = (array_keys($time_chart));
                
                $chartTime->dataset = (array_values($time_chart));
                $chartTime->colours = $colours;

                $circleData = DB::table('sms_tbl_longcodes')->select(\DB::raw("COUNT(*) as count"), \DB::raw("circle as day_name"), \DB::raw("circle as day"))
                                ->whereBetween(DB::raw('DATE(created_at)'), array(date("Y-m-d", strtotime($request->from_date)), date("Y-m-d", strtotime($request->to_date))))
                                
                                ->where('created_at', '>', Carbon::today()->subDay(6))
                                ->groupBy('day_name')
                                ->orderBy('day')
                                ->get();
  
                $finalData = [];
         
                foreach($circleData as $row) {

                    $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    $finalData['label'][] = $row->day_name;
                    $finalData['data'][] = (int) $row->count;
                }
         
                $finalData['chart_data'] = json_encode($finalData);
                $finalData['colours'] = $colours;
                $one_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=1) tbl_lgcd');
                $one_time = $one_time[0]->cnt;
                $two_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=2) tbl_lgcd');
                $two_time = $two_time[0]->cnt;
                $three_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=3) tbl_lgcd');
                $three_time = $three_time[0]->cnt;
                $four_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=4) tbl_lgcd');
                $four_time = $four_time[0]->cnt;

                
                $circle_chart = json_encode(DB::select('SELECT `circle` as name , count(*) as circle FROM `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'"  group by circle '));


                
            }elseif($request->campaign_id != ''){
               

                $totalEntries = LongCode::where('parts',$request->campaign_id)->count();

                $totalValidEntries = LongCode::where('parts',$request->campaign_id)->Where(['status'=>'valid'])->count();

                $totalInvalidEntries = LongCode::where('parts',$request->campaign_id)->WhereIn('status',['INVALID','DUPLICATE','EXPIRED','REWARDOVER'])->count();

                $totalExcessTriesEntries = LongCode::where('parts',$request->campaign_id)->Where(['status'=>'EXCESS'])->count();

                $totalUniqueEntries= LongCode::where('parts',$request->campaign_id)->Where(['status'=>'valid'])->distinct('from')->count();

                $campaignID = Campaign::where('campaign_keyword',$request->campaign_id)->first();

                $circleWiseEntries = LongCode::where('parts',$request->campaign_id)->groupBy('circle')->get();

                $circleArray = array();

                foreach ($circleWiseEntries as $key => $entries) {
                    
                    $TotalCount11 = LongCode::where('circle',$entries['circle'])->where('parts',$entries['parts'])->count();

                    $BalanceCount11 = LongCode::where('circle',$entries['circle'])->where('parts',$entries['parts'])->where('status','VALID')->count();

                    $array11[] = array('parts'=>$entries['parts'],
                        'circle'=>$entries['circle'],
                        'TotalCount'=>$TotalCount11,
                        'BalanceCount'=>$BalanceCount11);

                    $circleNewData = array_merge($circleArray,$array11);

                }

                $SKUWiseEntries = LongCode::whereNotNull('sku_name')->where('parts',$request->campaign_id)->groupBy('sku_name')->get();
                
                $SKUArray = Array();

                if(count($SKUWiseEntries) != 0){

                    foreach ($SKUWiseEntries as $key => $entries) {
                   
                    $TotalCount66 = LongCode::
                                            where('parts',$entries->parts)
                                            ->where('sku_name',$entries->sku_name)
                                            ->count();

                    $TotalValidCount66 = LongCode::where('sku_name',$entries->sku_name)
                                        ->where('parts',$entries->parts)->where('status','VALID')
                                        ->count();

                    
                    $TotalVoucherCount66 = LongCode::whereHas('voucher', function($q) use($entries){
                                            $q->where('sku_name', $entries->sku_name);
                                            $q->whereNotNull('sku_name');})
                                            
                                            ->where('parts',$entries->parts)
                                            ->count();
                    

                    $array66[] = array('campaign_keyword'=>$entries->parts,
                        'sku_name'=>$entries->sku_name,
                        'TotalCount'=>$TotalCount66,
                        'TotalVoucherCount' => $TotalVoucherCount66,
                        'TotalValidCount'=>$TotalValidCount66);

                    $SKUNewData = array_merge($SKUArray,$array66);

                    }
                }else{

                    $SKUNewData = [];
                }
                

                $batchWiseEntries = LongCode::whereNotNull('batch_name')->where('parts',$request->campaign_id)->groupBy('batch_name')->get();
               
                $batchArray = Array();
                if(count($batchWiseEntries) != 0){

                    foreach ($batchWiseEntries as $key => $entries) {
                       
                        $TotalCount22 = LongCode::where('parts',$entries->parts)
                                                ->where('batch_name',$entries->batch_name)
                                                ->count();

                        $TotalValidCount22 = LongCode::where('batch_name',$entries->batch_name)
                                            ->where('parts',$entries->parts)->where('status','VALID')
                                            ->count();

                        
                        $TotalVoucherCount22 = LongCode::whereHas('voucher', function($q) use($entries){
                                                $q->where('batch_names', $entries->batch_name);})
                                                ->where('parts',$entries->parts)
                                                ->count();
                        

                        $array22[] = array('campaign_keyword'=>$entries->parts,
                            'batch_name'=>$entries->batch_name,
                            'TotalCount'=>$TotalCount22,
                            'TotalVoucherCount'=>$TotalVoucherCount22,
                            'TotalValidCount'=>$TotalValidCount22);

                        $batchNewData = array_merge($batchArray,$array22);

                    }
                }else{

                    $batchNewData = [];

                }
                
                $OfferTypeWiseEntries  = LongCode::whereNotNull('offer_type_name')->where('parts',$request->campaign_id)->groupBy('offer_type_name')->get();

                $OfferTypeArray = Array();
                if(count($OfferTypeWiseEntries) != 0){

                    foreach ($OfferTypeWiseEntries as $key => $entries) {
                       
                        $TotalCount33 = LongCode::where('parts',$entries->parts)
                                                ->where('offer_type_name',$entries->offer_type_name)
                                                ->count();

                        $TotalValidCount33 = LongCode::where('offer_type_name',$entries->offer_type_name)
                                            ->where('parts',$entries->parts)->where('status','VALID')
                                            ->count();

                        $offerTypeId = Offer::where('name',$entries->offer_type_name)->first();

                        if($offerTypeId != null){

                            $TotalVoucherCount33 = LongCode::whereHas('voucher', function($q) use($offerTypeId){
                                                $q->where('offer_type_id', $offerTypeId->id);})
                                                
                                                ->where('parts',$entries->parts)
                                                ->count();

                        }else{

                            $TotalVoucherCount33 = 0;
                        }
                        

                        $array33[] = array('campaign_keyword'=>$entries->parts,
                            'parts3'=>$entries->offer_type_name,
                            'TotalCount'=>$TotalCount33,
                            'TotalVoucherCount33'=>$TotalVoucherCount33,
                            'TotalValidCount'=>$TotalValidCount33);

                        $OfferTypeNewData = array_merge($OfferTypeArray,$array33);

                    }
                }else{

                    $OfferTypeNewData = [];

                }

                $keywordWiseEntries  = LongCode::where('parts',$request->campaign_id)->groupBy('parts')->get();

                $keywordWiseArray = Array();

                foreach ($keywordWiseEntries as $key => $entries) {
                   
                    $TotalCount44 = LongCode::where('parts',$entries->parts)
                                            
                                            ->count();

                    $TotalValidCount44 = LongCode::
                                        where('parts',$entries->parts)
                                        ->where('status','VALID')
                                        ->count();

                    

                    

                    $array44[] = array('campaign_keyword'=>$entries->parts,
                       
                        'TotalCount'=>$TotalCount44,
                        'TotalValidCount'=>$TotalValidCount44);

                    $keywordWiseNewData = array_merge($keywordWiseArray,$array44);

                }

                $totalSmsSentReport  = LongCode::whereNotNull('field_slug_value')->where('parts',$request->campaign_id)->groupBy('field_slug_value')->get();

                $totalSmsSentReportArray = Array();

                foreach ($totalSmsSentReport as $key => $entries) {
                   
                   

                    $TotalCount55 = LongCode::where('parts',$entries->parts)->groupBy()
                                            ->count();

                    $TotalSMSDelivered55 = LongCode::where('parts',$entries->parts)->where('field_slug_value',$entries->field_slug_value)->count();

                    

                    

                    

                    $array55[] = array('campaign_keyword'=>$entries->parts,
                        'field_slug_value'=>$entries->field_slug_value,
                        'TotalCount'=>$TotalCount55,
                        'TotalSMSDelivered'=>$TotalSMSDelivered55);

                    $totalSmsSentReportNewData = array_merge($totalSmsSentReportArray,$array55);

                }

                $groups = DB::table('sms_tbl_longcodes')
                  ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                    ->where(['parts'=>$request->campaign_id])

                  ->groupBy('day')
                  ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                  ->pluck('day_entries','day')->all();

                $groups = DB::table('sms_tbl_longcodes')
                  ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                    ->where(['parts'=>$request->campaign_id])

                  ->groupBy('day')
                  ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                  ->pluck('day_entries','day')->all();

                $groupsValidEntries = DB::table('sms_tbl_longcodes')
                                        ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                                        ->where(['parts'=>$request->campaign_id,'status'=>'VALID'])
                                        ->groupBy('day')
                                        ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                                        ->pluck('day_entries','day')->all();



                // Prepare the data for returning with the view
                $chart = new Chart;
                $chart->labels = (array_keys($groups));
                $chart->total = (array_values($groups));
                $chart->validTotal = (array_values($groupsValidEntries));
                        



                $groupsWeek = DB::table('sms_tbl_longcodes')
                      ->select(DB::raw('WEEK(created_at) as week'), DB::raw('count(*) as entries'))
                        ->where(['parts'=>$request->campaign_id])

                      ->groupBy('week')
                      /*->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))*/
                      ->pluck('entries','week')->all();

                // Generate random colours for the groups
                for ($i=0; $i<=count($groupsWeek); $i++) {
                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartWeek = new Chart;
                $chartWeek->labels = (array_keys($groupsWeek));
                $chartWeek->dataset = (array_values($groupsWeek));
                $chartWeek->colours = $colours;


                $a = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"00:00 AM - 03:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','00:00:00')
                                ->whereTime('created_at','<=','03:00:00')
                                ->get();

                $b = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"03:00 AM - 06:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','03:00:00')
                                ->whereTime('created_at','<=','06:00:00')
                                ->get();
                $c = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"06:00 AM - 09:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','06:00:00')
                                ->whereTime('created_at','<=','09:00:00')
                                ->get();
                $d = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"09:00 AM - 12:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->where(['parts'=>$request->campaign_id])
                                ->whereTime('created_at','>=','09:00:00')
                                ->whereTime('created_at','<=','12:00:00')
                                ->get();

                $e= array($a[0]); 
                $f= array($b[0]);
                $g= array($c[0]);
                $h= array($d[0]);

                $time_chart = array_merge($e,$f,$g,$h);
               
                // Generate random colours for the groups
                for ($i=0; $i<=count($time_chart); $i++) {

                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartTime = new Chart;
                $chartTime->labels = (array_keys($time_chart));
                
                $chartTime->dataset = (array_values($time_chart));
                $chartTime->colours = $colours;

                $circleData = DB::table('sms_tbl_longcodes')->select(\DB::raw("COUNT(*) as count"), \DB::raw("circle as day_name"), \DB::raw("circle as day"))
                                ->where(['parts'=>$request->campaign_id])
                                ->where('created_at', '>', Carbon::today()->subDay(6))
                                ->groupBy('day_name')
                                ->orderBy('day')
                                ->get();
  
                $finalData = [];
         
                foreach($circleData as $row) {

                    $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    $finalData['label'][] = $row->day_name;
                    $finalData['data'][] = (int) $row->count;
                }
         
                $finalData['chart_data'] = json_encode($finalData);
                $finalData['colours'] = $colours;
                $one_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=1) tbl_lgcd');
                $one_time = $one_time[0]->cnt;
                $two_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=2) tbl_lgcd');
                $two_time = $two_time[0]->cnt;
                $three_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=3) tbl_lgcd');
                $three_time = $three_time[0]->cnt;
                $four_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=4) tbl_lgcd');
                $four_time = $four_time[0]->cnt;

                
                $circle_chart = json_encode(DB::select('SELECT `circle` as name , count(*) as circle FROM `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'"  group by circle '));
                
            }

            else{
              
                $totalEntries = LongCode::count();
                $totalValidEntries = LongCode::Where(['status'=>'valid'])->count();
                $totalInvalidEntries = LongCode::WhereIn('status',['INVALID','DUPLICATE','EXPIRED','REWARDOVER'])->count();
                $totalExcessTriesEntries = LongCode::Where(['status'=>'EXCESS'])->count();
                $totalUniqueEntries= LongCode::Where(['status'=>'valid'])->distinct('from')->count();


                $groups = DB::table('sms_tbl_longcodes')
                  ->select(DB::raw('DAYNAME(created_at) as day'), DB::raw('count(*) as day_entries'))
                  ->groupBy('day')
                  ->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))
                  ->pluck('day_entries','day')->all();

                // Generate random colours for the groups
                for ($i=0; $i<=count($groups); $i++) {
                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chart = new Chart;
                        $chart->labels = (array_keys($groups));
                        $chart->dataset = (array_values($groups));
                        $chart->colours = $colours;



                $groupsWeek = DB::table('sms_tbl_longcodes')
                      ->select(DB::raw('WEEK(created_at) as week'), DB::raw('count(*) as entries'))
                      ->groupBy('week')
                      /*->orderBy(DB::raw('FIELD(day, "SUNDAY","MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY")'))*/
                      ->pluck('entries','week')->all();

                // Generate random colours for the groups
                for ($i=0; $i<=count($groupsWeek); $i++) {
                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartWeek = new Chart;
                $chartWeek->labels = (array_keys($groupsWeek));
                $chartWeek->dataset = (array_values($groupsWeek));
                $chartWeek->colours = $colours;

                $circleData = DB::table('sms_tbl_longcodes')->select(\DB::raw("COUNT(*) as count"), \DB::raw("circle as day_name"), \DB::raw("circle as day"))
                                ->where('created_at', '>', Carbon::today()->subDay(6))
                                ->groupBy('day_name')
                                ->orderBy('day')
                                ->get();
  
                $finalData = [];
         
                foreach($circleData as $row) {

                    $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    $finalData['label'][] = $row->day_name;
                    $finalData['data'][] = (int) $row->count;
                }
         
                $finalData['chart_data'] = json_encode($finalData);
                $finalData['colours'] = $colours;

                $a = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"00:00 AM - 03:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->whereTime('created_at','>=','00:00:00')
                                ->whereTime('created_at','<=','03:00:00')
                                ->get();
                $b = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"03:00 AM - 06:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->whereTime('created_at','>=','03:00:00')
                                ->whereTime('created_at','<=','06:00:00')
                                ->get();
                $c = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"06:00 AM - 09:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->whereTime('created_at','>=','06:00:00')
                                ->whereTime('created_at','<=','09:00:00')
                                ->get();
                $d = DB::table('sms_tbl_longcodes')
                                ->select(DB::raw('"09:00 AM - 12:00 AM" as time'), DB::raw('count(*) as entries'))
                                ->whereTime('created_at','>=','09:00:00')
                                ->whereTime('created_at','<=','12:00:00')
                                ->get();

                $e= array($a[0]); 
                $f= array($b[0]);
                $g= array($c[0]);
                $h= array($d[0]);

                $time_chart = array_merge($e,$f,$g,$h);
               
                // Generate random colours for the groups
                for ($i=0; $i<=count($time_chart); $i++) {

                            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                        }



                // Prepare the data for returning with the view
                $chartTime = new Chart;
                $chartTime->labels = (array_keys($time_chart));
                
                $chartTime->dataset = (array_values($time_chart));
                $chartTime->colours = $colours;
                $one_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=1) tbl_lgcd');
                $one_time = $one_time[0]->cnt;
                $two_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=2) tbl_lgcd');
                $two_time = $two_time[0]->cnt;
                $three_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=3) tbl_lgcd');
                $three_time = $three_time[0]->cnt;
                $four_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'" GROUP BY `from` HAVING COUNT(`from`)=4) tbl_lgcd');
                $four_time = $four_time[0]->cnt;

                
                $circle_chart = json_encode(DB::select('SELECT `circle` as name , count(*) as circle FROM `sms_tbl_longcodes` WHERE status="VALID" AND parts="'.$request->campaign_id.'"  group by circle '));
                $campaignID = Campaign::where('campaign_keyword',$request->campaign_id)->first();

                $circleWiseEntries = LongCode::groupBy('circle')->get();
                $SKUWiseEntries = [];
                $batchWiseEntries = Voucher::groupBy('batch_name')->get();
                
                $OfferTypeWiseEntries  = Voucher::groupBy('offer_type_id')->get();
                $keywordWiseEntries  = Voucher::groupBy('campaign_id')->get();
                $totalSmsSentReport  = Outgoing::groupBy('field_slug_value')->get();
            }
            
           
            return view('sms.dashboard_filter',$finalData,compact('one_time','two_time','three_time','four_time','time_chart','circle_chart','chart','chartWeek','chartTime','totalEntries','totalValidEntries','totalInvalidEntries','totalExcessTriesEntries','totalUniqueEntries','circleWiseEntries','batchWiseEntries','OfferTypeWiseEntries','SKUWiseEntries','keywordWiseEntries','totalSmsSentReport','from_date','to_date','circleNewData','batchNewData','OfferTypeNewData','keywordWiseNewData','SKUNewData','totalSmsSentReportNewData'))->render();;
        }

        return Redirect::to("/")->withSuccess('Opps! You do not have access');

    }

 
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
     
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('/');
    }

    public function changeStatus(Request $request)
    {
        
        $model = app("App\SmsModels\\$request->model_name")->where('id',$request->id)->first();
       
        $model->status = $request->status;
        $model->save();
        
        if(!empty($model->campaign_keyword)){

            $dynamic_mail = '<p>Dear {{AdminName}},</p>

                            <p>Your campaign has been {{Status}} as per the below details:</p>

                            <p><strong>Here are your campaign details:</strong></p>

                            <p>{{CampaignDetails}}</p>

                            <p>&nbsp;</p>

                            <p>Regards<br />
                            Team BigCity</p>

                            <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                <tbody>
                                    <tr>
                                        <td>
                                        <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>';
            
            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                            <tr>
                                <td><strong>Campaign Id</strong></td>
                                <td><strong>Campaign Type</strong></td>
                                <td><strong>Campaign Keyword</strong></td>
                                <td><strong>Campaign Name</strong></td>
                                <td><strong>Created Date/Time</strong></td>
                                <td><strong>Updated Date/Time</strong></td>
                            </tr>';
            
            $codeMessage .= "<tr>
                                <td>".$model->id."</td>
                                <td>".$model->campaign_type['name']."</td>
                                <td>".$model->campaign_keyword."</td>
                                <td>".$model->campaign_name."</td>
                                <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                            </tr>";
            
            $codeMessage .= '</table></td></tr>';
    
            $replacements = [
                    "{{AdminName}}" => 'Admin',
                    "{{CampaignDetails}}" => $codeMessage,
                    "{{Status}}"  => ($model->status == "ACTIVE") ? "activate":"deactivate",
                    "{{emailId}}" => $model->status_trigger_rule,
                ];
    
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                  <tr>
                    <td align="center">
                    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                      <tr>
                        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                        
            $message .= strtr($dynamic_mail, $replacements);
            $message .= '</table></td></tr></table></td></tr></table>';
            $from = 'bigreg@bigcity.in';
            //$to = 'prashant@bigcity.in';
            
            $subject = "Campaign Notification";
            $to = $model->status_trigger_rule;

            $subject = "Campaign Notification";
            //dd(explode(',',str_replace(" ",$to)));
            
            foreach(explode(',',$to) as $email){

                //call helper function for sendign sms template
                Helper::sendEmail($email, $subject, $message, $from);
    
            }
            //call helper function for sendign sms template
           // Helper::sendEmail($to, $subject, $message, $from);
        }
        
        
        return response()->json(['success'=>'status change successfully.']);
    }
    
}
?>