<?php

namespace App\Http\Controllers\SMS\campaign;

use App\SmsModels\CashbackType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CashbackTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cashbackTypes = CashbackType::get();

        return view('sms.cashback_type.index')->with([
        'cashbackTypes'  => $cashbackTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sms.cashback_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:sms_tbl_offers',
            
            
        ]);

        CashbackType::create($validatedData);

        return redirect('admin/sms/cashbacktypes')->withSuccess('Cashback Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cashbackType = CashbackType::find($id);
        return view('sms.cashback_type.show',compact('cashbackType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $cashbackType = CashbackType::find($id);
       
        return view('sms.cashback_type.edit', compact('cashbackType'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'name'=>'required',
            
            
        ]);        
        $cashbackType = CashbackType::find($id);
        $cashbackType->name =  $request->get('name');
        $cashbackType->touch();        
        
        return redirect('/admin/sms/cashbacktypes')->with('success', 'Cashback Types updated!');
    }

    public function getCashbackTypeList()
    {
        $cashbacktypes = CashbackType::where(['status'=>'ACTIVE'])->pluck("name","id");

    
        return response()->json($cashbacktypes);
    }

}
