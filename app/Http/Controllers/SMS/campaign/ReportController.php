<?php

namespace App\Http\Controllers\SMS\campaign;


use App\SmsModels\LongCode;
use App\SmsModels\Outgoing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use DB;
use App\SmsModels\WhatsAppData;
use App\SmsModels\Campaign;
use App\Exports\AllOutgoingExport;
use App\Exports\AllIncomingExport;
use Excel;

class ReportController extends Controller
{
    

    
    public function incomingReportList(Request $request)
    {
        
        ini_set('memory_limit', '-1');
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = LongCode::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();

        return view('sms.report.incoming',compact('campaigns'));
        /*$incomingData = LongCode::get();
        
        return view('admin.report.incoming',compact('incomingData'));*/
    }

    
    public function outgoingReportList(Request $request)
    {
        ini_set('memory_limit', '-1');
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = Outgoing::with('incoming_msg')->whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = Outgoing::with('incoming_msg')->get();
                
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();
        
        return view('sms.report.outgoing',compact('campaigns'));
        
        
    }

    public function whatsappReportList(Request $request)
    {
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = WhatsAppData::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = WhatsAppData::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        
        
        return view('sms.report.whatsapp-data-list');
        
        
    }
    
    public function allOutgoingDownload(Request $request)
    {
        
        
        $outgoing_data = Outgoing::with('incoming_msg')->whereHas('incoming_msg', function($q) use ($request){
                                    $q->where('parts',$request->campaign_name);})
                                    ->whereBetween('created_at', array($request->from_date, $request->to_date))
                                    ->get()
                                    ->toArray();
      
        
            if(!empty($outgoing_data)){
              foreach($outgoing_data as $index=>$outgoing)
              {

                  $data[] = array(
                      
                     'SN'                    => ++$index,
                     'ID'           => $outgoing['id'],
                     'From'         => $outgoing['incoming_msg']['from'],
                     'incoming Message ID'         => $outgoing['incoming_msg']['id'],
                     'From'         => $outgoing['incoming_msg']['from'],
                     'Incoming Message'         => $outgoing['incoming_msg']['message'],
                     'Outgoing Message'         => $outgoing['outgoing_msg'],
                     
                      'Status'                => $outgoing['incoming_msg']['status'],

                     'Created Date & Time'   => date("Y-m-d", strtotime($outgoing['created_at'])).'&'.date("H:i", strtotime($outgoing['created_at'])),
                     'Updated Date & Time'   => date("Y-m-d", strtotime($outgoing['updated_at'])).'&'.date("H:i", strtotime($outgoing['updated_at'])),
                     
                  );
              }
            }else{


              $data[] = array(
                      
                     'SN'           => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'      => '',
                     'Outgoing Message'         => '',
                     
                      'Status'                => '',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );
              

            }
            ob_end_clean(); // this
            ob_start(); // and this
        return Excel::download(new AllOutgoingExport($data), 'outgoing.xlsx');
    }


    public function allIncomingDownload(Request $request)
    {
        

        //$campaigns = Campaign::where('id',$request->campaign_id)->first();

        $incoming_data_camp = LongCode::where('parts',$request->campaign_name)->get()->toArray();

        $incoming_data_date = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))->get()->toArray();

        $incoming_data = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))->where('parts',$request->campaign_name)->get()->toArray();
            

        
            if($request->campaign_name != '' && $request->from_date != '' && $request->from_date != ''){

                if(!empty($incoming_data)){

                  foreach($incoming_data as $index=>$incoming){

                    $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                    $data[] = array(
                        
                       'SN'           => ++$index,
                       'ID'           => $incoming['id'],
                       'From'         => $incoming['from'],
                       'Message'      => $incoming['message'],
                       'ts'           => $incoming['ts'],
                       'circle'       => $incoming['circle'],
                       'operator'     => $incoming['operator'],
                       'msgid'        => $incoming['msgid'],
                       'parts'        => $incoming['parts'],
                       'Message Delivered Status'  => $message_delivered_status,
                        'Status'      =>     $incoming['status'],

                       'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                       'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                       
                    );


                    }

                }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );


              }
              
                
            }elseif($request->campaign_name != '' ){

                if(!empty($incoming_data_camp) ){

                  foreach($incoming_data_camp as $index=>$incoming){

                    $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                    $data[] = array(
                        
                       'SN'           => ++$index,
                       'ID'           => $incoming['id'],
                       'From'         => $incoming['from'],
                       'Message'      => $incoming['message'],
                       'ts'           => $incoming['ts'],
                       'circle'       => $incoming['circle'],
                       'operator'     => $incoming['operator'],
                       'msgid'        => $incoming['msgid'],
                       'parts'        => $incoming['parts'],
                       'Message Delivered Status'  => $message_delivered_status,
                        'Status'      =>     $incoming['status'],

                       'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                       'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                       
                    );
                  }



                }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }
              
                
            }elseif($request->from_date != '' && $request->from_date != ''){
dd($incoming_data_date);
              if(!empty($incoming_data_date)){

                foreach($incoming_data_date as $index=>$incoming){

                  $message_delivered_status = ($incoming['is_read'] ='1') ? "Delivered":"Not Delivered";

                  $data[] = array(
                      
                     'SN'           => ++$index,
                     'ID'           => $incoming['id'],
                     'From'         => $incoming['from'],
                     'Message'      => $incoming['message'],
                     'ts'           => $incoming['ts'],
                     'circle'       => $incoming['circle'],
                     'operator'     => $incoming['operator'],
                     'msgid'        => $incoming['msgid'],
                     'parts'        => $incoming['parts'],
                     'Message Delivered Status'  => $message_delivered_status,
                      'Status'      =>     $incoming['status'],

                     'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                     'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                     
                  );

                }
              }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }
              
                
            }else{

              
                  $data[] = array(
                      
                     'SN'                    => '',
                     'ID'           => '',
                     'From'         => '',
                     'Message'         => '' ,
                     'ts'         => '',
                     'circle'         => '',
                     'operator'        => '',
                     'msgid'         => '' ,
                     'parts'        =>'',
                     'Message Delivered Status'         => '' ,
                      'Status'               =>'',

                     'Created Date & Time'   => '',
                     'Updated Date & Time'   => '',
                     
                  );

              }


              ob_end_clean(); // this
              ob_start(); // and this
              


            return Excel::download(new AllIncomingExport($data), 'cms_sms_incoming.xlsx');
            
            
    }

    public function issuePanelList(Request $request)
    {
        
        
        $data = Outgoing::with('incoming_msg')->paginate(10);

        $campaigns = Campaign::/*whereHas( 'longCode', function($query) {
    
        $query->groupBy('parts');})->*/get();
        
        return view('sms.issue.list',compact('campaigns','data'));
        
        
    }
    
    
    


    
}
