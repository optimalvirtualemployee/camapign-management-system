<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\SmsModels\Voucher;
use App\SmsModels\LongCode;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Carbon\Carbon;

class ApiController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkVoucher(Request $request)
    {

        
        

           
            $vcode = $request->query('vcode');

            $voucherData = Voucher::where('voucher_code',$vcode)->first();
            
            $currentDate = Carbon::now()->format('Y-m-d');
            $duplicateVoucherCount = LongCode::where('parts2' ,$vcode)->whereIn('status' ,['VALID'])->whereIn('is_read',['1'])->groupBy('parts2')->count();

            if(!empty($voucherData)){

                if($currentDate > $voucherData->end_date){

                    $data = collect(["vcode"=>$vcode,"expirydate" => $voucherData->end_date, "status" => false, "is_used" => false,"message"=>"success"]);

                    return response()->json($data, 200);

                }elseif($duplicateVoucherCount >= '1'){

                    $data = collect(["vcode"=>$vcode,"expirydate" => $voucherData->end_date, "status" => false, "is_used" => true,"message"=>"Already Booked"]);

                    return response()->json($data, 200);

                }else{

                    $data = collect(["vcode"=>$vcode,"expirydate" => $voucherData->end_date, "status" => false, "is_used" => true,"message"=>"Voucher Code Expired"]);

                    return response()->json($data, 200);
                }

                

            }else{

                $data = collect(["vcode"=>$vcode, "status" => false,"is_used" => true,"message"=>"Invalid"]);

                return response()->json($data, 200);

            }
            
        
        
    }

    
    
}
