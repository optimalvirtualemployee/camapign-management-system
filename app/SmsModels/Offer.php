<?php

namespace App\SmsModels;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    
    protected $table = 'sms_tbl_offers';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];


    //User  function
    public function cashback()
    {
        return $this->belongsTo('App\SmsModels\CashbackType','cashback_type_id','id');
    }

}
