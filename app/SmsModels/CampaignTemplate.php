<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;


class CampaignTemplate extends Model
{
    
    protected $table = 'sms_tbl_campaign_templates';
    protected $fillable = ['field_id','field_value','template_keyword_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    //Campaign Type  function
    public function campaign()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
