<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;


class CampaignRedirect extends Model
{
    
    protected $table = 'sms_tbl_campaign_redirects';
    protected $fillable = ['campaign_id','redirect_url'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\SmsModels\Campaigns','campaign_id');
    }
    
}
