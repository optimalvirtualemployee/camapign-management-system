<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class RewardCode extends Model
{
    protected $table = 'sms_tbl_reward_codes';
    protected $fillable = ['campaign_id','reward_code','offer_type','offer_type_slug'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
