<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;


class CampaignOfferTemplate extends Model
{
    
    protected $table = 'sms_tbl_campaign_offer_templates';
    protected $fillable = ['campaign_id','field_value','template_keyword_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
