<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class Outgoing extends Model
{
	
    protected $table = 'sms_tbl_outgoing_msg';
    protected $fillable = ['incoming_msg_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    public function scopeBetween($query, Carbon $from, Carbon $to)
    {
        $query->whereBetween('created_at', [$from, $to]);
    }

    //outgoing  function
    public function incoming_msg()
    {
        return $this->belongsTo('App\SmsModels\LongCode','incoming_msg_id');
    }
    //outgoing  function
    public function campaign()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
