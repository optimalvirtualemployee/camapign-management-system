<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Field extends Model
{
    use UserStampsTrait;
    protected $table = 'sms_tbl_fields';
    protected $fillable = ['module_name','field_name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //User  function
    public function field_type()
    {
        return $this->belongsTo('App\SmsModels\FieldType','field_type_id','id');
    }
    
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
    
}
