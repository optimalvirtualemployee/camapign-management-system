<?php

namespace App\SmsModels;
use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class FieldTemplate extends Model
{
	use UserStampsTrait;
    protected $table = 'field_template';
    protected $fillable = ['template_id','field_id','keyword','value','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function field()
    {
        return $this->belongsTo('App\SmsModels\Field','field_id');
    }

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
