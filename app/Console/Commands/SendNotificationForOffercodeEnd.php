<?php
 
namespace App\Console\Commands;
 
use Illuminate\Console\Command;
use DB;
use GuzzleHttp\Client;

use App\SmsModels\CampaignFieldValue;
use App\SmsModels\CampaignMobileUsageRule;
use App\SmsModels\CampaignTemplate;
use App\SmsModels\Template;
use App\SmsModels\Campaign;
use App\SmsModels\Voucher;
use App\SmsModels\Outgoing;
use Carbon\Carbon;
use Helper;
use App\SmsModels\RewardCode;
use App\SmsModels\CampaignOfferTemplate;
use App\SmsModels\MobileUsageRule;
use App\SmsModels\Offer;
use DateTime;


class SendNotificationForOffercodeEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendNotificationForOffercodeEnd:data';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification For Offer End Data';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $val = LongCode::whereIn('is_read',['0','2'])->where('id',$incoming_msg_id)->first();
      
        
        if(!empty($val)){
            
            
            $keyword = explode(" ",$val->message);
        
            /*$campaignData = CampaignFieldValue::where('field_value',$data[0])->where('campaign_field_id',7)->first();*/
            $campaignData = Campaign::where('campaign_keyword',$keyword[0])->first();
            
            
            if(!empty($campaignData)){
                
                
                $currentDate = Carbon::now()->format('Y-m-d');
                
                //get end date 
                $campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->where('campaign_field_id',env('NUMBER_OF_HOURS'))->first();
                 
                $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                
                $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                
                $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                
                $excessTriesCount = LongCode::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                
                
                //Campaign Status (Activate & Deactivate)
                $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                

                if(!empty($keyword[1])){

                    $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$keyword[1])->first();
                    
                    $duplicateVoucherCount = LongCode::where('parts2' ,$val->parts2)->whereIn('status' ,['VALID'])->whereIn('is_read',['1'])->groupBy('parts2')->count();

                    
                    $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('reward_code',$keyword[1])->first();

                    $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                    $BatchCodeData = BatchCode::where('campaign_id',$campaignData->id)->where('batch_code',$keyword[1])->first();
   
                }
                
                
            
               
                // Valid Vouchercode exist sms Template   
                
                
                if(!empty($offerData->field_value)){

        
                    $jsonnewdata = json_decode($offerData->field_value);
                    $newArray = [];
                    foreach ($jsonnewdata as $key => $value) {

                  
                        //checked offer id data
                        $offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->get();

                        $offerChecked = Offer::where('name', $val->parts3)->where('status','ACTIVE')->get();
                        
                        $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$name->name)->first();

                        $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('offer_type_slug',$checkedOfferId['parts3'])->where('isused','0')->get();
 
                        if(count($RewardData) == 0){
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
        
                                // Invalid Code vouchercode SMS Template
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);


                                if(!empty($model->campaign_keyword)){

                                $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                                <p>Your campaign has been {{Status}} as per the below details:</p>

                                                <p><strong>Here are your campaign details:</strong></p>

                                                <p>{{CampaignDetails}}</p>

                                                <p>&nbsp;</p>

                                                <p>Regards<br />
                                                Team BigCity</p>

                                                <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                                <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                            <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>';

                                $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                                <tr>
                                                    <td><strong>Campaign Id</strong></td>
                                                    <td><strong>Campaign Type</strong></td>
                                                    <td><strong>Campaign Keyword</strong></td>
                                                    <td><strong>Campaign Name</strong></td>
                                                    <td><strong>Created Date/Time</strong></td>
                                                    <td><strong>Updated Date/Time</strong></td>
                                                </tr>';

                                $codeMessage .= "<tr>
                                                    <td>".$model->id."</td>
                                                    <td>".$model->campaign_type['name']."</td>
                                                    <td>".$model->campaign_keyword."</td>
                                                    <td>".$model->campaign_name."</td>
                                                    <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                                    <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                                </tr>";

                                $codeMessage .= '</table></td></tr>';

                                $replacements = [
                                        "{{AdminName}}" => 'Admin',
                                        "{{CampaignDetails}}" => $codeMessage,
                                        "{{Status}}"  => ($model->status == "ACTIVE") ? "activate":"deactivate",
                                        "{{emailId}}" => $model->status_trigger_rule,
                                    ];

                                $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                                      <tr>
                                        <td align="center">
                                        <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                                          <tr>
                                            <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                            <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                            
                                $message .= strtr($dynamic_mail, $replacements);
                                $message .= '</table></td></tr></table></td></tr></table>';
                                $from = 'bigreg@bigcity.in';
                                //$to = 'prashant@bigcity.in';

                                $subject = "Campaign Notification";
                                $to = $model->status_trigger_rule;

                                $subject = "Campaign Notification";
                                //dd(explode(',',str_replace(" ",$to)));

                                foreach(explode(',',$to) as $email){

                                    //call helper function for sendign sms template
                                    Helper::sendEmail($email, $subject, $message, $from);

                                }
                                //call helper function for sendign sms template
                               // Helper::sendEmail($to, $subject, $message, $from);
                            }
        
                                
                        }
                        

                    }
                    
                    
                    
                }
                                
                    
                
                
            }
    
        }
        
        
        $this->info('Notification not send');

      
        
    }
    
    
}