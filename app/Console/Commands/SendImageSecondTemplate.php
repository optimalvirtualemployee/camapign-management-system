<?php
 
namespace App\Console\Commands;
use Illuminate\Console\Command;

use App\WhatsappModels\WhatsAppData;
use App\WhatsappModels\CampaignFieldValue;
use App\WhatsappModels\CampaignMobileUsageRule;
use App\Http\Controllers\Controller;
use App\WhatsappModels\CampaignTemplate;
use Illuminate\Http\Request;
use App\WhatsappModels\Template;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\Voucher;
use App\WhatsappModels\Outgoing;
use Carbon\Carbon;
use Helper;
use App\WhatsappModels\BatchCode;
use App\WhatsappModels\RewardCode;
use App\WhatsappModels\CampaignOfferTemplate;
use App\WhatsappModels\MobileUsageRule;
use App\WhatsappModels\Offer;
//use App\SmsModels\CampaignBatchcodeUsageRule;


class SendImageSecondTemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendimagesecondtemplate:data';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Image Second Template Data';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        
        $data = WhatsAppData::where('status','IMAGECONF')->where('image_view_status','APPROVED')->get();
/*dd($data);*/
        if(count($data) > 0){

            foreach ($data as $key => $val) {


                if(!empty($val)){
                    
                    if($val->type == 'text'){

                        $keyword = explode(" ",$val->body);


                    }else{

                        $keyword = explode(" ",$val->wanumber);

                    }
                    
                    

                    
                    $campaignData = Campaign::where('campaign_keyword',$keyword[0])->first();
                    
                    
                    if(!empty($campaignData)){
                        
                        
                        $currentDate = Carbon::now()->format('Y-m-d');
                        
                        //get NUMBER OF HOURS
                        $campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->where('campaign_field_id',env('NUMBER_OF_HOURS'))->first();
                        
                        $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                        
                        $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                        
                        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                        
                        $excessTriesCount = WhatsAppData::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                        
                        
                        //Campaign Status (Activate & Deactivate)
                        $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                        

                        if(!empty($keyword[1])){

                            $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$keyword[1])->first();
                            
                            $duplicateVoucherCount = WhatsAppData::where('parts2' ,$val->parts2)->whereIn('status' ,['VALID','SEND24HOUR'])->whereIn('is_read',['1','2'])->groupBy('parts2')->count();

                            $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                            //$BatchCodeData = Voucher::where('campaign_id',$campaignData->id)->where('offer_type_id',env('BATCH_CODE_OFFER_TYPE'))->where('voucher_code',$keyword[1])->first();
                            $BatchCodeData = BatchCode::where('campaign_id',$campaignData->id)->where('batch_code',$keyword[1])->first();
           

                            
                            
                        }
                        
                        //Campaign SMS Template
                        if(!empty($campaignStatus)){
                            
                            
                            
                            //Updating data
                            $longcodeRecordUpdated = WhatsAppData::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                            $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Promo_Over_SMS',
                                    'offerData'=>"",
                                    'status'=>"",
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'incoming_msg_id'=>$val->id);
                                   
                            //call helper function for sendign sms template
                            Helper::sendWhatsappTemplate($data);
                            
                        }

                        //Image confirmation Template
                        elseif(!empty($val->type == 'image') && $val->status != 'IMAGECONF'){
                            
                            
                           
                            //Updating data
                            $longcodeRecordUpdated = WhatsAppData::where('is_read','0')->where('from', $val->from)->update(['status' => 'IMAGECONF','is_read'=>'2']);


                            $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'WhatsApp_Image_Upload_Confirmation_Message',
                                    'offerData'=>"",
                                    'status'=>"",
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'incoming_msg_id'=>$val->id);
                                   
                            //call helper function for sendign sms template
                            Helper::sendWhatsappTemplate($data);
           
                            
                        }
                        elseif(!empty($val->type == 'image') && $val->status == 'IMAGECONF'){
                            
                           
                            if($val->image_view_status  != null){

                                if($val->image_view_status  == 'APPROVED'){

                                    
                                    //Updating data
                                    $longcodeRecordUpdated = WhatsAppData::where('is_read','2')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);


                                    $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Approved_Image_/_Bill_Message',
                                            'offerData'=>"",
                                            'status'=>"VALID",
                                            'offer_type_id'=>null,
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'incoming_msg_id'=>$val->id);
                                           
                                    //call helper function for sendign sms template
                                    Helper::sendWhatsappTemplate($data);

                                }else{

                                    //Updating data
                                    $longcodeRecordUpdated = WhatsAppData::where('is_read','2')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);


                                    $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Rejected_Image_/_Bill_Message',
                                            'offerData'=>"",
                                            'status'=>"VALID",
                                            'offer_type_id'=>null,
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'incoming_msg_id'=>$val->id);
                                           
                                    //call helper function for sendign sms template
                                    Helper::sendWhatsappTemplate($data);

                                }


                            }


                            
                            
                        }

                        //Before Promo SMS
                        elseif($currentDate < $startDate){
                            
                            
                            
                            //Updating data
                            $longcodeRecordUpdated = WhatsAppData::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                            $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Before_Promo_Start_Date_SMS',
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'offerData'=>"",
                                    'status'=>"",
                                    'incoming_msg_id'=>$val->id);
                                   
                            //call helper function for sendign sms template
                            Helper::sendWhatsappTemplate($data);
           
                        }
                        
                        //Promo Over SMS
                        elseif($currentDate > $endDate){
                            
                            //Updating data
                            $longcodeRecordUpdated = WhatsAppData::where('is_read','0')->where('from', $val->from)->update(['status' => 'EXPIRED','is_read'=>'1']);

                            $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Promo_Over_SMS',
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'offerData'=>"",
                                    'status'=>"",
                                    'incoming_msg_id'=>$val->id);
                                   
                            //call helper function for sendign sms template
                            Helper::sendWhatsappTemplate($data);

                            $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

                
                            //Notification to admin when campaign is now over
                            if(!empty($campaignFieldValue)){

                                $model = Campaign::where('id',$data['campaign_id'])->first();

                                $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                            <p>This promotion is now over as per the below details:</p>

                                            <p><strong>Here are your campaign details:</strong></p>

                                            <p>{{CampaignDetails}}</p>

                                            <p>&nbsp;</p>

                                            <p>Regards<br />
                                            Team BigCity</p>

                                            <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                        <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>';
                            
                                $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                            <tr>
                                                <td><strong>Campaign Id</strong></td>
                                                <td><strong>Campaign Type</strong></td>
                                                <td><strong>Campaign Keyword</strong></td>
                                                <td><strong>Campaign Name</strong></td>
                                                <td><strong>Start Date</strong></td>
                                                <td><strong>End Date</strong></td>
                                                <td><strong>Created Date/Time</strong></td>
                                                <td><strong>Updated Date/Time</strong></td>
                                            </tr>';
                            
                                $codeMessage .= "<tr>
                                                <td>".$model->id."</td>
                                                <td>".$model->campaign_type->name."</td>
                                                <td>".$model->campaign_keyword."</td>
                                                <td>".$model->campaign_name."</td>
                                                <td>".$model->start_date."</td>
                                                <td>".$model->end_date."</td>
                                                <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                                <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                            </tr>";
                            
                                $codeMessage .= '</table></td></tr>';
                    
                                $replacements = [
                                    "{{AdminName}}" => 'Admin',
                                    "{{CampaignDetails}}" => $codeMessage,
                                    
                                    "{{emailId}}" => $campaignFieldValue->field_value,
                                ];
                    
                                $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                                  <tr>
                                    <td align="center">
                                    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                                      <tr>
                                        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                        
                                $message .= strtr($dynamic_mail, $replacements);
                                $message .= '</table></td></tr></table></td></tr></table>';
                                $from = 'bigreg@bigcity.in';
                                $to = $campaignFieldValue->field_value;

                                $subject = "Campaign Notification";
                            
                                //call helper function for sendign sms template
                                Helper::sendEmail($to, $subject, $message, $from);
                                    
                            }
           
                        }
                        // Valid Vouchercode exist sms Template   
                        
                    }
            
                }
            

                return redirect()->back()->withSuccess('SMS has been sended successfully!');
            }
        }
        
        
        $this->info('Booking Data Not Found');

      
        
    }
    
    
}