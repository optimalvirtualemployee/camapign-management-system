<?php
 
namespace App\Console\Commands;
 
use Illuminate\Console\Command;
use DB;
use GuzzleHttp\Client;

use App\SmsModels\CampaignFieldValue;
use App\SmsModels\CampaignMobileUsageRule;
use App\SmsModels\CampaignTemplate;
use App\SmsModels\Template;
use App\SmsModels\Campaign;
use App\SmsModels\Voucher;
use App\SmsModels\Outgoing;
use Carbon\Carbon;
use Helper;
use App\SmsModels\RewardCode;
use App\SmsModels\CampaignOfferTemplate;
use App\SmsModels\MobileUsageRule;
use App\SmsModels\Offer;
use DateTime;


class SendNotificationForCampaignEnd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendNotificationForCampaignEnd:data';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification For Campaign End Data';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $now = Carbon::now()->toDateTimeString();
        $campaigns = Campaign::query()
            ->where(['send_alert' => 1])
            ->get();
        $data = [];
        foreach($campaigns as $key=>$campaign){
            $data[$key]['id'] = $campaign->id;
            $data[$key]['campaign_no'] = $campaign->campaign_no;
            $data[$key]['name'] = $campaign->name;
            $data[$key]['id_no_cnic'] = $campaign->id_no_cnic;
            $data[$key]['issue_date'] = $campaign->issue_date;
            $data[$key]['expiry_date'] = $campaign->expiry_date;
            $data[$key]['post_designation'] = $campaign->post_designation;
            $data[$key]['dob'] = $campaign->dob;

            $to = Carbon::createFromFormat('Y-m-d H:s:i', $now);
            $from =   Carbon::createFromFormat('Y-m-d H:s:i', $campaign->expiry_date);
            $data[$key]['now']   =   $now;
            
            if( Carbon::parse($campaign->expiry_date) > Carbon::now() ){
                $number_of_days = Carbon::parse(Carbon::now())->diffInDays($campaign->expiry_date);
                $data[$key]['days']   =    $number_of_days;
                $data[$key]['days_in_human']   =    Carbon::parse($campaign->expiry_date)->diffForHumans();
                if( $number_of_days == 7){
                    $data[$key]['alert_for_7_days'] = 'Alert will be sent now 7 days';
                }
                if( $number_of_days == 15){
                    $data[$key]['alert_for_15_days'] = 'Alert will be sent now 15 days';
                }
                if( $number_of_days == 30){
                    $data[$key]['alert_for_30_days'] = 'Alert will be sent now 30 days';
                }
            }else{
                $data[$key]['days']     =   0;
                $data[$key]['days_in_human']   =    Carbon::parse($campaign->expiry_date)->diffForHumans();
            }
        }
        $alert = [
            'campaign_nname'   =>  'ex12312312',
            'end_date'   =>  '01-11-2020',
            
        ];
        $this->sendEmailToAdmin($alert);
    }

    private function sendEmailToAdmin($alerts){
        $alerts = [
            'campaign_nname'   =>  'ex12312312',
            'end_date'   =>  '01-11-2020',
        ];
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

            
        //Notification to admin when campaign is now over
        if(!empty($campaignFieldValue)){

            $model = Campaign::where('id',$val['campaign_id'])->first();

            $dynamic_mail = '<p>Dear {{AdminName}},</p>

                        <p>This promotion is now over as per the below details:</p>

                        <p><strong>Here are your campaign details:</strong></p>

                        <p>{{CampaignDetails}}</p>

                        <p>&nbsp;</p>

                        <p>Regards<br />
                        Team BigCity</p>

                        <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                            <tbody>
                                <tr>
                                    <td>
                                    <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>';
        
            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                        <tr>
                            <td><strong>Campaign Id</strong></td>
                            <td><strong>Campaign Type</strong></td>
                            <td><strong>Campaign Keyword</strong></td>
                            <td><strong>Campaign Name</strong></td>
                            <td><strong>Start Date</strong></td>
                            <td><strong>End Date</strong></td>
                            <td><strong>Created Date/Time</strong></td>
                            <td><strong>Updated Date/Time</strong></td>
                        </tr>';
        
            $codeMessage .= "<tr>
                            <td>".$val->id."</td>
                            <td>".$val->campaign_type->name."</td>
                            <td>".$val->campaign_keyword."</td>
                            <td>".$val->campaign_name."</td>
                            <td>".$val->start_date."</td>
                            <td>".$val->end_date."</td>
                            <td>".$val->created_at->format('d M Y')." & ".$val->created_at->format('g:i A')."</td>
                            <td>".$val->updated_at->format('d M Y')." & ".$val->updated_at->format('g:i A')."</td>
                        </tr>";
        
            $codeMessage .= '</table></td></tr>';

            $replacements = [
                        "{{AdminName}}" => 'Admin',
                        "{{CampaignDetails}}" => $codeMessage,
                        "{{Status}}"  => ($val->status == "ACTIVE") ? "activate":"deactivate",
                        "{{emailId}}" => $val->status_trigger_rule,
                    ];
        
                $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                      <tr>
                        <td align="center">
                        <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                          <tr>
                            <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                            <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                            
                $message .= strtr($dynamic_mail, $replacements);
                $message .= '</table></td></tr></table></td></tr></table>';
                $from = 'bigreg@bigcity.in';
                //$to = 'prashant@bigcity.in';
                
                $subject = "Campaign Notification";
                $to = $val->status_trigger_rule;

                $subject = "Campaign Notification";
                //dd(explode(',',str_replace(" ",$to)));
                
                foreach(explode(',',$to) as $email){

                    //call helper function for sendign sms template
                    Helper::sendEmail($email, $subject, $message, $from);
        
                }
                
        }

        DB::table('tbl_test')->insert(
            ['data_text' => 'test data']
        );
        print_r($alerts);
    }
    
    
    
}