<?php

namespace App\Http\Controllers\SMS\campaign;


use App\SmsModels\LongCode;
use App\SmsModels\CampaignFieldValue;
use App\SmsModels\CampaignMobileUsageRule;
use App\Http\Controllers\Controller;
use App\SmsModels\CampaignTemplate;
use Illuminate\Http\Request;
use App\SmsModels\Template;
use App\SmsModels\Campaign;
use App\SmsModels\Voucher;
use App\SmsModels\Outgoing;
use Carbon\Carbon;
use Helper;
use App\SmsModels\RewardCode;
use App\SmsModels\CampaignOfferTemplate;
use App\SmsModels\MobileUsageRule;
use App\SmsModels\Offer;
class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $campaign_temlate = CampaignTemplate::where('campaign_id',$campaign_id)->get();

        return view('sms.sms_template.index')->with([
        'campaign_temlate'  => $campaign_temlate,
        'campaign_id'  => $campaign_id
        ]);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($campaign_id,$template_id)
    {


        
        $templates = Template::where('id',$template_id)->where('status','ACTIVE')->first();
        
        $field_template = $templates->field_template()->where('panel_name', 'SMS')->get();

        $campaign_template = CampaignTemplate::where('campaign_id',$campaign_id)->get();
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->where('campaign_field_id',env('NUMBER_OF_OFFER_TEMPLATE'))->first();
       
        return view('sms.sms_template.create',compact('campaign_id','templates','template_id','campaign_template','campaignFieldValue','field_template'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$campaign_id,$template_id)
    {
        $fieldvalue          = $request->field_value;
        $field_id            = $request->field_key;
        $field_slug            = $request->field_slug;
        $template_keyword_id = $request->template_keyword_id;
        $template_entity_id  = $request->template_entity_id;
        
        $deleteTemplateFieldListRecords    = CampaignTemplate::where('campaign_id',$campaign_id)->delete();
        
        
        if(!empty($fieldvalue)){
    
            foreach ($fieldvalue as $i => $val) {
                
                
                    
                $campaign_template = new CampaignTemplate;
                $campaign_template->campaign_id = $campaign_id;
                $campaign_template->field_id = $field_id[$i];

                if(!empty($val)){

                    $campaign_template->field_value = $val;

                }else{

                    $campaign_template->field_value = 'NA';

                }
                $campaign_template->field_slug = $field_slug[$i];
                $campaign_template->template_keyword_id = $template_keyword_id[$i];
                $campaign_template->template_entity_id = $template_entity_id[$i];
                $campaign_template->save();
                    
                
                
            }   
            
            return redirect()->back()->withSuccess('SMS has been sended successfully!');
            
        }


    
    }
    
    public function sendSmsTemplate($incoming_msg_id="59516")
    {
        
        $val = LongCode::whereIn('is_read',['0','2'])->where('id',$incoming_msg_id)->first();
        
        /*$val2 = LongCode::where('partss','happydent')->where('from','918826592901')->first();
        
        dd($val2);*/

        if(!empty($val)){
            
            
            $keyword = explode(" ",$val->message);
        
            /*$campaignData = CampaignFieldValue::where('field_value',$data[0])->where('campaign_field_id',7)->first();*/
            $campaignData = Campaign::where('campaign_keyword',$keyword[0])->first();
            

            
            if(!empty($campaignData)){
                
                
                $currentDate = Carbon::now()->format('Y-m-d');
                
                //get end date 
                $campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->where('campaign_field_id',env('NUMBER_OF_HOURS'))->first();
                 
                $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                
                $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                
                $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                
                $excessTriesCount = LongCode::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                
                
                //Campaign Status (Activate & Deactivate)
                $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                

                if(!empty($keyword[1])){

                    $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$keyword[1])->first();
                    
                    $duplicateVoucherCount = LongCode::where('parts2' ,$val->parts2)->whereIn('status' ,['VALID'])->whereIn('is_read',['1'])->groupBy('parts2')->count();

                    $duplicateVoucherForSecondSmsCount = LongCode::where('parts2' ,$val->parts2)->whereIn('status' ,['SEND24HOUR'])->whereIn('is_read',['2'])->groupBy('parts2')->count();
                    
                    
                    $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('reward_code',$keyword[1])->first();

                    $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                    $BatchCodeData = Voucher::where('campaign_id',$campaignData->id)->where('offer_type_id',env('BATCH_CODE_OFFER_TYPE'))->where('voucher_code',$keyword[1])->first();

                    
   
                }
                
                //Campaign SMS Template
                if(!empty($campaignStatus)){
                    
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Promo_Over_SMS',
                            'offerData'=>"",
                            'status'=>"",
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);
                    
                }

                //Before Promo SMS
                elseif($currentDate < $startDate){
                    
                    
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Before_Promo_Start_Date_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);

                }
                
                //Promo Over SMS
                elseif($currentDate > $endDate){
                    
                    
                   
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'EXPIRED','is_read'=>'1']);


                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Promo_Over_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);

                    // $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

        
                    //Notification to admin when campaign is now over
                    if(!empty($campaignFieldValue)){

                        $model = Campaign::where('id',$data['campaign_id'])->first();

                        $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                    <p>This promotion is now over as per the below details:</p>

                                    <p><strong>Here are your campaign details:</strong></p>

                                    <p>{{CampaignDetails}}</p>

                                    <p>&nbsp;</p>

                                    <p>Regards<br />
                                    Team BigCity</p>

                                    <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>';
                    
                        $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                    <tr>
                                        <td><strong>Campaign Id</strong></td>
                                        <td><strong>Campaign Type</strong></td>
                                        <td><strong>Campaign Keyword</strong></td>
                                        <td><strong>Campaign Name</strong></td>
                                        <td><strong>Start Date</strong></td>
                                        <td><strong>End Date</strong></td>
                                        <td><strong>Created Date/Time</strong></td>
                                        <td><strong>Updated Date/Time</strong></td>
                                    </tr>';
                    
                        $codeMessage .= "<tr>
                                        <td>".$model->id."</td>
                                        <td>".$model->campaign_type->name."</td>
                                        <td>".$model->campaign_keyword."</td>
                                        <td>".$model->campaign_name."</td>
                                        <td>".$model->start_date."</td>
                                        <td>".$model->end_date."</td>
                                        <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                        <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                    </tr>";
                    
                        $codeMessage .= '</table></td></tr>';
            
                        $replacements = [
                            "{{AdminName}}" => 'Admin',
                            "{{CampaignDetails}}" => $codeMessage,
                            
                            "{{emailId}}" => $campaignFieldValue->field_value,
                        ];
            
                        $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                          <tr>
                            <td align="center">
                            <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                              <tr>
                                <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                
                        $message .= strtr($dynamic_mail, $replacements);
                        $message .= '</table></td></tr></table></td></tr></table>';
                        $from = 'bigreg@bigcity.in';
                        $to = $campaignFieldValue->field_value;

                        $subject = "Campaign Notification";
                    
                        //call helper function for sendign sms template
                        Helper::sendEmail($to, $subject, $message, $from);
                            
                        }

                        
                      
                    
                }
            
               
                // Valid Vouchercode exist sms Template   
                
                elseif(!empty($VoucherData) && !empty($campaignData->id)){
                    

                    //Excess Tries SMS Template
                    if($duplicateVoucherCount >= '1' || $duplicateVoucherForSecondSmsCount >= '1'){
                    
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                            
                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'offerData'=>"",
                                'status'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);

                      
                      
                    } 
                    
                    elseif($this->validateMobilePerPromotion($campaignData->id,$val->from,$val->id,$val->parts,$val->parts2) ==false){
                        
                        // Valid SMS (TN) SMS Template
                        if($val->circle == "Tamil Nadu"){

                            //If the reward code gets over
                            if($currentDate > $endDate){
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);
                                    
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Valid_SMS_(TN)_If_the_reward_code_gets_over',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                                  
                                
                            }elseif(!empty($offerData->field_value)){
    
                        
                                $jsonnewdata = json_decode($offerData->field_value);
        
                                foreach ($jsonnewdata as $key => $value) {
        
                               
                                    //checked offer id data
                                    $offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->first();
                                    
                                    $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$offer->name)->first();
             
                                    if(!empty($checkedOfferId)){
        
                                        //Updating data
                                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
        
        
                                        $data = array(
                                                'campaign_id'     =>$campaignData->id,
                                                'offerData'=>$value,
                                                'status'=>"VALID",
                                                'from'            =>$val->from,
                                                'campaign'            =>$val->parts,
                                                'code'            =>$val->parts2,
                                                'incoming_msg_id'=>$val->id);
                                               
                                        //call helper function for sendign sms template
                                        Helper::sendSmsTemplate($data);
        
                                    }
                                }
                            }
                            else{
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);

                                $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Valid_SMS_(TN)',
                                'offerData'=>"",
                                'status'=>"VALID",
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'incoming_msg_id'=>$val->id );
                               
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                            }
                            
                            
                        }else{

                            /*$url = 'https://budspacerewards.com/api/checkvoucher';
                            
                            $sms_data = array(
                              'vcode'        => $keyword[1], 
                            
                              
                            );


                            $username = 'budsrewardsAPI';
                            $password = '5sPkxFecZqBUL4pJG5yupXasyUt8y6';
                            
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL,$url);
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $sms_data);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $result = curl_exec($ch);
                            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                            curl_close($ch);
                            $resultArr = json_decode($result, true);

                            if($resultArr['status'] == true){*/

                                if(!empty($offerData->field_value)){
    
                        
                                    $jsonnewdata = json_decode($offerData->field_value);
                                    $newArray = [];
                                    foreach ($jsonnewdata as $key => $value) {
            
                                  
                                        //checked offer id data
                                        //$offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->get();

                                        $offerChecked = Offer::where('name', $val->parts3)->where('status','ACTIVE')->get();
                                        
                                        $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$value->offer_name_slug)->first();


                                        //checked offer id data
                                        $offer = Offer::where('id', $VoucherData->offer_type_id)->where('status','ACTIVE')->first();


                                        //$offerChecked = Offer::where('name', $val->parts3)->where('status','ACTIVE')->get();
                                        
                                        $arr = explode(' ',trim($offer->name));
                                
                                        $upadtedLongCodeData = LongCode::where('id',$val['id'])->update(['field_slug_value'=>$value->offer_name_slug]);

                                        $checkedVoucherOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('field_slug_value',preg_replace('/(\s|&(amp;)?|\.)+/', '_', $offer->name))->first();
//dd($checkedVoucherOfferId);
                                        if(!empty($checkedVoucherOfferId)){
            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
            
            
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>$value->offer_name_slug,
                                                    'offerData'=>$value,
                                                    'status'=>"VALID",
                                                    'offer_type_id'=>$value->offer_type_id,
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'incoming_msg_id'=>$val->id);
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendVoucherRechargeBasedSmsTemplate($data);
                                            
            
                                        }
                                        /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('offer_type_slug',$checkedOfferId['parts3'])->where('isused','0')->get();
                 
                                        if(count($RewardData) == 0){
                                                
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                        
                                                // Invalid Code vouchercode SMS Template
                                                $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'offerData'=>"",
                                                        'status'=>"",
                                                        'incoming_msg_id'=>$val->id );
                                                       
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                                                
                                        }else*/if(!empty($checkedOfferId)){
            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
            
            
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>$value->offer_name_slug,
                                                    'offerData'=>$value,
                                                    'status'=>"VALID",
                                                    'offer_type_id'=>$value->offer_type_id,
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'incoming_msg_id'=>$val->id);
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
            
                                        }/*elseif(isset($value->offer_name_slug) && count($offerChecked) == 0){
                                            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);
                    
                                            // Invalid Code vouchercode SMS Template
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>'Invalid_Code_SMS',
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'offerData'=>"",
                                                    'status'=>"",
                                                    'incoming_msg_id'=>$val->id );
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
                                        }*/
                                        

                                    }
                                    
                                    //dd($newArray);
                                    
                                }
                                else{
                                    

                                    /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('isused','0')->get();
                                    
                                    if(count($RewardData) == 0){
                                            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                    
                                            // Invalid Code vouchercode SMS Template
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'offerData'=>"",
                                                    'status'=>"",
                                                    'incoming_msg_id'=>$val->id );
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
                                            
                                    }else{*/

                                        if($campaignEndDateFieldValue == null){

                                            

                                            $status = "VALID";
                                            $is_read = "1";

                                        }else{


                                            $currentTime = Carbon::now()->format('H:i');
                                            $addTime =  Carbon::now()->addHour($campaignEndDateFieldValue->field_value)->format('H:i');
                                            
                                            if(strtotime($currentTime) == strtotime($addTime)){
                                              
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','2')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
                
                
                                                // Valid SMS (ROI) SMS Template
                                                $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>'Valid_SMS_2_(24_Hours)',
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'offerData'=>"",
                                                    'status'=>'VALID',
                                                    'offer_type_id'=>null,
                                                    'incoming_msg_id'=>$val->id );
                                               
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                                            }

                                            $status = "SEND24HOUR";
                                            $is_read = "2";


                                        }

                                        //Updating data
                                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => $status,'is_read'=>$is_read]);
        
        
                                        // Valid SMS (ROI) SMS Template
                                        $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Valid_SMS_(ROI)',
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'offerData'=>"",
                                            'status'=>$status,
                                            'offer_type_id'=>null,
                                            'incoming_msg_id'=>$val->id );
                                       
                                        //call helper function for sendign sms template
                                        Helper::sendSmsTemplate($data);
                                    /*}*/
                                      
                                }
                                
                            /*}elseif($resultArr['is_used'] == true){

                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                            
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id);
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);


                            }else{

                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                                // Invalid Code vouchercode SMS Template
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Invalid_Code_SMS',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);  

                            }*/
                            
                        }
                    }
                    
                
                }else{
              
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                    // Invalid Code vouchercode SMS Template
                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Invalid_Code_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id );
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);  
                    
                }
            }
    
        }
        

        return redirect()->back()->withSuccess('SMS has been sended successfully!');

      
        
    }

    public function sendSmsSecondTemplate()
    {
        
        $data = LongCode::where('is_read','2')->where('id','49551')->where('status','SEND24HOUR')->get();

        
        foreach ($data as $key => $val) {


            if(!empty($val)){
            
                
                $keyword = explode(" ",$val->message);
            
                /*$campaignData = CampaignFieldValue::where('field_value',$data[0])->where('campaign_field_id',7)->first();*/
                $campaignData = Campaign::where('campaign_keyword',$keyword[0])->first();
                
                
                if(!empty($campaignData)){
                    
                    
                    $currentDate = Carbon::now()->format('Y-m-d');
                    
                    //get end date 
                    $campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->where('campaign_field_id',env('NUMBER_OF_HOURS'))->first();
                     
                    $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                    
                    $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                    
                    $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                    
                    $excessTriesCount = LongCode::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                    
                    
                    //Campaign Status (Activate & Deactivate)
                    $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                    

                    if(!empty($keyword[1])){

                        $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$keyword[1])->first();
                        
                        $duplicateVoucherCount = LongCode::where('parts2' ,$val->parts2)->whereIn('status' ,['VALID'])->whereIn('is_read',['1'])->groupBy('parts2')->count();
                        
                        
                        $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('reward_code',$keyword[1])->first();

                        $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                        $BatchCodeData = Voucher::where('campaign_id',$campaignData->id)->where('offer_type_id',env('BATCH_CODE_OFFER_TYPE'))->where('voucher_code',$keyword[1])->first();
       
                    }
                    
                    //Campaign SMS Template
                    /*if(!empty($campaignStatus)){
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Promo_Over_SMS',
                                'offerData'=>"",
                                'status'=>"",
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);
                        
                    }*/

                    //Before Promo SMS
                    /*elseif($currentDate < $startDate){
                        
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Before_Promo_Start_Date_SMS',
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'offerData'=>"",
                                'status'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);

                    }*/
                    
                    //Promo Over SMS
                    /*elseif($currentDate > $endDate){
                        
                        
                       
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'EXPIRED','is_read'=>'1']);


                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Promo_Over_SMS',
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'offerData'=>"",
                                'status'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);

                        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

            
                        //Notification to admin when campaign is now over
                        if(!empty($campaignFieldValue)){

                            $model = Campaign::where('id',$data['campaign_id'])->first();

                            $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                        <p>This promotion is now over as per the below details:</p>

                                        <p><strong>Here are your campaign details:</strong></p>

                                        <p>{{CampaignDetails}}</p>

                                        <p>&nbsp;</p>

                                        <p>Regards<br />
                                        Team BigCity</p>

                                        <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                    <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>';
                        
                            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                        <tr>
                                            <td><strong>Campaign Id</strong></td>
                                            <td><strong>Campaign Type</strong></td>
                                            <td><strong>Campaign Keyword</strong></td>
                                            <td><strong>Campaign Name</strong></td>
                                            <td><strong>Start Date</strong></td>
                                            <td><strong>End Date</strong></td>
                                            <td><strong>Created Date/Time</strong></td>
                                            <td><strong>Updated Date/Time</strong></td>
                                        </tr>';
                        
                            $codeMessage .= "<tr>
                                            <td>".$model->id."</td>
                                            <td>".$model->campaign_type->name."</td>
                                            <td>".$model->campaign_keyword."</td>
                                            <td>".$model->campaign_name."</td>
                                            <td>".$model->start_date."</td>
                                            <td>".$model->end_date."</td>
                                            <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                            <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                        </tr>";
                        
                            $codeMessage .= '</table></td></tr>';
                
                            $replacements = [
                                "{{AdminName}}" => 'Admin',
                                "{{CampaignDetails}}" => $codeMessage,
                                
                                "{{emailId}}" => $campaignFieldValue->field_value,
                            ];
                
                            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                              <tr>
                                <td align="center">
                                <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                                  <tr>
                                    <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                    <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                    
                            $message .= strtr($dynamic_mail, $replacements);
                            $message .= '</table></td></tr></table></td></tr></table>';
                            $from = 'bigreg@bigcity.in';
                            $to = $campaignFieldValue->field_value;

                            $subject = "Campaign Notification";
                        
                            //call helper function for sendign sms template
                            Helper::sendEmail($to, $subject, $message, $from);
                                
                            }

                            
                          
                        
                    }*/
                
                   
                    // Valid Vouchercode exist sms Template   
                    
                    if(!empty($VoucherData)){
                        

                        //Excess Tries SMS Template
                        /*if($duplicateVoucherCount >= '1'){
                        
                            //Updating data
                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                                
                            $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'offerData'=>"",
                                    'status'=>"",
                                    'incoming_msg_id'=>$val->id);
                                   
                            //call helper function for sendign sms template
                            Helper::sendSmsTemplate($data);
                          
                          
                        } */
                        
                        /*elseif($this->validateMobilePerPromotion($campaignData->id,$val->from,$val->id,$val->parts,$val->parts2) ==false){*/
                            
                            // Valid SMS (TN) SMS Template
                            if($val->circle == "Tamil Nadu"){

                                //If the reward code gets over
                                if($currentDate > $endDate){
                                    
                                    
                                    
                                    //Updating data
                                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);
                                        
                                    $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Valid_SMS_(TN)_If_the_reward_code_gets_over',
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'offerData'=>"",
                                            'status'=>"",
                                            'incoming_msg_id'=>$val->id );
                                           
                                    //call helper function for sendign sms template
                                    Helper::sendSmsTemplate($data);
                                      
                                    
                                }elseif(!empty($offerData->field_value)){
        
                            
                                    $jsonnewdata = json_decode($offerData->field_value);
            
                                    foreach ($jsonnewdata as $key => $value) {
            
                                   
                                        //checked offer id data
                                        $offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->first();
                                        
                                        $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$offer->name)->first();
                 
                                        if(!empty($checkedOfferId)){
            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
            
            
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'offerData'=>$value,
                                                    'status'=>"VALID",
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'incoming_msg_id'=>$val->id);
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
            
                                        }
                                    }
                                }
                                else{
                                    
                                    
                                    
                                    //Updating data
                                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);

                                    $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Valid_SMS_(TN)',
                                    'offerData'=>"",
                                    'status'=>"VALID",
                                    'from'            =>$val->from,
                                    'campaign'            =>$val->parts,
                                    'code'            =>$val->parts2,
                                    'incoming_msg_id'=>$val->id );
                                   
                                    //call helper function for sendign sms template
                                    Helper::sendSmsTemplate($data);
                                }
                                
                                
                            }else{

                                /*$url = 'https://budspacerewards.com/api/checkvoucher';
                                
                                $sms_data = array(
                                  'vcode'        => $keyword[1], 
                                
                                  
                                );


                                $username = 'budsrewardsAPI';
                                $password = '5sPkxFecZqBUL4pJG5yupXasyUt8y6';
                                
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL,$url);
                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $sms_data);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                $result = curl_exec($ch);
                                $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                                curl_close($ch);
                                $resultArr = json_decode($result, true);

                                if($resultArr['status'] == true){*/

                                    if(!empty($offerData->field_value)){
        
                            
                                        $jsonnewdata = json_decode($offerData->field_value);
                                        $newArray = [];
                                        foreach ($jsonnewdata as $key => $value) {
                
                                      
                                            //checked offer id data
                                            $offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->get();

                                            $offerChecked = Offer::where('name', $val->parts3)->where('status','ACTIVE')->get();
                                            
                                            $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$name->name)->first();

                                            /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('offer_type_slug',$checkedOfferId['parts3'])->where('isused','0')->get();
                     
                                            if(count($RewardData) == 0){
                                                    
                                                    //Updating data
                                                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                            
                                                    // Invalid Code vouchercode SMS Template
                                                    $data = array(
                                                            'campaign_id'     =>$campaignData->id,
                                                            'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                            'from'            =>$val->from,
                                                            'campaign'            =>$val->parts,
                                                            'code'            =>$val->parts2,
                                                            'offerData'=>"",
                                                            'status'=>"",
                                                            'incoming_msg_id'=>$val->id );
                                                           
                                                    //call helper function for sendign sms template
                                                    Helper::sendSmsTemplate($data);
                                                    
                                            }else*/if(!empty($checkedOfferId)){
                
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
                
                
                                                $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'offerData'=>$value,
                                                        'status'=>"VALID",
                                                        'offer_type_id'=>$value->offer_type_id,
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'incoming_msg_id'=>$val->id);
                                                       
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                
                                            }/*elseif(isset($value->offer_name_slug) && count($offerChecked) == 0){
                                                
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);
                        
                                                // Invalid Code vouchercode SMS Template
                                                $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'field_slug_value'=>'Invalid_Code_SMS',
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'offerData'=>"",
                                                        'status'=>"",
                                                        'incoming_msg_id'=>$val->id );
                                                       
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                                            }*/
                                            

                                        }
                                        
                                        //dd($newArray);
                                        
                                    }
                                    else{


                                        


                                        /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('isused','0')->get();
                                        
                                        if(count($RewardData) == 0){
                                                
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                        
                                                // Invalid Code vouchercode SMS Template
                                                $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'offerData'=>"",
                                                        'status'=>"",
                                                        'incoming_msg_id'=>$val->id );
                                                       
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                                                
                                        }else{*/

                                            if($campaignEndDateFieldValue == null){

                                                

                                                $status = "VALID";
                                                $is_read = "1";

                                            }else{


                                                $currentTime = Carbon::now()->format('H:i');
                                                
                                                $addTime =  $val->created_at->addHour($campaignEndDateFieldValue->field_value)->format('H:i');
                                                
                                                if(strtotime($currentTime) >= strtotime($addTime)){
                                                  
                                                    //Updating data
                                                    $longcodeRecordUpdated = LongCode::where('is_read','2')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
                    
                    
                                                    // Valid SMS (ROI) SMS Template
                                                    $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'field_slug_value'=>'Valid_SMS_2_(24_Hours)',
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'offerData'=>"",
                                                        'status'=>'VALID',
                                                        'offer_type_id'=>null,
                                                        'incoming_msg_id'=>$val->id );
                                                   
                                                    //call helper function for sendign sms template
                                                    Helper::sendSmsTemplate($data);
                                                }

                                                $status = "SEND24HOUR";
                                                $is_read = "2";


                                            }


                                            //Updating data
                                            //$longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => $status,'is_read'=>$is_read]);
            
            
                                            // Valid SMS (ROI) SMS Template
                                            /*$data = array(
                                                'campaign_id'     =>$campaignData->id,
                                                'field_slug_value'=>'Valid_SMS_(ROI)',
                                                'from'            =>$val->from,
                                                'campaign'            =>$val->parts,
                                                'code'            =>$val->parts2,
                                                'offerData'=>"",
                                                'status'=>$status,
                                                'offer_type_id'=>null,
                                                'incoming_msg_id'=>$val->id );
                                           
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);*/
                                        /*}*/
                                          
                                    }
                                    
                                /*}elseif($resultArr['is_used'] == true){

                                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                                
                                    $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'offerData'=>"",
                                            'status'=>"",
                                            'incoming_msg_id'=>$val->id);
                                           
                                    //call helper function for sendign sms template
                                    Helper::sendSmsTemplate($data);


                                }else{

                                    //Updating data
                                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                                    // Invalid Code vouchercode SMS Template
                                    $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Invalid_Code_SMS',
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'offerData'=>"",
                                            'status'=>"",
                                            'incoming_msg_id'=>$val->id );
                                           
                                    //call helper function for sendign sms template
                                    Helper::sendSmsTemplate($data);  

                                }*/
                                
                            }
                        /*}*/
                        
                    
                    }
                }
    
            }
        

            return redirect()->back()->withSuccess('SMS has been sended successfully!');
        }
        
        

      
        
    }

    public function sendVoucherRechargeBasedTemplate($incoming_msg_id="")
    {
        
        $val = LongCode::whereIn('is_read',['0','2'])->where('id',$incoming_msg_id)->first();
      
        
        if(!empty($val)){
            
            
            $keyword = explode(" ",$val->message);
        
            /*$campaignData = CampaignFieldValue::where('field_value',$data[0])->where('campaign_field_id',7)->first();*/
            $campaignData = Campaign::where('campaign_keyword',$keyword[0])->first();
            
            
            if(!empty($campaignData)){
                
                
                $currentDate = Carbon::now()->format('Y-m-d');
                
                //get end date 
                $campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->where('campaign_field_id',env('NUMBER_OF_HOURS'))->first();
                 
                $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                
                $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                
                $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                
                $excessTriesCount = LongCode::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                
                
                //Campaign Status (Activate & Deactivate)
                $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                

                if(!empty($keyword[1])){

                    $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$keyword[1])->first();
                    
                    $duplicateVoucherCount = LongCode::where('parts2' ,$val->parts2)->whereIn('status' ,['VALID'])->whereIn('is_read',['1'])->groupBy('parts2')->count();
                    
                    
                    $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('reward_code',$keyword[1])->first();

                    $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                    $BatchCodeData = Voucher::where('campaign_/id',$campaignData->id)->where('offer_type_id',env('BATCH_CODE_OFFER_TYPE'))->where('voucher_code',$keyword[1])->first();
   
                }
                
                //Campaign SMS Template
                if(!empty($campaignStatus)){
                    
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Promo_Over_SMS',
                            'offerData'=>"",
                            'status'=>"",
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);
                    
                }

                //Before Promo SMS
                elseif($currentDate < $startDate){
                    
                    
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Before_Promo_Start_Date_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);

                }
                
                //Promo Over SMS
                elseif($currentDate > $endDate){
                    
                    
                   
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'EXPIRED','is_read'=>'1']);


                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Promo_Over_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);

                    $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

        
                    //Notification to admin when campaign is now over
                    if(!empty($campaignFieldValue)){

                        $model = Campaign::where('id',$data['campaign_id'])->first();

                        $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                    <p>This promotion is now over as per the below details:</p>

                                    <p><strong>Here are your campaign details:</strong></p>

                                    <p>{{CampaignDetails}}</p>

                                    <p>&nbsp;</p>

                                    <p>Regards<br />
                                    Team BigCity</p>

                                    <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                    <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td>
                                                <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>';
                    
                        $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                    <tr>
                                        <td><strong>Campaign Id</strong></td>
                                        <td><strong>Campaign Type</strong></td>
                                        <td><strong>Campaign Keyword</strong></td>
                                        <td><strong>Campaign Name</strong></td>
                                        <td><strong>Start Date</strong></td>
                                        <td><strong>End Date</strong></td>
                                        <td><strong>Created Date/Time</strong></td>
                                        <td><strong>Updated Date/Time</strong></td>
                                    </tr>';
                    
                        $codeMessage .= "<tr>
                                        <td>".$model->id."</td>
                                        <td>".$model->campaign_type->name."</td>
                                        <td>".$model->campaign_keyword."</td>
                                        <td>".$model->campaign_name."</td>
                                        <td>".$model->start_date."</td>
                                        <td>".$model->end_date."</td>
                                        <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                        <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                    </tr>";
                    
                        $codeMessage .= '</table></td></tr>';
            
                        $replacements = [
                            "{{AdminName}}" => 'Admin',
                            "{{CampaignDetails}}" => $codeMessage,
                            
                            "{{emailId}}" => $campaignFieldValue->field_value,
                        ];
            
                        $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                          <tr>
                            <td align="center">
                            <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                              <tr>
                                <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                
                        $message .= strtr($dynamic_mail, $replacements);
                        $message .= '</table></td></tr></table></td></tr></table>';
                        $from = 'bigreg@bigcity.in';
                        $to = $campaignFieldValue->field_value;

                        $subject = "Campaign Notification";
                    
                        //call helper function for sendign sms template
                        Helper::sendEmail($to, $subject, $message, $from);
                            
                        }
                        

                        
                      
                    
                }
            
               
                // Valid Vouchercode exist sms Template   
                
                elseif(!empty($VoucherData)){
                    

                    //Excess Tries SMS Template
                    if($duplicateVoucherCount >= '1'){
                    
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                            
                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'offerData'=>"",
                                'status'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);
                      
                      
                    } 
                    
                    elseif($this->validateMobilePerPromotion($campaignData->id,$val->from,$val->id,$val->parts,$val->parts2) ==false){
                        
                        // Valid SMS (TN) SMS Template
                        if($val->circle == "Tamil Nadu"){

                            //If the reward code gets over
                            if($currentDate > $endDate){
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);
                                    
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Valid_SMS_(TN)_If_the_reward_code_gets_over',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                                  
                                
                            }elseif(!empty($offerData->field_value)){
    
                        
                                $jsonnewdata = json_decode($offerData->field_value);
        
                                foreach ($jsonnewdata as $key => $value) {
        
                               
                                    //checked offer id data
                                    $offer = Offer::where('id', $value->offer_type_id)->where('status','ACTIVE')->first();
                                    
                                    $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('parts3',$offer->name)->first();
             
                                    if(!empty($checkedOfferId)){
        
                                        //Updating data
                                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
        
        
                                        $data = array(
                                                'campaign_id'     =>$campaignData->id,
                                                'offerData'=>$value,
                                                'status'=>"VALID",
                                                'from'            =>$val->from,
                                                'campaign'            =>$val->parts,
                                                'code'            =>$val->parts2,
                                                'incoming_msg_id'=>$val->id);
                                               
                                        //call helper function for sendign sms template
                                        Helper::sendSmsTemplate($data);
        
                                    }
                                }
                            }
                            else{
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);

                                $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Valid_SMS_(TN)',
                                'offerData'=>"",
                                'status'=>"VALID",
                                'from'            =>$val->from,
                                'campaign'            =>$val->parts,
                                'code'            =>$val->parts2,
                                'incoming_msg_id'=>$val->id );
                               
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                            }
                            
                            
                        }else{

                            /*$url = 'https://budspacerewards.com/api/checkvoucher';
                            
                            $sms_data = array(
                              'vcode'        => $keyword[1], 
                            
                              
                            );


                            $username = 'budsrewardsAPI';
                            $password = '5sPkxFecZqBUL4pJG5yupXasyUt8y6';
                            
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL,$url);
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $sms_data);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $result = curl_exec($ch);
                            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                            curl_close($ch);
    
                            $resultArr = json_decode($result, true);

                            if($resultArr['status'] == true){*/

                                if(!empty($offerData->field_value)){
    
                        
                                    $jsonnewdata = json_decode($offerData->field_value);

                                    $newArray = [];
                                    foreach ($jsonnewdata as $key => $value) {
            
                                 
                                        //checked offer id data
                                        $offer = Offer::where('id', $VoucherData->offer_type_id)->where('status','ACTIVE')->first();


                                        $offerChecked = Offer::where('name', $val->parts3)->where('status','ACTIVE')->get();
                                        
                                        $arr = explode(' ',trim($offer->name));
                                
                                        $upadtedLongCodeData = LongCode::where('id',$val['id'])->update(['field_slug_value'=>$value->offer_name_slug]);

                                        $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$val->parts2)->where('field_slug_value',preg_replace('/(\s|&(amp;)?|\.)+/', '_', $offer->name))->first();
                                        
                                        /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('offer_type_slug',$checkedOfferId['parts3'])->where('isused','0')->get();
                 
                                        if(count($RewardData) == 0){
                                                
                                                //Updating data
                                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                        
                                                // Invalid Code vouchercode SMS Template
                                                $data = array(
                                                        'campaign_id'     =>$campaignData->id,
                                                        'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                        'from'            =>$val->from,
                                                        'campaign'            =>$val->parts,
                                                        'code'            =>$val->parts2,
                                                        'offerData'=>"",
                                                        'status'=>"",
                                                        'incoming_msg_id'=>$val->id );
                                                       
                                                //call helper function for sendign sms template
                                                Helper::sendSmsTemplate($data);
                                                
                                        }else*/if(!empty($checkedOfferId)){
            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);
            
            
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>$value->offer_name_slug,
                                                    'offerData'=>$value,
                                                    'status'=>"VALID",
                                                    'offer_type_id'=>$value->offer_type_id,
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'incoming_msg_id'=>$val->id);
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
            
                                        }/*elseif(isset($value->offer_name_slug) && count($offerChecked) == 0){
                                            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);
                    
                                            // Invalid Code vouchercode SMS Template
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>'Invalid_Code_SMS',
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'offerData'=>"",
                                                    'status'=>"",
                                                    'incoming_msg_id'=>$val->id );
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
                                        }*/
                                        

                                    }
                                    
                                    //dd($newArray);
                                    
                                }
                                else{
                                    


                                    /*$RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('isused','0')->get();
                                    
                                    if(count($RewardData) == 0){
                                            
                                            //Updating data
                                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'REWARDOVER','is_read'=>'1']);
                    
                                            // Invalid Code vouchercode SMS Template
                                            $data = array(
                                                    'campaign_id'     =>$campaignData->id,
                                                    'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                                    'from'            =>$val->from,
                                                    'campaign'            =>$val->parts,
                                                    'code'            =>$val->parts2,
                                                    'offerData'=>"",
                                                    'status'=>"",
                                                    'incoming_msg_id'=>$val->id );
                                                   
                                            //call helper function for sendign sms template
                                            Helper::sendSmsTemplate($data);
                                            
                                    }else{*/

                                        

                                        //Updating data
                                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => $status,'is_read'=>'1']);
        
        
                                        // Valid SMS (ROI) SMS Template
                                        $data = array(
                                            'campaign_id'     =>$campaignData->id,
                                            'field_slug_value'=>'Valid_SMS_3GB_Data_Top_Up(TN)',
                                            'from'            =>$val->from,
                                            'campaign'            =>$val->parts,
                                            'code'            =>$val->parts2,
                                            'offerData'=>"",
                                            'status'=>'VALID',
                                            'offer_type_id'=>null,
                                            'incoming_msg_id'=>$val->id );
                                       
                                        //call helper function for sendign sms template
                                        Helper::sendVoucherRechargeBasedSmsTemplate($data);
                                    /*}*/
                                      
                                }
                                
                            /*}elseif($resultArr['is_used'] == true){

                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'DUPLICATE']);
                            
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Duplicate_Voucher_Code_SMS',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id);
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);


                            }else{

                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                                // Invalid Code vouchercode SMS Template
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Invalid_Code_SMS',
                                        'from'            =>$val->from,
                                        'campaign'            =>$val->parts,
                                        'code'            =>$val->parts2,
                                        'offerData'=>"",
                                        'status'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);  

                            }*/
                            
                        }
                    }
                    
                
                }else{
               
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                    // Invalid Code vouchercode SMS Template
                    $data = array(
                            'campaign_id'     =>$campaignData->id,
                            'field_slug_value'=>'Invalid_Code_SMS',
                            'from'            =>$val->from,
                            'campaign'            =>$val->parts,
                            'code'            =>$val->parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$val->id );
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);  
                    
                }
            }
    
        }
        

        return redirect()->back()->withSuccess('SMS has been sended successfully!');

      
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMultiOfferTemplate($campaign_id,$template_id)
    {


        $templates = Template::where('id',$template_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        
        $offers = Offer::where('status','ACTIVE')->get();
    
        $campaign_template = CampaignOfferTemplate::where('campaign_id',$campaign_id)->first();
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->where('campaign_field_id',env('NUMBER_OF_OFFER_TEMPLATE'))->first();
        
        return view('sms.sms_template.multioffertemplate',compact('campaign_id','templates','template_id','campaign_template','campaignFieldValue','offers'));
    }

    public function validateMobilePerPromotion($campaign_id,$mobile_no,$longcodeID,$parts,$parts2){

        $MobileUsageRule = CampaignMobileUsageRule::where('campaign_id',$campaign_id)->get();

        $CountMobileDatePerTotal = LongCode::where('parts' ,$parts)->whereIn('status',['VALID','SEND24HOUR'])->where('from' ,$mobile_no)->whereIn('is_read',['1','2'])->count();

        $CountMobileDatePerDay = LongCode::where('parts' ,$parts)->whereDate('created_at', Carbon::today())->where('from' ,$mobile_no)->whereIn('is_read',['1','2'])->where('is_read','1')->count();

        $CountMobileDatePerWeek = LongCode::where('parts' ,$parts)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('from' ,$mobile_no)->whereIn('status',['VALID','SEND24HOUR'])->whereIn('is_read',['1','2'])->count();
         
        $CountMobileDatePerMonth = LongCode::where('parts' ,$parts)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->where('from' ,$mobile_no)->whereIn('status',['VALID','SEND24HOUR'])->whereIn('is_read',['1','2'])->count();
        
        foreach($MobileUsageRule as $rule){
            
            
            switch ($rule->mobile_no_usage_rules) {

            case '1':

                
                if ($CountMobileDatePerTotal >= $rule->mobile_usage_value) {
                    
                    //Updating data
                            $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $mobile_no)->update(['is_read'=>'1','status' => 'EXCESS']);
                                
                            $data = array(
                                    'campaign_id'     =>$campaign_id,
                                    'field_slug_value'=>'Excess_Tries_SMS',
                                    'from'            =>$mobile_no,
                                    'campaign'            =>$parts,
                                    'code'            =>$parts2,
                                    'offerData'=>"",
                                    'status'=>"",
                                    'incoming_msg_id'=>$longcodeID);
                                   
                            //call helper function for sendign sms template
                            Helper::sendSmsTemplate($data);
                        return true;
                
                    
                }
                
                break;
            case '2':
               
                if ($CountMobileDatePerDay >= $rule->mobile_usage_value) {
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $mobile_no)->update(['is_read'=>'1','status' => 'EXCESS']);
                        
                    $data = array(
                            'campaign_id'     =>$campaign_id,
                            'field_slug_value'=>'Excess_Tries_SMS',
                            'from'            =>$mobile_no,
                            'campaign'            =>$parts,
                                'code'            =>$parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$longcodeID);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);
                    return true;
                }
                break;
            case '3':
               
                if ($CountMobileDatePerWeek >= $rule->mobile_usage_value) {
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $mobile_no)->update(['is_read'=>'1','status' => 'EXCESS']);
                        
                    $data = array(
                            'campaign_id'     =>$campaign_id,
                            'field_slug_value'=>'Excess_Tries_SMS',
                            'from'            =>$mobile_no,
                            'campaign'            =>$parts,
                                'code'            =>$parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$longcodeID);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);
                    return true;
                    
                }
                break;
            case '4':
               
                if ($CountMobileDatePerMonth >= $rule->mobile_usage_value) {
                    
                    //Updating data
                    $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $mobile_no)->update(['is_read'=>'1','status' => 'EXCESS']);
                        
                    $data = array(
                            'campaign_id'     =>$campaign_id,
                            'field_slug_value'=>'Excess_Tries_SMS',
                            'from'            =>$mobile_no,
                            'campaign'            =>$parts,
                                'code'            =>$parts2,
                            'offerData'=>"",
                            'status'=>"",
                            'incoming_msg_id'=>$longcodeID);
                           
                    //call helper function for sendign sms template
                    Helper::sendSmsTemplate($data);
                    return true;
                }
                break;
            default:
                return false;
    
                
            
            
            }
            
        }
        
    }
    
    public function storeMultiOfferTemplate(Request $request,$campaign_id,$template_id)
    {
        
        $deleteTemplateFieldListRecords    = CampaignOfferTemplate::where('campaign_id',$campaign_id)->delete();
        
        $i=0;
        $newdata = array();
        foreach ($request->offer_field_value as $k => $val) {
            if($k==$i){
                $newdata[$i]['offer_field_value'] = $val;
            }
            $i++;
        }
        $j=0;
        foreach ($request->offer_template_keyword_id as $ke => $vals) {
            if($ke==$j){
                $newdata[$j]['offer_template_keyword_id'] = $vals;
            }
            $j++;
        }
        $k=0;
        foreach ($request->offer_template_entity_id as $ke => $value) {
            if($ke==$k){
                $newdata[$k]['offer_template_entity_id'] = $value;
            }
            $k++;
        }
        $a=0;
        foreach ($request->offer_name_slug as $ke => $value) {
            if($ke==$a){
                $newdata[$a]['offer_name_slug'] = preg_replace('/(\s|&(amp;)?|\.)+/', '_', $value);
            }
            $a++;
        }
        
        $b=0;

        foreach ($request->offer_type_id as $ke => $value) {
            if($ke==$b) {
                $newdata[$b]['offer_type_id'] = $value;
            }
            $b++;
        }
        $w=0;
        
        foreach ($request->offer_template_id as $ke => $value) {
            if($ke==$w){
                $newdata[$w]['offer_template_id'] = $value;
            }
            $w++;
        }

        $fieldjsonData = json_encode($newdata);
        $campaign_template = new CampaignOfferTemplate;
        $campaign_template->campaign_id = $campaign_id;
        $campaign_template->template_id = $template_id;
        $campaign_template->field_value = $fieldjsonData;
        $campaign_template->save();

        return redirect()->back()->withSuccess('SMS has been sended successfully!');
        


    
    }
}


