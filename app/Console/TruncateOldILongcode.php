<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LongCode;
use Carbon\Carbon;

class TruncateOldILongcode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'longcode:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        LongCode::where('created_at', '<', Carbon::now())->each(function ($item) {
         $item->delete();
        });
        $this->info('Long code truncated Successfully!');
    }
}
