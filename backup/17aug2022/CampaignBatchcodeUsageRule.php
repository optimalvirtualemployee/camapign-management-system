<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class CampaignBatchcodeUsageRule extends Model
{
    protected $table = 'sms_tbl_campaign_batchcode_usage_rules';
    protected $fillable = ['campaign_id','campaign_field_id','batchcode_no_usage_rules','batchcode_usage_value'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
