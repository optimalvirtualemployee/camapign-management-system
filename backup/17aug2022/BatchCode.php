<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class BatchCode extends Model
{
    protected $table = 'sms_tbl_batch_codes';
    protected $fillable = ['campaign_id','batch_code','offer_type','offer_type_slug'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
