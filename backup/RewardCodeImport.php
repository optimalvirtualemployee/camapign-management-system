<?php

  

namespace App\Imports;

use App\SmsModels\LongCode;
use Maatwebsite\Excel\Concerns\WithStartRow;

use Maatwebsite\Excel\Concerns\ToModel;

  

class RewardCodeImport implements ToModel,WithStartRow

{
    //protected $id;
    
    
    
    
    
    public function startRow(): int
    {
        return 2;
    }
    /**

    * @param array $row

    *

    * @return \Illuminate\Database\Eloquent\Model|null

    */

    public function model(array $row)

    {

        /*$fieldData = new LongCode();
        
        $fieldData->from =  $request->from;
        $fieldData->message =  $request->message;
        $fieldData->ts =  $request->ts;
        $fieldData->circle =  $request->circle;
        $fieldData->operator =  $request->operator;
        $fieldData->msgid =  $request->msgid;
        $fieldData->parts =  $request->parts[1];
        $fieldData->parts2 =  $request->parts[2];
        $fieldData->parts3 =  $request->parts[3];
        $fieldData->parts4 =  $request->parts[4];
        
        

        
        $fieldData->save();*/
        $messageArray = explode(" ", $row[1]);
        
        //ini_set('max_execution_time', 0);
        //dd();
        return new LongCode([
            'from' =>$row[0],
            'message' =>$row[1],
            'ts'     => $row[2],
            'operator' =>$row[3],
            'circle' =>$row[4],
            'msgid' =>null,
            'parts'     => $messageArray[0],
            'parts2' =>isset($messageArray[1]) ? $messageArray[1] : null, 
            'parts3' =>isset($messageArray[2]) ? $messageArray[2] : null,
            'parts4' =>isset($messageArray[3]) ? $messageArray[3] : null, 

        ]);

    }

}