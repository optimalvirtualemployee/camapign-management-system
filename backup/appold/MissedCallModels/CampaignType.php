<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CampaignType extends Model
{
    
    use UserStampsTrait;
    
    protected $table = 'missedcall_tbl_campaign_types';
    protected $fillable = ['name','types','created_at','updated_at', 'missedcallnumber', 'cstatus'];

    protected $dates = [
        'created_at',
        'updated_at',        
    ];

    public function smstemplate_details()
    {
        return $this->hasOne('App\MissedCallModels\SmsTemplate','camp_id','id');
    }
       
    //Template  function
    public function template()
    {
        return $this->belongsTo('App\MissedCallModels\Template','template_id','id');
    }
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
    
}
