<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;

class Outgoing extends Model
{
	
    protected $table = 'missedcall_tbl_outgoing_msg';
    protected $fillable = ['incoming_msg_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    public function scopeBetween($query, Carbon $from, Carbon $to)
    {
        $query->whereBetween('created_at', [$from, $to]);
    }

    //outgoing  function
    public function incoming_msg()
    {
        return $this->belongsTo('App\MissedCallModels\MissedCallData','incoming_msg_id');
    }
}
