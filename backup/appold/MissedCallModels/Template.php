<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Template extends Model
{
	use UserStampsTrait;
    protected $table = 'missedcall_tbl_templates';
    protected $fillable = ['template_name','template_detail','campaign_type_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
    
    //User  function
    public function campaign_type()
    {
    	return $this->belongsTo('App\MissedCallModels\CampaignType','campaign_type_id','id');
    }

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }


    public function field_template()
    {
        return $this->belongsToMany('App\MissedCallModels\Field')->withPivot('field_id','id','keyword');
    }
}
