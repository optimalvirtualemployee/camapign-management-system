<?php

namespace App\MissedCallModels;

use Illuminate\Database\Eloquent\Model;

class MissedCallData extends Model
{
	
    protected $table = 'missedcall_tbl_missedcall_data';
    protected $fillable = ['from','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
