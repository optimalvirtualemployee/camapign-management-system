<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsAppData extends Model
{
	
    protected $table = 'tbl_whatsapp_data';
    protected $fillable = ['from','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
