<?php

namespace App\WhatsappModels;

use Illuminate\Database\Eloquent\Model;

class SmsTemplate extends Model
{
    protected $table = 'whatsapp_tbl_sms_templates';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
