<?php

namespace App\WhatsappModels;
use App\UserStampsTrait;
use Illuminate\Database\Eloquent\Model;

class BatchCodeUsageRule extends Model
{
    use UserStampsTrait;
    protected $table = 'whatsapp_tbl_batchcode_usage_rules';
    protected $fillable = ['name'];

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
