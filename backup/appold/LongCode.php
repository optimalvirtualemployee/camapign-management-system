<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LongCode extends Model
{
	
    protected $table = 'tbl_longcodes';
    protected $fillable = ['from','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    
}
