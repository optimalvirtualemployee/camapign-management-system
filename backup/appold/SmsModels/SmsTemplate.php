<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class SmsTemplate extends Model
{
    protected $table = 'sms_tbl_sms_templates';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
