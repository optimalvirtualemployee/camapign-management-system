<?php

namespace App\SmsModels;
use App\UserStampsTrait;
use Illuminate\Database\Eloquent\Model;

class MobileUsageRule extends Model
{
    use UserStampsTrait;
    protected $table = 'sms_tbl_mobile_usage_rules';
    protected $fillable = ['name'];

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
