<?php

namespace App\SmsModels;
use Illuminate\Database\Eloquent\Model;

class LongCode extends Model
{
	
    protected $table = 'sms_tbl_longcodes';
    protected $fillable = ['from','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign()
    {
        return $this->belongsTo('App\SmsModels\Campaign','parts');
    }

    //Campaign Type  function
    public function voucher()
    {
        return $this->belongsTo('App\SmsModels\Voucher','parts2','voucher_code');
    }

    
}
