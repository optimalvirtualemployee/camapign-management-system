<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'sms_tbl_campaigns';
    protected $fillable = ['campaign_type_id','campaign_keyword','start_date','end_date','status_trigger_rule','campaign_name','same_mobile_allowed','microsite_api'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign_type()
    {
        return $this->belongsTo('App\SmsModels\CampaignType','campaign_type_id');
    }

    //Campaign Type  function
    public function longCode()
    {
        return $this->hasMany('App\SmsModels\LongCode','parts','campaign_keyword');
    }
}
