<?php

namespace App\SmsModels;

use Illuminate\Database\Eloquent\Model;

class CampaignMobileUsageRule extends Model
{
    protected $table = 'sms_tbl_campaign_mobile_usage_rules';
    protected $fillable = ['campaign_id','campaign_field_id','mobile_no_usage_rules','mobile_usage_value'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign()
    {
        return $this->belongsTo('App\SmsModels\Campaign','campaign_id');
    }
}
