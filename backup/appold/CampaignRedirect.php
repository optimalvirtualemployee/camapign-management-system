<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class CampaignRedirect extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_campaign_redirects';
    protected $fillable = ['campaign_id','redirect_url'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\Campaigns','campaign_id');
    }
    //User  function
    public function created_user()
    {
        return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }
}
