<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardCode extends Model
{
    protected $table = 'tbl_reward_codes';
    protected $fillable = ['campaign_id','reward_code'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaigns()
    {
        return $this->belongsTo('App\Campaign','campaign_id');
    }
}
