<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'tbl_campaigns';
    protected $fillable = ['campaign_type_id','campaign_keyword','start_date','end_date','status_trigger_rule','campaign_name','same_mobile_allowed'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //Campaign Type  function
    public function campaign_type()
    {
        return $this->belongsTo('App\CampaignType','campaign_type_id');
    }
}
