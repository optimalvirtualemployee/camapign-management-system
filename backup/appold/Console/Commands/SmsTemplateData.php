<?php
 
namespace App\Console\Commands;
 
use Illuminate\Console\Command;
use DB;
use GuzzleHttp\Client;


class SmsTemplateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smstemplate:data';
     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sms Template Data';
     
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->first();
        
        
        $voucher = Voucher::where('campaign_id',$campaign_id)->first();
        
        $message = $campaignFieldValue['field_value'].' '.$voucher->voucher_code;*/
        $longCodeData = LongCode::where('message','LIKE', '%' . $message . '%')->get();
       
        foreach ($fieldvalue as $i => $val) {
            
            
            $campaign_template = new CampaignTemplate;
            $campaign_template->campaign_id = $campaign_id;
            $campaign_template->field_id = $field_id[$i];
            $campaign_template->field_value = $val;
            $campaign_template->template_keyword_id = $template_keyword_id[$i];
            $campaign_template->template_entity_id = $template_entity_id[$i];
            $campaign_template->save();
            
            //$mobile = "8826592901";
   
            //$smsText = "This is an invalid voucher code. Please submit a valid voucher code to claim your reward. Team BigCity";
            
            /*$this->sendsms('8826592901',$val,$template_keyword_id[$i],$template_entity_id[$i]);*/
            
            $dynamic_otpmsg = $val;
    		$replacements = [
    	    "{#var#}" => "test",
    	    " " => '%20',
    			"&" => '%26',
    		];
    		
    		$smsText = strtr($dynamic_otpmsg, $replacements);
    		
    		$curl = curl_init();
    		
            $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$longCodeData[$i]->from."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=$template_keyword_id[$i]";
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",)
            );
            $response = curl_exec($curl);
            
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
              echo "cURL Error #:" . $err;
              
            } else {
                
                
                $outgoing = new Outgoing;
                $outgoing->from = $longCodeData[$i]->from;
                $outgoing->message = $longCodeData[$i]->message;
                $outgoing->save();
                
              
            }
            return redirect()->back()->withSuccess('SMS has been sended successfully!');
            
            
        }

        $this->info('Booking Data');
        
    }
    
    
}