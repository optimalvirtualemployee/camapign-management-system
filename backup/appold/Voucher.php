<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class Voucher extends Model
{
    use UserStampsTrait;
    protected $table = 'tbl_vouchers';
    protected $fillable = ['campaign_id','offer_type_id','start_date','end_date','voucher_code','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }


    public function campaign()
    {
        return $this->belongsTo('App\Campaign','campaign_id','id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Offer','offer_type_id','id');
    }
}
