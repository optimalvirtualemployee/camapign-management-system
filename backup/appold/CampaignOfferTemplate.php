<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CampaignOfferTemplate extends Model
{
    
    protected $table = 'tbl_campaign_offer_templates';
    protected $fillable = ['campaign_id','field_value','template_keyword_id','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];
}
