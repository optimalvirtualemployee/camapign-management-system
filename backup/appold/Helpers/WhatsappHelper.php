<?php

namespace App\Helpers; // Your helpers namespace 
use App\User;
use Auth;
use App\WhatsappModels\CampaignTemplate;
use App\WhatsappModels\Outgoing;
use App\WhatsappModels\WhatsAppData;
use App\WhatsappModels\CampaignFieldValue;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\RewardCode;

class WhatsappHelper
{
    public static function sendWhatsappTemplate($data)
    {
        
      dd($data);
        if($data['status'] == "VALID"){
            
            //$where = ['offerType'=>$data['offer_type']];
            
            $RewardData = RewardCode::where('isused','0')->where('campaign_id',$data['campaign_id'])->where('offer_type',$data['offer_type_id'])->get();
            
            
            $code = $RewardData[0]->reward_code;
            $updatedRewardData = RewardCode::where('reward_code',$RewardData[0]->reward_code)->update(['isused' => '1']);
            
        }else{
            $code = '';
        }
        
        
        if(isset($data['offerData']) && $data['offerData'] == ""){

          $campaignTemplate = CampaignTemplate::where('campaign_id',$data['campaign_id'])->where('field_slug',$data['field_slug_value'])->first();
          $field_value = $campaignTemplate->field_value;
          $template_keyword_id = $campaignTemplate->template_keyword_id;

        }else{
            
          $field_value = $data['offerData']->offer_field_value;

          $template_keyword_id = $data['offerData']->offer_template_keyword_id;
        }
        
        $dynamic_otpmsg = $field_value;

        $replacements = [
          "{#var#}" => $data['campaign'],
          "{#url#}" => "http://bigcityexperience.com/",
          "{#email#}" => "feedback@bigcity.in",
          "{#code#}" => $code,
          
          " " => '%20',
          "&" => '%26',
        ];

        $smsText = strtr($dynamic_otpmsg, $replacements);

        $curl = curl_init();
        
        $to     = $data['from'];
        $msg =  $smsText;
        $from   = 916366921965;
        $url = "https://api.kaleyra.io/v1/HXAP1689843537IN/messages?channel=WhatsApp&to=".$to."&from=916366921965&type=text&callback_url=''&body=".$msg."&entity_id=1701158219659617455&template_id=".$template_keyword_id;
       // $response = $this->httpGet($url);
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",)
        );
        $response = curl_exec($curl);
        
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
          
        } else {
            
           
            
            $outgoing = new Outgoing;
            $outgoing->incoming_msg_id = $data['incoming_msg_id'];
            
            $outgoing->outgoing_msg = urldecode($smsText);
           
            $outgoing->save();
            

            

          
        }
        
    }   
    
    //send email function ------- 
    public static function sendEmail($to, $subject, $message, $from) {   
      
        $email = new \SendGrid\Mail\Mail(); 
        $email->setfrom($from);
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addContent( "text/html", $message );
        $sendgrid = new \SendGrid('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');
        
        if($sendgrid->send($email)){
            
          return true;
          
        }else{
            
          return false; 
        }  
    }
    //send email function -------
    
    
}