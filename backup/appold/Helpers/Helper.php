<?php

namespace App\Helpers; // Your helpers namespace 
use App\User;
use Auth;

//use App\SmsModels\CampaignTemplate;
//use App\SmsModels\Outgoing;
use App\SmsModels\LongCode;
//use App\SmsModels\CampaignFieldValue;
//use App\SmsModels\Campaign;
//use App\SmsModels\RewardCode;
use DB;
use Carbon\Carbon;

class Helper
{
    public static function sendSmsTemplate($data){
      
         
      if($data['status'] == "VALID"){
        
        
        $RewardData = DB::table('sms_tbl_reward_codes')->where('isused','0')->where('campaign_id',$data['campaign_id'])->where('offer_type',$data['offer_type_id'])->get();
        


        if(count($RewardData) > 0){
          
          $code = $RewardData[0]->reward_code;
          $updatedRewardData = DB::table('sms_tbl_reward_codes')->where('reward_code',$RewardData[0]->reward_code)->update(['isused' => '1']);


          

        }else {
            
          $code = '';
        }
        
      }else{

          $code = '';
          
      }
        
      
      if(isset($data['offerData']) && $data['offerData'] == ""){

        $campaignTemplate = DB::table('sms_tbl_campaign_templates')->where('campaign_id',$data['campaign_id'])->where('field_slug',$data['field_slug_value'])->first();

        $field_value = $campaignTemplate->field_value;
        $template_keyword_id = $campaignTemplate->template_keyword_id;

      }else{
          
        $field_value = $data['offerData']->offer_field_value;

        $template_keyword_id = $data['offerData']->offer_template_keyword_id;

      }
      
      $dynamic_otpmsg = $field_value;

      $replacements = [
        "{#var#}" => $data['campaign'],
        "{#url#}" => "http://bigcityexperience.com/",
        "{#email#}" => "feedback@bigcity.in",
        "{#code#}" => $code,
        
        " " => '%20',
        "&" => '%26',
      ];

      $smsText = strtr($dynamic_otpmsg, $replacements);

      $curl = curl_init();
  
      $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$data['from']."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=$template_keyword_id";
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",)
      );
      $response = curl_exec($curl);
     
      $err = curl_error($curl);
      curl_close($curl);
      if ($err) {
        echo "cURL Error #:" . $err;
        
      } else {
            
            
            
        /*$outgoing = new Outgoing;
        $outgoing->incoming_msg_id = $data['incoming_msg_id'];
        
        $outgoing->outgoing_msg = urldecode($smsText);
       
        $outgoing->save();*/
        $insertedOutgoingData = DB::table('sms_tbl_outgoing_msg')->insert([ 'incoming_msg_id' => $data['incoming_msg_id'], 'outgoing_msg' => urldecode($smsText),'created_at' => Carbon::now(),'field_slug_value'=>$data['field_slug_value'],'campaign_id'=>$data['campaign_id']]);

        if($data['field_slug_value'] != ""){

          $upadtedLongCodeData = DB::table('sms_tbl_longcodes')->where('id',$data['incoming_msg_id'])->update(['field_slug_value'=>$data['field_slug_value']]);

        }

        $LongCode = DB::table('sms_tbl_longcodes')->where('id',$data['incoming_msg_id'])->first();
        $Outgoing = DB::table('sms_tbl_outgoing_msg')->where('incoming_msg_id',$data['incoming_msg_id'])->first();

        $voucher_code = $LongCode->parts2;// voucher code from post request

        //cashback campaign id
        $cashbackCampaignId = DB::table('sms_tbl_campaign_field_values')->where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('CASHBACK_CAMPAIGN_ID'))->first();

        //cashback amount
        $cashbackAmount = DB::table('sms_tbl_campaign_field_values')->where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('CASHBACK_AMOUNT'))->first();

        if(!empty($voucher_code) && $data['status'] == "VALID" && !empty($cashbackCampaignId) && $cashbackCampaignId->field_value > 0){
//dd('ss');
            
                /* API URL */
          $url = 'https://cbcapi.bigcityreward.com/api/claimcb';
          /* Array Parameter Data */
          //dd($cashbackCampaignId['field_value']);
          $cashback_data = array(
            'campaign_id'        => $cashbackCampaignId['field_value'], 
            'customer_mobile'    => $LongCode->from, 
            'customer_email'    => 'noreply@bigcityexperience.com', 
            'customer_name'    => "NA", 
            'voucher_code'     => $LongCode->parts2, 
            'transaction_type' => 3, 
            'transaction_amt'   => $cashbackAmount->field_value, 
            'transaction_date'  => $Outgoing->created_at, 
            'wallet_type'   => 1,
            'wallet_mobile' => $LongCode->from
            
          );


          $username = 'bcApi5894';
          $password = 'fJECQzy686DG2thK5x9XqbMaqjMm3D';
          
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $cashback_data);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          $result = curl_exec($ch);
          $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
          curl_close($ch);
          $resultArr = json_decode($result, true);

          if($resultArr['status'] == true){

            $upadtedOutgoingData = DB::table('sms_tbl_outgoing_msg')->where('incoming_msg_id',$data['incoming_msg_id'])->update(['cashback_api_status'=>'Sent To API']);
            return true;
          }
          
          return true;
        }
//dd('sssssssss');
        return true;
            
      }

      return true;
=======
use App\CampaignTemplate;
use App\Outgoing;
use App\LongCode;
use App\CampaignFieldValue;
use App\Campaign;

class Helper
{
    public static function sendSmsTemplate($data)
    {
        
        if(isset($data['offerData']) && $data['offerData'] == 0){

          $campaignTemplate = CampaignTemplate::where('campaign_id',$data['campaign_id'])->where('field_slug',$data['field_slug_value'])->first();
          $field_value = $campaignTemplate->field_value;
          $template_keyword_id = $campaignTemplate->template_keyword_id;

        }else{

          $field_value = $data['offerData']->offer_field_value;

          $template_keyword_id = $data['offerData']->offer_template_keyword_id;
        }
        
        $dynamic_otpmsg = $field_value;

        $replacements = [
          "{#var#}" => "test",
          " " => '%20',
          "&" => '%26',
        ];

        $smsText = strtr($dynamic_otpmsg, $replacements);

        $curl = curl_init();
    
        $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$data['from']."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=$template_keyword_id";
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",)
        );
        $response = curl_exec($curl);
        
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
          
        } else {
            
            
            
            $outgoing = new Outgoing;
            $outgoing->incoming_msg_id = $data['incoming_msg_id'];
            
            $outgoing->outgoing_msg = urldecode($smsText);
           
            $outgoing->save();
            

            

          
        }
>>>>>>> 3e59314b528c9d26478d3a4a736c5caa1171adf3
        
    }   
    
    //send email function ------- 
    public static function sendEmail($to, $subject, $message, $from) {   
      
        $email = new \SendGrid\Mail\Mail(); 
        $email->setfrom($from);
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addContent( "text/html", $message );
        $sendgrid = new \SendGrid('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');
        
        if($sendgrid->send($email)){
            
          return true;
          
        }else{
            
          return false; 
        }  
    }
    //send email function -------
    
<<<<<<< HEAD
    public static function sendWhatsappTemplate($data)
    {
        
        
      
        if($data['status'] == "VALID"){
            
            //$where = ['offerType'=>$data['offer_type']];
            
            
            $RewardData = DB::table('whatsapp_tbl_reward_codes')->where('isused','0')->where('campaign_id',$data['campaign_id'])->where('offer_type',$data['offer_type_id'])->get();
            
            if(count($RewardData) > 0){
                
                $code = $RewardData[0]->reward_code;
                $updatedRewardData = DB::table('whatsapp_tbl_reward_codes')->where('reward_code',$RewardData[0]->reward_code)->update(['isused' => '1']);

            }else {
               
              $code = '';
            }
            
            
            
        }else{
            
           
            $code = '';
            
        }

        if(isset($data['offerData']) && $data['offerData'] == ""){
 
              $campaignTemplate = DB::table('whatsapp_tbl_campaign_templates')->where('campaign_id',$data['campaign_id'])->where('field_slug',$data['field_slug_value'])->first();
               
              $field_value = $campaignTemplate->field_value;
              $template_keyword_id = $campaignTemplate->template_keyword_id;
    
        }else{
                
              $field_value = $data['offerData']->offer_field_value;
      
              //$template_keyword_id = $data['offerData']->offer_template_keyword_id;
        }
        
        
        
        $dynamic_otpmsg = $field_value;

        $replacements = [
          "{#var#}" => $data['campaign'],
          "{#url#}" => "http://bigcityexperience.com/",
          "{#email#}" => "feedback@bigcity.in",
          "{#code#}" => $code,
          
          " " => '%20',
          "&" => '%26',
        ];

        $smsText = strtr($dynamic_otpmsg, $replacements);
        try{
            
            $curl = curl_init();
            
            $to     = $data['from'];
            $msg =  $smsText;

            $from   = 916366921965;
            $url = "https://api.kaleyra.io/v1/HXAP1689843537IN/messages?channel=WhatsApp&to=".$to."&from=916366921965&type=text&body=".$msg;
           // $response = $this->httpGet($url);
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_HTTPHEADER => array("api-key:A714af0a570cf003da623225d34558f78"),
              CURLOPT_CUSTOMREQUEST => "GET",)
            );
            $response = curl_exec($curl);
        // dd($response);  
            $err = curl_error($curl);

            curl_close($curl);
            
            if ($err) {
                
              echo "cURL Error #:" . $err;
              
            } else {
                
                DB::table('whatsapp_tbl_outgoing_msg')->insert([ 'incoming_msg_id' => $data['incoming_msg_id'], 'outgoing_msg' => urldecode($smsText) ,'created_at' => Carbon::now()]);
            }
            
        }catch(Exception $e){
            
            file_put_contents('smstemplate.txt', $e);
            
        }
       
        
    }  


    public static function sendVoucherRechargeBasedSmsTemplate($data){
      
         
      if($data['status'] == "VALID"){
        
        
        $RewardData = DB::table('sms_tbl_reward_codes')->where('isused','0')->where('campaign_id',$data['campaign_id'])->where('offer_type',$data['offer_type_id'])->get();
        


        if(count($RewardData) > 0){
          
          $code = $RewardData[0]->reward_code;
          $updatedRewardData = DB::table('sms_tbl_reward_codes')->where('reward_code',$RewardData[0]->reward_code)->update(['isused' => '1']);


          

        }else {
            
          $code = '';
        }
        
      }else{

          $code = '';
          
      }
        
      
      if(isset($data['offerData']) && $data['offerData'] == ""){

        $campaignTemplate = DB::table('sms_tbl_campaign_templates')->where('campaign_id',$data['campaign_id'])->where('field_slug',$data['field_slug_value'])->first();

        $field_value = $campaignTemplate->field_value;
        $template_keyword_id = $campaignTemplate->template_keyword_id;

      }else{
          
        $field_value = $data['offerData']->offer_field_value;

        $template_keyword_id = $data['offerData']->offer_template_keyword_id;

      }
      
      $dynamic_otpmsg = $field_value;

      $replacements = [
        "{#var#}" => $data['campaign'],
        "{#url#}" => "http://bigcityexperience.com/",
        "{#email#}" => "feedback@bigcity.in",
        "{#code#}" => $code,
        
        " " => '%20',
        "&" => '%26',
      ];

      $smsText = strtr($dynamic_otpmsg, $replacements);

      $curl = curl_init();
  
      $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$data['from']."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=$template_keyword_id";
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",)
      );
      $response = curl_exec($curl);
     
      $err = curl_error($curl);
      curl_close($curl);
      if ($err) {
        echo "cURL Error #:" . $err;
        
      } else {
            
            
            
        /*$outgoing = new Outgoing;
        $outgoing->incoming_msg_id = $data['incoming_msg_id'];
        
        $outgoing->outgoing_msg = urldecode($smsText);
       
        $outgoing->save();*/
        $insertedOutgoingData = DB::table('sms_tbl_outgoing_msg')->insert([ 'incoming_msg_id' => $data['incoming_msg_id'], 'outgoing_msg' => urldecode($smsText),'created_at' => Carbon::now(),'field_slug_value'=>$data['field_slug_value'],'campaign_id'=>$data['campaign_id']]);

        if($data['field_slug_value'] != ""){


          

          $upadtedLongCodeData = DB::table('sms_tbl_longcodes')->where('id',$data['incoming_msg_id'])->update(['field_slug_value'=>$data['field_slug_value']]);

        }

        $LongCode = DB::table('sms_tbl_longcodes')->where('id',$data['incoming_msg_id'])->first();
        $Outgoing = DB::table('sms_tbl_outgoing_msg')->where('incoming_msg_id',$data['incoming_msg_id'])->first();

        $voucher_code = $LongCode->parts2;// voucher code from post request

        //cashback campaign id
        $cashbackCampaignId = DB::table('sms_tbl_vouchers')->where('campaign_id',$data['campaign_id'])->where('offer_type_id',$data['offer_type_id'])->where('voucher_code',$data['code'])->first();

        //cashback amount
        $cashbackAmount = DB::table('sms_tbl_offers')->where('id',$cashbackCampaignId->offer_type_id)->first();

        if(!empty($voucher_code) && $data['status'] == "VALID" && !empty($cashbackCampaignId)){

            
                /* API URL */
          $url = 'https://cbcapi.bigcityreward.com/api/claimcb';
          /* Array Parameter Data */
          $cashback_data = array(
            'campaign_id'        => $cashbackCampaignId->cashback_camp_id, 
            'customer_mobile'    => $LongCode->from, 
            'customer_email'    => 'noreply@bigcityexperience.com', 
            'customer_name'    => "NA", 
            'voucher_code'     => $LongCode->parts2, 
            'transaction_type' => 3, 
            'transaction_amt'   => $cashbackAmount->cashback_price, 
            'transaction_date'  => $Outgoing->created_at, 
            'wallet_type'   => 1,
            'wallet_mobile' => $LongCode->from
            
          );


          $username = 'bcApi5894';
          $password = 'fJECQzy686DG2thK5x9XqbMaqjMm3D';
          
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $cashback_data);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          $result = curl_exec($ch);
          $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
          curl_close($ch);
          $resultArr = json_decode($result, true);

          if($resultArr['status'] == true){

            $upadtedOutgoingData = DB::table('sms_tbl_outgoing_msg')->where('incoming_msg_id',$data['incoming_msg_id'])->update(['cashback_api_status'=>'Sent To API']);

            return true;
          }
          return true;
        }
        return true;
            
      }
      return true;
        
    }   
}
=======
    
}
>>>>>>> 3e59314b528c9d26478d3a4a736c5caa1171adf3
