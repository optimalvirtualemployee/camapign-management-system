<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserStampsTrait;

class RuleType extends Model
{
    use UserStampsTrait;

    protected $table = 'tbl_rule_types';
    protected $fillable = ['name','created_at','updated_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        
    ];

    

    //User  function
    public function created_user()
    {
    	return $this->belongsTo('App\User','created_by','id');
    }
    
    //User  function
    public function updated_user()
    {
    	return $this->belongsTo('App\User','updated_by','id');
    }
}
