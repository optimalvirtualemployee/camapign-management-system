<?php

namespace App\Http\Controllers;

use App\Feild;
use App\FeildType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class FeildController extends Controller
{

    /**
     * Create dynamic table along with dynamic fields
     *
     * @param       $table_name
     * @param array $fields
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTable($table_name, $fields = [])
    {
        // check if table is not already exists
        /*if (!Schema::hasTable($table_name)) {*/
            Schema::table($table_name, function (Blueprint $table) use ($fields, $table_name) {
                
                if (count($fields) > 0) {
                    foreach ($fields as $field) {
                        $table->{$field['type']}($field['name'])->after('id');;
                    }
                }
                
            });

            return response()->json(['message' => 'Given table has been successfully created!'], 200);
        /*}

        return response()->json(['message' => 'Given table is already existis.'], 400);*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feilds = Feild::get();

        return view('admin.feilds.index')->with([
        'feilds'  => $feilds
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $feildtypes = Feildtype::where('status','ACTIVE')->get();
        return view('admin.feilds.create',compact('feildtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$validatedData = $this->validate($request, [
            "sms_template_form"  => "required|not_in:0" ,
            "fieldid"             => "required|array|min:1",
            "fieldid.*"           => "required|min:1",
            "fieldname"             => "required|array|min:1",
            "fieldname.*"           => "required|string|distinct|min:1",
            
        ]);*/
        $sms_template_form       = $request->sms_template_form;
        $fieldid            = $request->fieldid;
        $fieldname      = $request->fieldname;
        $i=0;
        $newdata = array();
        foreach ($fieldid as $k => $val) {
            if($k==$i){
                $newdata[$i]['fieldid'] = $val;
            }
            $i++;
        }
        $c=0;
        foreach ($fieldname as $ke => $value) {
            if($ke==$c) {
                $newdata[$c]['fieldname'] = $value;
            }
            $c++;
        }

        $fieldjsonData = json_encode($newdata);
        $data =array(
            'sms_template_form'              =>  $sms_template_form,
            'fielddata'         =>  $fieldjsonData
        );

        
        $feilds = Feild::create($data);
        // set dynamic table name according to your requirements

        $table_name = 'tbl_sms_templates';
        $i=1;
        // set your dynamic fields (you can fetch this data from database this is just an example)
        $jsonnewdata = json_decode($feilds->fielddata);
        foreach($jsonnewdata as $k => $value){
            
            $fields = [
                ['name' => str_replace(' ', '', $value->fieldname), 'type' => 'string']
                
            ];
            
            $i++;
            //return $this->createTable($table_name, $fields);
        }
        
        
        
        return redirect('admin/feilds')->withSuccess('Feild has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feilds = Feild::find($id);
        
        return view('admin.feilds.show', compact('feilds')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feilds = Feild::find($id);
        $feildtypes = Feildtype::where('status','ACTIVE')->get();
        return view('admin.feilds.edit', compact('feilds','feildtypes')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $feilds = Feild::find($id);
        $jsonnewdata = json_decode($feilds->fielddata);
        $sms_template_form       = $request->sms_template_form;
        $fieldid            = $request->fieldid;
        $fieldname      = $request->fieldname;
        $i=0;
        $newdata = array();
        foreach ($fieldid as $k => $val) {
            if($k==$i){
                $newdata[$i]['fieldid'] = $val;
            }
            $i++;
        }
        $c=0;
        foreach ($fieldname as $ke => $value) {
            if($ke==$c) {
                $newdata[$c]['fieldname'] = $value;
            }
            $c++;
        }
        $g=0;
            
        foreach ($jsonnewdata as $json1 => $column) {
            if($json1==$g) {
                $newdata[$g]['column'] = $column->fieldname;
            }
            $g++;
        }
        $h=0;
        
        foreach ($jsonnewdata as $json => $columnid) {
            if($json==$h) {
                $newdata[$h]['columnid'] = $columnid->fieldid;
            }
            $h++;
        }
        $fieldjsonData = json_encode($newdata);
        $feilds->sms_template_form =  $sms_template_form;
        $feilds->fielddata =  $fieldjsonData;
        $feilds->touch();        
        return redirect('/admin/feilds')->with('success', 'Feild updated!');
        
    }

    
}
