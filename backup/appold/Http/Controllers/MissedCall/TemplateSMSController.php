<?php
namespace App\Http\Controllers\MissedCall;
use App\Http\Controllers\Controller;
use App\SmsModels\FieldTemplate;
use App\MissedCallModels\CampaignType;
use App\MissedCallModels\SmsTemplate;
use Illuminate\Http\Request;

class TemplateSMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $smstemplates = SmsTemplate::where('cstatus','1')->with('campaign_type')->get();
      return view('missedcall.sms_template.index')->with([
      'smstemplates'  => $smstemplates,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id) {
      $campaigns = CampaignType::where(['id'=>$id])->get();
      $smstemplates = SmsTemplate::where('camp_id',$id)->get();
      return view('missedcall.sms_template.create',compact('campaigns', 'smstemplates', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id) {
      $smstemplates = SmsTemplate::where('camp_id',$id)->get();
      if ($request->get('types') == 1) {
        if(count($smstemplates)==0) {
          $validatedData = $this->validate($request, [
            'camp_id'      => 'required',
            'smsentityid'     => 'required',
            'smstemplateid' => 'required',
            'cstatus'        => 'required',
            'sms_text'        => 'required',
          ]);
          SmsTemplate::create($validatedData);
          return redirect('admin/missedcall/missedcallcampaigntypes')->withSuccess('SMS Template has been created successfully!');
        } else {
          $SmsTemplate = SmsTemplate::find($smstemplates[0]['id']);
          $SmsTemplate->camp_id =  $request->get('camp_id');
          $SmsTemplate->smsentityid =  $request->get('smsentityid');
          $SmsTemplate->smstemplateid = $request->get('smstemplateid');
          $SmsTemplate->cstatus = $request->get('cstatus');
          $SmsTemplate->sms_text = $request->get('sms_text');
          $SmsTemplate->touch();
          return redirect('admin/missedcall/missedcallcampaigntypes')->withSuccess('SMS Template has been updated successfully!');
        }  
      } else {
        if(count($smstemplates)==0) {
          $validatedData = $this->validate($request, [
            'camp_id'      => 'required',
            'smsentityid'     => 'required',
            'smstemplateid' => 'required',
            'cstatus'        => 'required',
            'sms_text'        => 'required',
            //'audioclip'   => 'required|mimes:mp3|max:10240',
          ]);
          
          /*$file = $request->file('audioclip');
          $fileName = time().'-'.$file->getClientOriginalName();
          $file->move(public_path('uploads/audio'), $fileName);*/

          $insdata = array(
            'camp_id'      => $request->get('camp_id'),
            'smsentityid'     => $request->get('smsentityid'),
            'smstemplateid' => $request->get('smstemplateid'),
            'cstatus'        => $request->get('cstatus'),
            'sms_text'        => $request->get('sms_text'),
            //'recorded_msg'   => $fileName,
          );
          SmsTemplate::create($insdata);
          return redirect('admin/missedcall/missedcallcampaigntypes')->withSuccess('SMS Template has been created successfully!');
        } else {
          /*if ($request->file('audioclip')=="") {
            $fileName = $request->get('rec_msg');
          } else {
            $file = $request->file('audioclip');
            $fileName = time().'-'.$file->getClientOriginalName();
            $file->move(public_path('uploads/audio'), $fileName);
          }*/

          $SmsTemplate = SmsTemplate::find($smstemplates[0]['id']);
          $SmsTemplate->camp_id =  $request->get('camp_id');
          $SmsTemplate->smsentityid =  $request->get('smsentityid');
          $SmsTemplate->smstemplateid = $request->get('smstemplateid');
          $SmsTemplate->cstatus = $request->get('cstatus');
          $SmsTemplate->sms_text = $request->get('sms_text');
          //$SmsTemplate->recorded_msg = $fileName;
          $SmsTemplate->touch();

          return redirect('admin/missedcall/missedcallcampaigntypes')->withSuccess('SMS Template has been updated successfully!');
        }  
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TemplateSMS  $templateSMS
     * @return \Illuminate\Http\Response
     */
    public function show(TemplateSMS $templateSMS,$id)
    {
      $smstemplates = SmsTemplate::find($id);
      return view('missedcall.sms_template.show',compact('smstemplates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TemplateSMS  $templateSMS
     * @return \Illuminate\Http\Response
     */
    public function edit(TemplateSMS $templateSMS,$id)
    {
      $smstemplates = SmsTemplate::find($id);
      return view('missedcall.sms_template.edit', compact('smstemplates')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TemplateSMS  $templateSMS
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TemplateSMS $templateSMS,$id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TemplateSMS  $templateSMS
     * @return \Illuminate\Http\Response
     */
    public function destroy(TemplateSMS $templateSMS)
    {
        //
    }
}
