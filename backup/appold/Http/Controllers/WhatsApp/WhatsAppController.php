<?php

namespace App\Http\Controllers\WhatsApp;


use App\Campaign;
use App\CampaignType;
use App\Template;
use App\FieldTemplate;
use App\FieldType;
use App\CampaignField;
use App\CampaignFieldValue;
use App\WhatsAppData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\RewardCodeImport;
use Excel;
use App\MobileUsageRule;
use App\CampaignMobileUsageRule;

class WhatsAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $campaigns = Campaign::get();
        
        return view('admin.campaign.index')->with([
        'campaigns'  => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
        
        return view('admin.campaign.create',compact('campaign_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validatedData = $this->validate($request, [
            'campaign_name'              => 'required|unique:tbl_campaigns,campaign_name',
            'campaign_type_id'     => 'required|not_in:0' ,
            
            
        ]);

        Campaign::create($validatedData);

        return redirect('admin/campaigns')->withSuccess('Campaign has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign->id)->first();
        return view('admin.campaign.show',compact('campaign','campaignFieldValue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {

        
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
    
        $templates = Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->get();
        /*$campaignField = CampaignField::where('campaign_type_id',$campaign->campaign_type_id)->with('field.field_type')->get();*/
        $campaignField = Template::where('campaign_type_id',$campaign->campaign_type_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        //dd($campaignField);
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign->id)->get();
        $campaignMobileUsageRuleValue = CampaignMobileUsageRule::where('campaign_id',$campaign->id)->get();
        
        $mobile_no_usage = MobileUsageRule::get();
       
        
        return view('admin.campaign.edit', compact('campaign','campaign_type','templates','campaignField','campaignFieldValue','mobile_no_usage','campaignMobileUsageRuleValue')); 
    }

    /**
     * Update the specified resource in storage.

     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Campaign $campaign)
    {
        
        
        $request->validate([
            'campaign_name'    => 'required',
            'campaign_type_id' => 'required|not_in:0',
            /*'template_id'      => 'required|not_in:0',*/
        ]);        
       
        $campaign->campaign_name               =  $request->get('campaign_name');
        $campaign->template_id               =  $request->get('template_id');
        
        $campaign->campaign_type_id      =  $request->get('campaign_type_id');
        $campaign->touch();
        
        if($request->field_value){

            $fieldvalue          = $request->field_value;
            $field_id           = $request->field_key;
            $mobile_no_usage_rules          = $request->mobile_no_usage_rules;
            $mobile_usage_value           = $request->mobile_usage_value;
            
            
            $deleteTemplateFieldListRecords    = CampaignFieldValue::whereIn('campaign_field_id',$field_id)->delete();
            foreach ($fieldvalue as $i => $val) {
                $campaign_template = new CampaignFieldValue;
                $campaign_template->campaign_id = $campaign->id;
                $campaign_template->campaign_field_id = $field_id[$i];
                $campaign_template->field_value = $val;
                $campaign_template->save();

                
            } 
            
            foreach ($mobile_no_usage_rules as $i => $val) {
                $campaign_template = new CampaignMobileUsageRule;
                $campaign_template->campaign_id = $campaign->id;
                $campaign_template->campaign_field_id = $field_id[$i];
                $campaign_template->mobile_no_usage_rules = $val;
                $campaign_template->mobile_usage_value = $mobile_usage_value[$i];
                $campaign_template->save();

                
            } 
            
            if($request->field_value_file){
                
                $this->uploadRewardCode($campaign->id,$request->field_value_file);
                
            }
            
            /*if($request->field_value[5]){*/
                
                /*$this->sendsms('8826592901','hello','1701158219659617455','1707160404740698653');*/
            /*}*/
        }
        return redirect('/admin/campaigns')->with('success', 'Campaign updated!');
    }

    public function getFieldTemplateList(Request $request)
    {
        $campaignField = CampaignField::where('campaign_type_id',$request->campaign_type_id)->with('field_type')->get();
        

        /*$field_template = FieldTemplate::where("template_id",$request->template_id)
                       ->pluck("keyword","id");*/
        
        return response()->json($campaignField);
    }
    
    public function getWhatsAppData(Request $request)
    {
        
        /*https://cms.optimaldevelopments.com/get-whatapp-data/?body={body}&from={from}&name={name}&type={type}&created_at={created_at}
        &reply_to={reply_to}&wanumber={wanumber}&mobile={mobile}&custom_response={custom_response}&message={message}&mobile={mobile}*/
        
        file_put_contents('whatapp.txt', $request);
        
        $fieldData = new WhatsAppData();
        
        $fieldData->body =  $request->body;
        $fieldData->from =  $request->from;
        $fieldData->name =  $request->name;
        $fieldData->type =  $request->type;
        $fieldData->created_at =  $request->created_at;
        $fieldData->reply_to =  $request->reply_to;
        $fieldData->wanumber =  $request->wanumber;
        $fieldData->mobile =  $request->mobile;
        $fieldData->custom_response =  $request->custom_response;
        $fieldData->message =  $request->message;
       
        
        $fieldData->save();

    }
    
    //Upload mulitple city through excel file
    public function uploadRewardCode($campaign_id,$field_value_file)
    {   
        $id = $campaign_id;

        try {
            
           Excel::import(new RewardCodeImport($id),$field_value_file);
           

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        
        return redirect('/admin/campaigns')->withSuccess('You have successfully created a Rewrad Code!');
    }
    
    // function for sending SMS on given mobile number
    function sendsms(){
       
       
       
        $mobile = "8826592901";
       
        $smsText = "This is an invalid voucher code. Please submit a valid voucher code to claim your reward. Team BigCity";
        $responseBody = file_get_contents("https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=1707161536585787684");
        if($responseBody){
            
            
          return true;
        }else{
            return false;
        }
    }
    
    
}
