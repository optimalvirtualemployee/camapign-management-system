<?php

namespace App\Http\Controllers\campaign;


use App\LongCode;
use App\Outgoing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use DB;
use App\WhatsAppData;
use App\Exports\AllOutgoingExport;
use App\Exports\AllIncomingExport;
use Excel;

class ReportController extends Controller
{
    

    
    public function incomingReportList(Request $request)
    {
        
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = LongCode::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = LongCode::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        

        return view('admin.report.incoming');
        /*$incomingData = LongCode::get();
        
        return view('admin.report.incoming',compact('incomingData'));*/
    }

    
    public function outgoingReportList(Request $request)
    {
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = Outgoing::with('incoming_msg')->whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = Outgoing::with('incoming_msg')->get();
                
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        
        
        return view('admin.report.outgoing');
        
        
    }

    public function whatsappReportList(Request $request)
    {
        
        if ($request->ajax()) {
            
            if(!empty($request->from_date)) {
                
                $data = WhatsAppData::whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->get();
                    
            }else {
                
                $data = WhatsAppData::get();
            }
            
            return Datatables::of($data)
                ->addIndexColumn()
                
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d M Y').'/'.$request->created_at->format('g:i A'); // human readable format
                })
                ->editColumn('message', function ($request) {
                    return urldecode($request->message); // human readable format
                })
            ->make(true);

        }

        
        
        return view('admin.report.whatsapp-data-list');
        
        
    }
    
    public function allOutgoingDownload(Request $request)
    {
        
        
        $outgoing_data = Outgoing::with('incoming_msg')->get()->toArray();
      
        
             
            foreach($outgoing_data as $index=>$outgoing)
            {

                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'ID'           => $outgoing['id'],
                   'From'         => $outgoing['incoming_msg']['from'],
                   'Message'         => $outgoing['incoming_msg']['message'],
                   'Outgoing Message'         => $outgoing['outgoing_msg'],
                   
                    'Status'                => $outgoing['incoming_msg']['status'],

                   'Created Date & Time'   => date("Y-m-d", strtotime($outgoing['created_at'])).'&'.date("H:i", strtotime($outgoing['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($outgoing['updated_at'])).'&'.date("H:i", strtotime($outgoing['updated_at'])),
                   
                );
            }
        return Excel::download(new AllOutgoingExport($data), 'outgoing.xlsx');
    }


    public function allIncomingDownload(Request $request)
    {
        
        
        $incoming_data = LongCode::get()->toArray();
      
        
             
            foreach($incoming_data as $index=>$incoming)
            {

                $data[] = array(
                    
                   'SN'                    => ++$index,
                   'ID'           => $incoming['id'],
                   'From'         => $incoming['from'],
                   'Message'         => $incoming['message'],
                   'ts'         => $incoming['ts'],
                   'circle'         => $incoming['circle'],
                   'operator'         => $incoming['operator'],
                   'msgid'         => $incoming['msgid'],
                   'parts'         => $incoming['parts'],
                   'Message Delivered Status'         => $incoming['is_read'] == '1' ? "Delivered":"Not Delivered",
                    'Status'                => $incoming['status'],

                   'Created Date & Time'   => date("Y-m-d", strtotime($incoming['created_at'])).'&'.date("H:i", strtotime($incoming['created_at'])),
                   'Updated Date & Time'   => date("Y-m-d", strtotime($incoming['updated_at'])).'&'.date("H:i", strtotime($incoming['updated_at'])),
                   
                );
            }
        return Excel::download(new AllIncomingExport($data), 'cms_incoming.xlsx');
    }

    
    
    


    
}
