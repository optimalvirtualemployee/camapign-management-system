<?php

namespace App\Http\Controllers\campaign;

use App\Campaign;
use App\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $campaigns = Campaign::get();

        return view('admin.campaign.index')->with([
        'campaigns'  => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
        return view('admin.campaign.create',compact('campaign_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'keyword'              => 'required',
            'same_mobile_allowed'  => 'required' ,
            'campaign_type_id'     => 'required|not_in:0' ,
            
            
        ]);

        Campaign::create($validatedData);

        return redirect('admin/campaigns')->withSuccess('Campaign has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $campaign = Campaign::find($id);
        return view('admin.campaign.show',compact('campaign'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $campaign = Campaign::find($id);
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
        return view('admin.campaign.edit', compact('campaign','campaign_type')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'keyword'              => 'required',
            'same_mobile_allowed'  => 'required' ,
            'campaign_type_id'     => 'required|not_in:0' ,
            
        ]);        
        $campaign = Campaign::find($id);
        $campaign->keyword               =  $request->get('keyword');
        $campaign->same_mobile_allowed   =  $request->get('same_mobile_allowed');
        $campaign->campaign_type_id      =  $request->get('campaign_type_id');
        
        $campaign->touch();        
        return redirect('/admin/campaigns')->with('success', 'Campaign updated!');
    }

    
}
