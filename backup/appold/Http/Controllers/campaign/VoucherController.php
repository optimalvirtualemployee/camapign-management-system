<?php

namespace App\Http\Controllers\campaign;


use App\Http\Controllers\Controller;
use App\Voucher;
use App\Offer;
use App\Campaign;
use App\CampaignFieldValue;
use Illuminate\Http\Request;
use App\Imports\VoucherCodeImport;
use Excel;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers = Voucher::with('campaign')->get();

        return view('admin.voucher.index')->with([
        'vouchers'  => $vouchers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('admin.voucher.create',compact('campaigns','offers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'voucher_code'   => 'required|unique:tbl_vouchers,voucher_code',
            'campaign_id'    => 'required|not_in:0',
            'offer_type_id'       => 'required|not_in:0',
            'start_date'     => 'required',
            'end_date'       => 'required',
            
            
            
        ]);

        Voucher::create($validatedData);

        return redirect('admin/vouchers')->withSuccess('Voucher has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$voucher->campaign_id)->first();
        return view('admin.voucher.show',compact('voucher','campaignFieldValue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('admin.voucher.edit', compact('voucher','campaigns','offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        $request->validate([
            'voucher_code'   => 'required|unique:tbl_vouchers,voucher_code',
            'campaign_id'    => 'required|not_in:0',
            'offer_id'       => 'required|not_in:0',
            'start_date'     => 'required',
            'end_date'       => 'required',
            
        ]);        
        
        $voucher->voucher_code =  $request->get('voucher_code');
        $voucher->campaign_id  =  $request->get('campaign_id');
        $voucher->offer_type_id     =  $request->get('offer_id');
        $voucher->touch();        
        return redirect('/admin/vouchers')->with('success', 'Voucher updated!');
    }
    
    public function uploadVoucherCodeCreate()
    {
        
        $campaigns = Campaign::where('status','ACTIVE')->get();
        $offers = Offer::where('status','ACTIVE')->get();
        return view('admin.voucher.upload',compact('campaigns','offers'));
    }
    
    //Upload mulitple city through excel file
    public function uploadVoucherCodeStore(Request $request)
    {   
        $request->validate([
            
            'campaign_id' => 'required|not_in:0',
            'offer_id'    => 'required|not_in:0',
            'start_date'  => 'required',
            'end_date'    => 'required',
            
            
        ]);
        $campaign_id = $request->campaign_id;
        $offer_id = $request->offer_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;


        try {
            
           Excel::import(new VoucherCodeImport($campaign_id,$offer_id,$start_date,$end_date),$request->voucher_code_file);
           

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        
        return redirect('/admin/vouchers')->withSuccess('You have successfully created a voucher Code!');
    }

    
}
