<?php

namespace App\Http\Controllers\campaign;

use App\FieldTemplate;
use App\Field;
use App\Template;
use App\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CampaignFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $campaign_fields = Template::where('module_name','2')->get();
        
        return view('admin.campaign_field.index')->with([
        'campaign_fields'  => $campaign_fields
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fields = Field::where(['status'=>'ACTIVE','module_name'=>'2'])->get();
        $campaigntypes = Campaigntype::where('status','ACTIVE')->get();
        
        return view('admin.campaign_field.create',compact('fields','campaigntypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            
            'campaigntypeid'=> 'required|not_in:0' ,
            'field_id'=>'required'
            
        ]);
        
        
        
        $template = new  Template();
        
        $template->module_name = "2";
        $template->campaign_type_id = $request->campaigntypeid;
        $template->save();
        
        foreach($request->field_id as $key => $fieldId){

            $fields = Field::where('id',$fieldId)->first();
            $FieldTemplate = new FieldTemplate;
            $FieldTemplate->template_id = $template->id;
            $FieldTemplate->field_id = $fieldId;
            $FieldTemplate->keyword = str_replace(' ','_',$fields->field_name);
            $FieldTemplate->save();
            
        }
        
        /*$fieldid            = $request->field_id;
        foreach($fieldid as $key => $fieldId){

            $fields = Field::where('id',$fieldId)->first();
            
            $FieldTemplate = new CampaignField;
            $FieldTemplate->campaign_type_id = $request->campaigntypeid;
            $FieldTemplate->field_id = $fieldId;
            $FieldTemplate->field_name = str_replace(' ','_',$fields->field_name);
            $FieldTemplate->save();
            
        }*/

        

        
        
        // set dynamic table name according to your requirements

        //$table_name = 'tbl_sms_templates';
        /*$i=1;*/
        // set your dynamic fields (you can fetch this data from database this is just an example)
        //$jsonnewdata = json_decode($feilds->fielddata);
        //dd($jsonnewdata);
        /*foreach($jsonnewdata as $k => $value){*/
            
            //$fields = $jsonnewdata;
            
           /* $i++;*/
            //return $this->createTable($table_name, $fields);
        /*}
        */
        
        
        return redirect('admin/campaign-fields')->withSuccess('Campaign Feild has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignField  $campaignField
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $campaignField = Template::find($id);
        return view('admin.campaign_field.show', compact('campaignField'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignField  $campaignField
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        
        /*$campaignField = CampaignField::where('campaign_type_id',$campaign_type_id)->first();*/
        $campaignField = Template::find($id);
        $campaigntypes = Campaigntype::where('status','ACTIVE')->get();
        $fields = Field::where(['status'=>'ACTIVE','module_name'=>'2'])->get();
        

        return view('admin.campaign_field.edit', compact('campaignField','fields','campaigntypes')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignField  $campaignField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        
        $validatedData = $this->validate($request, [
            
            'campaigntypeid'=> 'required|not_in:0' ,
            'field_id'=>'required'
            
        ]);
        /*$deleteTemplateFieldListRecords    = CampaignField::where('campaign_type_id',$campaign_type_id)->delete();

            $fieldid            = $request->field_id;
            
            foreach($fieldid as $key => $fieldId){
    
                $fields = Field::where('id',$fieldId)->first();
                
                $FieldTemplate = new CampaignField;
                $FieldTemplate->campaign_type_id = $request->campaigntypeid;
                $FieldTemplate->field_id = $fieldId;
                $FieldTemplate->field_name = str_replace(' ','_',$fields->field_name);
                $FieldTemplate->save();
                
            }*/
            
        $template = Template::find($id);
        $template->campaign_type_id               =  $request->get('campaigntypeid');
       
        
        if($template->touch()){

            //delete template field data

            $deleteTemplateFieldListRecords    = FieldTemplate::where('template_id',$id)->delete();

            foreach($request->field_id as $key => $fieldId){

            $fields = Field::where('id',$fieldId)->first();
            $FieldTemplate = new FieldTemplate;
            $FieldTemplate->template_id = $template->id;
            $FieldTemplate->field_id = $fieldId;
            $FieldTemplate->keyword = str_replace(' ','_',$fields->field_name);
            $FieldTemplate->save();
            
            }
        }   
        
        return redirect('/admin/campaign-fields')->with('success', 'Campaign Feild updated!');
    }

    
}
