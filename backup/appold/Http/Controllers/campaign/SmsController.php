<?php

namespace App\Http\Controllers\campaign;


use App\LongCode;
use App\CampaignFieldValue;
use App\Http\Controllers\Controller;
use App\CampaignTemplate;
use Illuminate\Http\Request;
use App\Template;
use App\Campaign;
use App\Voucher;
use App\Outgoing;
use Carbon\Carbon;
use Helper;
use App\RewardCode;
use App\CampaignOfferTemplate;
use App\MobileUsageRule;
class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $campaign_temlate = CampaignTemplate::where('campaign_id',$campaign_id)->get();

        return view('admin.sms_template.index')->with([
        'campaign_temlate'  => $campaign_temlate,
        'campaign_id'  => $campaign_id
        ]);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($campaign_id,$template_id)
    {


        $templates = Template::where('id',$template_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        
    
        $campaign_template = CampaignTemplate::where('campaign_id',$campaign_id)->get();
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->where('campaign_field_id',env('NUMBER_OF_OFFER_TEMPLATE'))->first();
       
        return view('admin.sms_template.create',compact('campaign_id','templates','template_id','campaign_template','campaignFieldValue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$campaign_id,$template_id)
    {
        
        $fieldvalue          = $request->field_value;
        $field_id            = $request->field_key;
        $field_slug            = $request->field_slug;
        $template_keyword_id = $request->template_keyword_id;
        $template_entity_id  = $request->template_entity_id;

        
        /*$campaign = Campaign::where('id',$campaign_id)->first();   
        
        $campaign = Campaign::where('campaign_name',$campaign->campaign_name)->get();*/
        
        $deleteTemplateFieldListRecords    = CampaignTemplate::where('campaign_id',$campaign_id)->delete();
        $deleteTemplateFieldListRecords    = CampaignOfferTemplate::where('campaign_id',$campaign_id)->delete();
        
        if($fieldvalue){
    
            /*$campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->first();
                
            $voucher = Voucher::where('campaign_id',$campaign_id)->first();
            
            $message = $campaignFieldValue['field_value'].' '.$voucher->voucher_code;
            
            
            $longCodeData = LongCode::where('message','LIKE', '%' . $message . '%')->get();*/
            $newArray = [];
            foreach ($fieldvalue as $i => $val) {
                
                //$newArray[] = $i;
                if(!empty($val)){
                    
                    $campaign_template = new CampaignTemplate;
                    $campaign_template->campaign_id = $campaign_id;
                    $campaign_template->field_id = $field_id[$i];
                    $campaign_template->field_value = $val;
                    $campaign_template->field_slug = $field_slug[$i];
                    $campaign_template->template_keyword_id = $template_keyword_id[$i];
                    $campaign_template->template_entity_id = $template_entity_id[$i];
                    $campaign_template->save();
                    
                }
                
                

       
                //$smsText = "This is an invalid voucher code. Please submit a valid voucher code to claim your reward. Team BigCity";
                
                
               
                /*$dynamic_otpmsg = $val;
                $replacements = [
                "{#var#}" => "test",
                " " => '%20',
                    "&" => '%26',
                ];
                
                $smsText = strtr($dynamic_otpmsg, $replacements);
            
                $curl = curl_init();
                $url = "https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$longCodeData[$i]->from."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=$template_keyword_id[$i]";
                curl_setopt_array($curl, array(
                  CURLOPT_URL => $url,
                  CURLOPT_RETURNTRANSFER => TRUE,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",)
                );
                $response = curl_exec($curl);
                 
                $err = curl_error($curl);
                curl_close($curl);
                
                if ($err) {
                  echo "cURL Error #:" . $err;
                  
                } else {
                    
                    $outgoing = new Outgoing;
                    $outgoing->from = $longCodeData[$i]->from;
                    $outgoing->message = $smsText;
                    $outgoing->save();
                    
                  
                }*/
               
                
                
            }   
            //dd($newArray);
            
             return redirect()->back()->withSuccess('SMS has been sended successfully!');
            
        }else{

            $i=0;
            $newdata = array();
            foreach ($request->offer_field_value as $k => $val) {
                if($k==$i){
                    $newdata[$i]['offer_field_value'] = $val;
                }
                $i++;
            }
            $j=0;
            foreach ($request->offer_template_keyword_id as $ke => $vals) {
                if($ke==$j){
                    $newdata[$j]['offer_template_keyword_id'] = $vals;
                }
                $j++;
            }
            $a=0;
            foreach ($request->offer_template_entity_id as $ke => $value) {
                if($ke==$a){
                    $newdata[$a]['offer_template_entity_id'] = $value;
                }
                $a++;
            }
            $f=0;

            foreach ($request->offer_template_id as $ke => $value) {
                if($ke==$f) {
                    $newdata[$f]['offer_template_id'] = $value;
                }
                $f++;
            }

            $fieldjsonData = json_encode($newdata);
            $campaign_template = new CampaignOfferTemplate;
            $campaign_template->campaign_id = $campaign_id;
            $campaign_template->template_id = $template_id;
            $campaign_template->field_value = $fieldjsonData;
            $campaign_template->save();

            return redirect()->back()->withSuccess('SMS has been sended successfully!');
        }


    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function show($campaign_id, $sms_id)
    {
        $campaign_temlate = CampaignTemplate::find($sms_id);
        return view('admin.sms_template.show',compact('campaign_temlate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function edit($campaign_id,$sms_id)
    {
        
        $campaign_temlate = CampaignTemplate::find($sms_id);
        $template = Template::where('status','ACTIVE')->get();
        return view('admin.sms_template.edit', compact('campaign_temlate','template','campaign_id')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sms_id,$campaign_id)
    {

        /*$request->validate([
            'basic_rule_name'    => 'required',
            'basic_rule_sequence'=> 'required',
            'rule_name'          => 'required',
            'rule_sequence'      => 'required',
            'basic_rule_type_id' => 'required|not_in:0',
            'rule_type_id'       => 'required|not_in:0',
            
            
        ]);  */      
        

        $fieldvalue          = $request->field_value;
        $field_id           = $request->field_key;
        $template_keyword_id = $request->template_keyword_id;

        foreach ($fieldvalue as $i => $val) {
            $campaign_temlate = CampaignTemplate::find($sms_id);
            $campaign_template->campaign_id = $id;
            $campaign_template->field_id = $field_id[$i];
            $campaign_template->field_value = $val;
            $campaign_template->template_keyword_id = $template_keyword_id[$i];
            $campaign_template->save();

        }   
        return redirect()->route('campaign.sms.index',$campaign_id)->with('success', 'SMS updated!');
    }
    
    
    public function sendSmsTemplate($incoming_msg_id="")
    {
        
        $val = LongCode::where('is_read','0')->where('id',$incoming_msg_id)->first();
       
        /*foreach ($longCodeData as $i => $val) {*/
             
            if(!empty($val)){
                
                $data = explode(" ",$val->message);
            
                /*$campaignData = CampaignFieldValue::where('field_value',$data[0])->where('campaign_field_id',7)->first();*/
                $campaignData = Campaign::where('campaign_keyword',$data[0])->first();
                
                if(!empty($campaignData)){
                    
                    $currentDate = Carbon::now()->format('Y-m-d');
                    
                    //get end date 
                    /*$campaignEndDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->campaign_id)->where('campaign_field_id',9)->first();
                    
                    $campaignStartDateFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->campaign_id)->where('campaign_field_id',8)->first();*/
                    
                    $startDate = Carbon::parse($campaignData->start_date)->format('Y-m-d');
                    
                    $endDate = Carbon::parse($campaignData->end_date)->format('Y-m-d');
                    
                    $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaignData->id)->first();
                    
                    $excessTriesCount = LongCode::orderBy('id' , 'desc')->where('parts' ,$val->parts)->where('from' ,$val->from)->where('status' ,'VALID')->where('is_read','1')->get();
                    
                    //Campaign Status (Activate & Deactivate)
                    $campaignStatus = Campaign::where('id',$campaignData->id)->where('status','INACTIVE')->first();
                    

                    if(!empty($data[1])){
    
                        $VoucherData = Voucher::where('campaign_id',$campaignData->id)->where('voucher_code',$data[1])->first();

                        $RewardData = RewardCode::where('campaign_id',$campaignData->id)->where('reward_code',$data[1])->first();

                        $offerData = CampaignOfferTemplate::where('campaign_id',$campaignData->id)->first();

                        $BatchCodeData = Voucher::where('campaign_id',$campaignData->id)->where('offer_type_id',env('BATCH_CODE_OFFER_TYPE'))->where('voucher_code',$data[1])->first();

                        dd($BatchCodeData);
                        
                    }

                    //Campaign SMS Template
                    if(!empty($campaignStatus)){
                        
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Promo_Over_SMS',
                                'offerData'=>"",
                                'from'            =>$val->from,
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);
                        
                    }


                    elseif(!empty($offerData->field_value)){
        
                            
                        $jsonnewdata = json_decode($offerData->field_value);

                        foreach ($jsonnewdata as $key => $value) {

                            
                            //checked offer id data
                            $checkedOfferId = LongCode::where('is_read','0')->where('from', $val->from)->where('parts2',$value->offer_template_id)->get();

                            if($checkedOfferId){

                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'offerData'=>$value,
                                        'from'            =>$val->from,
                                        'incoming_msg_id'=>$val->id);
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);

                            }
                        }
                    }

                    //Before Promo SMS
                    elseif($currentDate < $startDate){
                        
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Before_Promo_Start_Date_SMS',
                                'from'            =>$val->from,
                                'offerData'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);

                        

                        
                        
                    }
                    
                    //Promo Over SMS
                    elseif($currentDate > $endDate){
                        
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);


                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Promo_Over_SMS',
                                'from'            =>$val->from,
                                'offerData'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);

                        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$data['campaign_id'])->where('campaign_field_id',env('NOTIFICATION_ONCE_THE_REWARD_CODE_GETS_OVER'))->first();

            
                        //Notification to admin when campaign is now over
                        if(!empty($campaignFieldValue)){

                            $model = Campaign::where('id',$data['campaign_id'])->first();

                            $dynamic_mail = '<p>Dear {{AdminName}},</p>

                                        <p>This promotion is now over as per the below details:</p>

                                        <p><strong>Here are your campaign details:</strong></p>

                                        <p>{{CampaignDetails}}</p>

                                        <p>&nbsp;</p>

                                        <p>Regards<br />
                                        Team BigCity</p>

                                        <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                    <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>';
                        
                            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                                        <tr>
                                            <td><strong>Campaign Id</strong></td>
                                            <td><strong>Campaign Type</strong></td>
                                            <td><strong>Campaign Keyword</strong></td>
                                            <td><strong>Campaign Name</strong></td>
                                            <td><strong>Start Date</strong></td>
                                            <td><strong>End Date</strong></td>
                                            <td><strong>Created Date/Time</strong></td>
                                            <td><strong>Updated Date/Time</strong></td>
                                        </tr>';
                        
                            $codeMessage .= "<tr>
                                            <td>".$model->id."</td>
                                            <td>".$model->campaign_type->name."</td>
                                            <td>".$model->campaign_keyword."</td>
                                            <td>".$model->campaign_name."</td>
                                            <td>".$model->start_date."</td>
                                            <td>".$model->end_date."</td>
                                            <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                            <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                                        </tr>";
                        
                            $codeMessage .= '</table></td></tr>';
                
                            $replacements = [
                                "{{AdminName}}" => 'Admin',
                                "{{CampaignDetails}}" => $codeMessage,
                                
                                "{{emailId}}" => $campaignFieldValue->field_value,
                            ];
                
                            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                              <tr>
                                <td align="center">
                                <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                                  <tr>
                                    <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                                    <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                                    
                            $message .= strtr($dynamic_mail, $replacements);
                            $message .= '</table></td></tr></table></td></tr></table>';
                            $from = 'bigreg@bigcity.in';
                            $to = $campaignFieldValue->field_value;

                            $subject = "Campaign Notification";
                        
                            //call helper function for sendign sms template
                            Helper::sendEmail($to, $subject, $message, $from);
                                
                            }

                            
                          
                        
                    }
                    
                    //Excess Tries SMS Template
                    elseif($excessTriesCount->count() >= '3'){
                        
                        
                        
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);
                            
                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Excess_Tries_SMS',
                                'from'            =>$val->from,
                                'offerData'=>"",
                                'incoming_msg_id'=>$val->id);
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);
                          
                    
                    // Valid Vouchercode exist sms Template   
                    }elseif(!empty($VoucherData) || !empty($RewardData) !empty($BatchCodeData){
                    
                        // Valid SMS (TN) SMS Template
                        if($val->circle == "Tamil Nadu"){
                            //If the reward code gets over
                            if($currentDate > $endDate){
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);
                                    
                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Valid_SMS_(TN)_If_the_reward_code_gets_over',
                                        'from'            =>$val->from,
                                        'offerData'=>"",
                                        'incoming_msg_id'=>$val->id );
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                                  
                                
                            }else{
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);

                                $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Valid_SMS_(TN)',
                                'offerData'=>"",
                                'from'            =>$val->from,
                                'incoming_msg_id'=>$val->id );
                               
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                            }
                            
                            
                        }else{
                            
                            //If the reward code gets over
                            if($currentDate > $endDate){
                                
                                
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['is_read'=>'1','status' => 'INVALID']);


                                $data = array(
                                        'campaign_id'     =>$campaignData->id,
                                        'field_slug_value'=>'Valid_SMS_(ROI)_If_the_reward_code_gets_over',
                                        'from'            =>$val->from,
                                        'offerData'=>"",
                                        'incoming_msg_id'=>$val->id);
                                       
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                                    
                                  
                                
                            }else{
                                
                            
                                
                                //Updating data
                                $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'VALID','is_read'=>'1']);


                                // Valid SMS (ROI) SMS Template
                                $data = array(
                                    'campaign_id'     =>$campaignData->id,
                                    'field_slug_value'=>'Valid_SMS_(ROI)',
                                    'from'            =>$val->from,
                                    'offerData'=>"",
                                    'incoming_msg_id'=>$val->id );
                               
                                //call helper function for sendign sms template
                                Helper::sendSmsTemplate($data);
                                    
                                  
                            }
                            
                            
                        }
                    
                    }else{
                    
                        //Updating data
                        $longcodeRecordUpdated = LongCode::where('is_read','0')->where('from', $val->from)->update(['status' => 'INVALID','is_read'=>'1']);

                        // Invalid Code vouchercode SMS Template
                        $data = array(
                                'campaign_id'     =>$campaignData->id,
                                'field_slug_value'=>'Invalid_Code_SMS',
                                'from'            =>$val->from,
                                'offerData'=>"",
                                'incoming_msg_id'=>$val->id );
                               
                        //call helper function for sendign sms template
                        Helper::sendSmsTemplate($data);
                        
                        
                            
                          
                        
                        
                    }
                }
        
            }
        /*}*/

        return redirect()->back()->withSuccess('SMS has been sended successfully!');

      
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMultiOfferTemplate($campaign_id,$template_id)
    {


        $templates = Template::where('id',$template_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        
    
        $campaign_template = CampaignOfferTemplate::where('campaign_id',$campaign_id)->first();
        
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign_id)->where('campaign_field_id',env('NUMBER_OF_OFFER_TEMPLATE'))->first();
        
        return view('admin.sms_template.multioffertemplate',compact('campaign_id','templates','template_id','campaign_template','campaignFieldValue'));
    }

    public function validateMobilePerPromotion($campaign_id="3",$mobile_no="918826592901"){

        $MobileUsageRule = CampaignFieldValue::where('campaign_id',$campaign_id)->get();

        $CountMobileDatePerTotal = LongCode::where('from' ,$mobile_no)->where('is_read','0')->count();

        $CountMobileDatePerDay = LongCode::orderBy('id' , 'desc')->where('from' ,$mobile_no)->where('status' ,'VALID')->where('is_read','1')->get();

        $CountMobileDatePerWeek = LongCode::orderBy('id' , 'desc')->where('from' ,$mobile_no)->where('status' ,'VALID')->where('is_read','1')->get();

        $CountMobileDatePerMonth = LongCode::orderBy('id' , 'desc')->where('from' ,$mobile_no)->where('status' ,'VALID')->where('is_read','1')->get();

        switch ($MobileUsageRule) {

            case '1':
                if ($MobileUsageRule->mobile_usage_value > $CountMobileDatePerTotal) {
                    dd($MobileUsageRule->mobile_usage_value);
                    
                
                    $erMsg = $this->MainModel->getErrorMsg($dta);
                    $this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => 'alert alert-danger'));
                    redirect('');
                }
                else{
                    return 'ddd';
                }
                break;
            case '2':
                if ($caplimitData > $CountRegisterDatePerWeek) {
                    
                    $erMsg = $this->MainModel->getErrorMsg($dta);
                    $this->session->set_flashdata('msgshow', array('message' => $erMsg->meta_value,'class' => 'alert alert-danger'));                           
              redirect('');
                }
                break;
            
            
        }
    }
}
