<?php

namespace App\Http\Controllers\Whatsapp\rule;

use App\Http\Controllers\Controller;
use App\WhatsappModels\RuleType;
use Illuminate\Http\Request;

class RuleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ruleTypes = RuleType::get();

        return view('sms.ruletype.index')->with([
        'ruleTypes'  => $ruleTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sms.ruletype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:tbl_rule_types',
            
            
        ]);

        RuleType::create($validatedData);

        return redirect('admin/sms/ruletypes')->withSuccess('Rule Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RuleType  $ruleType
     * @return \Illuminate\Http\Response
     */
    public function show(RuleType $ruleType,$id)
    {
        $ruleType = RuleType::find($id);
        return view('sms.ruletype.show',compact('ruleType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RuleType  $ruleType
     * @return \Illuminate\Http\Response
     */
    public function edit(RuleType $ruleType,$id)
    {
        $ruleType = RuleType::find($id);
        return view('sms.ruletype.edit', compact('ruleType'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RuleType  $ruleType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RuleType $ruleType,$id)
    {
        $request->validate([
            'name'=>'required',
            
            
        ]);        
        $RuleType = RuleType::find($id);
        $RuleType->name =  $request->get('name');
        $RuleType->touch();        
        return redirect('/admin/sms/ruletypes')->with('success', 'Rule Types updated!');
    }

    
}
