<?php

namespace App\Http\Controllers\Whatsapp;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Helper;
use App\WhatsappModels\CampaignFieldValue;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\WhatsAppData;
use DB;

class AuthController extends Controller
{
    
     
    /*public function dashboard()
    {
        
        
        if(Auth::check()){

            return view('admin.dashboard');
        }

        return Redirect::to("/")->withSuccess('Opps! You do not have access');
    }*/

    public function dashboard(Request $request)
    {
        
        $method = $request->method();


        if(Auth::check()){

            $campaigns = Campaign::orderBy('id', 'DESC')->get();
            $data = WhatsAppData::Where('parts',$request->post('campaign'))->count();
            $campaign_keyword = $request->post('campaign');
            $valid = WhatsAppData::Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->count();
            $invalid = WhatsAppData::Where(['parts'=>$request->post('campaign'),'status'=>'invalid'])->count();
            $unique = WhatsAppData::Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->distinct('from')->count();
            $campaign = $request->post('campaign');
            $one_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'" GROUP BY `from` HAVING COUNT(`from`)=1) tbl_lgcd');
            $one_time = $one_time[0]->cnt;
            $two_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'" GROUP BY `from` HAVING COUNT(`from`)=2) tbl_lgcd');
            $two_time = $two_time[0]->cnt;
            $three_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'" GROUP BY `from` HAVING COUNT(`from`)=3) tbl_lgcd');
            $three_time = $three_time[0]->cnt;
            $four_time = DB::select('SELECT count(tbl_lgcd.num) as cnt FROM (select COUNT(`from`) as num from `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'" GROUP BY `from` HAVING COUNT(`from`)=4) tbl_lgcd');
            $four_time = $four_time[0]->cnt;

            $day_chart = DB::select('SELECT DAYNAME(created_at) as day , count(*)
 as day_entries FROM `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'"  group by day ');
            $week_chart = DB::select('SELECT WEEK(created_at) as week , count(*)
 as entries FROM `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'"  group by week ');
            $circle_chart = json_encode(DB::select('SELECT `circle` as name , count(*)
 as circle FROM `whatsapp_tbl_whatsapp_data` WHERE status="VALID" AND parts="'.$campaign.'"  group by circle '));
            
            
            $a = DB::table('whatsapp_tbl_whatsapp_data')->select(DB::raw('"00:00:00 - 03:00:00" as time'), DB::raw('count(*)
 as entries'))->Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->whereTime('created_at','>=','00:00:00')->whereTime('created_at','<=','03:00:00')->get();
            $b = DB::table('whatsapp_tbl_whatsapp_data')->select(DB::raw('"03:00:00 - 06:00:00" as time'), DB::raw('count(*)
 as entries'))->Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->whereTime('created_at','>=','03:00:00')->whereTime('created_at','<=','06:00:00')->get();
            $c = DB::table('whatsapp_tbl_whatsapp_data')->select(DB::raw('"06:00:00 - 09:00:00" as time'), DB::raw('count(*)
 as entries'))->Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->whereTime('created_at','>=','06:00:00')->whereTime('created_at','<=','09:00:00')->get();
            $d = DB::table('whatsapp_tbl_whatsapp_data')->select(DB::raw('"09:00:00 - 12:00:00" as time'), DB::raw('count(*)
 as entries'))->Where(['parts'=>$request->post('campaign'),'status'=>'valid'])->whereTime('created_at','>=','09:00:00')->whereTime('created_at','<=','12:00:00')->get();
            $e= array($a[0]);
            $f= array($b[0]);
            $g= array($c[0]);
            $h= array($d[0]);
            $time_chart = json_encode(array_merge($e,$f,$g,$h));
            
            
            return view('whatsapp.dashboard',compact('campaigns','data','campaign_keyword','valid','invalid','unique','one_time','two_time','three_time','four_time','day_chart','time_chart','week_chart','circle_chart'));
        }

        return Redirect::to("/")->withSuccess('Opps! You do not have access');

    }

 
    

    public function changeStatus(Request $request)
    {
        
        $model = app("App\SmsModels\\$request->model_name")->where('id',$request->id)->first();
       
        $model->status = $request->status;
        $model->save();
        
        if(!empty($model->campaign_keyword)){

            $dynamic_mail = '<p>Dear {{AdminName}},</p>

                            <p>Your campaign has been {{Status}} as per the below details:</p>

                            <p><strong>Here are your campaign details:</strong></p>

                            <p>{{CampaignDetails}}</p>

                            <p>&nbsp;</p>

                            <p>Regards<br />
                            Team BigCity</p>

                            <p><img alt="" src="https://gallery.mailchimp.com/38c89703792e6736714dfaae8/images/fa25bbc2-aff7-473a-84b3-429eb5b69c55.png" style="width:69px" /></p>

                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                                <tbody>
                                    <tr>
                                        <td>
                                        <p><em>&copy; 2020 BigCity Promotions, All rights reserved.</em></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>';
            
            $codeMessage = '<table class="eWidth" border="1" cellspacing="0" cellpadding="10" style="width: 100%;text-align: center; font-family:Arial, Helvetica, sans-serif;">
                            <tr>
                                <td><strong>Campaign Id</strong></td>
                                <td><strong>Campaign Type</strong></td>
                                <td><strong>Campaign Keyword</strong></td>
                                <td><strong>Campaign Name</strong></td>
                                <td><strong>Created Date/Time</strong></td>
                                <td><strong>Updated Date/Time</strong></td>
                            </tr>';
            
            $codeMessage .= "<tr>
                                <td>".$model->id."</td>
                                <td>".$model->campaign_type['name']."</td>
                                <td>".$model->campaign_keyword."</td>
                                <td>".$model->campaign_name."</td>
                                <td>".$model->created_at->format('d M Y')." & ".$model->created_at->format('g:i A')."</td>
                                <td>".$model->updated_at->format('d M Y')." & ".$model->updated_at->format('g:i A')."</td>
                            </tr>";
            
            $codeMessage .= '</table></td></tr>';
    
            $replacements = [
                    "{{AdminName}}" => 'Admin',
                    "{{CampaignDetails}}" => $codeMessage,
                    "{{Status}}"  => ($model->status == "ACTIVE") ? "activate":"deactivate",
                    "{{emailId}}" => $model->status_trigger_rule,
                ];
    
            $message = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="background-color:#fff;">
                  <tr>
                    <td align="center">
                    <table class="eWidth" border="0" cellspacing="0" cellpadding="0" style="max-width:700px; width:700px;">
                      <tr>
                        <td bgcolor="#FFFFFF" style="background-color:#FFF;">
                        <table border="0" cellspacing="0" cellpadding="0" style="max-width:700px;">';
                        
            $message .= strtr($dynamic_mail, $replacements);
            $message .= '</table></td></tr></table></td></tr></table>';
            $from = 'bigreg@bigcity.in';
            //$to = 'prashant@bigcity.in';
            
            $subject = "Campaign Notification";
            $to = $model->status_trigger_rule;

            $subject = "Campaign Notification";
            //dd(explode(',',str_replace(" ",$to)));
            foreach(explode(',',$to) as $email){

                //call helper function for sendign sms template
                Helper::sendEmail($email, $subject, $message, $from);
    
            }
            //call helper function for sendign sms template
           // Helper::sendEmail($to, $subject, $message, $from);
        }
        
        
        return response()->json(['success'=>'status change successfully.']);
    }
    
}
?>