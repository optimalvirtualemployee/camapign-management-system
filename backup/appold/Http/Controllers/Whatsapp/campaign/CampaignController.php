<?php

namespace App\Http\Controllers\Whatsapp\campaign;

use App\Http\Controllers\Whatsapp\campaign\SmsController;
use App\WhatsappModels\Campaign;
use App\WhatsappModels\Voucher;
use App\WhatsappModels\WhatsAppData;
use App\WhatsappModels\RewardCode;
use App\WhatsappModels\CampaignType;
use App\WhatsappModels\Template;
use App\WhatsappModels\FieldTemplate;
use App\WhatsappModels\FieldType;
use App\WhatsappModels\CampaignField;
use App\WhatsappModels\CampaignFieldValue;
use App\WhatsappModels\LongCode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\WhatsappRewardCodeImport;
use Excel;
use App\WhatsappModels\MobileUsageRule;
use App\WhatsappModels\CampaignMobileUsageRule;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $campaigns = Campaign::orderBy('id', 'DESC')->get();
        
        return view('whatsapp.campaign.index')->with([
        'campaigns'  => $campaigns
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
        $campaignField = Template::where('campaign_type_id',$request->campaign_type_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        //dd($campaignField);
        $campaignFieldValue = CampaignFieldValue::get();
        $campaignMobileUsageRuleValue = CampaignMobileUsageRule::get();
        
        $mobile_no_usage = MobileUsageRule::get();
        return view('whatsapp.campaign.create',compact('campaign_type','campaignField','campaignFieldValue','campaignMobileUsageRuleValue','mobile_no_usage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validatedData = $this->validate($request, [
            'campaign_name'              => 'required|unique:whatsapp_tbl_campaigns,campaign_name',
           /* 'campaign_keyword'              => 'required',*/
            'start_date'              => 'required',
            'end_date'              => 'required',
            'status_trigger_rule'              => 'required',
            'campaign_type_id'     => 'required|not_in:0' ,
            
            
        ]);

        $campaign = new Campaign();
        $campaign->campaign_name               =  $request->campaign_name;
       
        $campaign->campaign_type_id      =  $request->campaign_type_id;
        $campaign->campaign_keyword      =  $request->campaign_keyword;
        $campaign->start_date      =  $request->start_date;
        $campaign->end_date      =  $request->end_date;
        $campaign->status_trigger_rule      =  $request->status_trigger_rule;
        $campaign->save();

        $mobile_usage_value           = $request->mobile_usage_value;

        $mobile_no_usage_rules           = $request->mobile_no_usage_rules;

        if(!empty($mobile_no_usage_rules)){

            foreach ($mobile_no_usage_rules as $i => $val) {
            
            $campaign_template = new CampaignMobileUsageRule;
            $campaign_template->campaign_id = $campaign->id;
            
            $campaign_template->mobile_no_usage_rules = $val;
            $campaign_template->mobile_usage_value = $mobile_usage_value[$i];
            $campaign_template->save();

            
            }
        }
        return redirect('admin/whatsapp/whatsappcampaigns')->withSuccess('Campaign has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign->id)->first();
        return view('whatsapp.campaign.show',compact('campaign','campaignFieldValue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign,$id)
    {

        $campaign = Campaign::find($id);
        $campaign_type = CampaignType::where('status','ACTIVE')->get();
    
        $templates = Template::where(['status'=>'ACTIVE','module_name'=>'1'])->with('field_template')->get();
        
        /*$campaignField = CampaignField::where('campaign_type_id',$campaign->campaign_type_id)->with('field.field_type')->get();*/
        //$campaignField = Template::where('module_name','2')->where('campaign_type_id',$campaign->campaign_type_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        $campaignField = Template::where('module_name','2')->where('campaign_type_id',$campaign->campaign_type_id)->where('status','ACTIVE')
                                    ->first();
        $field_template = $campaignField->field_template()->where('panel_name', 'WHATSAPP')->get();
        $campaignFieldValue = CampaignFieldValue::where('campaign_id',$campaign->id)->get();
        
        $mobile_no_usage = MobileUsageRule::get();
      
        
        return view('whatsapp.campaign.edit', compact('campaign','campaign_type','templates','campaignField','campaignFieldValue','mobile_no_usage','field_template')); 
    }

    /**
     * Update the specified resource in storage.

     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Campaign $campaign,$id)
    {
        
        
        $request->validate([
            'campaign_name'    => 'required',
            'campaign_type_id' => 'required|not_in:0',
            
            'campaign_keyword'              => 'required',
            'start_date'              => 'required',
            'end_date'              => 'required',
            'status_trigger_rule'              => 'required',
        ]);   

        $deleteTemplateFieldListRecords    = CampaignFieldValue::where('campaign_id',$id)->delete();
        $deleteTemplateFieldListRecords    = CampaignMobileUsageRule::where('campaign_id',$id)->delete();
        $campaign = Campaign::find($id);
        $campaign->campaign_name               =  $request->get('campaign_name');
        $campaign->template_id               =  $request->get('template_id');
        $campaign->campaign_type_id      =  $request->get('campaign_type_id');
        $campaign->campaign_keyword      =  $request->get('campaign_keyword');
        $campaign->start_date      =  $request->get('start_date');
        $campaign->end_date      =  $request->get('end_date');
        $campaign->status_trigger_rule      =  $request->get('status_trigger_rule');
        $campaign->touch();
        
        $mobile_no_usage_rules          = $request->mobile_no_usage_rules;
        $mobile_usage_value           = $request->mobile_usage_value;
        
        if($mobile_no_usage_rules){

            foreach ($mobile_no_usage_rules as $i => $val) {
            
            $campaign_template = new CampaignMobileUsageRule;
            $campaign_template->campaign_id = $campaign->id;
            $campaign_template->mobile_no_usage_rules = $val;
            $campaign_template->mobile_usage_value = $mobile_usage_value[$i];
            $campaign_template->save();

            
            }
        }
        if($request->field_value){

            $fieldvalue          = $request->field_value;
            $field_id           = $request->field_key;
           
            foreach ($fieldvalue as $i => $val) {
                $campaign_template = new CampaignFieldValue;
                $campaign_template->campaign_id = $campaign->id;
                $campaign_template->campaign_field_id = $field_id[$i];
                $campaign_template->field_value = $val;
                $campaign_template->save();

                
            } 
           
        }
        if($request->field_value_file){
                
            $this->uploadRewardCode($campaign->id,$request->field_value_file);
            
        }
        return redirect('/admin/whatsapp/whatsappcampaigns')->with('success', 'Campaign updated!');
    }

    public function getFieldTemplateList(Request $request)
    {
        /*$campaignField = CampaignField::where('campaign_type_id',$request->campaign_type_id)->with('field_type')->get();*/
        
        $campaignField = Template::where('campaign_type_ids',$request->campaign_type_id)->where('status','ACTIVE')->with('field_template.field_type')->first();
        
        /*$field_template = FieldTemplate::where("template_id",$request->template_id)
                       ->pluck("keyword","id");*/
        
        return response()->json($campaignField);
    }
    
    public function getKeywordData(Request $request)
    {
        /*https://cms.optimaldevelopments.com/get-keyword-data/?from={from}&message={message}&ts={ts}&circle={circle}&operator={optr}&msgid={msgid}&parts[1]={parts[1]}*/
        file_put_contents('sonam1.txt', $request);
        
        $fieldData = new LongCode();
        
        $fieldData->from =  $request->from;
        $fieldData->message =  $request->message;
        $fieldData->ts =  $request->ts;
        $fieldData->circle =  $request->circle;
        $fieldData->operator =  $request->operator;
        $fieldData->msgid =  $request->msgid;
        $fieldData->parts =  $request->parts[1];
        $fieldData->parts2 =  $request->parts[2];
        $fieldData->parts3 =  $request->parts[3];
        
        $fieldData->save();

        /*(new SmsController)->sendWhatsappTemplate($fieldData->id);*/
        
    }
    
    //Upload mulitple city through excel file
    public function uploadRewardCode($campaign_id,$field_value_file)
    {   
        $id = $campaign_id;
        
        
        try {
            
           Excel::import(new WhatsappRewardCodeImport($id),$field_value_file);
           

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            
            return back()->withError($e->getMessage())->withInput();
        }
        
        return redirect('/admin/whatsapp/whatsappcampaigns')->withSuccess('You have successfully created a Rewrad Code!');
    }
    
    // function for sending SMS on given mobile number
    function sendsms(){
       
       
       
        $mobile = "8826592901";
       
        $smsText = "This is an invalid voucher code. Please submit a valid voucher code to claim your reward. Team BigCity";
        $responseBody = file_get_contents("https://api-alerts.kaleyra.com/v4/?method=sms&sender=BIGCTY&to=".$mobile."&message=".$smsText."&unicode=auto&api_key=A4bce192ad153819bb81150367d5d17f3&entity_id=1701158219659617455&template_id=1707161536585787684");
        if($responseBody){
            
            
          return true;
        }else{
            return false;
        }
    }
    // function for sending SMS on given mobile number

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function whatsappstats($campaign_id)
    {
       
        $campaigns = Campaign::where('id',$campaign_id)->first();
        $data = WhatsAppData::Where('parts',$campaigns->campaign_name)->count();
        $excessTriesCount = WhatsAppData::orderBy('id' , 'desc')->where('parts' ,$campaigns->campaign_name)->where('status' ,'EXCESS')->count();
        $expiredCount = WhatsAppData::orderBy('id' , 'desc')->where('parts' ,$campaigns->campaign_name)->where('status' ,'EXPIRED')->count();
        $voucherCount = Voucher::where('campaign_id',$campaign_id)->count();
        $rewardCount = RewardCode::where('campaign_id',$campaign_id)->where('isused','1')->count();
        
        $duplicateVoucherCount = WhatsAppData::where('parts',$campaigns->campaign_name)->where('status' ,'DUPLICATE')->where('is_read','1')->count();
                        
        $valid = WhatsAppData::Where(['parts'=>$campaigns->campaign_name,'status'=>'valid'])->count();
        $invalid = WhatsAppData::Where(['parts'=>$campaigns->campaign_name,'status'=>'invalid'])->count();
        $unique = WhatsAppData::Where(['parts'=>$campaigns->campaign_name,'status'=>'valid'])->distinct('from')->count();
        return view('whatsapp.campaign.stats',compact('campaigns','data','valid','invalid','unique','excessTriesCount','expiredCount','voucherCount','duplicateVoucherCount','rewardCount'));
    }
    
    public function rewardcodeList()
    {
       
        $rewards = RewardCode::with('campaigns')->get();
        
        return view('whatsapp.campaign.reward-list')->with([
        'rewards'  => $rewards
        ]);
    }
    
}
