<?php

namespace App\Http\Controllers\Whatsapp\campaign;

use App\WhatsappModels\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\WhatsappModels\Template;
use App\WhatsappModels\Offer;

class OfferTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offerTypes = Offer::get();

        return view('whatsapp.offer_type.index')->with([
        'offerTypes'  => $offerTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whatsapp.offer_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:whatsapp_tbl_offers',
            
            
        ]);

        $offerType = new Offer();
        $offerType->name = $request->name;
        $offerType->cashback_price =  $request->cashback_price;
        $offerType->cashback_type_id =  $request->cashback_type_id;
        $offerType->save();

        return redirect('admin/whatsapp/whatsappoffers')->withSuccess('Offer Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer,$id)
    {
        $offerType = Offer::find($id);
        return view('whatsapp.offer_type.show',compact('offerType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer,$id)
    {
        
        $offerType = Offer::find($id);
       
        return view('whatsapp.offer_type.edit', compact('offerType'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer,$id)
    {
        $request->validate([
            'name'=>'required',
            
            
        ]);        
        $offerType = Offer::find($id);
        $offerType->name =  $request->get('name');
        $CampaignType->touch();        
        
        return redirect('/admin/whatsapp/offers')->with('success', 'Offer Types updated!');
    }

}
