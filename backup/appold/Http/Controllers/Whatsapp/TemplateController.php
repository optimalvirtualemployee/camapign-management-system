<?php

namespace App\Http\Controllers\Whatsapp;

use App\Http\Controllers\Controller;

use App\SmsModels\FieldTemplate;
use App\WhatsappModels\Field;
use App\WhatsappModels\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $templates = Template::where('module_name','1')->get();
       
        return view('whatsapp.template.index')->with([
        'templates'  => $templates,
        
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
        
        $fields = Field::where(['status'=>'ACTIVE','module_name'=>'1'])->get();
       
        return view('whatsapp.template.create',compact('fields'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'template_name'  => 'required|unique:sms_tbl_templates',
            'template_detail'=> 'required' ,
            'field_id'=>'required'
            
        ]);
        $validatedData['modue_name'] = "1";
        $template = Template::create($validatedData);
        

        foreach($request->field_id as $key => $fieldId){

            $fields = Field::where('id',$fieldId)->first();
            $FieldTemplate = new FieldTemplate;
            $FieldTemplate->template_id = $template->id;
            $FieldTemplate->field_id = $fieldId;
            $FieldTemplate->panel_name = 'WHATSAPP';
            $FieldTemplate->keyword = str_replace(' ','_',$fields->field_name);
            $FieldTemplate->save();
            
        }
        return redirect('admin/whatsapp/whatsapptemplates')->withSuccess('Template has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmsTemplate  $smsTemplate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = Template::find($id);
        return view('whatsapp.template.show', compact('template')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmsTemplate  $smsTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Template::find($id);
        $field_template = $template->field_template()->where('panel_name', 'WHATSAPP')->get();
        $fields = Field::where(['status'=>'ACTIVE','module_name'=>'1'])->get();

        return view('whatsapp.template.edit', compact('fields','template','field_template')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmsTemplate  $smsTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $request->validate([
            'template_name'  => 'required',
            'template_detail'=> 'required' ,
            'field_id' =>'required'
            
        ]);        

        $template = Template::find($id);
        $template->template_name               =  $request->get('template_name');
        $template->template_detail   =  $request->get('template_detail');
        
        if($template->touch()){

            //delete template field data

            $deleteTemplateFieldListRecords    = FieldTemplate::where('template_id',$id)->where('panel_name', 'WHATSAPP')->delete();

            foreach($request->field_id as $key => $fieldId){

            $fields = Field::where('id',$fieldId)->first();
            $FieldTemplate = new FieldTemplate;
            $FieldTemplate->template_id = $template->id;
            $FieldTemplate->field_id = $fieldId;
            $FieldTemplate->panel_name = 'WHATSAPP';
            $FieldTemplate->keyword = str_replace(' ','_',$fields->field_name);
            $FieldTemplate->save();
            
            }
        }   
        return redirect('/admin/whatsapp/whatsapptemplates')->with('success', 'Template updated!');
        
    }

    

    
}
