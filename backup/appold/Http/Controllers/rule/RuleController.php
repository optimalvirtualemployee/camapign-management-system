<?php

namespace App\Http\Controllers\rule;

use App\Http\Controllers\Controller;
use App\Rule;
use App\RuleType;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $rules = Rule::where('campaign_id',$campaign_id)->get();

        return view('admin.rule.index')->with([
        'rules'  => $rules,
        'campaign_id'  => $campaign_id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($campaign_id)
    {

        $ruleType = RuleType::where('status','ACTIVE')->get();
        return view('admin.rule.create',compact('ruleType','campaign_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$campaign_id)
    {


        $validatedData = $this->validate($request, [
            'basic_rule_name'    => 'required',
            'basic_rule_sequence'=> 'required',
            'rule_name'          => 'required',
            'rule_sequence'      => 'required',
            'basic_rule_type_id' => 'required|not_in:0',
            'rule_type_id'       => 'required|not_in:0',
           

            
            
        ]);
        $rule = new Rule;
        $rule->basic_rule_name    = $request->basic_rule_name;
        $rule->basic_rule_sequence = $request->basic_rule_sequence;
        $rule->rule_name           = $request->rule_name;
        $rule->rule_sequence      = $request->rule_sequence;
        $rule->basic_rule_type_id = $request->basic_rule_type_id;
        $rule->rule_type_id       = $request->rule_type_id;
        $rule->campaign_id       = $campaign_id;
        $rule->save();
        

        return redirect()->route('campaign.rules.index',$campaign_id)->withSuccess('Rule has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function show($campaign_id, $rule_id)
    {
        $rule = Rule::find($rule_id);
        return view('admin.rule.show',compact('rule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function edit($campaign_id,$rule_id)
    {
        
        $rule = Rule::find($rule_id);
        $ruleType = RuleType::where('status','ACTIVE')->get();
        return view('admin.rule.edit', compact('rule','ruleType','campaign_id')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rule_id,$campaign_id)
    {

        $request->validate([
            'basic_rule_name'    => 'required',
            'basic_rule_sequence'=> 'required',
            'rule_name'          => 'required',
            'rule_sequence'      => 'required',
            'basic_rule_type_id' => 'required|not_in:0',
            'rule_type_id'       => 'required|not_in:0',
            
            
        ]);        
        $rule = Rule::find($rule_id);

        $rule->basic_rule_name =  $request->get('basic_rule_name');
        $rule->basic_rule_sequence =  $request->get('basic_rule_sequence');
        $rule->rule_name =  $request->get('rule_name');
        $rule->rule_sequence =  $request->get('rule_sequence');
        $rule->basic_rule_type_id =  $request->get('basic_rule_type_id');
        $rule->rule_type_id =  $request->get('rule_type_id');
        $rule->touch();        
        return redirect()->route('campaign.rules.index',$campaign_id)->with('success', 'Rule updated!');
    }

    
}
