<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;

use App\SmsModels\CampaignTemlate;
use Illuminate\Http\Request;
use App\SmsModels\Template;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $campaign_temlate = CampaignTemlate::where('campaign_id',$campaign_id)->get();

        return view('sms.sms_template.index')->with([
        'campaign_temlate'  => $campaign_temlate,
        'campaign_id'  => $campaign_id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($campaign_id,$template_id)
    {

        $templates = Template::where('status','ACTIVE')->with('field_template')->get();
        $field_template = FieldTemplate::where("template_id",$template_id)
                       ->pluck("keyword","id");
                       
        return view('sms.sms_template.create',compact('campaign_id','templates','field_template'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$campaign_id)
    {


        $validatedData = $this->validate($request, [
            'basic_rule_name'    => 'required',
            'basic_rule_sequence'=> 'required',
            'rule_name'          => 'required',
            'rule_sequence'      => 'required',
            'basic_rule_type_id' => 'required|not_in:0',
            'rule_type_id'       => 'required|not_in:0',
           

            
            
        ]);
        $fieldvalue          = $request->field_value;
        $field_id           = $request->field_key;
        $template_keyword_id = $request->template_keyword_id;

        foreach ($fieldvalue as $i => $val) {
            $campaign_temlate = new CampaignTemlate;
            $campaign_template->campaign_id = $id;
            $campaign_template->field_id = $field_id[$i];
            $campaign_template->field_value = $val;
            $campaign_template->template_keyword_id = $template_keyword_id[$i];
            $campaign_template->save();

        }   
        

        return redirect()->route('campaign.sms.index',$campaign_id)->withSuccess('SMS has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function show($campaign_id, $sms_id)
    {
        $campaign_temlate = CampaignTemlate::find($sms_id);
        return view('sms.sms_template.show',compact('campaign_temlate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function edit($campaign_id,$sms_id)
    {
        
        $rule = Rule::find($sms_id);
        $ruleType = RuleType::where('status','ACTIVE')->get();
        return view('sms.sms_template.edit', compact('rule','ruleType','campaign_id')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sms_id,$campaign_id)
    {

        $request->validate([
            'basic_rule_name'    => 'required',
            'basic_rule_sequence'=> 'required',
            'rule_name'          => 'required',
            'rule_sequence'      => 'required',
            'basic_rule_type_id' => 'required|not_in:0',
            'rule_type_id'       => 'required|not_in:0',
            
            
        ]);        
        

        $fieldvalue          = $request->field_value;
        $field_id           = $request->field_key;
        $template_keyword_id = $request->template_keyword_id;

        foreach ($fieldvalue as $i => $val) {
            $campaign_temlate = CampaignTemlate::find($sms_id);
            $campaign_template->campaign_id = $id;
            $campaign_template->field_id = $field_id[$i];
            $campaign_template->field_value = $val;
            $campaign_template->template_keyword_id = $template_keyword_id[$i];
            $campaign_template->save();

        }   
        return redirect()->route('campaign.sms.index',$campaign_id)->with('success', 'SMS updated!');
    }

    
}
