<?php

namespace App\Http\Controllers\SMS\campaign;

use App\SmsModels\CampaignType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SmsModels\Template;
use App\SmsModels\Offer;
use App\SmsModels\CashbackType;
class OfferTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offerTypes = Offer::get();

        return view('sms.offer_type.index')->with([
        'offerTypes'  => $offerTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sms.offer_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'name'      => 'required|unique:sms_tbl_offers',
            
            
        ]);

        $offerType = new Offer();
        $offerType->name = $request->name;
        $offerType->cashback_price =  $request->cashback_price;
        $offerType->cashback_type_id =  $request->cashback_type_id;
        $offerType->save();
        return redirect('admin/sms/offers')->withSuccess('Offer Types has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer,$id)
    {
        $offerType = Offer::find($id);
        return view('sms.offer_type.show',compact('offerType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $offerType = Offer::find($id);
        $cashbacktypes = CashbackType::where(['status'=>'ACTIVE'])->get();
        return view('sms.offer_type.edit', compact('offerType','cashbacktypes'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CampaignType  $campaignType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'name'=>'required',
            
            
        ]);        
        $offerType = Offer::find($id);
        $offerType->name =  $request->get('name');
        $offerType->cashback_price =  $request->get('cashback_price');
        $offerType->cashback_type_id =  $request->get('cashback_type_id');
        $offerType->touch();        
        
        return redirect('/admin/sms/offers')->with('success', 'Offer Types updated!');
    }

}
