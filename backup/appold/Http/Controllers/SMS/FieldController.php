<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;

use App\SmsModels\Field;
use App\SmsModels\FieldType;
use App\SmsModels\FieldTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class FieldController extends Controller
{

    /**
     * Create dynamic table along with dynamic fields
     *
     * @param       $table_name
     * @param array $fields
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTable($table_name, $fields = [])
    {
        // check if table is not already exists
        /*if (!Schema::hasTable($table_name)) {*/
            Schema::table($table_name, function (Blueprint $table) use ($fields, $table_name) {
                
                if (count($fields) > 0) {

                    foreach ($fields as $field) {

                        $table->string(str_replace(' ','_', $field->fieldname))->after('id');
                        $table->string(str_replace(' ','_', $field->templateid))->after(str_replace(' ','_', $field->fieldname));
                    }
                }
                
            });

            return redirect('admin/feilds')->withSuccess('Feild has been created successfully!');
        /*}

        return response()->json(['message' => 'Given table is already existis.'], 400);*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $fields = Field::get();

        return view('sms.fields.index')->with([
        'fields'  => $fields
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fieldtypes = Fieldtype::where('status','ACTIVE')->get();
        
        return view('sms.fields.create',compact('fieldtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validatedData = $this->validate($request, [

            "fieldid.*" => 'required|not_in:0',
            "field_name.*" => 'required|not_in:0',
            "field_name" => "required|unique:sms_tbl_fields",
            'module_name' =>'required|in:1,2' 
            
            
        ]);
        
        $fieldid            = $request->fieldid;
        $fieldname      = $request->field_name;
        $modulename      = $request->module_name;

        foreach ($fieldid as $i => $val) {
            $feilds = new Field;
            $feilds->module_name = $modulename;
            $feilds->field_name = $fieldname[$i];
            $feilds->field_slug = str_replace(' ', '_', $fieldname[$i]);
            $feilds->field_type_id = $val;
            $feilds->save();

            /*$FieldTemplate = new FieldTemplate;

            $FieldTemplate->field_id = $feilds->id;
            $FieldTemplate->keyword = str_replace(' ','_', $fieldname)[$i];
            $FieldTemplate->save();*/
        }

        

        
        
        // set dynamic table name according to your requirements

        //$table_name = 'tbl_sms_templates';
        /*$i=1;*/
        // set your dynamic fields (you can fetch this data from database this is just an example)
        //$jsonnewdata = json_decode($feilds->fielddata);
        //dd($jsonnewdata);
        /*foreach($jsonnewdata as $k => $value){*/
            
            //$fields = $jsonnewdata;
            
           /* $i++;*/
            //return $this->createTable($table_name, $fields);
        /*}
        */
        
        
        return redirect('admin/sms/fields')->withSuccess('Feild has been created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fields = Field::find($id);
        
        return view('sms.fields.show', compact('fields')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fields = Field::find($id);
        $fieldtypes = Fieldtype::where('status','ACTIVE')->get();
        return view('sms.fields.edit', compact('fields','fieldtypes')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\feild  $feild
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $feilds = Field::find($id);
        
        $feilds->field_type_id        = $request->fieldid;
        $feilds->field_name      = $request->field_name;
        $feilds->field_slug = str_replace(' ', '_', $request->field_name);
        $feilds->touch();        
        return redirect('/admin/sms/fields')->with('success', 'Feild updated!');
        
    }

    
}
