<?php

namespace App\Exports;

use App\WhatsappModels\WhatsAppData;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AllWhatsappExport implements FromView
{
    

    use Exportable;
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('sms.outgoing.xml', [
            'data' => $this->data
        ]);
    }
}
