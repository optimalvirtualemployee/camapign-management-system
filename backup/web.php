
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin/post-mail', 'campaign\CampaignController@sendsms');


Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'AuthController@index');


Route::post('post-login', 'AuthController@postLogin'); 

$router->group(['middleware'=>['basicAuth'],'prefix' => 'api'], function () use ($router) {

    
    $router->get('checkvch', 'API\ApiController@checkVoucher');
    
    
     
});
Route::get('/postman/csrf', function (Request $request) {
	return csrf_token();
});
Route::get('updateBatchName', 'SMS\campaign\CampaignController@updateBatchName');
Route::group(['middleware'=>['admin'],'prefix'=> 'admin/'], function (){

	Route::get('register', 'AuthController@register');
	Route::post('post-register', 'AuthController@postRegister'); 
	Route::get('dashboard', 'AuthController@dashboard'); 
	Route::get('logout', 'AuthController@logout');
    Route::post('dashboard', 'AuthController@dashboard');
    Route::get('dashboard/fetch_data', 'AuthController@fetch_data');




	
    
	
});
Route::group(['middleware'=>['admin'],'prefix'=> 'admin/sms/'], function (){

	/*Route::get('register', 'AuthController@register');
	Route::post('post-register', 'AuthController@postRegister'); 
	Route::get('dashboard', 'AuthController@dashboard'); 
	Route::get('logout', 'AuthController@logout');
    Route::post('dashboard', 'AuthController@dashboard');*/




	//Campaign type resource
	Route::resource('campaigntypes', 'SMS\campaign\CampaignTypeController');

	//Offer type resource
	Route::resource('cashbacktypes', 'SMS\campaign\CashbackTypeController');
	
	//Offer type resource
	Route::resource('offers', 'SMS\campaign\OfferTypeController');
	
	//Get template list
	Route::get('campaigntype/get-template-list', 'SMS\campaign\CampaignTypeController@getTemplateList');
	

	//Get cashback list
	Route::get('cashbacktype/get-cashbacktype-list', 'SMS\campaign\CashbackTypeController@getCashbackTypeList');

	//Get status change
    Route::get('changeStatus', 'AuthController@changeStatus');

    //Campaign resource
	Route::resource('campaigns', 'SMS\campaign\CampaignController');
	
	//Campaign rule 
	Route::get('campaign/stats/{campaign_id}', ['as' => 'campaign.stats' , 'uses' => 'SMS\campaign\CampaignController@stats']);

	//Campaign rule 
	Route::get('campaign/rules/index/{campaign_id}', ['as' => 'campaign.rules.index' , 'uses' => 'SMS\rule\RuleController@index']);
	//Campaign rule show 
	Route::get('campaign/rules/show/{campaign_id}/{rule_id}', ['as' => 'campaign.rules.show' , 'uses' => 'SMS\rule\RuleController@show']);
	//Campaign rule edit 
	Route::get('campaign/rules/edit/{campaign_id}/{rule_id}', ['as' => 'campaign.rules.edit' , 'uses' => 'SMS\rule\RuleController@edit']);

	//Campaign rule create 
	
	Route::get('campaign/rules/create/{campaign_id}', ['as' => 'campaign.rules.create' , 'uses' => 'SMS\rule\RuleController@create']);
	//Campaign rule store 
	Route::post('campaign/rules/store/{campaign_id}', ['as' => 'campaign.rules.store' , 'uses' => 'SMS\rule\RuleController@store']);
	//Campaign rule update 
	Route::PATCH('campaign/rules/update/{rule_id}/{campaign_id}', ['as' => 'campaign.rules.update' , 'uses' => 'SMS\rule\RuleController@update']);

	//Campaign sms Template list
	Route::get('campaign/smstemp/index/{campaign_id}', ['as' => 'campaign.sms.index' , 'uses' => 'SMS\campaign\SmsController@index']);
	//Campaign sms Template show
	Route::get('campaign/sms/show/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.show' , 'uses' => 'SMS\campaign\SmsController@show']);
	//Campaign sms Template edit
	Route::get('campaign/sms/edit/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.edit' , 'uses' => 'SMS\campaign\SmsController@edit']);

	//Campaign sms Template create
	Route::get('campaign/sms/create/{campaign_type_id}/{template_id}', ['as' => 'campaign.sms.create' , 'uses' => 'SMS\campaign\SmsController@create']);
	//Campaign sms Template store
	Route::post('campaign/sms/store/{campaign_type_id}/{template_id}', ['as' => 'campaign.sms.store' , 'uses' => 'SMS\campaign\SmsController@store']);
	//Campaign sms Template update
	Route::PATCH('campaign/sms/update/{sms_id}/{campaign_id}', ['as' => 'campaign.sms.update' , 'uses' => 'SMS\campaign\SmsController@update']);

	//Campaign multioffercode sms Template Create

	Route::get('campaign/sms/createmultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'campaign.sms.createmultioffertemplate' , 'uses' => 'SMS\campaign\SmsController@createMultiOfferTemplate']);
	//Campaign sms Template store
	Route::post('campaign/sms/storemultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'campaign.sms.storemultioffertemplate' , 'uses' => 'SMS\campaign\SmsController@storeMultiOfferTemplate']);


	//Get field list by template id
	Route::get('get-field-template-list','SMS\campaign\CampaignController@getFieldTemplateList');

	//Feilds resource
	Route::resource('fields', 'SMS\FieldController');

	//SMS Template resource
	Route::resource('templates', 'SMS\TemplateController');

	//Rule Type resource
	Route::resource('ruletypes', 'SMS\rule\RuleTypeController');

	//Rule resource
	//Route::resource('rules', 'rule\RuleController');

	//Vocuhers resource
	Route::resource('vouchers', 'SMS\campaign\VoucherController');
	//Get template list
	Route::get('voucher/get-offer-list', 'SMS\campaign\VoucherController@getOfferList');
	//Get Offer Cashback list
	Route::get('voucher/get-offer-cashback', 'SMS\campaign\VoucherController@getOfferCashback');
	Route::get('voucher/fetch_data', 'SMS\campaign\VoucherController@fetch_data');
	Route::get('voucher/upload/create', ['as' => 'voucher.upload.create' , 'uses' => 'SMS\campaign\VoucherController@uploadVoucherCodeCreate']);
	Route::post('voucher/upload/store', ['as' => 'voucher.upload.store' , 'uses' => 'SMS\campaign\VoucherController@uploadVoucherCodeStore']);
	Route::get('voucher/all/edit', ['as' => 'voucher.all.edit' , 'uses' => 'SMS\campaign\VoucherController@allVoucherCodeEdit']);
	
	Route::post('voucher/all/update', ['as' => 'voucher.all.update' , 'uses' => 'SMS\campaign\VoucherController@allVoucherCodeUpdate']);
	
	Route::get('voucher/all/view', ['as' => 'delete.all.view' , 'uses' => 'SMS\campaign\VoucherController@allVoucherCodeDeleteView']);
	
	Route::post('voucher/all/viewUpdate', ['as' => 'delete.all.update' , 'uses' => 'SMS\campaign\VoucherController@allVoucherCodeDeleteUpdate']);
	

	Route::get('voucher/list', ['as' => 'voucher.list' , 'uses' => 'SMS\campaign\VoucherController@index']);

	//Campaign Feilds resource
	Route::resource('campaign-fields', 'SMS\campaign\CampaignFieldController');
	
	//Campaign rule create 
	Route::get('campaign/setting/create', ['as' => 'campaign.setting.create' , 'uses' => 'SMS\campaign\SettingController@create']);
	//Campaign rule store 
	Route::post('campaign/setting/store/{id?}', ['as' => 'campaign.setting.store' , 'uses' => 'SMS\campaign\SettingController@store']);
	
	//Campaign rule create 
	Route::get('campaign/redirect', ['as' => 'campaign.redirect' , 'uses' => 'SMS\campaign\SettingController@createRidrect']);
	//Campaign rule store 
	Route::post('campaign/redirectStore/{id?}', ['as' => 'campaign.redirectStore' , 'uses' => 'SMS\campaign\SettingController@storeRidrect']);


    //Incoming SMS 
	Route::get('report/incoming/list', ['as' => 'report.incoming.list' , 'uses' => 'SMS\campaign\ReportController@incomingReportList']);
	
	//Outgoing SMS 
	Route::get('report/outgoing/list', ['as' => 'report.outgoing.list' , 'uses' => 'SMS\campaign\ReportController@outgoingReportList']);
	
	//Whatsapp SMS 
	//Route::get('report/whatsapp/list', ['as' => 'report.whatsapp.list' , 'uses' => 'SMS\campaign\ReportController@whatsappReportList']);
	
	//Whatsapp SMS 
	Route::get('report/list/{id}', ['as' => 'report.whatsapp.list' , 'uses' => 'SMS\campaign\ReportController@whatsappImageView']);
	
    //reward List 
	Route::get('rewardcode/list', ['as' => 'rewardcode.list' , 'uses' => 'SMS\campaign\CampaignController@rewardcodeList']);
	
	//Issue Pnnel List 
	Route::get('issue/panel/list', ['as' => 'issue.panel.list' , 'uses' => 'SMS\campaign\ReportController@issuePanelList']);

	//reward List 
	Route::get('rewardcode/dashboard/list', ['as' => 'rewardcode.dashbaord.list' , 'uses' => 'SMS\campaign\CampaignController@rewardcodeDashboardList']);
    
	
});
Route::get('admin/outgoing/download-outgoing-excel', 'SMS\campaign\ReportController@allOutgoingDownload')->name('outgoing.download-outgoing-excel');

Route::get('admin/incoming/download-incoming-excel', 'SMS\campaign\ReportController@allIncomingDownload')->name('incoming.download-incoming-excel');
//Get field list by template id
Route::get('get-keyword-data','SMS\campaign\CampaignController@getKeywordData');
    
//Get field list by template id
Route::get('get-whatsapp-data','Whatsapp\WhatsAppController@getWhatsAppData');

//Whatsapp SMS 
Route::get('send-sms-template/{incoming_msg_id?}', 'SMS\campaign\SmsController@sendSmsTemplate');

//Whatsapp SMS 
Route::get('send-sms-second-template', 'SMS\campaign\SmsController@sendSmsSecondTemplate');

//Whatsapp SMS 
Route::get('send-sms-voucher-recharge-template', 'SMS\campaign\SmsController@sendVoucherRechargeBasedTemplate');
//Whatsapp SMS 
Route::get('send-whatsapp-template/{incoming_msg_id?}', 'Whatsapp\campaign\SmsController@sendWhatsappTemplate');


Route::get('validate-mobile-per-promotion/{campaign_id?}/{mobile_no?}', 'SMS\campaign\SmsController@validateMobilePerPromotion');


//Whatsapp


Route::group(['middleware'=>['admin'],'prefix'=> 'admin/whatsapp/'], function (){

	/*Route::get('register', 'AuthController@register');
	Route::post('post-register', 'AuthController@postRegister'); */
	Route::get('dashboard', 'Whatsapp\AuthController@dashboard'); 
/*	Route::get('logout', 'AuthController@logout');*/
    Route::post('dashboard', 'Whatsapp\AuthController@dashboard');

    Route::get('dashboard/fetch_data', 'AuthController@fetch_data');

    //Offer type resource
	Route::resource('cashbacktypes', 'Whatsapp\campaign\CashbackTypeController');


	//Get cashback list
	Route::get('cashbacktype/get-cashbacktype-list', 'Whatsapp\campaign\CashbackTypeController@getCashbackTypeList');

	//Campaign type resource
	Route::resource('whatsappcampaigntypes', 'Whatsapp\campaign\CampaignTypeController');
	
	//Offer type resource
	Route::resource('whatsappoffers', 'Whatsapp\campaign\OfferTypeController');
	
	//Get template list
	Route::get('whatsappcampaigntype/get-template-list', 'Whatsapp\campaign\CampaignTypeController@getTemplateList');
	

	//Get status change
    Route::get('changeStatus', 'AuthController@changeStatus');

    //Campaign resource
	Route::resource('whatsappcampaigns', 'Whatsapp\campaign\CampaignController');
	
	//Campaign rule 
	Route::get('campaign/stats/{campaign_id}', ['as' => 'whatsappcampaign.stats' , 'uses' => 'Whatsapp\campaign\CampaignController@whatsappstats']);

	//Campaign rule 
	Route::get('campaign/rules/index/{campaign_id}', ['as' => 'whatsappcampaign.rules.index' , 'uses' => 'Whatsapp\rule\RuleController@index']);
	//Campaign rule show 
	Route::get('campaign/rules/show/{campaign_id}/{rule_id}', ['as' => 'whatsappcampaign.rules.show' , 'uses' => 'Whatsapp\rule\RuleController@show']);
	//Campaign rule edit 
	Route::get('campaign/rules/edit/{campaign_id}/{rule_id}', ['as' => 'whatsappcampaign.rules.edit' , 'uses' => 'Whatsapp\rule\RuleController@edit']);

	//Campaign rule create 
	
	Route::get('campaign/rules/create/{campaign_id}', ['as' => 'campaign.rules.create' , 'uses' => 'Whatsapp\rule\RuleController@create']);
	//Campaign rule store 
	Route::post('campaign/rules/store/{campaign_id}', ['as' => 'campaign.rules.store' , 'uses' => 'Whatsapp\rule\RuleController@store']);
	//Campaign rule update 
	Route::PATCH('campaign/rules/update/{rule_id}/{campaign_id}', ['as' => 'campaign.rules.update' , 'uses' => 'Whatsapp\rule\RuleController@update']);

	//Campaign sms Template list
	Route::get('campaign/smstemp/index/{campaign_id}', ['as' => 'campaign.sms.index' , 'uses' => 'Whatsapp\campaign\SmsController@index']);
	//Campaign sms Template show
	Route::get('campaign/sms/show/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.show' , 'uses' => 'Whatsapp\campaign\SmsController@show']);
	//Campaign sms Template edit
	Route::get('campaign/sms/edit/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.edit' , 'uses' => 'Whatsapp\campaign\SmsController@edit']);

	//Campaign sms Template create
	Route::get('campaign/sms/create/{campaign_type_id}/{template_id}', ['as' => 'whatsappcampaign.sms.create' , 'uses' => 'Whatsapp\campaign\SmsController@create']);
	//Campaign sms Template store
	Route::post('campaign/sms/store/{campaign_type_id}/{template_id}', ['as' => 'whatsappcampaign.sms.store' , 'uses' => 'Whatsapp\campaign\SmsController@store']);
	//Campaign sms Template update
	Route::PATCH('campaign/sms/update/{sms_id}/{campaign_id}', ['as' => 'whatsappcampaign.sms.update' , 'uses' => 'Whatsapp\campaign\SmsController@update']);

	//Campaign multioffercode sms Template Create

	Route::get('campaign/sms/createmultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'whatsappcampaign.sms.createmultioffertemplate' , 'uses' => 'Whatsapp\campaign\SmsController@createMultiOfferTemplate']);
	//Campaign sms Template store
	Route::post('campaign/sms/storemultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'whatsappcampaign.sms.storemultioffertemplate' , 'uses' => 'Whatsapp\campaign\SmsController@storeMultiOfferTemplate']);


	//Get field list by template id
	Route::get('get-field-template-list','Whatsapp\campaign\CampaignController@getFieldTemplateList');

	//Feilds resource
	Route::resource('whatsappfields', 'Whatsapp\FieldController');

	//SMS Template resource
	Route::resource('whatsapptemplates', 'Whatsapp\TemplateController');

	//Rule Type resource
	Route::resource('whatsappruletypes', 'Whatsapp\rule\RuleTypeController');

	//Rule resource
	//Route::resource('rules', 'rule\RuleController');

	//Vocuhers resource
	Route::resource('whatsappvouchers', 'Whatsapp\campaign\VoucherController');

	//Get template list
	Route::get('voucher/get-offer-list', 'Whatsapp\campaign\VoucherController@getOfferList');

	//Get Offer Cashback list
	Route::get('voucher/get-offer-cashback', 'Whatsapp\campaign\VoucherController@getOfferCashback');

	Route::get('voucher/upload/create', ['as' => 'whatsappvoucher.upload.create' , 'uses' => 'Whatsapp\campaign\VoucherController@uploadVoucherCodeCreate']);
	Route::post('voucher/upload/store', ['as' => 'whatsappvoucher.upload.store' , 'uses' => 'Whatsapp\campaign\VoucherController@uploadVoucherCodeStore']);
	

	//Campaign Feilds resource
	Route::resource('whatsappcampaign-fields', 'Whatsapp\campaign\CampaignFieldController');
	
	//Campaign rule create 
	Route::get('campaign/setting/create', ['as' => 'whatsappcampaign.setting.create' , 'uses' => 'Whatsapp\campaign\SettingController@create']);
	//Campaign rule store 
	Route::post('campaign/setting/store/{id?}', ['as' => 'whatsappcampaign.setting.store' , 'uses' => 'Whatsapp\campaign\SettingController@store']);
	
	//Campaign rule create 
	Route::get('campaign/redirect', ['as' => 'whatsappcampaign.redirect' , 'uses' => 'Whatsapp\campaign\SettingController@createRidrect']);
	//Campaign rule store 
	Route::post('campaign/redirectStore/{id?}', ['as' => 'whatsappcampaign.redirectStore' , 'uses' => 'Whatsapp\campaign\SettingController@storeRidrect']);

	//Outgoing SMS 
	Route::get('whatsappreport/outgoing/list', ['as' => 'whatsappreport.outgoing.list' , 'uses' => 'Whatsapp\campaign\ReportController@whatsappoutgoingReportList']);
	
	//Whatsapp SMS 
	Route::get('report/whatsapp/list', ['as' => 'report.whatsapp.list' , 'uses' => 'Whatsapp\campaign\ReportController@whatsappReportList']);
	
    //reward List 
	Route::get('rewardcode/list', ['as' => 'whatsapprewardcode.list' , 'uses' => 'Whatsapp\campaign\CampaignController@rewardcodeList']);
	
	//Whatsapp SMS 
	
	Route::get('report/list/{id}', 'Whatsapp\campaign\ReportController@whatsappImageView');


	Route::get('report/list/status/{id}/{status}', ['as' => 'report.list.status' , 'uses' => 'Whatsapp\campaign\ReportController@whatsappImageViewStatus']);
});

//Missed Call
Route::get('get-missedcall-data','MissedCall\MissedCallController@getMissedCallData');
Route::get('get-missedcall-ivr-data','MissedCall\MissedCallController@getMissedCallToIVRSMS');
Route::get('send-missedcall-template/{incoming_msg_id?}', 'MissedCall\campaign\SmsController@sendWhatsappTemplate');

Route::group(['middleware'=>['admin'],'prefix'=> 'admin/missedcall/'], function (){
	Route::get('dashboard', 'MissedCall\AuthController@dashboard'); 
  Route::post('dashboard', 'MissedCall\AuthController@dashboard');
	//Campaign type resource
	Route::resource('missedcallcampaigntypes', 'MissedCall\campaign\CampaignTypeController');
	//Offer type resource
	//Route::resource('whatsappoffers', 'Whatsapp\campaign\OfferTypeController');
	//Get template list
	Route::get('missedcallcampaigntype/get-template-list', 'MissedCall\campaign\CampaignTypeController@getTemplateList');
	//Get status change
    Route::get('changeStatus', 'AuthController@changeStatus');
    //Campaign resource
	Route::resource('missedcallcampaigns', 'MissedCall\campaign\CampaignController');
	//Campaign rule 
	Route::get('campaign/stats/{campaign_id}', ['as' => 'missedcallcampaign.stats' , 'uses' => 'MissedCall\campaign\CampaignController@missedcallstats']);
	//Campaign rule 
	Route::get('campaign/rules/index/{campaign_id}', ['as' => 'missedcallcampaign.rules.index' , 'uses' => 'MissedCall\rule\RuleController@index']);
	//Campaign rule show 
	//Route::get('campaign/rules/show/{campaign_id}/{rule_id}', ['as' => 'missedcallcampaign.rules.show' , 'uses' => 'MissedCall\rule\RuleController@show']);
	//Campaign rule edit 
	//Route::get('campaign/rules/edit/{campaign_id}/{rule_id}', ['as' => 'missedcallcampaign.rules.edit' , 'uses' => 'MissedCall\rule\RuleController@edit']);
	//Campaign rule create 
	//Route::get('campaign/rules/create/{campaign_id}', ['as' => 'campaign.rules.create' , 'uses' => 'MissedCall\rule\RuleController@create']);
	//Campaign rule store 
	//Route::post('campaign/rules/store/{campaign_id}', ['as' => 'campaign.rules.store' , 'uses' => 'MissedCall\rule\RuleController@store']);
	//Campaign rule update 
	//Route::PATCH('campaign/rules/update/{rule_id}/{campaign_id}', ['as' => 'campaign.rules.update' , 'uses' => 'MissedCall\rule\RuleController@update']);
	//Campaign sms Template list
	//Route::get('campaign/smstemp/index/{campaign_id}', ['as' => 'campaign.sms.index' , 'uses' => 'MissedCall\campaign\SmsController@index']);
	//Campaign sms Template show
	//Route::get('campaign/sms/show/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.show' , 'uses' => 'MissedCall\campaign\SmsController@show']);
	//Campaign sms Template edit
	//Route::get('campaign/sms/edit/{campaign_id}/{sms_id}', ['as' => 'campaign.sms.edit' , 'uses' => 'MissedCall\campaign\SmsController@edit']);
	//Campaign sms Template create
	Route::get('campaign/sms/create/{campaign_type_id}/{template_id}', ['as' => 'missedcallcampaign.sms.create' , 'uses' => 'MissedCall\campaign\SmsController@create']);
	//Campaign sms Template store
	Route::post('campaign/sms/store/{campaign_type_id}/{template_id}', ['as' => 'missedcallcampaign.sms.store' , 'uses' => 'MissedCall\campaign\SmsController@store']);
	//Campaign sms Template update
	Route::PATCH('campaign/sms/update/{sms_id}/{campaign_id}', ['as' => 'missedcallcampaign.sms.update' , 'uses' => 'MissedCall\campaign\SmsController@update']);
	//Campaign multioffercode sms Template Create
	//Route::get('campaign/sms/createmultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'missedcallcampaign.sms.createmultioffertemplate' , 'uses' => 'MissedCall\campaign\SmsController@createMultiOfferTemplate']);
	//Campaign sms Template store
	//Route::post('campaign/sms/storemultioffertemplate/{campaign_type_id}/{template_id}', ['as' => 'missedcallcampaign.sms.storemultioffertemplate' , 'uses' => 'MissedCall\campaign\SmsController@storeMultiOfferTemplate']);
	//Get field list by template id
	Route::get('get-field-template-list','MissedCall\campaign\CampaignController@getFieldTemplateList');
	//Feilds resource
	Route::resource('missedcallfields', 'MissedCall\FieldController');
	//SMS Template resource
	Route::resource('missedcalltemplates', 'MissedCall\TemplateController');
	//SMS Template resource -
	Route::resource('missedcallsmstemplates', 'MissedCall\TemplateSMSController');

	Route::post('missedcallsmstemplates/store/{id}', ['as' => 'missedcallsmstemplates.store' , 'uses' => 'MissedCall\TemplateSMSController@store']);
	//Campaign sms Template create
	Route::get('missedcall/sms/create/{campaign_type_id}', ['as' => 'missedcall.sms.create' , 'uses' => 'MissedCall\TemplateSMSController@create']);

	//Rule Type resource
	Route::resource('missedcallruletypes', 'MissedCall\rule\RuleTypeController');
	//Rule resource
	//Route::resource('rules', 'rule\RuleController');
	//Vocuhers resource
	//Route::resource('whatsappvouchers', 'MissedCall\campaign\VoucherController');
	//Route::get('voucher/upload/create', ['as' => 'missedcallvoucher.upload.create' , 'uses' => 'MissedCall\campaign\VoucherController@uploadVoucherCodeCreate']);
	//Route::post('voucher/upload/store', ['as' => 'missedcallvoucher.upload.store' , 'uses' => 'MissedCall\campaign\VoucherController@uploadVoucherCodeStore']);
	//Campaign Feilds resource
	Route::resource('missedcallcampaign-fields', 'MissedCall\campaign\CampaignFieldController');
	//Campaign rule create 
	//Route::get('campaign/setting/create', ['as' => 'missedcallcampaign.setting.create' , 'uses' => 'MissedCall\campaign\SettingController@create']);
	//Campaign rule store 
	//Route::post('campaign/setting/store/{id?}', ['as' => 'missedcallcampaign.setting.store' , 'uses' => 'MissedCall\campaign\SettingController@store']);
	//Campaign rule create 
	//Route::get('campaign/redirect', ['as' => 'missedcallcampaign.redirect' , 'uses' => 'MissedCall\campaign\SettingController@createRidrect']);
	//Campaign rule store 
	//Route::post('campaign/redirectStore/{id?}', ['as' => 'missedcallcampaign.redirectStore' , 'uses' => 'MissedCall\campaign\SettingController@storeRidrect']);
	//Outgoing SMS 
	Route::get('missedcallreport/outgoing/list', ['as' => 'missedcallreport.outgoing.list' , 'uses' => 'MissedCall\campaign\ReportController@missedcalloutgoingReportList']);
	//MissedCall SMS 
	Route::get('report/missedcall/list', ['as' => 'report.missedcall.list' , 'uses' => 'MissedCall\campaign\ReportController@whatsappReportList']);
    //reward List 
	//Route::get('rewardcode/list', ['as' => 'whatsapprewardcode.list' , 'uses' => 'MissedCall\campaign\CampaignController@rewardcodeList']);
	//MissedCall SMS 	
	//Route::get('report/list/{id}', 'MissedCall\campaign\ReportController@missedcallImageView');
	//Route::get('report/list/status/{id}/{status}', ['as' => 'report.list.status' , 'uses' => 'MissedCall\campaign\ReportController@whatsappImageViewStatus']);
});