<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_campaigns', function (Blueprint $table) {
            $table->id();
            $table->integer('campaign_type_id')->unsigned()->index()->nullable();
            $table->string('keyword');
            $table->bigInteger('same_mobile_allowed');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_campaigns');
        Schema::dropForeign('tbl_campaigns_campaign_type_id_foreign');
        Schema::dropIndex('tbl_campaigns_campaign_type_id_index');
        Schema::dropColumn('campaign_type_id');
    }
}
