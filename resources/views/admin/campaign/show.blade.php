@extends('layouts.default')
@section('title', 'Show Camapign')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigns')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Campaign Details</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                    <th scope="col"><h4>Fiellds</h4></th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>
          
                <tr>
                    <td>Campaign Id</td>
                    <td>{{$campaign->id}}</td>
                </tr>
                <tr>
                    <td>Campaign Type Name</td>
                    <td>{{$campaign->campaign_type->name}}</td>
                </tr>
                <tr>
                  <td>Campaign Name</td>
                  <td>{{$campaign->campaign_name}}</td>
                </tr>
                <tr>
                    <td>Campaign Keyword</td>
                    <td>{{isset($campaignFieldValue['field_value']) ? $campaignFieldValue['field_value'] : 'NA'}}</td>
                </tr>
                <!--<tr>
                    <td>Times Mobile Is Allowed</td>
                    <td>{{$campaign->same_mobile_allowed}}</td>
                </tr>-->
                <tr>
                    <td>Created By</td>
                    <td>{{isset($campaignType->created_user['name']) ? $campaignType->created_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Updated By</td>
                    <td>{{isset($campaignType->updated_user['name']) ? $campaign->updated_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Created Date/Time</td>
                    <td>{{$campaign->created_at->format('d M Y')}}/{{$campaign->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Updated Date/Time</td>
                    <td>{{$campaign->created_at->format('d M Y')}}/{{$campaign->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$campaign->status}}</td>
                </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@stop