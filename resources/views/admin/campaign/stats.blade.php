@extends('layouts.default')
@section('title', 'Stats Camapign')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Stats</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigns')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Campaign Stats</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                    <th scope="col"><h4>Fields</h4></th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>
          
                <tr>
                    <td>Total SMS in Campaign</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Valid SMS in Campaign</td>
                    <td>3</td>
                </tr>
                <tr>
                  <td>New SMS in Campaign</td>
                  <td>0</td>
                </tr>
                <tr>
                    <td>Error SMS in Campaign</td>
                    <td>4</td>
                </tr>
                
                <tr>
                    <td>Invalid Vouchers in Campaign</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Excess Tries with Mobile in Campaign</td>
                    <td>8</td>
                </tr>
                
                <tr>
                    <td>Expired SMS in Campaign</td>
                    <td>3</td>
                </tr>
                <tr>
                    <td>Total Vouchers in Campaign</td>
                    <td>2</td>
                </tr>
                <tr>
                    <td>Total Vouchers Used in Campaign</td>
                    <td>1</td>
                </tr>
                <tr>
                    <td>Total OfferCodes UnUsed in Campaign</td>
                    <td>0</td>
                </tr>
                
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@stop