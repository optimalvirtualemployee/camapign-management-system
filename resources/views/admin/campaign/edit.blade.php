
@extends('layouts.default')
@section('title', 'Campaign Type Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigns')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  
  <form action="{{ route('campaigns.update', $campaign->id) }}" method="POST" class="form-horizontal mt-3" id="" enctype="multipart/form-data">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
          
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
      <!-- <div class="row">
          <div class="col-md-8"> -->
              <div class="row">
                  <div class="form-group col-md-4">
                     <label>Campaign Type</label>
                     <select class="select2 form-select shadow-none"
                        name="campaign_type_id" id="campaignType" style="width: 100%; height:36px;">
                        <option>Campaign Type</option>
                        @forelse($campaign_type as $type)
                        
                        <option value="{{$type->id}}" {{$campaign->campaign_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                    </select>
                  </div>
                  <div class="form-group col-md-4">
                     <label for="hue-demo">Campaign Name</label>
                     <input type="text" id="hue-demo" class="form-control demo" name="campaign_name" data-control="hue"
                        placeholder="Enter Campaign Name" value="{{ $campaign->campaign_name }}">
                  </div>
                  <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Keyword</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="campaign_keyword" data-control="hue"
                        placeholder="Enter Campaign Keyword" value="{{ $campaign->campaign_keyword }}">
                  </div>   
                  
                </div>
                <div class="row">
                    
                    
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Start Date</label>
                        <input type="text"  id="filter-date" name="start_date" value="{{$campaign->start_date}}"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign End Date</label>
                        <input type="text"  id="filter-date" name="end_date" value="{{$campaign->end_date}}"/>
                    </div>
                               
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Status Trigger Rule </label>
                        <input type="text" id="hue-demo" class="form-control demo" name="status_trigger_rule" data-control="hue"
                        placeholder="Enter Campaign Keyword" value="{{ $campaign->status_trigger_rule }}">
                    </div>     
                    
                    
                            
                        
                        
                    
                    
                </div>
                <div class="row">

                  <div class="form-group col-md-4">
                    @php
                    $campaignMobileUsageRuleValue = App\CampaignMobileUsageRule::where('campaign_id',$campaign->id)->get();

                    @endphp

                    @foreach($mobile_no_usage as $key=>$usage)    
                            
                    <div class="form-check mr-sm-2">
                        
                        <input type="checkbox" class="form-check-input"
                            id="voucherperday_{{$usage->id}}" name="mobile_no_usage_rules[]" value="{{isset($campaignMobileUsageRuleValue[$key]->mobile_no_usage_rules) ? $campaignMobileUsageRuleValue[$key]->mobile_no_usage_rules : $usage->id}}" {{isset( $campaignMobileUsageRuleValue[$key]->mobile_no_usage_rules) ? 'checked' : '' }}>
                            
                        <label class="form-check-label mb-0" for="customControlAutosizing1" style="display:inline-flex;">

                            
                            <input type="text" class="voucherperday_{{$usage->id}} latFeild ml-2 form-control demo" id="voucherperday_{{$usage->id}}" name="mobile_usage_value[]"   readonly="readonly" value="{{isset($campaignMobileUsageRuleValue[$key]['mobile_usage_value']) ? $campaignMobileUsageRuleValue[$key]['mobile_usage_value'] : ""}}">
                            {{$usage->name}}
                            
                        </label>
                    </div>
                    @endforeach
                  </div>
                </div>
              
                <div class="fieldTemplate">
                @if($campaignField)
                @foreach($campaignField->field_template as $key=>$field)
 
                
                
                
                <div class="form-group row">
                    <div class="form-group col-md-6">
                        <input type="hidden" name="field_name[]" value="{{$field->field_name}}">
                        <input type="hidden" name="field_key[]" value="{{$field->pivot->field_id}}">
                        <label for="hue-demo">{{$field->field_name}}</label>
                        @if($field->field_type->name == 'text')

                        <input type="text" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter {{$field->field_name}}" value="{{isset($campaignFieldValue[$key]->field_value)? $campaignFieldValue[$key]->field_value:""}}">
                        @elseif($field->field_type->name == 'date')
                        <input type="text"  id="filter-date" name="field_value[]" value="{{isset($campaignFieldValue[$key]->field_value) ? $campaignFieldValue[$key]->field_value:""}}"/>

                        @elseif(!empty($field->field_type) && $field->field_type->name == 'number')
 
                        <input type="number" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter {{$field->field_name}}" value="{{isset($campaignFieldValue[$key]->field_value) ? $campaignFieldValue[$key]->field_value:""}}">

                        @elseif($field->field_type->name == 'textarea')
                        <textarea class="form-control" name="field_value[]">{{isset($campaignFieldValue[$key]->field_value) ? $campaignFieldValue[$key]->field_value : ""}}</textarea>
                        @elseif($field->field_type->name == 'file')
                        <img src="{{url(isset($campaignFieldValue[$key]->field_value) ? $campaignFieldValue[$key]->field_value:"")}}">

                        <input type="file" class="custom-file-input" id="validatedCustomFile" name="field_value_file">
                        <div class="form-group col-md-4">
                          <label for="hue-demo">Download Sample File</label>
                          <a href="{{url('assets/file/rewardCode.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton"><i class="fas fa-download"></i> Download Sample File</button></a>
                        </div>
                        @elseif($field->field_type->name == 'dropdown')
                        <select class="form-select shadow-none"
                        name="field_value[]" style="width: 100%; height:36px;">
                          <option value="0">Select Rule</option>
                          <option value="1"  {{($campaignFieldValue[$key]->field_value == 1) ? 'Selected' : ''}}>Cab</option>
                          <option value="2"  {{($campaignFieldValue[$key]->field_value == 2) ? 'Selected' : ''}}>Movie</option>
                          <option value="3"  {{($campaignFieldValue[$key]->field_value == 3) ? 'Selected' : ''}}>Food</option>
                        </select>
                         
                        
                            
                        
                        
                        @endif 
                    </div>
                    
                        
                        
                </div>
                
                @endforeach
                @endif
            </div> 
         <!-- </div>
      
      </div> -->
    </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop
