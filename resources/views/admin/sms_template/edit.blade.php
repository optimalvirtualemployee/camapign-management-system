@extends('layouts.default')
@section('title', 'Campaign Type Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigntypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('campaigns.update', $campaign->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
      <div class="form-group">
         <label for="hue-demo">Keyword</label>
         <input type="text" id="hue-demo" class="form-control demo" name="keyword" data-control="hue"
            placeholder="Enter Keyword" value="{{ $campaign->keyword }}">
      </div>
      <div class="form-group">
         <label for="hue-demo">Time Same Mobile Number Allowed</label>
         <input type="number" id="hue-demo" class="form-control demo" name="same_mobile_allowed" data-control="hue"
            placeholder="Enter Same Mobile Number Allowed" value="{{ $campaign->same_mobile_allowed }}">
      </div>
      <div class="form-group row">
        <label class="col-md-3 mt-3">Campaign Type</label>
        <div class="col-md-9">
            <select class="select2 form-select shadow-none"
                name="campaign_type_id" style="width: 100%; height:36px;">
                <option>Campaign Type</option>
                @forelse($campaign_type as $type)
                <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                <option value="{{$type->id}}" {{$campaign->campaign_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                
                <!-- </optgroup> -->
                
                @empty
                <p>No Found Result</p>
                @endforelse
                
                
            </select>
        </div>
      </div>
      
    </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop