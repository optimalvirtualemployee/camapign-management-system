@extends('layouts.default')
@section('title', 'SMS Template Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">SMS Template Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigns')}}">Back</a></li>
                        
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('campaign.sms.store',[$campaign_id,$template_id]) }}" method="POST" class="form-horizontal mt-3" id="">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-body">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div>
        @endif
        <!-- <h4 class="card-title">{{$templates->template_name}}</h4> -->
        <h4 class="card-title">Multi Offer Template</h4>
        @php
        $number = $campaignFieldValue->field_value;
        
        @endphp
          
        @if (!empty($campaign_template->field_value))
        @php
        $jsonnewdata = json_decode($campaign_template->field_value);
        @endphp
        @foreach($jsonnewdata as $val)
        <div class="sms-group row">
                <div class="form-group col-md-6">
                    <label for="hue-demo">Template Id</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="offer_template_keyword_id[]" data-control="hue" placeholder="Enter template Id" value="{{$val->offer_template_keyword_id}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="hue-demo">Entity Id</label>                   
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_template_entity_id[]" data-control="hue" placeholder="Enter Entity Id" value="{{$val->offer_template_entity_id}}">
                </div>
                
                <div class="form-group col-md-12">
                    <label for="hue-demo">Offer Type {{$val->offer_template_id}}</label>
                    
                    
                    <textarea class="form-control" name="offer_field_value[]" placeholder="Offer Type {{$val->offer_template_id}}">{{$val->offer_field_value}}</textarea>

                   

                </div>
                
                 <input type="hidden" name="offer_template_id[]" value="{{$val->offer_template_id}}">
            </div>
        @endforeach
        @else
        
        @for($i=1; $i<$number; $i++)
            

            
            <div class="sms-group row">
                <div class="form-group col-md-6">
                    <label for="hue-demo">Template Id</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="offer_template_keyword_id[]" data-control="hue" placeholder="Enter template Id" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="hue-demo">Entity Id</label>                   
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_template_entity_id[]" data-control="hue" placeholder="Enter Entity Id" value="">
                </div>
                
                <div class="form-group col-md-12">
                    <label for="hue-demo">Offer Type {{$i}}</label>
                    
                    
                    <textarea class="form-control" name="offer_field_value[]" placeholder="Offer Type {{$i}}"></textarea>

                   

                </div>
                
                 <input type="hidden" name="offer_template_id[]" value="{{$i}}">
            </div>
            
       
        @endfor
        @endif

            
                
      
      
    </div>     
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop