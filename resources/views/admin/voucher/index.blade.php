@extends('layouts.default')
@section('title', 'Voucher List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Voucher List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/vouchers/create')}}">Add Voucher</a></li>
                        <li class="breadcrumb-item"><a href="{{route('voucher.upload.create')}}"><button class="btn btn-success btn-sm"    type="button" id="dropdownMenuButton">
                            <i class="fas fa-plus"></i> &nbsp; Upload Voucher Code
                            </button></a></li>
                        
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              
              <th>Campaign ID</th>
              <th>Campaign Name</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Campaign Keyword</th>
              <th>Voucher Code</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          @forelse($vouchers as $index=>$voucher)
          
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$voucher->campaign_id}}</td>
            <td>{{$voucher->campaign['campaign_name']}}</td>
            <td>{{$voucher->offer_type_id}}</td>
            <td>{{isset($voucher->offer)? $voucher->offer['name']:"NA" }}</td>
            <td>{{$voucher->campaign['campaign_keyword']}}</td>
            <td>{{$voucher->voucher_code}}</td>
            <td>{{isset($voucher->start_date) ? $voucher->start_date : "NA"}}</td>
            <td>{{isset($voucher->end_date) ? $voucher->end_date : "NA"}}</td>
            <td>{{$voucher->created_at->format('d M Y')}}/{{$voucher->created_at->format('g:i A')}}</td>
            <td>{{$voucher->updated_at->format('d M Y')}}/{{$voucher->updated_at->format('g:i A')}}</td>
            
            <td>
              <a href="{{ route('vouchers.show',$voucher->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('vouchers.edit',$voucher->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
            <td>
                
              <div class="form-check form-switch">
                  <input data-id="{{$voucher->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
              data-off="InActive" {{ $voucher->status == 'ACTIVE' ? 'checked' : '' }}>
              <input type="hidden" name="mdoelName" id="mdoelName" value="Voucher">  
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <th >SN</th>
              
              <th>Campaign ID</th>
              <th>Campaign Name</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Campaign Keyword</th>
              <th>Voucher Code</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>us</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

