@extends('layouts.default')
@section('title', 'Show Campaign Fields')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Fields Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaign-fields')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Campaign fields Details</h5>
      </div>
      

      <table class="table">
        
        <tbody>
           
            <tr>
                <td>Campaign type Id</td>
                <td>{{$campaignField->campaign_type_id}}</td>
            </tr>
            <tr>
                <td>Campaign type name</td>
                <td>{{$campaignField->campaign_type->name}}</td>
            </tr>
            <tr>
                    <td>Fields</td>
                    <td>
                        @foreach($campaignField->field_template as $i=> $field)
                        {{$field->field_name}},
                        @endforeach
                    </td>
                    
                </tr>
            <tr>
                <td>Created By</td>
                <td>{{isset($campaignField->created_user['name']) ? $campaignField->created_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Updated By</td>
                <td>{{isset($campaignField->updated_user['name']) ? $campaignField->updated_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Created Date/Time</td>
                <td>{{$campaignField->created_at->format('d M Y')}}/{{$campaignField->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Updated Date/Time</td>
                <td>{{$campaignField->created_at->format('d M Y')}}/{{$campaignField->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{$campaignField->status}}</td>
            </tr>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
@stop