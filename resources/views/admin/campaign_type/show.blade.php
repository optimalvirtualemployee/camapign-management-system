@extends('layouts.default')
@section('title', 'Show Camapign Type')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigntypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Browesr statistics</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                    <th scope="col">Feilds</th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>
          
                <tr>
                    <td>Campaign Type Id</td>
                    <td>{{$campaignType->id}}</td>
                </tr>
                <tr>
                    <td>Campaign Type Name</td>
                    <td>{{$campaignType->name}}</td>
                </tr>
                <tr>
                  <td>SMS</td>
                  <td>{{$campaignType->sms}}</td>
                </tr>
                <tr>
                    <td>Stats</td>
                    <td>{{$campaignType->stats}}</td>
                </tr>
                <tr>
                    <td>Rules</td>
                    <td>{{$campaignType->rules}}</td>
                </tr>
                <tr>
                    <td>Created Date/Time</td>
                    <td>{{$campaignType->created_at->format('d M Y')}}/{{$campaignType->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Updated Date/Time</td>
                    <td>{{$campaignType->created_at->format('d M Y')}}/{{$campaignType->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$campaignType->status}}</td>
                </tr>
            </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
@stop