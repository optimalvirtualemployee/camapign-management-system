@extends('layouts.default')
@section('title', 'Campaign Type Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/campaigntypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('campaigntypes.store') }}" method="POST" class="form-horizontal mt-3" id="">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-body">
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
      <div class="row">
          <div class="col-md-6">
            <div class="form-group">
                <label for="hue-demo">Name</label>
                <input type="text" id="hue-demo" class="form-control demo" name="name" data-control="hue"
                placeholder="Enter Campaign Type" value="{{ old('name') }}">
            </div>
          </div>
          <div class="col-md-12">
              <h4>Actions</h4>
          </div>
            <div class="col-md-4">
                <label for="hue-demo" style="margin-right: 20px;">SMS</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input smsAction"
                    id="customControlValidation1" name="sms" value="YES" required>
                    <label class="form-check-label mb-0" for="customControlValidation1">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input smsAction"
                    id="customControlValidation2" name="sms" value="NO" required>
                    <label class="form-check-label mb-0" for="customControlValidation2">No</label>
                </div>
                <div class="form-group templateWrap" style="display:none;">
                    <label for="name" class="mr-sm-2">Template</label>
                    <select class="form-control js-example-basic-single template" name="template_id" id="sel1" style="height: 45px;"></select>
               </div>
            </div>
            
            <div class="col-md-4">
                <label for="hue-demo" style="margin-right: 20px;">Stats</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation3" name="stats" value="YES" required>
                    <label class="form-check-label mb-0" for="customControlValidation3">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation4" name="stats" value="NO" required>
                    <label class="form-check-label mb-0" for="customContr
                    olValidation4">No</label>
                </div>
            </div>
            <div class="col-md-4">
                <label for="hue-demo" style="margin-right: 20px;">Rules</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation5" name="rules" value="YES" required>
                    <label class="form-check-label mb-0" for="customControlValidation5">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation6" name="rules" value="NO" required>
                    <label class="form-check-label mb-0" for="customControlValidation6">No</label>
                </div>
            </div>
      </div>
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop