@extends('layouts.default')
@section('title', 'Show Camapign Type')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{route('campaign.rules.index',$campaign_id}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Browesr statistics</h5>
      </div>
      <table class="table">
        <thead>
            <tr>
                <th scope="col">Feilds</th>
                <th scope="col">Value</th>
            </tr>
        </thead>
        <tbody>
            
      
            <tr>
                <td>Campaign Id</td>
                <td>{{$rule->campaign_id}}</td>
            </tr>
            <tr>
                <td>Campaign Keyword</td>
                <td>{{$rule->campaign->keyword}}</td>
            </tr>
            <tr>
              <td>Basic Rule Name</td>
              <td>{{$rule->basic_rule_name}}</td>
            </tr>
            <tr>
                <td>Basic Rule Sequence</td>
                <td>{{$rule->basic_rule_sequence}}</td>
            </tr>
            <tr>
                <td>Rule Name</td>
                <td>{{$rule->rule_name}}</td>
            </tr>
            <tr>
                <td>Rule Sequence</td>
                <td>{{$rule->rule_sequence}}</td>
            </tr>
            <tr>
                <td>Rule Type Id</td>
                <td>{{$rule->rule_type_id}}</td>
            </tr>
            <tr>
                <td>Rule Type Name</td>
                <td>{{$rule->rule_type['name']}}</td>
            </tr>
            <tr>
                <td>Created By</td>
                <td>{{isset($rule->created_user['name']) ? $rule->created_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Updated By</td>
                <td>{{isset($rule->updated_user['name']) ? $rule->updated_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Created Date/Time</td>
                <td>{{$rule->created_at->format('d M Y')}}/{{$rule->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Updated Date/Time</td>
                <td>{{$rule->created_at->format('d M Y')}}/{{$rule->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{$rule->status}}</td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop