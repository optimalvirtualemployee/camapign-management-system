<!-- ============================================================== -->
<script src="{{url('/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{url('/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{url('/assets/extra-libs/sparkline/sparkline.js')}}"></script>
<!--Wave Effects -->
<script src="{{url('/assets/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{url('/assets/js/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{url('/assets/js/custom.min.js')}}"></script>
<script src="{{url('/assets/js/build/jquery.datetimepicker.full.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>




<!--This page JavaScript -->
<!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
<!-- Charts js Files -->
<script src="{{url('/assets/libs/flot/excanvas.js')}}"></script>
<script src="{{url('/assets/libs/flot/jquery.flot.js')}}"></script>
<script src="{{url('/assets/libs/flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('/assets/libs/flot/jquery.flot.time.js')}}"></script>
<script src="{{url('/assets/libs/flot/jquery.flot.stack.js')}}"></script>
<script src="{{url('/assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
<script src="{{url('/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
<!-- <script src="{{url('/assets/js/pages/chart/chart-page-init.js')}}"></script> -->
<script src="{{url('/assets/extra-libs/multicheck/datatable-checkbox-init.js')}}"></script>
<script src="{{url('/assets/extra-libs/multicheck/jquery.multicheck.js')}}"></script>
<script src="{{url('/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script src="{{url('/assets/js/custom.js')}}"></script>
<script src="{{url('/assets/libs/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{url('/assets/libs/select2/dist/js/select2.min.js')}}"></script>
<script src="{{url('/assets/libs/jquery-asColor/dist/jquery-asColor.min.js')}}"></script>
<script src="{{url('/assets/libs/jquery-asGradient/dist/jquery-asGradient.js')}}"></script>
<script src="{{url('/assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js')}}"></script>
<script src="{{url('/assets/libs/jquery-minicolors/jquery.minicolors.min.js')}}"></script>
<!--<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>-->
<script src="{{url('/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script> 
<!-- <script src="{{url('/assets/libs/chart/matrix.interface.js')}}"></script>
<script src="{{url('/assets/libs/chart/excanvas.min.js')}}"></script>
<script src="{{url('/assets/libs/chart/jquery.peity.min.js')}}"></script>
    <script src="{{url('/assets/libs/chart/matrix.charts.js')}}"></script>
    <script src="{{url('/assets/libs/chart/jquery.flot.pie.min.js')}}"></script>
    <script src="{{url('/assets/libs/chart/turning-series.js')}}"></script>
    <script src="{{url('/dist/js/pages/chart/chart-page-init.js')}}"></script> -->
<script>

    //***********************************//
        $(document).ready(function(){
            /*$('.ckeditor').ckeditor();*/
            
            var val = [];
            $('#dayschecked input[type=radio]:checked').filter(function(i){
                val[i] = $(this).attr("id");
                if(val[i]){
                    $('input[name="mobile_usage_value"]').attr('readonly','readonly');
               }
            });
            $(".form-check-input").click(function() {
                var idVal = $(this).attr("id");
                
                var res = idVal.split("_");
                if($(this).prop("checked") == true){
                    
                    $('.voucherperday_'+res[1]).removeAttr('readonly');
                } else if($(this).prop("checked") == false){
                    
                    $('.voucherperday_'+res[1]).attr('readonly','readonly');
                    $('.voucherperday_'+res[1]).val(' ');
                }      
            });
            $('#batchcodedayschecked input[type=radio]:checked').filter(function(i){
                val[i] = $(this).attr("id");
                if(val[i]){
                    $('input[name="batchcode_usage_value"]').attr('readonly','readonly');
               }
            });
            $(".batchcodeforminput").click(function() {
                var idVal = $(this).attr("id");
                
                var res = idVal.split("_");
                if($(this).prop("checked") == true){
                    
                    $('.batchcodeperday_'+res[1]).removeAttr('readonly');
                } else if($(this).prop("checked") == false){
                    
                    $('.batchcodeperday_'+res[1]).attr('readonly','readonly');
                    $('.batchcodeperday_'+res[1]).val(' ');
                }      
            });
            incoming_load_data(from_date = '', to_date = '');
            
            whatsapp_load_data(from_date = '', to_date = '');
            
            function incoming_load_data(from_date = '', to_date = ''){
                
                $('#order_table').DataTable({
        
                processing: true,
        
                serverSide: true,
        
                /*ajax: "{{ route('report.incoming.list') }}",
                
                data:{from_date:from_date, to_date:to_date},*/
                ajax: {
                    url:'{{ route("report.incoming.list") }}',
                    data:{from_date:from_date, to_date:to_date}
                },
                
                
                columns: [
        
                    {data: 'id', name: 'id'},
        
                    {data: 'from', name: 'from'},
        
                    {data: 'message', name: 'message'},
                    
                    {data: 'ts', name: 'ts'},
                    
                    {data: 'circle', name: 'circle'},

                    {data: 'operator', name: 'operator'},
                    
                    {data: 'msgid', name: 'msgid'},
                    
                    {data: 'parts', name: 'parts'},
                    
                    {data: 'created_at', name: 'created_at'},
        
                    
        
                ]
                });
        
            }
            
            outgoing_load_data(from_date = '', to_date = '');
            
            
            function outgoing_load_data(from_date = '', to_date = ''){

                var table = $('.data-table2').DataTable({
               
                processing: true,
        
                serverSide: true,
        
                /*ajax: "{{ route('report.outgoing.list') }}",*/
                ajax: {
                    url:'{{ route("report.outgoing.list") }}',
                    data:{from_date:from_date, to_date:to_date}
                },
        
                columns: [
        
                    {data: 'id', name: 'id'},
        
                    {data: 'incoming_msg.from', name: 'from'},

                    {data: 'incoming_msg_id', name: 'incoming_msg_id'},
        
                    {data: 'incoming_msg.message', name: 'message'},

                    {data: 'outgoing_msg', name: 'outgoing_msg'},
                    
                    {data: 'incoming_msg.status', name: 'incoming_msg.status'},
                    
                    {data: 'created_at', name: 'created_at'},
        
                    
        
                ]
                });
                
                
                
                
            }
            function whatsapp_load_data(){

                var table = $('.data-table3').DataTable({
        
                processing: true,
        
                serverSide: true,
        
                ajax: {
                    url:'{{ route("report.whatsapp.list") }}',
                    data:{from_date:from_date, to_date:to_date}
                },
                
        
                columns: [
        
                    {data: 'id', name: 'id'},
                    
                    /*{data: 'body', name: 'body'},*/
        
                    {data: 'from', name: 'from'},
        
                    {data: 'name', name: 'name'},
                    
                    {data: 'type', name: 'type'},
                    
                    {data: 'media_url', name: 'media_url',
                    /*render: function( data, type, full, meta ) {
                        return "<img src=\"" + data + "\" width=\"100\"/>";
                    }*/},
                    
                    {data: 'reply_to', name: 'reply_to'},
                    
                    {data: 'wanumber', name: 'wanumber'},
                    
                    {data: 'message', name: 'message'},
                    
                    {data: 'mobile', name: 'mobile'},

                    {data: 'media', name: 'media'},
                    
                    {data: 'custom_response', name: 'custom_response'},

                    {data: 'status', name: 'status'},
                    
                    {data: 'created_at', name: 'created_at'},
                    
                    
        
                ]
                });
                
                
                
                
            }
            whatsapp_outgoing_load_data(from_date = '', to_date = '');
            function whatsapp_outgoing_load_data(from_date = '', to_date = ''){

                var table = $('.data-table5').DataTable({
               
                processing: true,
        
                serverSide: true,
        
                /*ajax: "{{ route('report.outgoing.list') }}",*/
                ajax: {
                    url:'{{ route("whatsappreport.outgoing.list") }}',
                    data:{from_date:from_date, to_date:to_date}
                },
        
                columns: [
        
                    {data: 'id', name: 'id'},
        
                    {data: 'incoming_msg.from', name: 'from'},

                    {data: 'incoming_msg_id', name: 'incoming_msg_id'},
        
                    {data: 'incoming_msg.message', name: 'message'},

                    {data: 'outgoing_msg', name: 'outgoing_msg'},
                    
                    {data: 'incoming_msg.status', name: 'incoming_msg.status'},
                    
                    {data: 'created_at', name: 'created_at'},
        
                    
        
                ]
                });
                
                
                
                
            }
            /*voucher_load_data(from_date = '', to_date = '');
            function voucher_load_data(from_date = '', to_date = ''){
                
                $('#voucher_table').DataTable({
        
                processing: true,
        
                serverSide: true,
        
                
                ajax: {
                    url:'{{ route("voucher.list") }}',
                    data:{from_date:from_date, to_date:to_date}
                },
                
                
                columns: [
        
                    {data: 'id', name: 'id'},
        
                    {data: 'campaign_id', name: 'campaign_id'},
        
                    {data: 'campaign_name', name: 'campaign_name'},
                    
                    {data: 'offer_type_id', name: 'offer_type_id'},
                    
                    {data: 'offer_type_name', name: 'offer_type_name'},

                    {data: 'campaign_keyword', name: 'campaign_keyword'},
                    
                    {data: 'voucher_code', name: 'voucher_code'},
                    
                    {data: 'start_date', name: 'start_date'},
                    
                    {data: 'end_date', name: 'end_date'},

                    {data: 'created_at', name: 'created_at'},

                    {data: 'updated_at', name: 'updated_at'},
        
                    
        
                ]
                });
        
            }*/
            $('#filter').click(function(){
                
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                if(from_date != '' &&  to_date != '')
                {
                    $('#order_table').DataTable().destroy();
                    $('.data-table2').DataTable().destroy();
                    $('.data-table3').DataTable().destroy();
                    $('.data-table5').DataTable().destroy();
                    /*$('.voucher_table').DataTable().destroy();*/
                    incoming_load_data(from_date, to_date);
                    outgoing_load_data(from_date, to_date);
                    whatsapp_outgoing_load_data(from_date, to_date);
                    whatsapp_load_data(from_date, to_date);
                    /*voucher_load_data(from_date, to_date);*/
                }
                else
                {
                    alert('Both Date is required');
                }
            });
            
            $('#refresh').click(function(){
                
                $('#from_date').val('');
                $('#to_date').val('');
                $('#order_table').DataTable().destroy();
                incoming_load_data();
                $('.data-table2').DataTable().destroy();
                outgoing_load_data();
                $('.data-table3').DataTable().destroy();
                whatsapp_load_data();
                $('.data-table5').DataTable().destroy();
                whatsapp_outgoing_load_data();
                $('#voucher_table').DataTable().destroy();
                voucher_load_data();
            });
            
            
            
            
        });
        
            
            
        // For select 2
        //***********************************//
        $(".select2").select2();
        /*colorpicker*/
        $('.demoColor').each(function () {
            //
            // Dear reader, it's actually very easy to initialize MiniColors. For example:
            //
            //  $(selector).minicolors();
            //
            // The way I've done it below is just for the demo, so don't get confused
            // by it. Also, data- attributes aren't supported at this time...they're
            // only used for this demo.
            //
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function (value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        
        jQuery(document).ready(function () {
                'use strict';

                jQuery('#filter-date, #search-from-date, #search-to-date').datetimepicker();
            });
        $('.input-daterange').datepicker({
          todayBtn:'linked',
          format:'yyyy-mm-dd',
          autoclose:true
         });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker({
            format:'yyyy-mm-dd'
        });
        jQuery('#datepicker-autoclose').datepicker({

            autoclose: true,
            format:'yyyy-mm-dd',
            todayHighlight: true
        });
        $("#datepickerMonth").datepicker( {
            format: "mm-yyyy",
            startView: "months", 
            minViewMode: "months"
        });
        
    /****************************************
     *       Basic Table                   *
     ****************************************/
    $('#zero_config').DataTable();
    $('#zero_config1').DataTable();
    $('#zero_config2').DataTable();
    $('#zero_config3').DataTable();
    $('#zero_config4').DataTable();
    $('#zero_config5').DataTable();
    $('#zero_config6').DataTable();
</script>

<script type="text/javascript">
    //End js for category status change
    $('.changeStatus').change(function() {
        var status = $(this).prop('checked') == true ? 'ACTIVE' : 'INACTIVE'; 
        var id = $(this).data('id'); 
        var model_name = $('#mdoelName').val(); 
        //alert(model_name);
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{url('/admin/sms/changeStatus')}}",
            beforeSend: function(){
                
                    // Show image container
                    $("#loading").show();
                   },
            
            data: {'status': status, 'id': id,'model_name': model_name},
            success: function(data){
              console.log(data.success)
            },
            complete: function(){
                    $('#loading').hide();

                }
        });
    });
    $('.whatsappAction').on('change',function(){
        var actionData = $('.whatsappAction:checked').val();
        
        if(actionData == "YES"){
            
            $(".templateWrap").css('display','block');
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:'/admin/whatsapp/whatsappcampaigntype/get-template-list',
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
        
                    $(".template").empty();
                    $(".template").append('<option value="0">Select Template</option>')
                    $.each(res,function(key,value){
                        $(".template").append('<option value="'+key+'">'+value+'</option>');
                    });
             
                    }else{
                     $(".template").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".templateWrap").css('display','none');
        }
      
    });
    $("#whatsappcampaigntype").on("select2:select", function (e) { 
      var select_val = $(e.currentTarget).val();
        if(select_val == 15){
            
            $("#whatsappnumber").css('display','block');
            $("#campaign_keyword").css('display','none');

        }else{

            $("#whatsappnumber").css('display','none');
            $("#campaign_keyword").css('display','block');

        }

            

    });
    $('#campaignAction').on('change',function(){
        
        var actionData = $('#campaignAction').val();
        
        if(actionData != ""){
            
            
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:"/admin/sms/voucher/get-offer-list?campaign_id="+actionData,
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
                    $(".offerTypeWrap").css('display','block');
                    $(".cashbackcampWrap").css('display','none');
                    $(".offer").empty();
                    $(".offer").append('<option value="0">Select Offer Type</option>')
                    $.each(res,function(key,value){

                        $(".offer").append('<option value="'+value.offer_type_id+'">'+value.offer_name_slug.replaceAll('_', ' ')+'</option>');
                    });
             
                    }else{
                        
                     $(".offer").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".templateWrap").css('display','none');
        }
      
    });


    $('.offer').on('change',function(){
        
        var actionData = $('.offer').val();
        
        if(actionData != ""){
            
            
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:"/admin/sms/voucher/get-offer-cashback?offer_id="+actionData,
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res.cashback_price != null){
                
                        $(".cashbackcampWrap").css('display','block');
                        /*$(".offer").empty();*/
                        /*$(".offer").append('<option value="0">Select Offer Type</option>')
                        $.each(res,function(key,value){

                            $(".offer").append('<option value="'+value.offer_type_id+'">'+value.offer_name_slug.replaceAll('_', ' ')+'</option>');
                        });*/
             
                    }else{

                        $(".cashbackcampWrap").css('display','none');
                        
                     
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".cashbackcampWrap").css('display','none');
        }
      
    });


    $('#whatsappCampaignAction').on('change',function(){
        
        var actionData = $('#whatsappCampaignAction').val();
        
        if(actionData != ""){
            
            
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:"/admin/whatsapp/voucher/get-offer-list?campaign_id="+actionData,
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res){
                    $(".whatsappofferTypeWrap").css('display','block');
                    $(".whatsappcashbackcampWrap").css('display','none');
                    $(".whatsappOffer").empty();
                    $(".whatsappOffer").append('<option value="0">Select Offer Type</option>')
                    $.each(res,function(key,value){

                        $(".whatsappOffer").append('<option value="'+value.offer_type_id+'">'+value.offer_name_slug.replaceAll('_', ' ')+'</option>');
                    });
             
                    }else{
                        
                     $(".whatsappOffer").empty();
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".templateWrap").css('display','none');
        }
      
    });

    $('.whatsappOffer').on('change',function(){
        
        var actionData = $('.whatsappOffer').val();
        
        if(actionData != ""){
            
            
            $.ajax({
                
                type:"GET",
                dataType: "json",
                url:"/admin/whatsapp/voucher/get-offer-cashback?offer_id="+actionData,
                beforeSend: function(){
                    $('#loading').show();

                },
                
                success:function(res){               
                    if(res.cashback_price != null){
                
                        $(".whatsappcashbackcampWrap").css('display','block');
                        /*$(".offer").empty();*/
                        /*$(".offer").append('<option value="0">Select Offer Type</option>')
                        $.each(res,function(key,value){

                            $(".offer").append('<option value="'+value.offer_type_id+'">'+value.offer_name_slug.replaceAll('_', ' ')+'</option>');
                        });*/
             
                    }else{

                        $(".whatsappcashbackcampWrap").css('display','none');
                        
                     
                    }
                },
                complete: function(){
                    $('#loading').hide();

                }
            });
            
        }else{
            
          $(".cashbackcampWrap").css('display','none');
        }
      
    });
    
</script>