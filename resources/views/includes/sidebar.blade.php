<!--Sidebar navigation-->
<nav class="sidebar-nav">
    <ul id="sidebarnav" class="pt-4">
        <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)" aria-expanded="false"><i
                    class="fa fa-commenting-o"></i><span class="hide-menu">SMS </span></a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"> <a class="sidebar-link  sidebar-link" href="{{url('admin/dashboard')}}"
                        aria-expanded="false"><i class="fa fa-tachometer"></i><span
                            class="hide-menu">Dashboard</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Campaign Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/sms/campaigntypes')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Campaign Type
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/sms/campaigns')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-plus"></i><span class="hide-menu"> Add New Campaign
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/sms/campaign-fields')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign Fields
                                </span></a></li>
                        <!-- <li class="sidebar-item"><a href="{{url('admin/sms/campaign/setting/create')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign
                                    Setting
                                </span></a></li> -->
                        <!-- <li class="sidebar-item"><a href="{{url('admin/sms/campaign/redirect')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign
                                    Redirect
                                </span></a></li> -->
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Template Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/sms/fields')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Fields Creation
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/sms/templates')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-plus"></i><span class="hide-menu">Templates Creation
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Cashback Type
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/sms/cashbacktypes')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Cashback Type List
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Offer Type
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/sms/offers')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Offer List
                                </span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-tag"></i><span class="hide-menu">Voucher Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/sms/vouchers')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Voucher Creation
                                </span></a></li>


                        <li class="sidebar-item"><a href="{{route('vouchercode.dashbaord.list')}}" class="sidebar-link"><i
                                    class="fa fa-trophy"></i><span class="hide-menu"> Voucher Dashboard
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Reward </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{route('rewardcode.list')}}" class="sidebar-link"><i
                                    class="fa fa-trophy"></i><span class="hide-menu"> Reward List
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{route('rewardcode.dashbaord.list')}}" class="sidebar-link"><i
                                    class="fa fa-trophy"></i><span class="hide-menu"> Reward Dashboard
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-file-text-o"></i><span class="hide-menu">Report Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{route('report.incoming.list')}}" class="sidebar-link"><i
                                    class="fa fa-commenting-o"></i><span class="hide-menu"> Incoming SMS List
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{route('report.outgoing.list')}}" class="sidebar-link"><i
                                    class="fa fa-commenting-o"></i><span class="hide-menu"> Outgoing SMS List
                                </span></a></li>
                        <!--<li class="sidebar-item"><a href="{{route('report.whatsapp.list')}}" class="sidebar-link"><i
                            class="mdi mdi-note-outline"></i><span class="hide-menu"> Whatsapp List
                        </span></a></li>-->
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link" href="{{route('issue.panel.list')}}"
                        aria-expanded="false"><i class="fa fa-file-text-o"></i><span class="hide-menu">Issue Panel
                        </span></a>
                    <!-- <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{route('report.incoming.list')}}" class="sidebar-link"><i
                                    class="fa fa-commenting-o"></i><span class="hide-menu">  List
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{route('report.outgoing.list')}}" class="sidebar-link"><i
                                    class="fa fa-commenting-o"></i><span class="hide-menu"> Outgoing SMS List
                                </span></a></li>
                        
                    </ul> -->
                </li>
            </ul>
        </li>
    </ul>
    <ul id="sidebarnav" class="pt-4">
        <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)" aria-expanded="false"><i
                    class="fa fa-whatsapp"></i><span class="hide-menu">Whatsapp</span></a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"> <a class="sidebar-link  sidebar-link"
                        href="{{url('admin/whatsapp/dashboard')}}" aria-expanded="false"><i
                            class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Campaign Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappcampaigntypes')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">
                                    Campaign Type
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappcampaigns')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Add New
                                    Campaign
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappcampaign-fields')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign
                                    Fields
                                </span></a></li>
                        <!-- <li class="sidebar-item"><a href="{{url('admin/whatsapp/campaign/setting/create')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign
                                    Setting
                                </span></a></li> -->
                        <!-- <li class="sidebar-item"><a href="{{url('admin/whatsapp/campaign/redirect')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Campaign
                                    Redirect
                                </span></a></li> -->
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Template Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappfields')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Fields
                                    Creation
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsapptemplates')}}"
                                class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Templates
                                    Creation
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Cashback Type
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/cashbacktypes')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Cashback Type List
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Offer Type
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappoffers')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Offer
                                    List
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-tag"></i><span class="hide-menu">Voucher Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/whatsapp/whatsappvouchers')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">
                                    Voucher Creation
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-trophy"></i><span class="hide-menu">Reward </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{route('whatsapprewardcode.list')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Reward List
                                </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow " href="javascript:void(0)"
                        aria-expanded="false"><i class="fa fa-file-text-o"></i><span class="hide-menu">Report Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <!--<li class="sidebar-item"><a href="{{route('report.incoming.list')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Incoming SMS List
                                </span></a></li>-->
                        <li class="sidebar-item"><a href="{{route('whatsappreport.outgoing.list')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">
                                    Outgoing SMS List
                                </span></a></li>
                        <li class="sidebar-item"><a href="{{route('report.whatsapp.list')}}" class="sidebar-link"><i
                                    class="mdi mdi-note-outline"></i><span class="hide-menu"> Whatsapp List
                                </span></a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
    <!-- <ul id="sidebarnav" class="pt-4">
        <li class="sidebar-item">
            <a class="sidebar-link has-arrow " href="javascript:void(0)" aria-expanded="false"><i
                    class="fa fa-phone"></i><span class="hide-menu">Missed Call</span></a>
            <ul aria-expanded="false" class="collapse  first-level">
                <li class="sidebar-item"> <a class="sidebar-link  sidebar-link"
                        href="{{url('admin/missedcall/dashboard')}}" aria-expanded="false"><i
                            class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow " href="javascript:void(0)" aria-expanded="false"><i
                            class="mdi mdi-receipt"></i><span class="hide-menu">Campaign Management
                        </span></a>
                    <ul aria-expanded="false" class="collapse  second-level">
                        <li class="sidebar-item"><a href="{{url('admin/missedcall/missedcallcampaigntypes')}}"
                                class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">
                                    Campaign Type
                                </span></a>
                        </li>
                                            </ul>
                </li>
                
            </ul>
        </li>
    </ul> -->
</nav>
<!-- End Sidebar navigation