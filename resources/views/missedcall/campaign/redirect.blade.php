@extends('layouts.default')
@section('title', 'Campaign Redirect')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Redirect</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <!--<li class="breadcrumb-item"><a href="{{url('admin/campaigns')}}">Back</a></li>-->
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('missedcallcampaign.redirectStore',isset($campaign_setting->id)) }}" method="POST" class="form-horizontal mt-3" id="">
   
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
        <div class="row">
          <div class="col-md-8">
              
              <div class="row">
                  <div class="form-group col-md-6">
                     <label>Campaign</label>
                     <select class="select2 form-select shadow-none"
                        name="campaign_id" style="width: 100%; height:36px;">
                        <option>Campaign</option>
                        @forelse($campaigns as $camp)
                        
                        <option value="{{$camp->id}}" {{isset($campaign_setting->campaign_id) == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                    </select>
                  </div>
                  
                  <div class="form-group col-md-6">
                        <label for="hue-demo">Campaign Redirect</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="redirect_url" data-control="hue"
                        placeholder="Enter campaign redirect" value="{{ isset($campaign_setting->redirect_url) ? $campaign_setting->redirect_url : old('redirect_url') }}">
                    </div>
                  </div>
                  
                  
                    
              </div>
          </div>
      
      </div>
    </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Submit</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop