@extends('layouts.default')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Dashboard</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/dashboard')}}">Home</a></li>

                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Sales Cards  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <div class="card">
        <div class="card-body">
            <form name="select-campaign" id="select-campaign" method="post">
                @csrf
                <div class="row">
                    
                    <div class="col-md-6">
                        
                        <select name="campaign" class="select2 form-select shadow-none" id="campaign">
                            <option value="0">Select Campaign</option>
                            @foreach ($campaigns as $campaign)
                            <option value="{{ $campaign->campaign_keyword }}"
                                <?php if($campaign->campaign_keyword == $campaign_keyword){echo "selected";}?>>
                                {{ $campaign->campaign_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="col-md-6">
                        
                        <input type="submit" name="submit" class="btn btn-primary">
                        
                    </div>
                    
                </div>
            </form>
        </div>
    </div>



    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-cyan text-center">
                    <h1 class="font-bold">{{ $data }}</h1>
                    <h6 class="text-white">Total Entries</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-success text-center">
                    <h1 class="font-bold">{{ $valid }}</h1>
                    <h6 class="text-white">Total Valid</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-danger text-center">
                    <h1 class="font-bold">{{ $invalid }}</h1>
                    <h6 class="text-white">Total Invalid</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-2 col-xlg-3">
            <div class="card card-hover">
                <div class="box bg-warning text-center">
                    <h1 class="font-bold">{{ $unique }}</h1>
                    <h6 class="text-white">Total Unique Entries</h6>
                </div>
            </div>
        </div>
        <!-- Column -->

    </div>
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <div>
                            <h4 class="card-title">Site Analysis</h4>
                        </div>
                    </div>
                    <div class="row">
                        
                    
                        <!-- ===============================  Day Chart  =============================== -->
                        <div class="col-lg-9">
                            <div id="container" style="width: 700px; height: 300px; "></div>
                            <!-- End Anychart -->
                        </div>
                        <!-- ===============================  Day Chart  =============================== -->



                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-6">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <h5 class="mb-0 mt-1">{{ $one_time }}</h5>
                                        <small class="font-light">1 Time Valid Entry</small>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <h5 class="mb-0 mt-1">{{ $two_time }}</h5>
                                        <small class="font-light">2 Time Valid Entry</small>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <h5 class="mb-0 mt-1">{{ $three_time }}</h5>
                                        <small class="font-light">3 Time Valid Entry</small>
                                    </div>
                                </div>
                                <div class="col-6 mt-3">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <h5 class="mb-0 mt-1">{{ $four_time }}</h5>
                                        <small class="font-light">4 Time Valid Entry</small>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!-- column -->

                        <!-- ===============================  Week Chart  =============================== -->
                        <div class="col-lg-12">
                            <!-- Anychart -->
                            <div id="week_chart" style="width: 100%; height: 300px; "></div>
                            <!-- End Anychart -->
                        </div>
                         <!-- ===============================  Week Chart  =============================== -->    
                         
                         
                        <!-- ===============================  Time Chart  =============================== -->
                        <div class="col-lg-12">
                            
                            <div id="time_chart" style="width: 100%; height: 300px; "></div>
                        </div>
                        <!-- ===============================  Time Chart  =============================== -->
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    
  

    <!-- =============================== Start Day Chart Script =============================== -->
    <script src="https://cdn.anychart.com/releases/8.0.0/js/anychart-base.min.js"></script>

    <script>
    anychart.onDocumentReady(function() {

        // set the data
        var data = <?php echo json_encode($day_chart); ?>

        // create the chart
        var chart = anychart.column();

        // add the data
        chart.data(data);

        // draw
        chart.container("container");
        chart.draw();
    });
    </script>


    <!-- =============================== End Day Chart Script =============================== -->

    <!-- =============================== End Week Chart Script =============================== -->
    <script>
    anychart.onDocumentReady(function() {

        // set the data
        var data = <?php echo json_encode($week_chart); ?>

        // create the chart
        var chart = anychart.column();

        // add the data
        chart.data(data);

        // draw
        chart.container("week_chart");
        chart.draw();
    });
    </script>
    <!-- =============================== End Week Chart Script =============================== -->
  
    <!-- =============================== Start Time Chart Script =============================== -->
    

    <script>
    anychart.onDocumentReady(function() {

        // set the data
        var data = <?php echo $time_chart;?>;
        
     
        // create the chart
        var chart = anychart.column();  

        // add the data
        chart.data(data);
       
        // draw
        chart.container("time_chart");
        chart.draw();
    });
    </script>
    <!-- =============================== End Time Chart Script =============================== -->

    <!-- =============================== Start Circle Chart Script =============================== -->
    <script>
        json_data = <?php echo $circle_chart;?>;
        var result = [];

        for(var i in json_data)
            result.push([json_data[i].name, json_data[i].circle]);
    </script>

    <script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
    <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js"></script>
    <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
    <script type="text/javascript">anychart.onDocumentReady(function () {
        // create pie chart with passed data
        var chart = anychart.pie3d(result);

        // set chart title text settings
        chart
            .title('Number of Request per Network Circle')
            // set chart radius
            .radius('43%');

        // set container id for the chart
        chart.container('circle_chart');
        // initiate chart drawing
        chart.draw();
        }); 
    </script>

    <!-- =============================== End Circle Chart Script =============================== -->


    

    @stop