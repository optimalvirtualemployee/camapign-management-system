@extends('layouts.default')
@section('title', 'SMS Template Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">SMS Template Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcallcampaigntypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif

  @if ($campaigns[0]['types'] == 'type-1')
    <form action="{{ route('missedcallsmstemplates.store', [$id]) }}" method="POST" class="form-horizontal mt-3" id="">
      {{ csrf_field() }}
      <div class="card">
        <div class="card-body">
          @if(session()->get('success'))
            <div class="alert alert-success">
              {{ session()->get('success') }}  
            </div>
          @endif
          <h4 class="card-title">SMS Template</h4>
          <div class="row">
            <input type="hidden" name="camp_id" value="{{$id}}">
            <input type="hidden" name="types" value="1">
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">SMS Entity ID</label>
                <input type="text" id="smsentityid" class="form-control demo" name="smsentityid" data-control="hue" placeholder="Enter SMS Entity ID" value="{{isset($smstemplates[0]['smsentityid']) ? $smstemplates[0]['smsentityid'] : ''}}" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">SMS Template ID</label>
                <input type="text" id="smstemplateid" class="form-control demo" name="smstemplateid" data-control="hue" placeholder="Enter SMS Template ID" value="{{isset($smstemplates[0]['smstemplateid']) ? $smstemplates[0]['smstemplateid'] : ''}}" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">Status</label>
                <select class="form-control demo" name="cstatus" id="cstatus" required>
                  <option value="">Please Select</option>
                  <option value="1" {{isset($smstemplates[0]['cstatus']) == '1' ? 'Selected' : ''}}>Active</option>
                  <option value="0" {{isset($smstemplates[0]['cstatus']) == '0' ? 'Selected' : ''}}>Inactive</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="types">SMS</label>
                <textarea class="form-control" placeholder="Enter SMS" name="sms_text" id="sms_text" required>{{isset($smstemplates[0]['sms_text']) ? $smstemplates[0]['sms_text'] : ''}}</textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="border-top">
          <div class="card-body">
            <button type="submit" class="btn btn-success text-white">Save</button>
            <button type="reset" class="btn btn-primary">Reset</button>
            <!-- <button type="submit" class="btn btn-info">Edit</button> -->
            <button type="submit" class="btn btn-danger text-white">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  @else
    <form action="{{ route('missedcallsmstemplates.store', [$id]) }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="card">
        <div class="card-body">
          @if(session()->get('success'))
            <div class="alert alert-success">
              {{ session()->get('success') }}  
            </div>
          @endif
          <h4 class="card-title">SMS Template</h4>
          <div class="row">
            <input type="hidden" name="camp_id" value="{{$id}}">
            <input type="hidden" name="types" value="2">
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">SMS Entity ID</label>
                <input type="text" id="smsentityid" class="form-control demo" name="smsentityid" data-control="hue" placeholder="Enter SMS Entity ID" value="{{isset($smstemplates[0]['smsentityid']) ? $smstemplates[0]['smsentityid'] : ''}}" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">SMS Template ID</label>
                <input type="text" id="smstemplateid" class="form-control demo" name="smstemplateid" data-control="hue" placeholder="Enter SMS Template ID" value="{{isset($smstemplates[0]['smstemplateid']) ? $smstemplates[0]['smstemplateid'] : ''}}" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="types">Status</label>
                <select class="form-control demo" name="cstatus" id="cstatus" required>
                  <option value="">Please Select</option>
                  <option value="1" {{isset($smstemplates[0]['cstatus']) == '1' ? 'Selected' : ''}}>Active</option>
                  <option value="0" {{isset($smstemplates[0]['cstatus']) == '0' ? 'Selected' : ''}}>Inactive</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="types">SMS</label>
                <textarea class="form-control" placeholder="Enter SMS" name="sms_text" id="sms_text" required>{{isset($smstemplates[0]['sms_text']) ? $smstemplates[0]['sms_text'] : ''}}</textarea>
              </div>
            </div>
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label for=""> Choose Voice Recorded Message</label> 
                <input type="file" class="form-control" name="audioclip" id="audioclip" accept=".mp3,audio/*" {{isset($smstemplates[0]['recorded_msg']) ? '' : 'required'}}>
              </div>
              <input type="hidden" name="rec_msg" value="{{isset($smstemplates[0]['recorded_msg']) ? $smstemplates[0]['recorded_msg'] : ''}}">
              <br><span>{{isset($smstemplates[0]['recorded_msg']) ? $smstemplates[0]['recorded_msg'] : ''}}</span>
            </div> -->
          </div>
        </div>
        <div class="border-top">
          <div class="card-body">
            <button type="submit" class="btn btn-success text-white">Save</button>
            <button type="reset" class="btn btn-primary">Reset</button>
            <!-- <button type="submit" class="btn btn-info">Edit</button> -->
            <button type="submit" class="btn btn-danger text-white">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  @endif 
@stop