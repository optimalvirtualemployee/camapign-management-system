@extends('layouts.default')
@section('title', 'SMS Template List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">SMS Template List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb" id="feildNav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>                          
                        <li class="breadcrumb-item addSms" ><a href="{{url('admin/missedcall/missedcallsmstemplates/create')}}">Add SMS Template</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th>SN</th>
              <th>Campaign Name</th>
              <th>SMS</th>
              <th>Template ID</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
          @forelse($smstemplates as $index=>$template)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$template->campaign_type->name}}</td>
            <td>{{$template->sms_text}}</td>
            <td>{{$template->smstemplateid}}</td>
            <td>
              <a href="{{ route('missedcallsmstemplates.show',$template->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('missedcallsmstemplates.edit',$template->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
             
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th>SN</th>
              <th>Campaign Name</th>
              <th>SMS</th>
              <th>Template ID</th>
              <th>Action</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

