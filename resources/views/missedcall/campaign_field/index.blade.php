@extends('layouts.default')
@section('title', 'Campaign Fields List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Fields List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcallcampaign-fields/create')}}">Add Campaign Fields</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Campaign Type Id</th>
              <th>Campaign Type Name</th>
              <th>Created By</th>
              <th>Updated By</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          @forelse($campaign_fields as $index=>$field)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$field->campaign_type_id}}</td>
            <td>{{$field->campaign_type->name}}</td>
            
            <td>{{isset($field->created_user['name']) ? $field->created_user['name'] : 'NA'}}</td>
            <td>{{isset($field->updated_user['name']) ? $field->updated_user['name'] : 'NA'}}</td>
            <td>{{$field->created_at->format('d M Y')}}/{{$field->created_at->format('g:i A')}}</td>
            <td>{{$field->updated_at->format('d M Y')}}/{{$field->updated_at->format('g:i A')}}</td>
            
            <td>
               
              <a href="{{ route('missedcallcampaign-fields.show',$field->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('missedcallcampaign-fields.edit',$field->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
            <td>
                
              <div class="form-check form-switch">
                  <input data-id="{{$field->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
              data-off="InActive" {{ $field->status == 'ACTIVE' ? 'checked' : '' }}>
              <input type="hidden" name="mdoelName" id="mdoelName" value="Template">  
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Campaign Type Id</th>
              <th>Campaign Type Name</th>
              <th>Created By</th>
              <th>Updated By</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

