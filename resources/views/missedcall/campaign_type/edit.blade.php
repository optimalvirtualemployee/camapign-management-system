@extends('layouts.default')
@section('title', 'Campaign Type Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcallcampaigntypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('missedcallcampaigntypes.update', $campaignType->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Campaign Type Edit</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hue-demo">Name</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="name" data-control="hue"
                    placeholder="Enter Campaign Type" value="{{$campaignType->name}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="types">Type</label>
                    <select class="form-control demo" name="types" id="types" required>
                        <option value="">Please Select</option>
                        <option value="type-1" {{$campaignType->types == 'type-1' ? 'Selected' : ''}}>Type-1 (Call this number to send thank you sms)</option>
                        <option value="type-2" {{$campaignType->types == 'type-2' ? 'Selected' : ''}}>Type-2 (Call this number to send recorded callback then sms)</option>
                    </select>
                </div>
              </div>
              <div class="col-md-6">
            <div class="form-group">
                <label for="types">Missed Call Number</label>
                <input type="text" id="missedcallnumber" class="form-control demo" name="missedcallnumber" data-control="hue" placeholder="Enter Missed Call Number" value="{{$campaignType->missedcallnumber}}" required minlength="10" maxlength="10">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control demo" name="status" id="status" required>
                    <option value="">Please Select</option>
                    <option value="1" {{$campaignType->cstatus == '1' ? 'Selected' : ''}}>Active</option>
                    <option value="0" {{$campaignType->cstatus == '0' ? 'Selected' : ''}}>Inactive</option>
                </select>
            </div>
          </div>
            <!-- <div class="col-md-12">
                <h4>Actions</h4>
            </div>
            <div class="col-md-4">
                <label for="hue-demo">SMS</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input smsAction"
                    id="customControlValidation1" name="sms" value="YES" {{ $campaignType->sms == 'YES' ? 'checked' : '' }} >
                    <label class="form-check-label mb-0" for="customControlValidation1">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input smsAction"
                    id="customControlValidation2" name="sms" value="NO" {{ $campaignType->sms == 'NO' ? 'checked' : '' }}>
                    <label class="form-check-label mb-0" for="customControlValidation2">No</label>
                </div>
                <div class="form-group col-md-6 templateWrap">
                    
                    <select class="select2 form-select shadow-none template"
                        name="template_id" style="width: 100%; height:36px;">
                        <option>Template Type</option>
                        @forelse($templates as $temp)
                        
                        <option value="{{$temp->id}}" {{$campaignType->template_id == $temp->id ? 'Selected' : ''}}>{{$temp->template_name}}</option>
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label for="hue-demo">Stats</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                        id="customControlValidation3" name="stats" value="YES" {{ $campaignType->stats == 'YES' ? 'checked' : '' }}>
                    <label class="form-check-label mb-0" for="customControlValidation3">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation4" name="stats" value="NO" {{ $campaignType->stats == 'NO' ? 'checked' : '' }}>
                    <label class="form-check-label mb-0" for="customControlValidation4">No</label>
                </div>
            </div>
            <div class="col-md-4">
                <label for="hue-demo">Rules</label>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation5" name="rules" value="YES" {{ $campaignType->rules == 'YES' ? 'checked' : '' }}>
                    <label class="form-check-label mb-0" for="customControlValidation5">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input"
                    id="customControlValidation6" name="rules" value="NO" {{ $campaignType->rules == 'NO' ? 'checked' : '' }}>
                    <label class="form-check-label mb-0" for="customControlValidation6">No</label>
                </div>
            </div> -->
        </div>
      </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop