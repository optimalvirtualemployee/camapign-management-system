@extends('layouts.default')
@section('title', 'Campaign Type List')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcallcampaigntypes/create')}}">Add Campaign Type</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Campaign Name</th>
              <th>Campaign Type</th>
              <th>Missed Call Number</th>
              <!-- <th>Template Name</th>
              <th>Stats</th>
              <th>Rules</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th> -->
              <th>Action</th>
              <!-- <th>Status</th> -->
            </tr>
        </thead>
        <tbody>
          @forelse($campaignTypes as $index=>$camp)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$camp->name}}</td>
            <td>{{$camp->types}}</td>
            <td>{{$camp->missedcallnumber}}</td>
            <!-- <td>{{isset($camp->template) ? $camp->template->template_name:"NA"}}</td>
            <td>{{$camp->stats}}</td>
            <td>{{$camp->rules}}</td>
            <td>{{$camp->created_at->format('d M Y')}}/{{$camp->created_at->format('g:i A')}}</td>
            <td>{{$camp->updated_at->format('d M Y')}}/{{$camp->updated_at->format('g:i A')}}</td> -->
            
            <td>
              <a href="{{ route('missedcallcampaigntypes.show',$camp->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
              <a href="{{ route('missedcallcampaigntypes.edit',$camp->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>

              <a href="{{route('missedcall.sms.create',[$camp->id])}}"><span style="color: blue;padding:5px;">SMS</span></a>
            </td>
            <input type="hidden" name="mdoelName" id="mdoelName" value="CampaignType"> 
            <!-- <td> -->
              <!-- <div class="form-check form-switch">
                <input data-id="{{$camp->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" data-off="InActive" {{ $camp->status == 'ACTIVE' ? 'checked' : '' }}>
                </div> -->
            <!-- </td>  -->
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Campaign Name</th>
              <th>Campaign Type</th>
              <th>Missed Call Number</th>
              <!-- <th>Template Name</th>
              <th>Stats</th>
              <th>Rules</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th> -->
              <th>Action</th>
              <!-- <th>Status</th> -->
            </tr>
        </tfoot>
      </table>
      

    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

