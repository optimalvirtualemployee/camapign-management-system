@extends('layouts.default')
@section('title', 'Offer Type List')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Offer Type List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcalloffers/create')}}">Add Offer Type</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          @forelse($offerTypes as $index=>$offer)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$offer->id}}</td>
            <td>{{$offer->name}}</td>
            <td>{{$offer->created_at->format('d M Y')}}/{{$offer->created_at->format('g:i A')}}</td>
            <td>{{$offer->updated_at->format('d M Y')}}/{{$offer->updated_at->format('g:i A')}}</td>
            
            <td>
              <a href="{{ route('missedcalloffers.show',$offer->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('missedcalloffers.edit',$offer->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                
              
            </td>
            <td>
                
              
              <input type="hidden" name="mdoelName" id="mdoelName" value="Offer"> 
              <div class="form-check form-switch">
                  <input data-id="{{$offer->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
              data-off="InActive" {{ $offer->status == 'ACTIVE' ? 'checked' : '' }}>
                  
                </div>
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </tfoot>
      </table>
      

    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

