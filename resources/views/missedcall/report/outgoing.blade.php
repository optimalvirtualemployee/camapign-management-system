@extends('layouts.default')
@section('title', 'Outgoing SMS List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Outgoing SMS List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/outgoing/download-outgoing-excel')}}"><button class="btn btn-success btn-sm"    type="button" id="dropdownMenuButton">
                            <i class="fas fa-plus"></i> &nbsp; Download Outgoing Message
                            </button></a></li>
                        <!--<li class="breadcrumb-item"><a href="{{url('admin/campaigns/create')}}">Add Campaign</a></li>-->
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <br />
    <div class="row input-daterange">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
            </div>
    <br />        
    <div class="table-responsive">
        <table class="table table-striped table-bordered data-table5">

        <thead>

            <tr>

                <th>SN</th>
                <th>From</th>
                <th>Incoming Message Id</th>
                <th>Incoming Message</th>
                <th>Outgoing Message</th>
                <th>Status</th>
                <th>Created Date/Time</th>

                

            </tr>

        </thead>

        <tbody>

        </tbody>

    </table>
      
    </div>
  </div>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

