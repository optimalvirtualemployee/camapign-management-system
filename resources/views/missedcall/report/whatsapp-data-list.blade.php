@extends('layouts.default')
@section('title', 'Whatsapp Data List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Missed Call Data List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        
                        <!--<li class="breadcrumb-item"><a href="{{url('admin/campaigns/create')}}">Add Campaign</a></li>-->
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <br />
        <div class="row input-daterange">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
            </div>
    <br />
    <div class="table-responsive">
        <table class="table table-striped table-bordered data-table3">

        <thead>

            <tr>

                <th >SN</th>
                <!--<th><pre>Body</pre></th>-->
                <th>From</th>
                <th>Name </th>
                <th>Type </th>
                <th>Image </th>
                <th>Reply To</th>
                <th>Whatsapp Number</th>
                <th>Message</th>
                <th>mobile</th>
                <th>Media URL</th>
                <th>Custom Response</th>
                <th>Image View Status</th>
                
                <th>Created Date/Time</th>
                

                

            </tr>

        </thead>

        <tbody>

        </tbody>

    </table>
      
    </div>
  </div>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

