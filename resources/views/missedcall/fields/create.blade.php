@extends('layouts.default')
@section('title', 'Fields Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Fields Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/missedcall/missedcallfields')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('missedcallfields.store') }}" method="POST" class="form-horizontal mt-3" id="fieldForm">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-body">
      
      
      
        <div class="col-md-4">
            <label for="hue-demo" style="margin-right: 20px;">Select Module </label>
            <div class="form-check form-check-inline">
                <input type="radio" class="form-check-input"
                id="customControlValidation5" name="module_name" value="{{old('module_name') ?  old('module_name') : '1'}}">
                


                <label class="form-check-label mb-0" for="customControlValidation5">Template</label>
            </div>
            <div class="form-check form-check-inline">
                <input type="radio" class="form-check-input"
                id="customControlValidation6" name="module_name" value="{{old('module_name') ?  old('module_name') : '2'}}">
                <label class="form-check-label mb-0" for="customControlValidation6">Campaign</label>
            </div>
            @if ($errors->has('module_name'))
            <div class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('module_name') }}.</strong>
            </div>
            @endif
        </div>
        <div class="row wrapper">
            <div class="col-md-8 cloningform">                  
                <div class="row">
                    <div class="form-group col-md-4" id="fieldtype">
                        <label>Field Type</label>
                        <select class="form-select shadow-none fieldid form-control" name="fieldid[]" id="fieldid">
                        <option value="">Please Select</option>
                        @forelse($fieldtypes as $type)
                        
                        <option value="{{$type->id}}" {{old('fieldid') == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                        
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                        </select>
                  
                        @error('fieldid.*')
                        <div class="invalid-feedback" style="display:block;">
                            <strong>{{ $message }}</strong>
                        </div>
                        
                        @enderror
                </div>
                <div class="form-group col-md-4">
                  <label for="hue-demo">Placeholder OR Label</label>
                  <input type="text" id="hue-demo" class="form-control demo fieldname" name="field_name[]" data-control="hue"
                      placeholder="Enter Feild label" value="{{ old('fieldname') }}">
                    @error('field_name.*')
                        <div class="invalid-feedback" style="display:block;">
                            <strong>{{ $message }}</strong>
                        </div>
                        
                    @enderror
                </div>
                
                <div class="form-group col-md-4 addbtn">
                  <label>&nbsp;</label><br>
                  <a class="btn btn-default add_item">Add</a>
                </div>
            </div>
          </div>
        </div>
      
      
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop
