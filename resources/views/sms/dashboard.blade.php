@extends('layouts.default')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Dashboard</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="filter_outer">
        <div class="card">
            <div class="card-body">
               
                <!-- Booking Filters Starts Here -->
                <div class="assign-book">
                    <div class="border-boking"></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>From Date</label>
                        <input type="date" name="from_date" id="from_date" class="form-control"
                            value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}">

                    </div>
                    <div class="col-md-4">
                        <label>To Date</label>
                        <input type="date" name="to_date" id="to_date" class="form-control"
                            value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}">
                    </div>
                    <div class="col-md-4">
                        <label>Select Keyword</label>

                        <select class="select2 form-select shadow-none"
                                            style="width: 100%; height:36px;" name="campaign_id" id="campaign_id">
                            <option value="0">Select Keyword</option>
                            @forelse($campaigns as $camp)
                            <option value="{{$camp->campaign_keyword}}"
                                {{(isset($_GET['campaign_keyword']) && $_GET['campaign_keyword'] == $camp->campaign_keyword) ? 'Selected' : ''}}>
                                {{$camp->campaign_name}}</option>
                            @empty
                            <p>No Product List</p>
                            @endforelse
                        </select>
                        <!-- <div class="error1" style="display: none;">This field is required.</div> -->
                    </div>
                   
                    <div class="col-md-4">
                        <input type="button" class="btn btn-primary btn-block search_btn " id="dashboard-filter" value="Search">
                        <!-- <input type="button" class="btn btn-info btn-block search_btn " id="dashboard-refresh" value="Clear"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="totalEntriesWrap">
        @include('sms.dashboard_filter')
       
    </div>
</div>
@stop
<!-- ============================================================== -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script>
   
    $(document).ready(function(){
       
       

        function fetch_data(from_date = '', to_date = '', campaign_id = '', datepicker = '')
        {


           
            $.ajax({
           
                url:"/admin/dashboard/fetch_data?from_date="+from_date+"&to_date="+to_date+"&campaign_id="+campaign_id+"&datepicker="+datepicker,
                beforeSend: function(){
                  // Show image container
                  $("#loading").show();
                },
                success:function(data)
                {
                    $('#next').hide();
                    $('#back').show();
                    $('#totalEntriesWrap').html(data);

                    $('#zero_config').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                    $('#zero_config2').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                    $('#zero_config3').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                    $('#zero_config4').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                    $('#zero_config5').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                    $('#zero_config6').DataTable({ 
                      "destroy": true, //use for reinitialize datatable
                    });
                },
                complete: function(){
                  $('#loading').hide();
                }
           
            });
        }
        var _token = $('input[name="_token"]').val();
   

        $('#dashboard-filter').click(function(){
         
            $("#next").css('display','block');
            var isError = false;
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            var campaign_id = $("#campaign_id").val()
            var datepickerMonth = $("#datepickerMonth").val()

            //alert(datepickerMonth);

            /**/
   
            if((from_date != '' &&  to_date != '') || campaign_id != ''){
                fetch_data(from_date, to_date, campaign_id);
            }/*else{
               alert('Both Date is required');
            }*/
        });
   
        $('#dashboard-refresh').click(function(){

          $('#from_date').val('');
          $('#to_date').val('');
          $("#campaign_id").val('').trigger('change');
          fetch_data();
        });
   
    });
    fetch_data();
</script>

	
	
	
