@extends('layouts.default')
@section('title', 'Show Offer Type')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Offer Type Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/sms/offers')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Browesr statistics</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                    <th scope="col">Feilds</th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>
          
                <tr>
                    <td>Offer Type Id</td>
                    <td>{{$offerType->id}}</td>
                </tr>
                <tr>
                    <td>Offer Type Name</td>
                    <td>{{$offerType->name}}</td>
                </tr>
                
                <tr>
                    <td>Created Date/Time</td>
                    <td>{{$offerType->created_at->format('d M Y')}}/{{$offerType->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Updated Date/Time</td>
                    <td>{{$offerType->created_at->format('d M Y')}}/{{$offerType->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$offerType->status}}</td>
                </tr>
            </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
@stop