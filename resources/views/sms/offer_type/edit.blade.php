@extends('layouts.default')
@section('title', 'Offer Type Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Offer Type Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/sms/offers')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('offers.update', $offerType->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Offer Type Edit</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hue-demo">Name</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="name" data-control="hue"
                    placeholder="Enter Campaign Type" value="{{$offerType->name}}">
                </div>
            </div>

            
            <div class="col-md-6">
                <!-- <label for="hue-demo">SMS</label> -->
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input smsAction"
                    id="customControlValidation1" name="sms" value="YES" {{ $offerType->cashback_price != '' ? 'checked' : '' }} >
                    <label class="form-check-label mb-0" for="customControlValidation1">Cashback</label>
                </div>
            
        </div>
        
        @if($offerType->cashback_price != '')
        <div class="row col-md-6 cashbackWrap">
            <div class="col-md-6">
              <div class="form-group">
                <label for="hue-demo">Cashback Price</label>
                <input type="text" id="hue-demo" class="form-control demo" name="cashback_price" data-control="hue"
                placeholder="Enter Cashback Price" value="{{ $offerType->cashback_price }}">  
              </div>
            </div>
            <div class="col-md-6">
              <label for="hue-demo">Cashback Type</label>
              <select class="select2 form-select shadow-none cashbacktype"
                  name="cashback_type_id" style="width: 100%; height:36px;">
                  <option>Select Cashback Type</option>
                  @forelse($cashbacktypes as $type)
                  
                  <option value="{{$type->id}}" {{$offerType->cashback_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                  @empty
                  <p>No Found Result</p>
                  @endforelse
              </select>
            </div>
            
        </div>
        @endif

      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop