@extends('layouts.default')
@section('title', 'Rule Type List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Rule Type List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/ruletypes/create')}}">Add Rule Type</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Rule Type ID</th>
              <th>Rule Type</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          @forelse($ruleTypes as $index=>$rule)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$rule->id}}</td>
            <td>{{$rule->name}}</td>
            <td>{{$rule->created_at->format('d M Y')}}/{{$rule->created_at->format('g:i A')}}</td>
            <td>{{$rule->updated_at->format('d M Y')}}/{{$rule->updated_at->format('g:i A')}}</td>
            
            <td>
              <a href="{{ route('ruletypes.show',$rule->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('ruletypes.edit',$rule->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
            <td>
                
              <input data-id="{{$rule->id}}" class="toggle-class changeStatus" type="checkbox" 
              data-onstyle="success" data-offstyle="danger" data-on="Active" 
              data-off="InActive" {{ $rule->status == 'ACTIVE' ? 'checked' : '' }}>  
              <input type="hidden" name="mdoelName" id="mdoelName" value="RuleType">  
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Rule Type ID</th>
              <th>Rule Type</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

