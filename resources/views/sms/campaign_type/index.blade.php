@extends('layouts.default')
@section('title', 'Campaign Type List')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Type List</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('admin/sms/campaigntypes/create')}}">Add Campaign Type</a>
                </li>
                
            </ol>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
    <div class="card-body">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        <!-- <h5 class="card-title">Campaign Type</h5> -->
        <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Campaign Type ID</th>
                        <th>Campaign Type</th>
                        <th>SMS Content</th>
                        <th>SMS</th>
                        <th>Template Name</th>
                        <th>Stats</th>
                        <th>Rules</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($campaignTypes as $index=>$camp)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$camp->id}}</td>
                        <td>{{$camp->name}}</td>
                        <td>{{$camp->sms_content}}</td>
                        <td>{{$camp->sms}}</td>
                        <td>{{isset($camp->template) ? $camp->template->template_name:"NA"}}</td>
                        <td>{{$camp->stats}}</td>
                        <td>{{$camp->rules}}</td>
                        <td>{{$camp->created_at->format('d M Y')}}/{{$camp->created_at->format('g:i A')}}</td>
                        <td>{{$camp->updated_at->format('d M Y')}}/{{$camp->updated_at->format('g:i A')}}</td>
                        <td>
                            <div class="action">
                                <a href="{{ route('campaigntypes.show',$camp->id) }}">
                                    <span>
                                        <i class="fa fa-eye fa-primary"></i>
                                    </span>
                                </a>
                                <a href="{{ route('campaigntypes.edit',$camp->id)}}">
                                    <span>
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                </a>
                            </div>
                        </td>
                        <td>
                            <input type="hidden" name="mdoelName" id="mdoelName" value="CampaignType">
                            <div class="form-check form-switch">
                                <input data-id="{{$camp->id}}" class="form-check-input changeStatus" type="checkbox"
                                    id="flexSwitchCheckDefault" data-on="Active" data-off="InActive"
                                    {{ $camp->status == 'ACTIVE' ? 'checked' : '' }}>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <p>No Found Data</p>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th>SN</th>
                        <th>Campaign Type ID</th>
                        <th>Campaign Type</th>
                        <th>SMS Content</th>
                        <th>SMS</th>
                        <th>Template Name</th>
                        <th>Stats</th>
                        <th>Rules</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop