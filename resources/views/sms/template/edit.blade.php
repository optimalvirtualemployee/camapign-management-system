@extends('layouts.default')
@section('title', 'Template Edit')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Template Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/sms/templates')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('templates.update', $template->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                  <label for="hue-demo">Template Name</label>
                  <input type="text" id="hue-demo" class="form-control demo fieldname" name="template_name" data-control="hue"
                      placeholder="Enter Template Name" value="{{ $template->template_name }}">
                </div>
                <div class="form-group">
                  <label for="hue-demo">Template Detail</label>
                  <textarea class="form-control" name="template_detail">{{$template->template_detail}}</textarea>
                </div>
                <div class="form-group template-field-list">
                  <h4>Select Field</h4>
                  <ul>
                      @forelse($fields as $id=> $field)
                      <li>
                          <div class="form-check form-check-inline mr-sm-2">
                            <!--<input type="checkbox" class="form-check-input"
                                  id="customControlAutosizing{{$field->id}}" value="{{$field->id}}" name="field_id[]" {{ (in_array($id, old('field_id', [])) || isset($template) && $template->field_template->contains($id)) ? 'checked' : '' }}>-->
                            <input type="checkbox" class="form-check-input"
                                id="customControlAutosizing{{$field->id}}" value="{{$field->id}}" name="field_id[]" {{ (in_array($id, old('field_id', [])) || isset($template) && $field_template->contains($field->id)) ? 'checked' : '' }}>
                            <label class="form-check-label mb-0" for="customControlAutosizing{{$field->id}}">{{$field->field_name}}</label>
                          </div>
                      </li>
                    @empty
                    <p>No Found Result</p>
                    @endforelse
                  </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop