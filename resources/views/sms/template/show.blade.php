@extends('layouts.default')
@section('title', 'Show Template')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Template Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/sms/templates')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Template Details</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                    <th scope="col">Feilds</th>
                    <th scope="col">Value</th>
                </tr>
            </thead>
            <tbody>
          
                <tr>
                    <td>Template Id</td>
                    <td>{{$template->id}}</td>
                </tr>
                <tr>
                    <td>template Name</td>
                    <td>{{$template->template_name}}</td>
                </tr>
                <tr>
                  <td>Template Detail</td>
                  <td>{{$template->template_detail}}</td>
                </tr>
    
                <tr>
                    <td>Fields</td>
                    <td>
                        @foreach($template->field_template as $i=> $field)
                        {{$field->field_name}},
                        @endforeach
                    </td>
                    
                </tr>
                <tr>
                    <td>Created By</td>
                    <td>{{isset($template->created_user['name']) ? $template->created_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Updated By</td>
                    <td>{{isset($template->updated_user['name']) ? $template->updated_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Created Date/Time</td>
                    <td>{{$template->created_at->format('d M Y')}}/{{$template->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Updated Date/Time</td>
                    <td>{{$template->created_at->format('d M Y')}}/{{$template->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$template->status}}</td>
                </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@stop