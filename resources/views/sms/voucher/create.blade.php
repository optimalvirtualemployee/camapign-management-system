

@extends('layouts.default')
@section('title', 'Voucher Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-12 d-flex no-block align-items-center">
         <h4 class="page-title">Voucher Create</h4>
         <div class="ms-auto text-end">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('admin/sms/vouchers')}}">Back</a></li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
</div>
<br/>
@endif
<form action="{{ route('vouchers.store') }}" method="POST" class="form-horizontal mt-3" id="">
   {{ csrf_field() }}
    <div class="card">
        <div class="card-body">
            <!-- <h4 class="card-title">Basic Detail</h4> -->
            <div class="row">
                <div class="col-md-4">
                    <label class="fbasic_rule_type_id">Campaign</label>
                        <select class="select2 form-select shadow-none"
                      name="campaign_id" id='campaignAction' style="width: 100%; height:36px;">
                        <option>Select Campaign</option>
                        @forelse($campaigns as $camp)
                        <option value="{{$camp->id}}" {{old('campaign_id') == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                    </select>
                </div>
                <div class="col-md-4 offerTypeWrap" style="display:none;">
                  <div class="form-group">
                      <label for="name" class="mr-sm-2">Offer Type</label>
                      <select class="form-control js-example-basic-single offer" name="offer_type_id" id="sel1" style="height: 45px;"></select>
                  </div>
                </div>
                <div class="form-group col-md-4">
            <label for="hue-demo">Batch Name</label>
            <input type="text" class="form-control"  name="batch_name" id="voucher_code" value="{{ old('batch_name') }}" >
           
        </div>

        <div class="form-group col-md-4">
            <label for="hue-demo">SKU Name</label>
            <input type="text" class="form-control"  name="sku_name" id="sku_name" value="{{ old('sku_name') }}" >
           
        </div>
                <div class="form-group col-md-4">
                    <label for="hue-demo">Voucher Code</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="voucher_code" data-control="hue"
                      placeholder="Enter Voucher Code" value="{{ old('voucher_code') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <div class="input-group">
                        <label class="fbasic_rule_type_id">Start Date</label>
                        <input type="text" class="form-control mydatepicker" name="start_date" placeholder="mm/dd/yyyy" value="{{ old('start_date') }}">
                        <div class="input-group-append">
                            <span class="input-group-text h-100"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="input-group">
                        <label class="fbasic_rule_type_id">End Date</label>
                        <input type="text" class="form-control mydatepicker" name="end_date" placeholder="mm/dd/yyyy" value="{{ old('end_date') }}">
                        <div class="input-group-append">
                            <span class="input-group-text h-100"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="input-group">
                        <label class="fbasic_rule_type_id">Voucher Activation Date Over SMS</label>
                        <input type="text" class="form-control mydatepicker" name="voucher_activation_date_over_sms" placeholder="mm/dd/yyyy" value="{{ old('voucher_activation_date_over_sms') }}">
                        <div class="input-group-append">
                            <span class="input-group-text h-100"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="border-top">
         <div class="card-body">
            <button type="submit" class="btn btn-success text-white">Save</button>
            <button type="reset" class="btn btn-primary">Reset</button>
            <!-- <button type="submit" class="btn btn-info">Edit</button> -->
            <button type="submit" class="btn btn-danger text-white">Cancel</button>
         </div>
      </div>
   </div>
</form>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop

