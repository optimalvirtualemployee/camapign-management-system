<div class="table-responsive">
      <table class="table table-striped table-bordered" id="voucher_table">
        <thead>
            <tr>
              <th >SN</th>
              
              <th>Campaign ID</th>
              <th>Campaign Name</th>
              <th>Batch Name</th>
              <th>SKU Name</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Campaign Keyword</th>
              <th>Voucher Code</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th> 
            </tr>
        </thead>
        <tbody>
          @forelse($vouchers as $index=>$voucher)
          
                
          <tr>
            <td>{{$index + $vouchers->firstItem()}}</td>
            <td>{{$voucher->campaign_id}}</td>
            
            <td>{{$voucher->campaign['campaign_name']}}</td>
            <td>{{$voucher->batch_name}}</td>
            <td>{{$voucher->sku_name}}</td>
            <td>{{$voucher->offer_type_id}}</td>
            <td>{{isset($voucher->offer)? $voucher->offer['name']:"NA" }}</td>
            <td>{{$voucher->campaign['campaign_keyword']}}</td>
            <td>{{$voucher->voucher_code}}</td>
            <td>{{isset($voucher->start_date) ? $voucher->start_date : "NA"}}</td>
            <td>{{isset($voucher->end_date) ? $voucher->end_date : "NA"}}</td>
            <td>{{$voucher->created_at->format('d M Y')}}/{{$voucher->created_at->format('g:i A')}}</td>
            <td>{{$voucher->updated_at->format('d M Y')}}/{{$voucher->updated_at->format('g:i A')}}</td>
            
            <td>
              <a href="{{ route('vouchers.show',$voucher->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('vouchers.edit',$voucher->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
            <td>
                
              <div class="form-check form-switch">
                  <input data-id="{{$voucher->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
              data-off="InActive" {{ $voucher->status == 'ACTIVE' ? 'checked' : '' }}>
              <input type="hidden" name="mdoelName" id="mdoelName" value="Voucher">  
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <th >SN</th>
              
              <th>Campaign ID</th>
              <th>Campaign Name</th>
              <th>Batch Name</th>
              <th>SKU Name</th>
              <th>Offer Type ID</th>
              <th>Offer Type</th>
              <th>Campaign Keyword</th>
              <th>Voucher Code</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </tfoot>
       
      </table>
      {!! $vouchers->appends(Request::all())->links() !!}
    </div>