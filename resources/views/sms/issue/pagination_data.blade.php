<div class="table-responsive">
      <table class="table table-striped table-bordered" id="issue_table">
        <thead>
            <tr>
              <th>SN</th>
                <th>From</th>
                <th>Incoming Message Id</th>
                <th>Incoming Message</th>
                <th>Outgoing Message</th>
                <th>Status</th>
                <th>Created Date/Time</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
          @forelse($data as $index=>$d)
          <tr>
              <td>{{$index + $data->firstItem()}}</td>
              <td>{{$d->id}}</td>
              <td>{{$d->incoming_msg->from}}</td>
              <td>{{$d->incoming_msg->message}}</td>
              <td>{{$d->outgoing_msg}}</td>
              <td>{{$d->status}}</td>
              <td>{{$camp->created_at->format('d M Y')}}/{{$camp->created_at->format('g:i A')}}</td>
              
              <td>
                  <div class="action">
                      <a href="{{ route('campaigns.show',$camp->id) }}">
                          <span>
                              <i class="fa fa-eye fa-primary"  title="View"></i>
                          </span>
                      </a>
                      <a href="{{ route('campaigns.edit',$camp->id)}}">
                          <span>
                              <i class="fa fa-pencil fa-primary"  title="Edit"></i>
                          </span>
                      </a>
                      
                  </div>
              </td>
              
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
            
           
        </tbody>
        <tfoot>
            <th>SN</th>
                <th>From</th>
                <th>Incoming Message Id</th>
                <th>Incoming Message</th>
                <th>Outgoing Message</th>
                <th>Status</th>
                <th>Created Date/Time</th>
            </tr>
        </tfoot>
       
      </table>
      {!! $data->appends(Request::all())->links() !!}
    </div>