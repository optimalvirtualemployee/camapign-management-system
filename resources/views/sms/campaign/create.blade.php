@extends('layouts.default')
@section('title', 'Campaign Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/sms/campaigns')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('campaigns.store') }}" method="POST" class="form-horizontal mt-3" id="">
  {{ csrf_field() }}      
  <div class="card">
    <div class="card-body">
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
        
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Campaign Type</label>
                        <select class="select2 form-select shadow-none"
                            name="campaign_type_id"   style="width: 100%; height:36px;">
                            <option>Campaign Type</option>
                            @forelse($campaign_type as $type)
                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                            <option value="{{$type->id}}" {{old('campaign_type_id') == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                            
                            <!-- </optgroup> -->
                            
                            @empty
                            <p>No Found Result</p>
                            @endforelse
                            
                            
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Name</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="campaign_name" data-control="hue"
                        placeholder="Enter Campaign Name" value="{{ old('campaign_name') }}">
                    </div>
                    
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Keyword</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="campaign_keyword" data-control="hue"
                        placeholder="Enter Campaign Keyword" value="{{ old('campaign_keyword') }}">
                    </div>     
                            
                    
                    
                            
                        
                        
                    <!--<div class="form-group col-md-6">
                        <label for="hue-demo">Time Same Mobile Number Allowed</label>
                        <input type="number" id="hue-demo" class="form-control demo" name="same_mobile_allowed" data-control="hue"
                        placeholder="Enter Same Mobile Number Allowed" value="{{ old('same_mobile_allowed') }}">
                    </div>-->
                    
                </div>
                <div class="row">
                    
                    
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Start Date</label>
                        <input type="text"  id="filter-date" name="start_date" value="{{old('start_date')}}"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign End Date</label>
                        <input type="text"  id="filter-date" name="end_date" value="{{old('end_date')}}"/>
                    </div>
                               
                    <div class="form-group col-md-4">
                        <label for="hue-demo">Campaign Status Trigger Rule </label>
                        <input type="text" id="hue-demo" class="form-control demo" name="status_trigger_rule" data-control="hue"
                        placeholder="Enter Campaign Keyword" value="{{ old('status_trigger_rule') }}">
                    </div>     
                    
                    
                            
                        
                        
                    
                    
                </div>
                <div class="row">
                    
                    
                    <div class="form-group col-md-4 dayschecked">
                        <label for="hue-demo">Set Mobile Number Usage Limit</label>
                        @foreach($mobile_no_usage as $usage)    
                        
                        <div class="form-check mr-sm-2">
                            
                            <input type="checkbox" class="form-check-input"
                                id="voucherperday_{{$usage->id}}" name="mobile_no_usage_rules[]" value="{{$usage->id}}">
                                
                            <label class="form-check-label mb-0" for="customControlAutosizing1" style="display:inline-flex;">

                                
                                <input type="text" class="voucherperday_{{$usage->id}} latFeild ml-2 form-control demo" id="voucherperday_{{$usage->id}}" name="mobile_usage_value[]"   readonly="readonly" value="">
                                {{$usage->name}}
                                
                            </label>
                        </div>
                        @endforeach
                    </div>
                    <div class="col-md-4">
                        <label for="hue-demo" style="margin-right: 20px;">Microsite API</label>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input"
                            id="customControlValidation3" name="microsite_api" value="YES" required>
                            <label class="form-check-label mb-0" for="customControlValidation3">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input"
                            id="customControlValidation4" name="microsite_api" value="NO" required>
                            <label class="form-check-label mb-0" for="customContr
                            olValidation4">No</label>
                        </div>
                    </div>
                   
                </div>

            
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop