@extends('layouts.default')
@section('title', 'Voucher List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Voucher Dashboard</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        
                        
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Campaign Keyword</th>
              <th>Campaign Name</th>
              <th>Total Voucher Added</th>
              <th>Total Voucher Used</th>
              <th>Balance Qty</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              
            </tr>
        </thead>
        <tbody>
          @forelse($voucher_code as $index=>$code)
          
          @php 
          $rewardTotalCount = App\SmsModels\VoucherCode::where('campaign_id',$code['campaign_id'])->count();
          $rewardBalanceCount = App\SmsModels\VoucherCode::where('campaign_id',$code['campaign_id'])->where('isused','0')->count();
          $rewardUsedCount = App\SmsModels\VoucherCode::where('campaign_id',$code['campaign_id'])->where('isused','1')->count()
            
            @endphp      
          <tr>
              
            <td>{{++$index}}</td>
            <td>{{$code->campaigns['campaign_keyword']}}</td>
            <td>{{$code->campaigns['campaign_name']}}</td>
            <td>{{$rewardTotalCount}}</td>
            <td>{{$rewardUsedCount}}</td>
            <td>{{$rewardBalanceCount}}</td>
            <td>{{$code->created_at->format('d M Y')}}/{{$code->created_at->format('g:i A')}}</td>
            <td>{{$code->updated_at->format('d M Y')}}/{{$code->updated_at->format('g:i A')}}</td>
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Campaign Keyword</th>
              <th>Campaign Name</th>
              <th>Total Voucher Added</th>
              <th>Total Voucher Used</th>
              <th>Balance Qty</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

