@extends('layouts.default')
@section('title', 'Campaign List')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign List</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('admin/sms/campaigns/create')}}">Add Campaign</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
    <div class="card-body">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        <!-- <h5 class="card-title">Campaign Type</h5> -->
        <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Campaign ID</th>
                        <th>Campaign Name</th>
                        <th>Campaign Keyword</th>
                        <th>Campaign Type</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($campaigns as $index=>$camp)
                    <tr>
                        <td>{{++$index}}</td>
                        <td>{{$camp->id}}</td>
                        <td>{{$camp->campaign_name}}</td>
                        <td>{{$camp->campaign_keyword}}</td>
                        <td>{{isset($camp->campaign_type) ? $camp->campaign_type->name : "NA"}}</td>
                        <td>{{$camp->created_at->format('d M Y')}}/{{$camp->created_at->format('g:i A')}}</td>
                        <td>{{$camp->updated_at->format('d M Y')}}/{{$camp->updated_at->format('g:i A')}}</td>
                        <td>
                            <div class="action">
                                <a href="{{ route('campaigns.show',$camp->id) }}">
                                    <span>
                                        <i class="fa fa-eye fa-primary"  title="View"></i>
                                    </span>
                                </a>
                                <a href="{{ route('campaigns.edit',$camp->id)}}">
                                    <span>
                                        <i class="fa fa-pencil fa-primary"  title="Edit"></i>
                                    </span>
                                </a>
                                @if(isset($camp->campaign_type->sms) && $camp->campaign_type->sms == 'YES' &&
                                $camp->campaign_type->template_id != "" )
                                <a
                                    href="{{route('campaign.sms.create',[$camp->id,$camp->campaign_type->template_id])}}">
                                    <span><i class="fa fa-comments" aria-hidden="true"  title="SMS"></i></span>
                                </a>
                                @endif
                                @if(isset($camp->campaign_type->stats) && $camp->campaign_type->stats == 'YES')
                                <a href="{{ route('campaign.stats',$camp->id) }}">
                                    <span><i class="fa fa-bar-chart" aria-hidden="true"  title="Stats"></i></span>
                                </a>
                                @endif
                                @if(isset($camp->campaign_type->rules) && $camp->campaign_type->rules == 'YES')
                                <a href="{{ route('campaign.rules.index',$camp->id) }}">
                                    <span><i class="fa fa-gavel" aria-hidden="true" title="Rule"></i></span>
                                </a>
                                @endif
                            </div>
                        </td>
                        <td>
                            <div class="form-check form-switch">
                                <input data-id="{{$camp->id}}" class="form-check-input changeStatus" type="checkbox"
                                    id="flexSwitchCheckDefault" data-on="Active" data-off="InActive"
                                    {{ $camp->status == 'ACTIVE' ? 'checked' : '' }}>
                                <input type="hidden" name="mdoelName" id="mdoelName" value="Campaign">
                        </td>
                    </tr>
                    @empty
                    <p>No Found Data</p>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <th>SN</th>
                        <th>Campaign ID</th>
                        <th>Campaign Name</th>
                        <th>Campaign Keyword</th>
                        <th>Campaign Type</th>
                        <th>Created Date/Time</th>
                        <th>Updated Date/Time</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop