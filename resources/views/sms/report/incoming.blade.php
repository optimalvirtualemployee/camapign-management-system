@extends('layouts.default')
@section('title', 'Incoming SMS List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Incoming SMS List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <!--<li class="breadcrumb-item"><a href="{{url('admin/campaigns/create')}}">Add Campaign</a></li>-->
                        <!-- <li class="breadcrumb-item"><a href="{{url('admin/incoming/download-incoming-excel')}}"><button class="btn btn-success btn-sm"    type="button" id="dropdownMenuButton">
                            <i class="fas fa-plus"></i> &nbsp; Download Incoming Message
                            </button></a></li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <br />
        <form id="downloadCityForm"  method="GET" action="{{url('admin/incoming/download-incoming-excel')}}">
        <div class="row input-daterange">
                <div class="form-group col-md-4">
                    
                    <select class="select2 form-select shadow-none"
                        name="campaign_name" id="campaign_name"   style="width: 100%; height:36px;">
                        <option>Select Campaign</option>
                        @forelse($campaigns as $camp)
                        <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                        <option value="{{$camp->campaign_keyword}}" >{{$camp->campaign_name}}</option>
                        
                        <!-- </optgroup> -->
                        
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                        
                        
                    </select>
                </div>
                <div class="form-group col-md-4">
                    
                    <select class="select2 form-select shadow-none"
                        name="campaign_name" id="campaign_name"   style="width: 100%; height:36px;">
                        <option>Select Status</option>
                        
                        
                        <option value="1" >Valid</option>
                        <option value="1" >Invalid</option>
                        <option value="1" >Excess</option>
                        <option value="1" >Duplicate</option>
                        

                        
                    </select>
                </div>
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                </div>
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                </div>
                <label for="name" class="mr-sm-2">Download Incoming Message</label>
                            <a><button class="btn btn-primary btn-sm Download_City"  onclick="downloadCityBtn();"   type="button" id="dropdownMenuButton">
                            <i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download Incoming Message
                            </button></a>

            </div>
            </form>
    <br />
    <div class="table-responsive">
        <table class="table table-striped table-bordered data-table" id="order_table">

        <thead>

            <tr>

                <th >SN</th>
                <th>From</th>
                <th>Message</th>
                <th>Ts</th>
                <th>Circle</th>
                <th>Operator</th>
                <th>Message Id</th>  
                <th>Parts</th>  
                <th>Created Date/Time</th>

            </tr>

        </thead>
        <tbody>

        </tbody>
        

    </table>
      
    </div>
  </div>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

