@extends('layouts.default')
@section('title', 'Rule Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Rule Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{route('campaign.rules.index',$campaign_id)}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('campaign.rules.update',[$rule->id,$campaign_id]) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
    <div class="card-body">
      <h4 class="card-title">Basic Detail</h4>
      <div class="row" style="padding: 12px;background: #80808029;">
        <div class="form-group col-md-4">
           <label for="hue-demo">Rule Name</label>
           <input type="text" id="hue-demo" class="form-control demo" name="basic_rule_name" data-control="hue"
              placeholder="Enter Basic Rule Name" value="{{ $rule->basic_rule_name }}">
        </div>
        <div class="form-group col-md-4">
           <label for="hue-demo">Rule Sequence</label>
           <input type="text" id="hue-demo" class="form-control demo" name="basic_rule_sequence" data-control="hue"
              placeholder="Enter Basic Rule Sequence" value="{{ $rule->basic_rule_sequence }}">
        </div>

        <div class="col-md-4">
          <label class="fbasic_rule_type_id">Rule Type</label>
            <select class="select2 form-select shadow-none"
                name="basic_rule_type_id" style="width: 100%; height:36px;">
              <option>Rule Type</option>
              @forelse($ruleType as $type)
              
              <option value="{{$type->id}}" {{$rule->basic_rule_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
              @empty
              <p>No Found Result</p>
              @endforelse
            </select>
        </div>
      </div>
      <h4 class="card-title"  style="margin-top: 10px;">Rule Detail</h4>
      <div class="row" style="padding: 12px;background: #80808029;">
        
        <div class="form-group col-md-4">
           <label for="hue-demo">Rule Name</label>
           <input type="text" id="hue-demo" class="form-control demo" name="rule_name" data-control="hue"
              placeholder="Enter Rule Name" value="{{ $rule->rule_name }}">
        </div>
        <div class="form-group col-md-4">
           <label for="hue-demo">Rule Sequence</label>
           <input type="text" id="hue-demo" class="form-control demo" name="rule_sequence" data-control="hue"
              placeholder="Enter Rule Sequence" value="{{ $rule->rule_sequence }}">
        </div>
        <div class="col-md-4">
          <label class="">Rule Type</label>
            <select class="select2 form-select shadow-none"
                name="rule_type_id" style="width: 100%; height:36px;">
              <option>Rule Type</option>
              @forelse($ruleType as $type)
              
              <option value="{{$type->id}}" {{$rule->rule_type_id == $type->id ? 'Selected' : ''}}>{{$type->name}}</option>
              @empty
              <p>No Found Result</p>
              @endforelse
            </select>
        </div>
      </div>
      
      
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop