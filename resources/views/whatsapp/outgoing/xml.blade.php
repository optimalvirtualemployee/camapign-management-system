<table>
    <thead>
	<tr>
	    @foreach($data[0] as $key => $value)
		<th>{{ ucfirst($key) }}</th>
	    @endforeach
    	</tr>
    </thead>
    <tbody>
       
    @foreach($data as $row)

    	<tr>
        @if (is_array($row) || is_object($row))
        @foreach ($row as $value)
    	    <td>{{ $value }}</td>
        @endforeach   
        @endif        
	</tr>
    @endforeach
    </tbody>
</table>