@extends('layouts.default')
@section('title', 'Show Voucher')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Voucher Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappvouchers')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Voucher Detail</h5>
      </div>
      <table class="table">
        <thead>
            <tr>
                <th scope="col">Feilds</th>
                <th scope="col">Value</th>
            </tr>
        </thead>
        <tbody>
      
            <tr>
                <td>Voucher Code</td>
                <td>{{$voucher->id}}</td>
            </tr>
            <tr>
                <td>Campaign Name</td>
                <td>{{isset($voucher->campaign) ? $voucher->campaign->campaign_name : "NA"}}</td>
            </tr>
            
            <tr>
                <td>Campaign Keyword</td>
                <td>{{isset($campaignFieldValue['field_value']) ? $campaignFieldValue['field_value'] : 'NA'}}</td>
            </tr>
            
            <tr>
                <td>Created By</td>
                <td>{{isset($voucher->created_user['name']) ? $voucher->created_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Updated By</td>
                <td>{{isset($voucher->updated_user['name']) ? $voucher->updated_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Created Date/Time</td>
                <td>{{isset($voucher->created_at) ? $voucher->created_at->format('d M Y') : "NA"}}/{{isset($voucher->created_at) ? $voucher->created_at->format('g:i A') : "NA"}}</td>
            </tr>
            <tr>
                <td>Updated Date/Time</td>
                <td>{{isset($voucher->created_at) ? $voucher->created_at->format('d M Y') : "NA"}}/{{isset($voucher->created_at) ? $voucher->created_at->format('g:i A') : "NA"}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{$voucher->status}}</td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop