@extends('layouts.default')
@section('title', 'Voucher Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Voucher Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappvouchers')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('whatsappvouchers.update', $voucher->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
    <div class="card-body">
      
      <div class="row">
        <div class="form-group col-md-4">
           <label for="hue-demo">Voucher Code</label>
           <input type="text" id="hue-demo" class="form-control demo" name="voucher_code" data-control="hue"
              placeholder="Enter Voucher Code" value="{{ $voucher->voucher_code }}">
        </div>
        

        <div class="col-md-4">
          <label class="fbasic_rule_type_id">Campaign</label>
            <select class="select2 form-select shadow-none"
                name="campaign_id" style="width: 100%; height:36px;">
              <option>Select Campaign</option>
              @forelse($campaigns as $camp)
              
              <option value="{{$camp->id}}" {{$voucher->campaign_id == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
              @empty
              <p>No Found Result</p>
              @endforelse
            </select>
        </div>
      </div>
      
      
      
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop