@extends('layouts.default')
@section('title', 'Campaign Setting')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign Setting</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappcampaigns')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('whatsappcampaign.setting.store',isset($campaign_setting->id)) }}" method="POST" class="form-horizontal mt-3" id="">
   
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
      <!-- <h4 class="card-title">Campaign Type Create</h4> -->
        <div class="row">
          <div class="col-md-8">
              
              <div class="row">
                  <div class="form-group col-md-6">
                     <label>Campaign</label>
                     <select class="select2 form-select shadow-none"
                        name="campaign_id" style="width: 100%; height:36px;">
                        <option>Campaign</option>
                        @forelse($campaigns as $camp)
                        
                        <option value="{{$camp->id}}" {{isset($campaign_setting->campaign_id) == $camp->id ? 'Selected' : ''}}>{{$camp->campaign_name}}</option>
                        @empty
                        <p>No Found Result</p>
                        @endforelse
                    </select>
                  </div>
                  
                  <div class="form-group col-md-6">
                        <label for="hue-demo">Campaign Title</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="title" data-control="hue"
                        placeholder="Enter campaign title" value="{{ isset($campaign_setting->title) ? $campaign_setting->title : old('title') }}">
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="form-group col-md-6">
                        <label for="hue-demo">Campaign Contact Number</label>
                        <input type="number" id="hue-demo" class="form-control demo" name="contact_no" data-control="hue"
                        placeholder="Enter contact number" value="{{ isset($campaign_setting->contact_no) ? $campaign_setting->contact_no : ('title') }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="position-bottom-left">Bottom left (default)</label>
                        <input type="text" id="position-bottom-left" name="menu_bar_color" class="form-control demoColor"
                            data-position="bottom left" value="{{ isset($campaign_setting->menu_bar_color) ? $campaign_setting->menu_bar_color : "#0088cc" }}">
                    </div>
                   
                    
                    </div>
                  
                    
              </div>
          </div>
      
      </div>
    </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Submit</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop