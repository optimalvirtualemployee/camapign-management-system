@extends('layouts.default')
@section('title', 'Campaign List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Campaign List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappcampaigns/create')}}">Add Campaign</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Campaign ID</th>
              <th>Campaign Name</th>
              <th>Campaign Keyword</th>
              <th>Campaign Type</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @forelse($campaigns as $index=>$camp)
            
            
            
            <tr>
                <td>{{++$index}}</td>
                <td>{{$camp->id}}</td>
                <td>{{$camp->campaign_name}}</td>
                <td>{{isset($camp->campaign_keyword) ? $camp->campaign_keyword : 'NA'}}</td>
                
                <td>{{isset($camp->campaign_type) ? $camp->campaign_type->name : "NA"}}</td>
                
            
                
                <td>{{$camp->created_at->format('d M Y')}}/{{$camp->created_at->format('g:i A')}}</td>
                <td>{{$camp->updated_at->format('d M Y')}}/{{$camp->updated_at->format('g:i A')}}</td>
                
                <td>
                  <a href="{{ route('whatsappcampaigns.show',$camp->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
    
                  <a href="{{ route('whatsappcampaigns.edit',$camp->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
                  @if(isset($camp->campaign_type->sms) && $camp->campaign_type->sms == 'YES' && $camp->campaign_type->template_id != "" )
                  <a href="{{route('whatsappcampaign.sms.create',[$camp->id,$camp->campaign_type->template_id])}}"><span style="color: blue;padding:5px;">SMS</span></a>
                  @endif
                  @if(isset($camp->campaign_type->stats) && $camp->campaign_type->stats == 'YES')
                  <a href="{{ route('whatsappcampaign.stats',$camp->id) }}"><span style="color: blue;padding:5px;">Stats</span></a>
                  @endif
                  @if(isset($camp->campaign_type->rules) && $camp->campaign_type->rules == 'YES')
                  <a href="{{ route('whatsappcampaign.rules.index',$camp->id) }}"><span style="color: blue;padding:5px;">Rules</span></a>
                  @endif
                   
                  
                </td>
                <td>
                    
                  <div class="form-check form-switch">
                      <input data-id="{{$camp->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
                  data-off="InActive" {{ $camp->status == 'ACTIVE' ? 'checked' : '' }}>
                  <input type="hidden" name="mdoelName" id="mdoelName" value="Campaign">  
                </td> 
            </tr>
            @empty
            <p>No Found Data</p>
            @endforelse
        </tbody>
        <tfoot>
            <tr>
                <th >SN</th>
                <th>Campaign ID</th>
                <th>Campaign Name</th>
                <th>Campaign Keyword</th>
                <th>Campaign Type</th>
                <th>Created Date/Time</th>
                <th>Updated Date/Time</th>
              
                <th>Action</th>
                <th>Status</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

