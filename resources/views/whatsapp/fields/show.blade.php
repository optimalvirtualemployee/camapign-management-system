@extends('layouts.default')
@section('title', 'Show Fields')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Fields Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappfields')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">fields Details</h5>
      </div>
      <div class="table-responsive">
          <table class="table">
            <tbody>
               
                <tr>
                    <td>Field Name</td>
                    <td>{{$fields->field_name}}</td>
                </tr>
                <tr>
                    <td>Created By</td>
                    <td>{{isset($fields->created_user['name']) ? $fields->created_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Updated By</td>
                    <td>{{isset($fields->updated_user['name']) ? $fields->updated_user['name'] : 'NA'}}</td>
                </tr>
                <tr>
                    <td>Created Date/Time</td>
                    <td>{{$fields->created_at->format('d M Y')}}/{{$fields->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Updated Date/Time</td>
                    <td>{{$fields->created_at->format('d M Y')}}/{{$fields->created_at->format('g:i A')}}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$fields->status}}</td>
                </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
</div>
@stop