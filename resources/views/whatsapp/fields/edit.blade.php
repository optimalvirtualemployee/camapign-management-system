@extends('layouts.default')
@section('title', 'Fields Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Fields Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappfields')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  
  <form action="{{ route('whatsappfields.update', $fields->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
        <div class="wrapper">
            <div class="row cloningform">
              <div class="col-md-8">
                  <div class="row">
                        <div class="form-group col-md-6" id="fieldtype">
                            <label>Field Type</label>
                            <select class="select2 form-select shadow-none fieldid" style="width: 100%; height:36px;" name="fieldid" id="">
                                <option value="">Please Select</option>
                                @forelse($fieldtypes as $type)
                
                                
                                <option value="{{$type->id}}" {{$fields->field_type_id==$type->id ? 'Selected' : ''}}>{{$type->name}}</option>
                                
                                @empty
                                <p>No Found Result</p>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="hue-demo">Placeholder OR Label</label>
                          <input type="text" id="hue-demo" class="form-control demo" name="field_name" data-control="hue"
                              placeholder="Enter Same Mobile Number Allowed" value="{{ $fields->field_name }}" id="fieldname">
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
      </div>
        <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
      </div>
    </div>
  </form>
  
   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop