
<style type="text/css">
    .label {
  padding: 3px 10px;
  line-height: 13px;
  color: #fff;
  font-weight: 700;
  border-radius: 2px;
  font-size: 90%;
}
</style>

<div class="card_outer">
        <div class="row">
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card">
                    <div class="box text-center">

                        <h1 class="font-bold">{{ $totalEntries }}</h1>
                        <h6 class="text-white">Total Entries</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card">
                    <div class="box text-center">
                        <h1 class="font-bold">{{ $totalValidEntries}}</h1>
                        <h6 class="text-white">Total Valid</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card">
                    <div class="box text-center">
                        <h1 class="font-bold">{{ $totalInvalidEntries}}</h1>
                        <h6 class="text-white">Total Invalid</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card">
                    <div class="box text-center">
                        <h1 class="font-bold">{{ $totalUniqueEntries }}</h1>
                        <h6 class="text-white">Total Unique Entries</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card">
                    <div class="box text-center">
                        <h1 class="font-bold">{{ $totalExcessTriesEntries }}</h1>
                        <h6 class="text-white">Total Excess Tries Entries</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    
    
                <div class="card">
                    <div class="card-body">
                        
                            <h4 class="card-title">Day Wise Entries</h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <canvas id="dayChart" class="rounded shadow" width="1443" height="721"></canvas> 
                                </div> 
                                
                            </div>
                        
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        
                            
                        	<h4 class="card-title">Week on Week Entries</h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <canvas id="weekChart" width="1443" height="721" class="rounded shadow"></canvas>
                                </div>

                            </div>
                            <button type="button" id="back" style="display: none;">Back</button>
                            <button type="button" id="next" style="display: none;">Next</button>

                                
                        
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <h4 class="card-title">Time Wise Entries</h4>
                                    <canvas id="timeChart" class="rounded shadow" width="1443" height="721"></canvas> 
                                </div>
                            </div>
                        
                        
                    </div>
                </div>
            
    
    </div>
    <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">SKU Wise Entries</h4>
          <!-- <h5 class="card-subtitle">Overview of Top Selling Items</h5> -->
          <div class="table-responsive mt-3">
            <div id="cc-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    
                    <div class="row">
                                    
                        <div class="col-sm-12">
                            <table id="zero_config2" class="table table-striped table-bordered mb-3 dataTable no-footer" data-page-length="10" role="grid" aria-describedby="cc-table_info">
                                <thead>
                                    
                                    <!-- start row -->
                                    <tr>
                                        <th>SN</th>
                                        <th>Keyword</th>
                                        <th>SKU Name</th>
                                        <th >Total Voucher</th>
                                        <th >Total Entries</th>
                                        <th>Total Valid Entries</th>

                                    </tr>
                                <!-- end row -->
                                </thead>
                                <tbody>
                                    
                                    @if(count($SKUNewData) != 0)
                                    @forelse($SKUNewData as $index=>$entries)
                                  
                                    
                                    <tr>
                                        <td>
                                            <span><a href="JavaScript: void(0);">{{++$index}}</a></span>
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['campaign_keyword']}}</a>
                                        </h6>
                                        
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['sku_name']}}</a>
                                        </h6>
                                        
                                        </td>
                                        
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{isset($entries['TotalVoucherCount']) ? $entries['TotalVoucherCount'] : ""}}</span></h2>
                                        </td>

                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{$entries['TotalCount']}}</span></h2>
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-danger">{{$entries['TotalValidCount']}}</span></h2>
                                        </td>
                                    </tr>
                                    @empty
                                      <p>No Found Data</p>
                                      @endforelse
                                      @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                    
                </div>
          </div>
        </div>
              
    </div>
    </div>
    <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">Batch Wise Entries</h4>
          <!-- <h5 class="card-subtitle">Overview of Top Selling Items</h5> -->
          <div class="table-responsive mt-3">
            <div id="cc-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    
                    <div class="row">
                                    
                        <div class="col-sm-12">
                            <table id="zero_config3" class="table table-striped table-bordered mb-3 dataTable no-footer" data-page-length="10" role="grid" aria-describedby="cc-table_info">
                                <thead>
                                    
                                    <!-- start row -->
                                    <tr>
                                        <th>SN</th>
                                        <th>Keyword</th>
                                        <th>Batch Name</th>
                                        <th>Total Voucher</th>
                                        <th>Total Entries</th>
                                        <th>Total Valid Entries</th>

                                    </tr>
                                <!-- end row -->
                                </thead>
                                <tbody>
                                    @if(count($batchNewData) != 0)
                                    @forelse($batchNewData as $index=>$entries)
                                  
                                    
                                    <tr>
                                        <td>
                                            <span><a href="JavaScript: void(0);">{{++$index}}</a></span>
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['campaign_keyword']}}</a>
                                        </h6>
                                        
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['batch_name']}}</a>
                                        </h6>
                                        
                                        </td>

                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{$entries['TotalVoucherCount']}}</span></h2>
                                        </td>
                                        
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{$entries['TotalCount']}}</span></h2>
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-danger">{{$entries['TotalValidCount']}}</span></h2>
                                        </td>
                                    </tr>
                                    @empty
                                      <p>No Found Data</p>
                                      @endforelse
                                      @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                    
                </div>
          </div>
        </div>
              
    </div>
    </div>
    <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">Offer Type Wise Entries</h4>
          <!-- <h5 class="card-subtitle">Overview of Top Selling Items</h5> -->
          <div class="table-responsive mt-3">
            <div id="cc-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    
                    <div class="row">
                                    
                        <div class="col-sm-12">
                            <table id="zero_config4" class="table table-striped table-bordered mb-3 dataTable no-footer" data-page-length="10" role="grid" aria-describedby="cc-table_info">
                                <thead>
                                    
                                    <!-- start row -->
                                    <tr>
                                        <th>SN</th>
                                        <th>Keyword</th>
                                        <th>Offer Type Name</th>
                                        <th>Total Voucher</th>
                                        <th>Total Entries</th>
                                        <th>Total Valid Entries</th>

                                    </tr>
                                <!-- end row -->
                                </thead>
                                <tbody>
                                    @if(count($OfferTypeNewData) != 0)
                                    @forelse($OfferTypeNewData as $index=>$entries)
                                    
                                    
                                    <tr>
                                        <td>
                                            <span><a href="JavaScript: void(0);">{{++$index}}</a></span>
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['campaign_keyword']}}</a>
                                        </h6>
                                        
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{isset($entries['parts3']) ? $entries['parts3'] : "" }}</a>
                                        </h6>
                                        
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{isset($entries['TotalVoucherCount']) ? $entries['TotalVoucherCount'] : 0}}</span></h2>
                                        </td>
                                        
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{$entries['TotalCount']}}</span></h2>
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-danger">{{$entries['TotalValidCount']}}</span></h2>
                                        </td>
                                    </tr>
                                    @empty
                                      <p>No Found Data</p>
                                      @endforelse
                                      @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                    
                </div>
          </div>
        </div>
              
    </div>
    </div>
    <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">Keyword Wise Entries</h4>
          <!-- <h5 class="card-subtitle">Overview of Top Selling Items</h5> -->
          <div class="table-responsive mt-3">
            <div id="cc-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    
                    <div class="row">
                                    
                        <div class="col-sm-12">
                            <table id="zero_config5" class="table table-striped table-bordered mb-3 dataTable no-footer" data-page-length="10" role="grid" aria-describedby="cc-table_info">
                                <thead>
                                    
                                    <!-- start row -->
                                    <tr>
                                        <th>SN</th>
                                        <th>Keyword</th>
                                        
                                        <th>Total Vouchers</th>
                                        <th>Total Valid Entries</th>

                                    </tr>
                                <!-- end row -->
                                </thead>
                                <tbody>
                                    @if(count($keywordWiseNewData) != 0)
                                    @forelse($keywordWiseNewData as $index=>$entries)
                                  
                                    
                                    <tr>
                                        <td>
                                            <span><a href="JavaScript: void(0);">{{++$index}}</a></span>
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['campaign_keyword']}}</a>
                                        </h6>
                                        
                                        </td>
                                        
                                        
                                        
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{$entries['TotalCount']}}</span></h2>
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-danger">{{$entries['TotalValidCount']}}</span></h2>
                                        </td>
                                    </tr>
                                    @empty
                                      <p>No Found Data</p>
                                      @endforelse
                                      @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                    
                </div>
          </div>
        </div>
              
    </div>
    </div>
    <div class="card">
        
        <div class="card-body">
          <h4 class="card-title">TOTAL SMS Sent Report</h4>
          <!-- <h5 class="card-subtitle">Overview of Top Selling Items</h5> -->
          <div class="table-responsive mt-3">
            <div id="cc-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                <div class="row">
                    
                    <div class="row">
                                    
                        <div class="col-sm-12">
                            <table id="zero_config6" class="table table-striped table-bordered mb-3 dataTable no-footer" data-page-length="10" role="grid" aria-describedby="cc-table_info">
                                <thead>
                                    
                                    <!-- start row -->
                                    <tr>
                                        <th>SN</th>
                                        <th>Keyword</th>
                                        <th>SMS Template ID</th>
                                        <th>Total SMS Sent</th>
                                        <th>Total SMS Delivered</th>

                                    </tr>
                                <!-- end row -->
                                </thead>
                                <tbody>
                                    @if(count($totalSmsSentReportNewData) != 0)
                                    @forelse($totalSmsSentReportNewData as $index=>$entries)
                                  
                                    
                                    <tr>
                                        <td>
                                            <span><a href="JavaScript: void(0);">{{++$index}}</a></span>
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{$entries['campaign_keyword']}}</a>
                                        </h6>
                                        
                                        </td>
                                        <td>
                                        <h6>
                                            <a class="font-medium link" href="JavaScript: void(0);"> {{isset($entries['field_slug_value']) ? $entries['field_slug_value'] :""}}</a>
                                        </h6>
                                        
                                        </td>
                                        
                                        
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-success"> {{isset($entries['TotalSMSDelivered']) ? $entries['TotalSMSDelivered'] :""}}</span></h2>
                                        </td>
                                        <td>
                                            <h2 style="font-size: 16px;"><span class="label label-danger">{{isset($entries['TotalSMSDelivered']) ? $entries['TotalSMSDelivered'] :""}}</span></h2>
                                        </td>
                                    </tr>
                                    @empty
                                      <p>No Found Data</p>
                                      @endforelse
                                      @endif
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                    
                </div>
          </div>
        </div>
              
    </div>
    </div>

<!-- =============================== Start Day Chart Script =============================== -->
   
    <!-- CHARTS -->
    <script>
        var ctx = document.getElementById("dayChart");

        var chart = new Chart(ctx, {
            type: "bar",
            data: {
                labels: {!! json_encode($chart->labels)!!} ,
                datasets: [
                            {
                                label: "Total Entries",
                                data: {!! json_encode($chart->total)!!} ,
                                backgroundColor: ["rgba(54, 162, 235, 0.2)"],
                                borderWidth: 2,
                                borderRadius: 30,
                                borderSkipped: false,
                            },
                            {
                                label: "Total Valid Entries",
                                data: {!! json_encode($chart->validTotal)!!},
                                backgroundColor: ["rgba(255, 99, 132, 0.2)"],
                                borderWidth: 2,
                                borderRadius: 5,
                                borderSkipped: false,
                              },
                        ],
            },
            options: {
                scales: {
                y: {
                    beginAtZero: true,
                },
            },
        },
    });
    </script>


    <!-- =============================== End Day Chart Script =============================== -->

    <!-- =============================== End Week Chart Script =============================== -->
    <script>
        var wtx = document.getElementById("weekChart");

        var weekChart = new Chart(wtx, {
              type: "bar",
              data: {
                labels: {!! json_encode($chartWeek->labels)!!},
                datasets: [
                  {
                    label: "Total Entries",
                    data: {!! json_encode($chartWeek->total)!!},
                    backgroundColor: ["rgba(54, 162, 235, 0.2)"],
                    borderWidth: 1,
                  },
                  {
                    label: "Total Valid Entries",
                    data: {!! json_encode($chartWeek->validTotal)!!},
                    backgroundColor: ["rgba(255, 99, 132, 0.2)"],
                    borderWidth: 1,
                  },
                ],
              },
              options: {
                scales: {
                  y: {
                    beginAtZero: true,
                  },
                },
              },
            });
    
    </script>
    <!-- =============================== End Week Chart Script =============================== -->
  
    <!-- =============================== Start Time Chart Script =============================== -->
    

    <script>
        var ttx = document.getElementById("timeChart");

    var timeChart = new Chart(ttx, {
      type: "bar",
      data: {
        labels: ['12AM - 3AM', '3AM - 6AM','6AM - 9AM','9AM - 12 Noon','12Noon - 3 PM','3PM - 6PM','6PM - 9PM','9PM - 12AM'],
        datasets: [
          {
            label: "Total Entries",
            data: {!! json_encode($chartWeek->total)!!},
            backgroundColor: ["rgba(54, 162, 235, 0.2)"],
            borderWidth: 2,
            borderRadius: 30,
            borderSkipped: false,
          },
          {
            label: "Total Valid Entries",
            data: {!! json_encode($chartWeek->validTotal)!!},
            backgroundColor: ["rgba(255, 99, 132, 0.2)"],
            borderWidth: 2,
            borderRadius: 5,
            borderSkipped: false,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
    var ctx = document.getElementById('time_chart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',
    // The data for our dataset
            data: {
                labels:  ['12AM - 3AM', '3AM-6AM','6AM - 9AM','9AM - 12 Noon','12Noon - 3 PM'] ,
                datasets: [
                    {
                        label: 'Count of total entries',
                        backgroundColor: {!! json_encode($chartTime->colours)!!} ,
                        data:  ['00:00 AM - 03:00 AM', '03:00 AM - 06:00 AM','06:00 AM - 09:00 AM','09:00 AM - 12:00 AM'],
                    },
                ]
            },
    // Configuration options go here
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {if (value % 1 === 0) {return value;}}
                        },
                        scaleLabel: {
                            display: false
                        }
                    }]
                },
                legend: {
                    labels: {
                        // This more specific font property overrides the global property
                        fontColor: '#122C4B',
                        fontFamily: "'Muli', sans-serif",
                        padding: 25,
                        boxWidth: 25,
                        fontSize: 14,
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 0,
                        bottom: 10
                    }
                }
            }
        });
    </script>
    <!-- =============================== End Time Chart Script =============================== -->

    <!-- =============================== Start Circle Chart Script =============================== -->
    
    
  

<!-- =============================== End Circle Chart Script =============================== -->



