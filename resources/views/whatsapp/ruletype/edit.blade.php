@extends('layouts.default')
@section('title', 'Rule Type Create')
@section('content')


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Rule Type Edit</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/ruletypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   <!-- Start Page Content -->
   <!-- ============================================================== -->
  @if ($errors->any())
     <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
     </div><br />
  @endif
  <form action="{{ route('ruletypes.update', $ruleType->id) }}" method="POST" class="form-horizontal mt-3" id="">
    @method('PATCH') 
    {{ csrf_field() }}
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Rule Type Edit</h4>
        <div class="form-group">
           <label for="hue-demo">Name</label>
           <input type="text" id="hue-demo" class="form-control demo" name="name" data-control="hue"
              placeholder="Enter Rule Type" value="{{$ruleType->name}}">
        </div>
        
        
      </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-success text-white">Update</button>
          <button type="reset" class="btn btn-primary">Reset</button>
          <!-- <button type="submit" class="btn btn-info">Edit</button> -->
          <button type="submit" class="btn btn-danger text-white">Cancel</button>
        </div>
      </div>
    </div>
  </form>

   <!-- ============================================================== -->
   <!-- End PAge Content -->
   <!-- ============================================================== -->
   
@stop