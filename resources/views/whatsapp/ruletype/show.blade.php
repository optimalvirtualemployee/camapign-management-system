@extends('layouts.default')
@section('title', 'Show Rule Type')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Rule Type Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/ruletypes')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
          <h5 class="card-title mb-0">Rule Type Details</h5>
      </div>
      <table class="table">
        <thead>
            <tr>
                <th scope="col">Feilds</th>
                <th scope="col">Value</th>
            </tr>
        </thead>
        <tbody>
      
            <tr>
                <td>Rule Type Id</td>
                <td>{{$ruleType->id}}</td>
            </tr>
            <tr>
                <td>Rule Type Name</td>
                <td>{{$ruleType->name}}</td>
            </tr>
            <tr>
                <td>Created By</td>
                <td>{{isset($ruleType->created_user['name']) ? $ruleType->created_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Updated By</td>
                <td>{{isset($ruleType->updated_user['name']) ? $ruleType->updated_user['name'] : 'NA'}}</td>
            </tr>
            <tr>
                <td>Created Date/Time</td>
                <td>{{$ruleType->created_at->format('d M Y')}}/{{$ruleType->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Updated Date/Time</td>
                <td>{{$ruleType->created_at->format('d M Y')}}/{{$ruleType->created_at->format('g:i A')}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{$ruleType->status}}</td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop