@extends('layouts.default')
@section('title', 'SMS Template Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">SMS Template Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappcampaigns')}}">Back</a></li>
                        @if(!empty($campaignFieldValue))
                        <li class="breadcrumb-item"><a href="{{route('whatsappcampaign.sms.createmultioffertemplate',[$campaign_id,$template_id])}}"><button class="btn btn-success btn-sm"    type="button" id="dropdownMenuButton">
                            <i class="fas fa-plus"></i> &nbsp; Multi Offer SMS Template
                            </button></a></li>
                        @endif
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('whatsappcampaign.sms.store',[$campaign_id,$template_id]) }}" method="POST" class="form-horizontal mt-3" id="">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-body">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div>
        @endif
        <h4 class="card-title">{{$templates->template_name}}</h4>

        @foreach($field_template as $key=> $field)
        
        <div class="sms-group row">
			<div class="form-group col-md-6">
                <label for="hue-demo">Template Id</label>
                <input type="text" id="hue-demo" class="form-control demo" name="template_keyword_id[]" data-control="hue" placeholder="Enter template Id" value="{{isset($campaign_template[$key]->template_keyword_id) ? $campaign_template[$key]->template_keyword_id : ''}}">
            </div>
            <div class="form-group col-md-6">
                <label for="hue-demo">Entity Id</label>
                <input type="text" id="hue-demo" class="form-control demo" name="template_entity_id[]" data-control="hue" placeholder="Enter Entity Id" value="{{isset($campaign_template[$key]->template_entity_id) ? $campaign_template[$key]->template_entity_id : ''}}">
            </div>
            
            <div class="form-group col-md-12">
                <label for="hue-demo">{{$field->field_name}}</label>
                @if($field->field_type->name == 'text')
                <input type="text" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter {{$field->field_name}}" value="{{isset($campaign_template[$key]->field_value)? $campaign_template[$key]->field_value : ''}}">
                @elseif($field->field_type->name == 'date')
                <input type="date" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter {{$field->field_name}}" value="{{isset($campaign_template[$key]->field_value)? $campaign_template[$key]->field_value : ''}}">
                @elseif($field->field_type->name == 'number')
                <input type="number" id="hue-demo" class="form-control demo" name="field_value[]" data-control="hue" placeholder="Enter {{$field->field_name}}" value="{{isset($campaign_template[$key]->field_value)? $campaign_template[$key]->field_value : ''}}">
                @elseif($field->field_type->name == 'textarea')
                <textarea class="form-control" name="field_value[]">{{isset($campaign_template[$key]->field_value)? $campaign_template[$key]->field_value : ''}}</textarea>

                @endif

            </div>
            <input type="hidden" name="field_key[]" value="{{$field->pivot->field_id}}">
            <input type="hidden" name="field_slug[]" value="{{$field->pivot->keyword}}">
            @if(!in_array($field->field_name,['Valid SMS (ROI) with offer type 1','Valid SMS (ROI) If the reward code gets over','Valid SMS (TN) If the reward code gets over']))
            @endif
        </div>
        @endforeach
                        
                
      
      
    </div>
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop