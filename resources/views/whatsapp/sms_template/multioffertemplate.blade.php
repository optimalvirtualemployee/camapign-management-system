@extends('layouts.default')
@section('title', 'SMS Template Create')
@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">SMS Template Create</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/whatsappcampaigns')}}">Back</a></li>
                        
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
 </ul>
</div><br/>
@endif
<form action="{{ route('whatsappcampaign.sms.storemultioffertemplate',[$campaign_id,$template_id]) }}" method="POST" class="form-horizontal mt-3" id="">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-body">
        @if(session()->get('success'))
            <div class="alert alert-success">
            {{ session()->get('success') }}  
            </div>
        @endif
        <!-- <h4 class="card-title">{{$templates->template_name}}</h4> -->
        <h4 class="card-title">Multi Offer Template</h4>
        @php
        $number = $campaignFieldValue->field_value;
        
        @endphp
         
        @if (!empty($campaign_template->field_value))
            @php
            
              $jsonnewdata = json_decode($campaign_template->field_value);
            
              if($campaignFieldValue->field_value > count($jsonnewdata) ){
            
                 $diff =   $campaignFieldValue->field_value - count($jsonnewdata) ; 
              }
              
            
            @endphp
        
            @foreach($jsonnewdata as $key=>$val)
            <div class="row">
            <div class="form-group col-md-6">
                <label for="hue-demo">Template Id {{$val->offer_template_id}}</label>
                <input type="text" id="hue-demo" class="form-control demo" name="offer_template_keyword_id[]" data-control="hue" placeholder="Enter template Id" value="{{isset($val->offer_template_keyword_id) ? $val->offer_template_keyword_id : ''}}">
            </div>
            <div class="form-group col-md-6">
                <label for="hue-demo">Entity Id {{$val->offer_template_id}}</label>                   
                    <input type="text" id="hue-demo" class="form-control demo" name="offer_template_entity_id[]" data-control="hue" placeholder="Enter Entity Id" value="{{isset($val->offer_template_entity_id) ? $val->offer_template_entity_id : '' }}">
            </div>
            </div>
            <div class="sms-group row">
                    <div class="form-group col-md-6">
                        <label>Offer Type {{$val->offer_template_id}}</label>
                        <select class="select2 form-select shadow-none"
                            name="offer_type_id[]"   style="width: 100%; height:36px;">
                            <option>Offer Type</option>
                            @forelse($offers as $offer)
                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                            <option value="{{$offer->id}}" {{(isset($val->offer_type_id) && $val->offer_type_id == $offer->id) ? 'Selected' : ''}}>{{$offer->name}}</option>
                            
                            <!-- </optgroup> -->
                            
                            @empty
                            <p>No Found Result</p>
                            @endforelse
                            
                            
                        </select>
                    </div>
                   
                    <div class="form-group col-md-6">
                        <label for="hue-demo">Offer Type Name {{$val->offer_template_id}}</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_name_slug[]" data-control="hue" placeholder="Enter Offer Type Name" value="{{isset($val->offer_name_slug) ?$val->offer_name_slug:"" }}">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="hue-demo">Offer Type Template {{$val->offer_template_id}}</label>
                        
                        
                        <textarea class="form-control" name="offer_field_value[]" placeholder="Offer Type Template {{$val->offer_template_id}}">{{$val->offer_field_value}}</textarea>
    
                       
    
                    </div>
                    
                    
                     <input type="hidden" name="offer_template_id[]" value="{{$val->offer_template_id}}">
                </div>
                @php
                $sonam = $key+1;
                @endphp
            @endforeach
            @isset($diff)
            @for($i=0; $i<$diff; $i++)
                
                <div class="row">
                <div class="form-group col-md-6">
                    <label for="hue-demo">Template Id {{$sonam+$i+1}}</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="offer_template_keyword_id[]" data-control="hue" placeholder="Enter template Id {{$sonam+$i+1}}" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="hue-demo">Entity Id {{$sonam+$i+1}}</label>                   
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_template_entity_id[]" data-control="hue" placeholder="Enter Entity Id {{$sonam+$i+1}}" value="">
                </div>
                </div>
                
                <div class="sms-group row">
                    <div class="form-group col-md-6">
                        <label>Offer Type {{$sonam+$i+1}}</label>
                        <select class="select2 form-select shadow-none"
                            name="offer_type_id[]"   style="width: 100%; height:36px;">
                            <option>Offer Type</option>
                            @forelse($offers as $offer)
                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                            <option value="{{$offer->id}}" {{old('offer_type_id') == $offer->id ? 'Selected' : ''}}>{{$offer->name}}</option>
                            
                            <!-- </optgroup> -->
                            
                            @empty
                            <p>No Found Result</p>
                            @endforelse
                            
                            
                        </select>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label for="hue-demo">Offer Type Name {{$sonam+$i+1}}</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_name_slug[]" data-control="hue" placeholder="Enter Offer Type Name {{$sonam+$i+1}}" value="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="hue-demo">Offer Type Template {{$sonam+$i+1}}</label>
                        <textarea class="form-control" name="offer_field_value[]" placeholder="Offer Type Template {{$sonam+$i+1}}"></textarea>
                    
                       
    
                    </div>
                    
                    
                      <input type="hidden" name="offer_template_id[]" value="{{$sonam+$i+1}}">
                </div>
              
            @endfor
            @endisset
            
        @else
            
            @for($j=0; $j<$number; $j++)
                
                <div class="row">
                <div class="form-group col-md-6">
                    <label for="hue-demo">Template Id {{$j+1}}</label>
                    <input type="text" id="hue-demo" class="form-control demo" name="offer_template_keyword_id[]" data-control="hue" placeholder="Enter template Id {{$j+1}}" value="">
                </div>
                <div class="form-group col-md-6">
                    <label for="hue-demo">Entity Id {{$j+1}}</label>                   
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_template_entity_id[]" data-control="hue" placeholder="Enter Entity Id {{$j+1}}" value="">
                </div>
                </div>
                
                <div class="sms-group row">
                    <div class="form-group col-md-6">
                        <label>Offer Type {{$j+1}}</label>
                        <select class="select2 form-select shadow-none"
                            name="offer_type_id[]"   style="width: 100%; height:36px;">
                            <option>Offer Type</option>
                            @forelse($offers as $offer)
                            <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                            <option value="{{$offer->id}}" {{old('offer_type_id') == $offer->id ? 'Selected' : ''}}>{{$offer->name}}</option>
                            
                            <!-- </optgroup> -->
                            
                            @empty
                            <p>No Found Result</p>
                            @endforelse
                            
                            
                        </select>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label for="hue-demo">Offer Type Name {{$j+1}}</label>
                        <input type="text" id="hue-demo" class="form-control demo" name="offer_name_slug[]" data-control="hue" placeholder="Enter Offer Type Name {{$j+1}}" value="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="hue-demo">Offer Type Template {{$j+1}}</label>
                        <textarea class="form-control" name="offer_field_value[]" placeholder="Offer Type Template {{$j+1}}"></textarea>
                    
                       
    
                    </div>
                    
                    
                      <input type="hidden" name="offer_template_id[]" value="{{$j+1}}">
                </div>
                
           
            @endfor
        @endif

            
                
      
      
    </div>     
    <div class="border-top">
      <div class="card-body">
        <button type="submit" class="btn btn-success text-white">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <!-- <button type="submit" class="btn btn-info">Edit</button> -->
        <button type="submit" class="btn btn-danger text-white">Cancel</button>
      </div>
    </div>
  </div>
</form>

<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
@stop