@extends('layouts.default')
@section('title', 'Template List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Template List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb" id="feildNav">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        
                        <li class="breadcrumb-item addSms"><a href="{{url('admin/whatsapp/whatsapptemplates/create')}}">Add Template</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div class="table-responsive">
      <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
              <th >SN</th>
              <th>Template Name</th>
              <th>Template Detail</th>
             
              <th>Created By</th>
              <th>Updated By</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </thead>
        <tbody>
          @forelse($templates as $index=>$template)
                
          <tr>
            <td>{{++$index}}</td>
            <td>{{$template->template_name}}</td>
            <td>{{$template->template_detail}}</td>
           

            
            <td>{{isset($template->created_user['name']) ? $template->created_user['name'] : 'NA'}}</td>
            <td>{{isset($template->updated_user['name']) ? $template->updated_user['name'] : 'NA'}}</td>
            <td>{{$template->created_at->format('d M Y')}}/{{$template->created_at->format('g:i A')}}</td>
            <td>{{$template->updated_at->format('d M Y')}}/{{$template->updated_at->format('g:i A')}}</td>
            
            <td>
              <a href="{{ route('whatsapptemplates.show',$template->id) }}"><span style="color: blue;padding:5px;"><i class="fas fa-eye fa-primary fa-md"> </i></span></a>
               
              <a href="{{ route('whatsapptemplates.edit',$template->id)}}"><span style="color: black;padding:5px;"   ><i class="fas fa-edit fa-primary fa-md"></i></span></a>
              
            </td>
            <td>
                
              <div class="form-check form-switch">
                  <input data-id="{{$template->id}}" class="form-check-input changeStatus" type="checkbox" id="flexSwitchCheckDefault" data-on="Active" 
              data-off="InActive" {{ $template->status == 'ACTIVE' ? 'checked' : '' }}>
              <input type="hidden" name="mdoelName" id="mdoelName" value="Template">  
            </td> 
          </tr>
          @empty
          <p>No Found Data</p>
          @endforelse
        </tbody>
        <tfoot>
            <tr>
              <th >SN</th>
              <th>Template Name</th>
              <th>Template Detail</th>
             
              <th>Created By</th>
              <th>Updated By</th>
              <th>Created Date/Time</th>
              <th>Updated Date/Time</th>
              <th>Action</th>
              <th>Status</th>
            </tr>
        </tfoot>
      </table>
    </div>
  </div>

  
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

