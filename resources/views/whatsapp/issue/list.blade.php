@extends('layouts.default')
@section('title', 'Issue Panel List')
@section('content')

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Issue Panel List</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="{{url('admin/sms/vouchers/create')}}">Add Voucher</a></li>
                        <li class="breadcrumb-item"><a href="{{route('voucher.upload.create')}}"><button class="btn btn-success btn-sm"    type="button" id="dropdownMenuButton">
                            <i class="fas fa-plus"></i> &nbsp; Upload Voucher Code
                            </button></a></li> -->
                        
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
   
<!-- DataTales Example -->
<div class="card form-horizontal mt-3">
  <div class="card-body">
    @if(session()->get('success'))
    <div class="alert alert-success">
    {{ session()->get('success') }}  
    </div>
    @endif
    <br />
        
        <div class="filter_outer">
        <div class="card">
            <div class="card-body">
                
                <!-- Booking Filters Starts Here -->
                <div class="assign-book">
                    <div class="border-boking"></div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>From Date</label>
                        <input type="date" name="from_date" id="from_date" class="form-control"
                            value="{{ isset($_GET['from_date']) ? $_GET['from_date'] : '' }}">

                    </div>
                    <div class="col-md-4">
                        <label>To Date</label>
                        <input type="date" name="to_date" id="to_date" class="form-control"
                            value="{{ isset($_GET['to_date']) ? $_GET['to_date'] : '' }}">
                    </div>
                    <div class="col-md-4">
                        <label>Select Campaign</label>
                        <select class="form-control select2" name="campaign_id[]" id="campaign_id"
                            data-placeholder="Select Campaign" multiple>
                            @forelse($campaigns as $camp)

                            <option value="{{$camp->id}}"
                                {{(isset($_GET['campaign_id']) && $_GET['campaign_id'] == $camp->id) ? 'Selected' : ''}}>
                                {{$camp->campaign_name}}</option>
                            @empty
                            <p>No Product List</p>
                            @endforelse
                        </select>
                        <div class="error1" style="display: none;">This field is required.</div>
                    </div>
                    <div class="col-md-4">
                      <label>Global Search</label>
                      <input type="text" name="global_search" id="global_search" class="form-control" value="{{ isset($_GET['global_search']) ? $_GET['global_search'] : '' }}" placeholder='Enter voucher code'>
                    </div>
                    
                    <div class="col-md-4">
                        <input type="button" class="btn btn-primary btn-block search_btn " id="voucher-filter" value="Search">
                        <input type="button" class="btn btn-info btn-block search_btn " id="voucher-refresh" value="Clear">
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    <br />
    <!-- <h5 class="card-title">Campaign Type</h5> -->
    <div id="table_data">
        @include('sms.issue.pagination_data')
        <input type="hidden"  value="" id="hidden_page">
    </div>
    
  </div>

  <meta name="csrf-token" content="{{ csrf_token() }}">
  

    

<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
@stop

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    
    
    $(document).ready(function(){
        $(document).on('click', '.pagination a', function(event){

            event.preventDefault(); 
            var page = $(this).attr('href').split('page=')[1];
            $("#hidden_page").val(page);
            var from_date = $('#from_date').val();
      
            var to_date = $('#to_date').val();
            var campaign_id = $( "#campaign_id").val();
            var global_search = $( "#global_search" ).val();
            fetch_data(page,from_date, to_date, campaign_id,global_search);
        });
        
        function fetch_data(page = '',from_date = '', to_date = '', campaign_id = '', global_search = '')
        {
          
            $.ajax({
            
                url:"/admin/sms/voucher/fetch_data?page="+page+"&from_date="+from_date+"&to_date="+to_date+"&campaign_id="+campaign_id+"&global_search="+global_search,
                beforeSend: function(){
                  // Show image container
                  $("#loading").show();
                },
                success:function(data)
                {
                    alert(data);
                  $('#table_data').html(data);
                },
                complete: function(){
                  $('#loading').hide();
                }
           
            });
        }
        var _token = $('input[name="_token"]').val();
        

        $('#voucher-filter').click(function(){
            
          var page = $("#hidden_page").val();
          
          var from_date = $('#from_date').val();
          var to_date = $('#to_date').val();
          var campaign_id = $("#campaign_id").val()
          var global_search = $("#global_search").val()
          if((from_date != '' &&  to_date != '') || campaign_id != '' || global_search != ''){
            fetch_data(page,from_date, to_date, campaign_id,global_search);
            }/*else{
               alert('Both Date is required');
            }*/
        });
    
        $('#voucher-refresh').click(function(){
          $('#from_date').val('');
          $('#to_date').val('');
          $('#global_search').val('');
          $("#campaign_id").val('').trigger('change');
    
          fetch_data();
        });
    
        
     
    });
    fetch_data();
</script>
