@extends('layouts.default')
@section('title', 'Show Image')
@section('content')
<style>
.w3-dark-grey, .w3-hover-dark-grey:hover, .w3-dark-gray, .w3-hover-dark-gray:hover {
    color: #fff !important;
    background-color: #616161 !important;
}
.w3-card-4, .w3-hover-shadow:hover {
    box-shadow: 0 4px 10px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19);
}
.w3-center {
    text-align: center !important;
}
.w3-container, .w3-panel {
    padding: 0.01em 16px;
}
img {
    vertical-align: middle;
}
img {
    border-style: none;
}
.w3-section, .w3-code {
    margin-top: 16px !important;
    margin-bottom: 16px !important;
}
.w3-green, .w3-hover-green:hover {
    color: #fff !important;
    background-color: #4CAF50 !important;
}
.w3-btn, .w3-button {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.w3-btn, .w3-button {
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    background-color: inherit;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
}
div[disabled=disabled] {
  pointer-events: none;
  opacity: 0.4;
}

</style>
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Whatsapp Details</h4>
            <div class="ms-auto text-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        
                        <li class="breadcrumb-item"><a href="{{url('admin/whatsapp/report/whatsapp/list')}}">Back</a></li>
                        
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row form-horizontal mt-3">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
        @endif
          <h5 class="card-title mb-0">Image</h5>
      </div>
      <div class="w3-card-4 "  style="box-shadow: none;">

        <div class="w3-container w3-center">
          <!-- <h3>Friend Request</h3> -->
          <img src="{{$data['media_url']}}" alt="Avatar" style="border: 2px solid #3e5569;padding: 10px;">
          <!-- <h5>John Doe</h5> -->
          @if($data->image_view_status == null)
          <div class="w3-section">
            <a  onclick="return confirm('Are you sure?')" href="{{route('report.list.status', [$data->id,'APPROVED'])}}"><button type="submit" class="btn btn-success text-white">Approve</button></a>
          
            <a  onclick="return confirm('Are you sure?')" href="{{route('report.list.status', [$data->id,'REJECTED'])}}"><button type="submit" class="btn btn-danger text-white">Reject</button></a>
          </div>
          @elseif($data->image_view_status == 'APPROVED'))
          <div class="w3-section" disabled="disabled">
            <a  onclick="return confirm('Are you sure?')" href="{{route('report.list.status', [$data->id,'APPROVED'])}}"><button type="submit" class="btn btn-success text-white">Approved</button></a>
          
            
          </div>
          @else
          <div class="w3-section" disabled="disabled">
            
          
            <a  onclick="return confirm('Are you sure?')" href="{{route('report.list.status', [$data->id,'REJECTED'])}}"><button type="submit" class="btn btn-danger text-white">Rejected</button></a>
          </div>
          @endif
        </div>

      </div>
      <!-- <div class="table-responsive">
          <table class="table">
            
            <tbody>
          
                
                
                <tr>
                    
                    <td><img src="{{$data['media_url']}}" width="400"></td>
                </tr>
            </tbody>
          </table>
      </div> -->
    </div>
  </div>
</div>
@stop